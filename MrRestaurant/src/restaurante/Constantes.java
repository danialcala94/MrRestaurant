package restaurante;

import java.awt.Insets;

import javax.swing.ImageIcon;

import interfazGrafica.GUIVentanaLogin;
import ui.sala.UISala;

public final class Constantes {
	public static final int NIVELESEMPLEADOS_COCINERO = 1;
	public static final int NIVELESEMPLEADOS_JEFEDECOCINA = 2;
	public static final int NIVELESEMPLEADOS_CAMARERO = 3;
	public static final int NIVELESEMPLEADOS_JEFEDESALA = 4;
	public static final int NIVELESEMPLEADOS_GERENTE = 5;
	
	public static final int WINDOW_FULLHD_1080_ALTO = 1080;
	public static final int WINDOW_FULLHD_1920_ANCHO = 1920;
	
	public static final int WINDOW_APP_NORMALSIZE_ALTO = 600;
	public static final int WINDOW_APP_NORMALSIZE_ANCHO = 800;
	public static final int WINDOW_LOGIN_ANCHO = 343;
	public static final int WINDOW_LOGIN_ALTO = 225;
	
	public static final int WINDOW_COMANDA_ANCHO = 312;
	public static final int WINDOW_COMANDA_ALTO = 532;
	
	public static final int COMANDA_GRIDX_NUMERO = 0;
	public static final int COMANDA_GRIDX_PLATO = 1;
	public static final int COMANDA_GRIDX_PRECIO = 2;
	
	public static final int BOTONES_ANCHO = 160;
	public static final int BOTONES_ALTO = 40;
	
	public static final Insets INSETS = new Insets(0, 0, 0, 0);
	
	public static final int SIZE_x64 = 64;
	public static final int SIZE_x32 = 32;
	public static final int SIZE_x16 = 16;
	
	public static final ImageIcon ICONO_CORRECTO = new ImageIcon(GUIVentanaLogin.class.getResource("/recursos/login_exito.png"));
	public static final ImageIcon ICONO_INCORRECTO = new ImageIcon(GUIVentanaLogin.class.getResource("/recursos/login_error.png"));
	public static final ImageIcon ICONO_AJUSTES = new ImageIcon(UISala.class.getResource("/recursos/iconos/ajustes_x16.png"));
	// ImageIcon de SALA
	public static final ImageIcon ICONO_MESA_LIBRE = new ImageIcon(UISala.class.getResource("/recursos/sala/mesa_libre_64x64_trans.png"));
	public static final ImageIcon ICONO_MESA_OCUPADA = new ImageIcon(UISala.class.getResource("/recursos/sala/mesa_ocupada_64x64_trans.png"));
	public static final ImageIcon ICONO_MESA_RESERVADA = new ImageIcon(UISala.class.getResource("/recursos/sala/mesa_reservada_64x64_trans.png"));
	public static final ImageIcon ICONO_MESA_APREPARAR = new ImageIcon(UISala.class.getResource("/recursos/sala/mesa_apreparar_64x64_trans.png"));
	public static final ImageIcon ICONO_TABURETE_RESERVADO = new ImageIcon(UISala.class.getResource("/recursos/sala/taburete_reservado_16x16_trans.png"));
	public static final ImageIcon ICONO_TABURETE_LIBRE = new ImageIcon(UISala.class.getResource("/recursos/sala/taburete_libre_16x16_trans.png"));
	public static final ImageIcon ICONO_TABURETE_OCUPADO = new ImageIcon(UISala.class.getResource("/recursos/sala/taburete_ocupado_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_DERECHA = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_derecha_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_CENTRO = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_centro_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_IZQUIERDA = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_izquierda_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_ESQUINA_ARRDER = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_esquina_arrder_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_ESQUINA_DERABA = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_esquina_deraba_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_ESQUINA_DERARR = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_esquina_derarr_16x16_trans.png"));
	public static final ImageIcon ICONO_BARRA_ESQUINA_IZQARR = new ImageIcon(UISala.class.getResource("/recursos/sala/barra_esquina_izqarr_16x16_trans.png"));
}
