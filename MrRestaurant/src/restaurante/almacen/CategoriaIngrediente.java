package restaurante.almacen;

public class CategoriaIngrediente {
	private int IDCategoria;
	private String nombre;
	private String descripcion;
	
	public CategoriaIngrediente(int Id, String nombre, String descripcion) {
		this.IDCategoria=Id;
		this.nombre=nombre;
		this.descripcion=descripcion;
	}

	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIDCategoria() {
		return IDCategoria;
	}	
	
}
