package restaurante.almacen;

public class Magnitud {
	private int IDMagnitud;
	private String nombre;
	private String descripcion;
	
	
	public Magnitud(int Id, String nombre, String descripcion) {
		this.IDMagnitud=Id;
		this.nombre=nombre;
		this.descripcion=descripcion;
	}
	
	public Magnitud(String nombre, String descripcion) {
		this.nombre=nombre;
		this.descripcion=descripcion;
	}
	
	public Magnitud(String nombre) {
		this.nombre = nombre;
		//this.descripcion = "";
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIDMagnitud() {
		return IDMagnitud;
	}
	
}
