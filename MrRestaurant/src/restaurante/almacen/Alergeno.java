package restaurante.almacen;

public class Alergeno {
	public int IDAlergeno;
	private String nombre;
	private String descripcion;
	
	public Alergeno(int Id, String nombre, String descripcion) {
		this.IDAlergeno=Id;
		this.nombre=nombre;
		this.descripcion=descripcion;
	}
	
	public Alergeno(int id, String nombre) {
		this.IDAlergeno = id;
		this.nombre = nombre;
		this.descripcion = null;
	}
	
	public Alergeno(String nombre, String descripcion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public Alergeno(String nombre) {
		this.nombre = nombre;
	}
	
	public Alergeno() {
		
	}
	
	/**
	 * Para borrar de BD solo busca por id
	 * @param id
	 */
	public Alergeno(int id) {
		this.IDAlergeno = id;
		this.nombre = "null";
		this.descripcion = "null";
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIDAlergeno() {
		return IDAlergeno;
	}
	
	
}
