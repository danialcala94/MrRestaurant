package restaurante.almacen;

import java.util.*;

public class Pedido {
	private int IDPedido;
	private Proveedor proveedor;
	private ArrayList<IngredientePedido> ingredientesPedido;
	private Date fecha;
	private float precio;
	private String estado;
	
	public Pedido(int Id, Proveedor proveedor, ArrayList<IngredientePedido> ingredientesPedido, Date fecha, float precio, String estado) {
		this.IDPedido=Id;
		this.proveedor=proveedor;
		this.ingredientesPedido=ingredientesPedido;
		this.fecha=fecha;
		this.precio=precio;
		this.estado=estado;
	}
	
	/**
	 * Constructor sin el precio. Lo autocalcula a partir de los precios de los ingredientes del pedido
	 * @return
	 */
	public Pedido(int id, Proveedor proveedor, ArrayList<IngredientePedido> ingredientesPedido, Date fecha, String estado) {
		this.IDPedido=id;
		this.proveedor=proveedor;
		this.ingredientesPedido=ingredientesPedido;
		this.fecha=fecha;
		this.precio=0;
		for (int i = 0; i < ingredientesPedido.size(); i++)
			this.precio += ingredientesPedido.get(i).getPrecio() * ingredientesPedido.get(i).getCantidad();
		this.estado=estado;
	}
	
	public Pedido(ArrayList<IngredientePedido> ingredientes, String estado) {
		this.ingredientesPedido = ingredientes;
		for (int i = 0; i < ingredientesPedido.size(); i++)
			this.precio += ingredientesPedido.get(i).getPrecio() * ingredientesPedido.get(i).getCantidad();
		this.estado=estado;
	}
	
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public ArrayList<IngredientePedido> getIngredientesPedido() {
		return ingredientesPedido;
	}
	public void setIngredientesPedido(ArrayList<IngredientePedido> ingredientesPedido) {
		this.ingredientesPedido = ingredientesPedido;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getIDPedido() {
		return IDPedido;
	}
}
