package restaurante.almacen;

public class Proveedor {
	private int IDProveedor;
	private String nombre;
	private int telefono;
	private int telefono2;
	private String email;
	private String direccion;
	private String paginaWeb;
	private String NIF;
	private String ciudad;
	private String pais;
	
	public Proveedor(int Id, String nombre, int telefono, int telefono2, String email, String direccion, String paginaWeb, String NIF, String ciudad, String pais) {
		this.IDProveedor=Id;
		this.nombre=nombre;
		this.telefono=telefono;
		this.telefono2=telefono2;
		this.email=email;
		this.direccion=direccion;
		this.paginaWeb=paginaWeb;
		this.NIF=NIF;
		this.ciudad=ciudad;
		this.pais=pais;
	}
	
	/**
	 * Para eliminar de la base de datos, al coincidir el id ya lo eliminarķa
	 * @param id
	 */
	public Proveedor(int id) {
		this.IDProveedor = id;
		this.nombre = "null";
		this.telefono = 999999999;
		this.telefono2 = 999999999;
		this.email = "null";
		this.direccion = "null";
		this.paginaWeb = "null";
		this.NIF = "null";
		this.ciudad = "null";
		this.pais = "null";
	}
	
	/**
	 * Constructor bd
	 * @param nombre
	 * @param NIF
	 * @param telefono
	 * @param email
	 * @param direccion
	 * @param ciudad
	 * @param pais
	 */
	public Proveedor(String nombre, String NIF, int telefono, String email, String direccion, String ciudad, String pais) {
		this.nombre = nombre;
		this.NIF = NIF;
		this.telefono = telefono;
		this.email = email;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}
	public Proveedor(String nombre, String NIF, int telefono, int telefono2, String email, String direccion, String ciudad, String pais) {
		this.nombre = nombre;
		this.NIF = NIF;
		this.telefono = telefono;
		this.telefono2 = telefono2;
		this.email = email;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}
	
	public Proveedor(String nombre, String NIF, int telefono, String email, String paginaWeb, String direccion, String ciudad, String pais) {
		this.nombre = nombre;
		this.NIF = NIF;
		this.telefono = telefono;
		this.email = email;
		this.paginaWeb = paginaWeb;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}
	
	public Proveedor(String nombre, String NIF, int telefono, int telefono2, String email, String paginaWeb, String direccion, String ciudad, String pais) {
		this.nombre = nombre;
		this.NIF = NIF;
		this.telefono = telefono;
		this.telefono2 = telefono2;
		this.email = email;
		this.paginaWeb = paginaWeb;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}
	
	public Proveedor(){
		
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public int getTelefono2() {
		return telefono2;
	}
	public void setTelefono2(int telefono2) {
		this.telefono2 = telefono2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getPaginaWeb() {
		return paginaWeb;
	}
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	public String getNIF() {
		return NIF;
	}
	public void setNIF(String nIF) {
		NIF = nIF;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public int getIDProveedor() {
		return IDProveedor;
	}
	
	
}
