package restaurante.almacen;

public class Ingrediente {
	private int IDIngrediente;
	private String nombreLargo;
	private String nombreCorto;
	private Magnitud magnitud;
	private CategoriaIngrediente categoria;
	private Proveedor proveedor;
	private String codigoProveedor;
	private float precioBase;
	private Alergeno alergeno;
	private float stockMin;
	private float stockAct;
	
	public Ingrediente(int Id, String nombreLargo, String nombreCorto, Magnitud magnitud, CategoriaIngrediente categoria, Proveedor proveedor, String codProveedor, float precioBase, Alergeno alergeno, float stockMin, float stockACt) {
		this.IDIngrediente=Id;
		this.nombreLargo=nombreLargo;
		this.nombreCorto=nombreCorto;
		this.magnitud=magnitud;
		this.categoria=categoria;
		this.proveedor=proveedor;
		this.codigoProveedor=codProveedor;
		this.precioBase=precioBase;
		this.alergeno=alergeno;
		this.stockMin=stockMin;
		this.stockAct=stockACt;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public Magnitud getMagnitud() {
		return magnitud;
	}
	public void setMagnitud(Magnitud magnitud) {
		this.magnitud = magnitud;
	}
	public CategoriaIngrediente getCategoria() {
		return categoria;
	}
	public void setCategoria(CategoriaIngrediente categoria) {
		this.categoria = categoria;
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public String getCodigoProveedor() {
		return codigoProveedor;
	}
	public void setCodigoProveedor(String codigoProveedor) {
		this.codigoProveedor = codigoProveedor;
	}
	public float getPrecioBase() {
		return precioBase;
	}
	public void setPrecioBase(float precioBase) {
		this.precioBase = precioBase;
	}
	public Alergeno getAlergeno() {
		return alergeno;
	}
	public void setAlergeno(Alergeno alergeno) {
		this.alergeno = alergeno;
	}
	public float getStockMin() {
		return stockMin;
	}
	public void setStockMin(float stockMin) {
		this.stockMin = stockMin;
	}
	public float getStockAct() {
		return stockAct;
	}
	public void setStockAct(float stockAct) {
		this.stockAct = stockAct;
	}
	public int getIDIngrediente() {
		return IDIngrediente;
	}
	
	
	
}
