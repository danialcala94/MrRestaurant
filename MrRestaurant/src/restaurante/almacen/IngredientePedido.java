package restaurante.almacen;

public class IngredientePedido {
	private int IDIngredientePedido;
	private Ingrediente ingrediente;
	private float precio;
	private float cantidad;
	
	
	public IngredientePedido(int Id, Ingrediente ingrediente, float precio) {
		this.IDIngredientePedido=Id;
		this.ingrediente=ingrediente;
		this.precio=precio;
		this.cantidad=ingrediente.getStockMin()-ingrediente.getStockAct();
		if(this.cantidad<0) {
			this.cantidad=0;
		}
	}
	
	public IngredientePedido(int idIngredientePedido, Ingrediente ingrediente, float precio, float cantidad) {
		this.IDIngredientePedido = idIngredientePedido;
		this.ingrediente = ingrediente;
		this.precio = precio;
		this.cantidad = cantidad;
	}
	
	public IngredientePedido(Ingrediente ingrediente, float cantidad) {
		this.ingrediente=ingrediente;
		this.cantidad=cantidad;
	}


	public Ingrediente getIngrediente() {
		return ingrediente;
	}
	public void setIngrediente(Ingrediente ingrediente) {
		this.ingrediente = ingrediente;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public float getCantidad() {
		return cantidad;
	}
	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}
	public int getIDIngredientePedido() {
		return IDIngredientePedido;
	}
	
	
}
