package restaurante.empleados;

import java.sql.Date;

public class PagoEmpleado {
	private int idPagoEmpleado;
	private float cantidad;
	private Empleado empleado;
	private Date fecha;
	
	public PagoEmpleado(int idPagoEmpleado, float cantidad, Empleado empleado, Date fecha) {
		this.idPagoEmpleado = idPagoEmpleado;
		this.cantidad = cantidad;
		this.empleado = empleado;
		this.fecha = fecha;
	}
	
	public PagoEmpleado(float cantidad, Empleado empleado, Date fecha) {
		this.cantidad = cantidad;
		this.empleado = empleado;
		this.fecha = fecha;
	}

	public int getIDPagoEmpleado() {
		return idPagoEmpleado;
	}

	public void setIDPagoEmpleado(int idPagoEmpleado) {
		this.idPagoEmpleado = idPagoEmpleado;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
