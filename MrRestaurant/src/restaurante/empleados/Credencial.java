package restaurante.empleados;

public class Credencial {
	public int IDCredencial;
	private String usuario;
	private String contrasena;
	private NivelCredencial nivel;
	private Empleado empleado;
	
	public Credencial(String usuario, String contrasena, NivelCredencial nivel, Empleado empleado) {
		this.usuario=usuario;
		this.contrasena=contrasena;
		this.nivel=nivel;
		this.empleado=empleado;
	}
	
	public Credencial (int idCredencial, String usuario, String contrasena, NivelCredencial nivelCredencial, Empleado empleado) {
		this.IDCredencial = idCredencial;
		this.usuario = usuario;
		this.contrasena = contrasena;
		this.nivel = nivelCredencial;
		this.empleado=empleado;
	}
	
	
	public Credencial() {
	}

	public int getIDCredencial() {
		return IDCredencial;
	}
	public void setIDCredencial(int iDCredencial) {
		IDCredencial = iDCredencial;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contrasena;
	}
	public void setContraseña(String contraseña) {
		this.contrasena = contraseña;
	}
	public NivelCredencial getNivel() {
		return nivel;
	}
	public void setNivel(NivelCredencial nivel) {
		this.nivel = nivel;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	
	
}
