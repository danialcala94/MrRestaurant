package restaurante.empleados;

public class CategoriaEmpleado {
	private int IDCategoriaEmpleado;
	private String nombre;
	private String descripcion;
	
	public CategoriaEmpleado(int Id, String nombre, String descripcion) {
		this.IDCategoriaEmpleado = Id;
		this.nombre=nombre;
		this.descripcion=descripcion;
	}
	
	public int getIDCategoriaEmpleado() {
		return IDCategoriaEmpleado;
	}
	public void setIDCategoriaEmpleado(int iDCategoriaEmpleado) {
		IDCategoriaEmpleado = iDCategoriaEmpleado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
