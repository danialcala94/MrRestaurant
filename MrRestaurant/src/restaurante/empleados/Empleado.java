package restaurante.empleados;

public class Empleado {
	private int IDEmpleado;
	private String dni;
	private String nombre;
	private int telefono;
	private CategoriaEmpleado categoria;
	private float salario;
	private Credencial credenciales;
	
	public Empleado(int ID, String dni, String nombre, int telefono, CategoriaEmpleado categoria, float salario, Credencial credenciales){
		this.IDEmpleado = ID;
		this.dni = dni;
		this.nombre = nombre;
		this.telefono = telefono;
		this.categoria = categoria;
		this.salario = salario;
		this.credenciales = credenciales;
	}
	
	public Empleado(String dni, String nombre, float salario) {
		this.dni = dni;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	public Empleado(int id, String dni, String nombre, float salario) {
		this.IDEmpleado = id;
		this.dni = dni;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	public Empleado(String nombre, float salario,String dni) {
		this.nombre=nombre;
		this.salario=salario;
		this.dni=dni;
	}

	public int getIDEmpleado() {
		return IDEmpleado;
	}

	public void setIDEmpleado(int iDEmpleado) {
		IDEmpleado = iDEmpleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public CategoriaEmpleado getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaEmpleado categoria) {
		this.categoria = categoria;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public Credencial getCredenciales() {
		return credenciales;
	}

	public void setCredenciales(Credencial credenciales) {
		this.credenciales = credenciales;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
}
