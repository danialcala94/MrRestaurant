package restaurante.empleados;

public class NivelCredencial {
	private int IDNivelCredencial;
	private int nivel; //PAQU�ES?
	private String nombre;
	private String descripcion;
	
	/*
	 * Los niveles utilizados por los empleados para el acceso ser�n los siguientes
	 * 
	 * 	No estoy seguro de si vamos a necesitar un nivel de root
	 * root			nivel -1
	 * 
	 * Cocinero 	nivel 1
	 * JefeCocina 	nivel 2
	 * Camarero		nivel 3
	 * JefeSala		nivel 4
	 * Gerente		nivel 5
	 */
	
	
	
	
	
	
	public NivelCredencial(int idNivelCredencial,int nivel, String nombre, String descripcion) {
		this.IDNivelCredencial = idNivelCredencial;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.nivel=nivel;
	}
	
	public NivelCredencial(int idNivelCredencial, String nombre, String descripcion) {
		this.IDNivelCredencial = idNivelCredencial;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public NivelCredencial(String nombre, String descripcion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public NivelCredencial(String nombre) {
		this.nombre = nombre;
	}
	
	public NivelCredencial(int nivel) {
		this.nivel=nivel;
	}
	
	public int getIDNivelCredencial() {
		return IDNivelCredencial;
	}
	public void setIDNivelCredencial(int IDNivelCredencial) {
		this.IDNivelCredencial = IDNivelCredencial;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel=nivel;
	}
}
