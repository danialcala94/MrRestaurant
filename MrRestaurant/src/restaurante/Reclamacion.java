package restaurante;

public class Reclamacion {
	private int IDReclamacion;
	private int numReclamacion;
	private String DNICliente;
	
	public Reclamacion(int idReclamacion, int numReclamacion, String DniCliente) {
		this.IDReclamacion = idReclamacion;
		this.numReclamacion=numReclamacion;
		this.DNICliente=DniCliente;
	}
	
	/** 
	 * Para eliminar de BD solo busca por ID
	 * @param id
	 */
	public Reclamacion(int id) {
		this.IDReclamacion = id;
		this.numReclamacion = 123123;
		this.DNICliente = "12345678L";
	}
	
	public Reclamacion(int numReclamacion, String DniCliente) {
		this.numReclamacion=numReclamacion;
		this.DNICliente=DniCliente;
	}

	public int getNumReclamacion() {
		return numReclamacion;
	}

	public void setNumReclamacion(int numRelamacion) {
		this.numReclamacion = numRelamacion;
	}

	public String getDNICliente() {
		return DNICliente;
	}

	public void setDNICliente(String dNICliente) {
		DNICliente = dNICliente;
	}

	public int getIDReclamacion() {
		return IDReclamacion;
	}
	
}
