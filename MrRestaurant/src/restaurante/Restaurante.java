package restaurante;

import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import persistencia.objetos.ConversorAlergeno;
import persistencia.objetos.ConversorCategoriaEmpleado;
import persistencia.objetos.ConversorCategoriaIngrediente;
import persistencia.objetos.ConversorCategoriaPlato;
import persistencia.objetos.ConversorComanda;
import persistencia.objetos.ConversorComandaPlato;
import persistencia.objetos.ConversorCredencial;
import persistencia.objetos.ConversorEmpleado;
import persistencia.objetos.ConversorIngrediente;
import persistencia.objetos.ConversorIngredientePedido;
import persistencia.objetos.ConversorIngredientePlato;
import persistencia.objetos.ConversorMagnitud;
import persistencia.objetos.ConversorNivelCredencial;
import persistencia.objetos.ConversorPagoEmpleado;
import persistencia.objetos.ConversorPedido;
import persistencia.objetos.ConversorPlato;
import persistencia.objetos.ConversorProveedor;
import persistencia.objetos.ConversorReclamacion;
import restaurante.sala.*;
import restaurante.almacen.Alergeno;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Magnitud;
import restaurante.almacen.Pedido;
import restaurante.almacen.Proveedor;
import restaurante.cocina.*;
import restaurante.empleados.*;

public class Restaurante {
	
	private static Restaurante instanciaRestaurante;
	private String nombre;
	private int aforoMax;
	private int aforoMesas=0;
	private Mesa[] mesas;
	private ArrayList<Plato> listaPlatos;
	private ArrayList<Empleado> empleados;
	private ArrayList<Pedido> pedidos;
	private ArrayList <Ingrediente> ingredientes;
	private ArrayList <Credencial> credenciales;
	
	// Objetos de BD en memoria principal
	private ArrayList<Alergeno> bd_Alergenos = new ArrayList<Alergeno>();
	private ArrayList<CategoriaEmpleado> bd_CategoriasEmpleados = new ArrayList<CategoriaEmpleado>();
	private ArrayList<CategoriaIngrediente> bd_CategoriasIngredientes = new ArrayList<CategoriaIngrediente>();
	private ArrayList<CategoriaPlato> bd_CategoriasPlatos = new ArrayList<CategoriaPlato>();
	private ArrayList<Comanda> bd_Comandas = new ArrayList<Comanda>();
	private ArrayList<ComandaPlato> bd_ComandasPlatos = new ArrayList<ComandaPlato>();
	private ArrayList<Credencial> bd_Credenciales = new ArrayList<Credencial>();
	private ArrayList<Empleado> bd_Empleados = new ArrayList<Empleado>();
	private ArrayList<Ingrediente> bd_Ingredientes = new ArrayList<Ingrediente>();
	private ArrayList<IngredientePedido> bd_IngredientesPedidos = new ArrayList<IngredientePedido>();
	private ArrayList<IngredientePlato> bd_IngredientesPlatos = new ArrayList<IngredientePlato>();
	private ArrayList<Magnitud> bd_Magnitudes = new ArrayList<Magnitud>();
	private ArrayList<NivelCredencial> bd_NivelesCredenciales = new ArrayList<NivelCredencial>();
	private ArrayList<PagoEmpleado> bd_PagosEmpleados = new ArrayList<PagoEmpleado>();
	private ArrayList<Pedido> bd_Pedidos = new ArrayList<Pedido>();
	private ArrayList<Plato> bd_Platos = new ArrayList<Plato>();
	private ArrayList<Proveedor> bd_Proveedores = new ArrayList<Proveedor>();
	private ArrayList<Reclamacion> bd_Reclamaciones = new ArrayList<Reclamacion>();
	

	/**
	 * Constructor de la clase restaurante, es privado para evitar que se acceda de forma distinta
	 * a con el metodo getRestaurante
	 * Est� ser� nuestra clase principal de la aplicaci�n
	 * Almacenar� las referencias a los distintos arrays de:
	 * 	Empleados
	 * 	Proveedores
	 *  Mesas
	 *  Platos
	 *  Ingredientes
	 * Almacenar� ademas todos los elementos necesarios para el desarrollo del restaurante como
	 * los diferentes gestores o una serie de informaci�n b�sica
	 */
	private Restaurante() {
		System.out.println("Se ha creado la instancia del restaurante");
		volcarBD();
		ConversorPlato.getConversor().limpiarKeysOld();
	}
	
	/**
	 * El metodo getRestaurante es la unica forma de acceder a una instancia del restaurante
	 * @return Devuelve la instancia del restaurante, en caso de que no la hubiera la crea
	 */
	public static synchronized Restaurante getRestaurante() {
		if(instanciaRestaurante==null) {
			instanciaRestaurante = new Restaurante();
		}else{
			System.out.println("El restaurante ya exist�a");
		}
		return instanciaRestaurante;
	}
	
	/**
	 * Trae todos los objetos de persistencia a memoria principal
	 */
	private void volcarBD() {
		bd_Alergenos = FachadaPersistencia.obtenerTodos_Alergenos();
		bd_CategoriasEmpleados = FachadaPersistencia.obtenerTodos_CategoriasEmpleados();
		bd_CategoriasIngredientes = FachadaPersistencia.obtenerTodos_CategoriasIngredientes();
		bd_CategoriasPlatos = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
		bd_Comandas = FachadaPersistencia.obtenerTodos_Comandas();
		bd_ComandasPlatos = FachadaPersistencia.obtenerTodos_ComandasPlatos();
		bd_Credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
		bd_Empleados = FachadaPersistencia.obtenerTodos_Empleados();
		bd_Ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
		bd_IngredientesPedidos = FachadaPersistencia.obtenerTodos_IngredientesPedidos();
		bd_IngredientesPlatos = FachadaPersistencia.obtenerTodos_IngredientesPlatos();
		bd_Magnitudes = FachadaPersistencia.obtenerTodos_Magnitudes();
		bd_NivelesCredenciales = FachadaPersistencia.obtenerTodos_NivelesCredenciales();
		bd_PagosEmpleados = FachadaPersistencia.obtenerTodos_PagosEmpleados();
		bd_Pedidos = FachadaPersistencia.obtenerTodos_Pedidos();
		bd_Platos = FachadaPersistencia.obtenerTodos_Platos();
		bd_Proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
		bd_Reclamaciones = FachadaPersistencia.obtenerTodos_Reclamaciones();
	}
	
	
	public void addMesa(Mesa mesa) {
		if(aforoMesas+mesa.getNumMaxComensales()>aforoMax) {	//Si esa mesa superaria el aforo lanzamos exepcion
			throw new IllegalArgumentException("No se puede superar el aforo m�ximo del restaurante");
		}
		for(int i=0;i<mesas.length;i++) {
			if(mesas[i]!=null) {
				if(mesas[i].getIDMesa()==mesa.getIDMesa()) {	//Si se pretende repetir el ID de una mesa lanzamos excepcion
					throw new IllegalArgumentException("No se puede repetir el ID de una mesa");
				}
			}
		}
		for(int i=0; i<mesas.length;i++) {
			if(mesas[i]==null) {	//Si el espacio de esa mesa esta vacio la guardamos
				aforoMesas += mesa.getNumMaxComensales();
				mesas[i]=mesa;
				return;
			}
		}
		//Si comprobamos todos los huecos y no hay sitio lanzamos excepcion
		throw new IllegalArgumentException("No queda sitio en el restaurante para m�s mesas");
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public ArrayList<Plato> getPlatos(){
		return listaPlatos;
	}

	public int getAforoMax() {
		return aforoMax;
	}

	public void setAforoMax(int aforoMax) {
		this.aforoMax = aforoMax;
	}

	public Mesa[] getMesas() {
		return mesas;
	}

	public void setMesas(Mesa[] mesas) {
		this.mesas = mesas;
	}

	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}

	public ArrayList<Pedido> getPedidos() {
		return pedidos;
	}
	
	public void addPedido(Pedido pedido) {
		pedidos.add(pedido);
	}
	
	public Pedido getUltimoPedido(){
		if(pedidos.size()==0) {
			return null;
		}
		return pedidos.get(pedidos.size()-1);
	}

	public void setPedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public ArrayList<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public void setPlatos(ArrayList<Plato> listaPlatos) {
		this.listaPlatos = listaPlatos;
	}

	public ArrayList<Credencial> getCredenciales() {
		return credenciales;
	}

	public void setCredenciales(ArrayList<Credencial> credenciales) {
		this.credenciales = credenciales;
	}
	
	
	
}


