package restaurante.cocina;

import restaurante.almacen.Ingrediente;

public class IngredientePlato {
	private int IDIngredientePlato;
	private Ingrediente ingrediente;
	private float cantidad;
	
	public IngredientePlato(int Id, Ingrediente ingrediente, float cantidad) {
		this.IDIngredientePlato=Id;
		this.ingrediente=ingrediente;
		this.cantidad=cantidad;
	}
	
	public IngredientePlato(Ingrediente ingrediente, float cantidad) {
		this.ingrediente = ingrediente;
		this.cantidad = cantidad;
	}

	public int getIDIngredientePlato() {
		return IDIngredientePlato;
	}
	public void setIDIngredientePlato(int iDIngredientePlato) {
		IDIngredientePlato = iDIngredientePlato;
	}
	public Ingrediente getIngrediente() {
		return ingrediente;
	}
	public void setIngrediente(Ingrediente ingrediente) {
		this.ingrediente = ingrediente;
	}
	public float getCantidad() {
		return cantidad;
	}
	
	
}
