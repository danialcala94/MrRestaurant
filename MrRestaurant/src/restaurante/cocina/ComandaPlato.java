package restaurante.cocina;

public class ComandaPlato {
	private int IDComandaPlato;
	private Plato plato;
	private int cantidad;
	private float precio;
	private String comentario;
	private boolean servido;
	private int idComanda;
	
	public ComandaPlato(int Id, Plato plato, int cantidad, float precio, String comentario, boolean servido, int idComanda) {
		this.IDComandaPlato=Id;
		this.plato=plato;
		this.cantidad=cantidad;
		this.precio=precio;
		this.comentario=comentario;
		this.servido=servido;
		this.setIdComanda(idComanda);
	}
	
	public ComandaPlato(int Id, Plato plato, int cantidad, float precio, String comentario, boolean servido) {
		this.IDComandaPlato=Id;
		this.plato=plato;
		this.cantidad=cantidad;
		this.precio=precio;
		this.comentario=comentario;
		this.servido=servido;
	}
	
	public ComandaPlato(Plato plato, int cantidad, String comentario) {
		this.plato=plato;
		this.cantidad=cantidad;
		this.comentario=comentario;
	}
	
	
	public Plato getPlato() {
		return plato;
	}
	public void setPlato(Plato plato) {
		this.plato = plato;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public boolean isServido() {
		return servido;
	}
	public void setServido(boolean servido) {
		this.servido = servido;
	}
	public int getIDComandaPlato() {
		return IDComandaPlato;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	
	
}
