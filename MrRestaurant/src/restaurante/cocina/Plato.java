package restaurante.cocina;

import java.util.ArrayList;

public class Plato {
	private int IDPlato;
	private String nombreLargo;
	private String nombreCorto;
	private String descripcion;
	private float precio;
	private ArrayList<IngredientePlato> ingredientes;
	private CategoriaPlato categoria;
	private String receta;
	
	public Plato(int Id, String nombreLargo, String nombreCorto, String descripcion, float precio, ArrayList<IngredientePlato> ingredientes, CategoriaPlato categoria, String receta){
		this.IDPlato=Id;
		this.nombreLargo=nombreLargo;
		this.nombreCorto=nombreCorto;
		this.descripcion=descripcion;
		this.precio=precio;
		this.ingredientes=ingredientes;
		this.categoria=categoria;
		this.receta=receta;
	}
	
	public Plato( String nombreLargo, String nombreCorto, String descripcion, float precio, ArrayList<IngredientePlato> ingredientes, CategoriaPlato categoria, String receta){
		this.nombreLargo=nombreLargo;
		this.nombreCorto=nombreCorto;
		this.descripcion=descripcion;
		this.precio=precio;
		this.ingredientes=ingredientes;
		this.categoria=categoria;
		this.receta=receta;
	}

	
	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public ArrayList<IngredientePlato> getIngredientes() {
		return ingredientes;
	}
	public void setIngredientes(ArrayList<IngredientePlato> ingredientes) {
		this.ingredientes = ingredientes;
	}
	public CategoriaPlato getCategoria() {
		return categoria;
	}
	public void setCategoria(CategoriaPlato categoria) {
		this.categoria = categoria;
	}
	public String getReceta() {
		return receta;
	}
	public void setReceta(String receta) {
		this.receta = receta;
	}
	public int getIDPlato() {
		return IDPlato;
	}
	
	
}
