package restaurante.cocina;

public class CategoriaPlato {
	private int IDCategoriaPlato;
	private String nombre;
	private String descripcion;
	
	public CategoriaPlato(int Id, String nombre, String descripcion) {
		this.IDCategoriaPlato=Id;
		this.nombre=nombre;
		this.descripcion=descripcion;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIDCategoriaPlato() {
		return IDCategoriaPlato;
	}
	
}
