package restaurante.sala;

import java.util.Date;

import restaurante.cocina.ComandaPlato;

import java.util.ArrayList;

public class Comanda {
	private int IDComanda;
	private Mesa mesa;
	// Campo para bd
	private int idMesa;
	private Date fecha;
	private float precio;
	private ArrayList<ComandaPlato> comandaPlatos;
	
	public Comanda(int Id, Mesa mesa, float precio, ArrayList<ComandaPlato> comandaPlatos) {
		this.IDComanda=Id;
		this.mesa=mesa;
		this.idMesa = mesa.getIDMesa();
		this.fecha=new Date();
		this.precio=precio;
		this.comandaPlatos=comandaPlatos;
	}
	
	public Comanda(int id, int idMesa, Date fecha, ArrayList<ComandaPlato> comandaPlatos) {
		this.IDComanda = id;
		this.idMesa = idMesa;
		this.fecha = fecha;
		this.comandaPlatos = comandaPlatos;
		this.precio = 0;
		for (int i = 0; i < comandaPlatos.size(); i++) {
			this.precio += comandaPlatos.get(i).getCantidad() * comandaPlatos.get(i).getPrecio();
		}
	}
	
	public Comanda(Mesa mesa,ArrayList<ComandaPlato> comandaPlatos) {
		this.mesa=mesa;
		this.comandaPlatos=comandaPlatos;
		this.fecha = new Date();
	}

	public Mesa getMesa() {
		return mesa;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public ArrayList<ComandaPlato> getComandaPlatos() {
		return comandaPlatos;
	}
	public void setComandaPlatos(ArrayList<ComandaPlato> comandaPlatos) {
		this.comandaPlatos = comandaPlatos;
	}
	public int getIDComanda() {
		return IDComanda;
	}
	
	
}
