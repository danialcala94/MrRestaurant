package restaurante.sala;

import restaurante.*;
import restaurante.cliente.Cliente;

public class Mesa {
	
	private final int MESA_LIBRE = 0;
	private final int MESA_OCUPADA = 1;
	private final int MESA_APREPARAR = 2;
	private final int MESA_RESERVADA = 3;
	
	public int IDMesa;
	private int capacidad;
	private int numActComensales;
	private int coordX, coordY;
	private Cliente clienteActual;
	private int estado;
	private Comanda comanda;
	
	public Mesa(int id, int capacidad, int coordX, int coordY) {
		this.IDMesa = id;
		this.capacidad = capacidad;
		this.numActComensales = 0;
		this.coordX = coordX;
		this.coordY = coordY;
		this.clienteActual = null;
		this.estado = MESA_LIBRE;
		this.comanda = null;
	}
	
	public Mesa(int ID, int capacidad) {
		this.IDMesa = ID;
		this.capacidad = capacidad;
		this.numActComensales = 0;
		this.clienteActual = null;
		this.comanda = null;
		this.estado = MESA_LIBRE; //Limpia
	}
	
	public Mesa(int numMaxComensales) {
		this.capacidad = numMaxComensales;
		this.estado = MESA_LIBRE;
	}
	
	public int getCoordX() {
		return this.coordX;
	}
	public int getCoordY() {
		return this.coordY;
	}
	public int getIDMesa() {
		return IDMesa;
	}
	public void setIDMesa(int iDMesa) {
		IDMesa = iDMesa;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	public int getNumActComensales() {
		return numActComensales;
	}
	public void setNumActComensales(int numActComensales) {
		this.numActComensales = numActComensales;
	}
	public Cliente getClienteActual() {
		return clienteActual;
	}
	public void setClienteActual(Cliente clienteActual) {
		this.clienteActual = clienteActual;
	}

	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		if (estado >= 0 && estado <= 4)
			this.estado = estado;
	}
	public Comanda getComanda() {
		return comanda;
	}
	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}
	
	public void marcarOcupada(int numComensales, Cliente cliente) {
		this.clienteActual=cliente;
		this.numActComensales=numComensales;
		this.estado = MESA_OCUPADA;
	}
	
	public void marcarSucia() {
		this.clienteActual=null;
		this.numActComensales=0;
		this.estado = MESA_APREPARAR;
		this.comanda=null;
	}
	
	public void marcarLimpia() {
		this.clienteActual = null;
		this.numActComensales = 0;
		this.estado = MESA_LIBRE;
		this.comanda = null;
	}
	
	public void marcarReservada(Cliente cliente) {
		this.clienteActual = cliente;
		this.numActComensales = 0;
		this.estado = MESA_RESERVADA;
		this.comanda = null;
	}
	
	public void asignarComanda(Comanda comanda) {
		this.comanda = comanda;
	}
	
}
