package restaurante.cliente;

public class Cliente {
	public int IDCliente;
	private String nombre;
	private int telefono;
	
	public Cliente(int IDCliente, String nombre, int telefono) {
		this.IDCliente=IDCliente;
		this.nombre=nombre;
		this.telefono=telefono;
	}
	
	public Cliente(String nombre) {
		this.nombre=nombre;
	}
}
