package restaurante;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.JFrame;

import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;
import restaurante.sala.*;

public class Main {

	static String nombre="MrRestaurant";
	static int aforoMax=60;
	static int numeroMesas=10;
	static int numMaxSillasMesa=8;
	static Restaurante restaurante;
	private static ArrayList<Empleado> listaEmpleados = FachadaPersistencia.obtenerTodos_Empleados();
	private static ArrayList<Credencial> listaCredenciales = new ArrayList<Credencial>();

	public static void main(String[] args) {
		restaurante = Restaurante.getRestaurante();
		relacionaEmpleados();
		seteaInfoInicialRestaurante();
		iniciaRestaurante();
	}
	
	private static void iniciaRestaurante() {
		GUIVentanaLogin login = new GUIVentanaLogin(listaCredenciales);
		login.setVisible(true);
	}
	
	private static void seteaInfoInicialRestaurante() {
		restaurante.setNombre(nombre);
		restaurante.setAforoMax(aforoMax);
		restaurante.setMesas(creaMesas());
		restaurante.setIngredientes(FachadaPersistencia.obtenerTodos_Ingredientes());
		restaurante.setPedidos(FachadaPersistencia.obtenerTodos_Pedidos());
		restaurante.setPlatos(FachadaPersistencia.obtenerTodos_Platos());
		restaurante.setCredenciales(listaCredenciales);
		restaurante.setEmpleados(listaEmpleados);
	}
	
	private static Mesa[] creaMesas() {
		Mesa[] listaMesas = new Mesa[numeroMesas];
		for(int i=0;i<numeroMesas;i++) {
			listaMesas[i] = new Mesa((int) (Math.random()*numMaxSillasMesa+2));	//Sillas de 2 a 8
		}
		return listaMesas;
	}
	
	private static void relacionaEmpleados() {
		for(int i=0;i<listaEmpleados.size();i++) {
			Empleado auxEmpleado = listaEmpleados.get(i);
			Credencial auxCredencial = auxEmpleado.getCredenciales();
			auxCredencial.setEmpleado(auxEmpleado);
			listaCredenciales.add(auxCredencial);
			System.out.println("Empleado: "+auxEmpleado.getNombre());
		}
	}

}
