package eventos.sala;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JLabel;

import restaurante.sala.Mesa;
import ui.sala.UIComanda;
import ui.sala.UISala;

public class MesaSalaMouseListener implements MouseListener, MouseMotionListener {
	
	private boolean modificandoSala = false;
	private JLabel thisMesa = null;
	private ArrayList<JLabel> mesas = null;
	
	// Movimientos de mesa
	boolean movida = false;
	int x = 0; int y = 0;
	
	public MesaSalaMouseListener(JLabel labelMesa, ArrayList<JLabel> mesas) {
		this.thisMesa = labelMesa;
		this.mesas = mesas;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Mesa " + this.getClass().getSimpleName() + " seleccionada");
		if (!modificandoSala) {
			new UIComanda();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		x = thisMesa.getX(); y = thisMesa.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if (modificandoSala) {
			int newX = thisMesa.getX() + e.getX() - thisMesa.getWidth() / 2;
			int newY = thisMesa.getY() + e.getY() - thisMesa.getHeight() / 2;
			boolean encimaDeMesa = false;
			for (int i = 0; i < mesas.size(); i++) {
				if (!thisMesa.equals(mesas.get(i))) {
					System.out.println("this.X = " + thisMesa.getX() + " otra.X = " + mesas.get(i).getX() + " --- this.Y = " + thisMesa.getY() + " otra.Y = " + mesas.get(i).getY());
					if (!(thisMesa.getX() < (mesas.get(i).getX() - mesas.get(i).getIcon().getIconWidth() + 1)) && !(thisMesa.getX() > (mesas.get(i).getX() + mesas.get(i).getIcon().getIconHeight() - 1)) /*&&
							thisMesa.getY() < (mesas.get(i).getY() - MESA_ANCHOyALTO) && thisMesa.getY() > (mesas.get(i).getY() + MESA_ANCHOyALTO)*/) {
						if (!(thisMesa.getY() < (mesas.get(i).getY() - mesas.get(i).getIcon().getIconWidth() + 1)) && !(thisMesa.getY() > (mesas.get(i).getY() + mesas.get(i).getIcon().getIconHeight() - 1))) {
							encimaDeMesa = true;
							break;
						}
					}
				}
			}
			if (newX < 0 || (newX > UISala.JPANELIMAGE_ANCHO + 1 - thisMesa.getIcon().getIconWidth()) || newY < 0 || (newY > UISala.JPANELIMAGE_ALTO + 1 - thisMesa.getIcon().getIconHeight())) {
				thisMesa.setLocation(x, y);
				movida = false;
			} else {
				if (encimaDeMesa) {
					thisMesa.setLocation(x, y);
					movida = false;
				} else {
					thisMesa.setLocation(newX, newY);
					x = newX; y = newY;
				}
			}
			System.out.println("Se dejado de presionar Mesa " + this.getClass().getSimpleName() + " seleccionada");
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		if (modificandoSala) {
			int newX = thisMesa.getX() + arg0.getX() - thisMesa.getWidth() / 2;
			int newY = thisMesa.getY() + arg0.getY() - thisMesa.getHeight() / 2;
			if (newX < 0 || (newX > UISala.JPANELIMAGE_ANCHO - UISala.MESA_ANCHOyALTO) || newY < 0 || (newY > UISala.JPANELIMAGE_ALTO - UISala.MESA_ANCHOyALTO)) {
				System.out.println("Fuera de los l�mites...");
			} else
				thisMesa.setLocation(newX, newY);
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void activarModificacion() {
		this.modificandoSala = true;
	}
	public void desactivarModificacion() {
		this.modificandoSala = false;
	}
}
