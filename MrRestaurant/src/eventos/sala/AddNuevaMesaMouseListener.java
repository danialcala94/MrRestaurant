package eventos.sala;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JLabel;

import restaurante.Constantes;
import ui.sala.UISala;

public class AddNuevaMesaMouseListener implements MouseListener, MouseMotionListener {

	UISala parent = null;
	JLabel thisMesa = null;
	private ArrayList<JLabel> mesas = null;
	int inicialX = 870 - 64; int inicialY = 1*64;
	Icon icono = null;
	
	public AddNuevaMesaMouseListener(UISala parent, JLabel nuevaMesa, ArrayList<JLabel> mesas) {
		this.parent = parent;
		this.thisMesa = nuevaMesa;
		this.mesas = mesas;
		inicialX = thisMesa.getX(); inicialY = thisMesa.getY();
		icono = nuevaMesa.getIcon();
		System.out.println(nuevaMesa.getIcon().getIconWidth());
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		int newX = thisMesa.getX() + e.getX() - thisMesa.getWidth() / 2;
		int newY = thisMesa.getY() + e.getY() - thisMesa.getHeight() / 2;
		thisMesa.setLocation(newX, newY);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		// Si han colocado Mesa en un lugar factible dentro del sal�n, creamos un nuevo JLabelMesa en dicha posici�n, lo a�adimos al array Mesas y lo tratamos.
		// Tambi�n devolveremos la mesa del panel a su posici�n.
		int newX = thisMesa.getX() + e.getX() - thisMesa.getWidth() / 2;
		int newY = thisMesa.getY() + e.getY() - thisMesa.getHeight() / 2;
		boolean encimaDeMesa = false;
		for (int i = 0; i < mesas.size(); i++) {
			System.out.println("this.X = " + thisMesa.getX() + " otra.X = " + mesas.get(i).getX() + " --- this.Y = " + thisMesa.getY() + " otra.Y = " + mesas.get(i).getY());
			if (!(thisMesa.getX() < (mesas.get(i).getX() - mesas.get(i).getIcon().getIconWidth() + 1)) && !(thisMesa.getX() > (mesas.get(i).getX() + mesas.get(i).getIcon().getIconHeight() - 1))) {
				if (!(thisMesa.getY() < (mesas.get(i).getY() - mesas.get(i).getIcon().getIconWidth() + 1)) && !(thisMesa.getY() > (mesas.get(i).getY() + mesas.get(i).getIcon().getIconHeight() - 1))) {
					encimaDeMesa = true;
					break;
				}
			}
		}
		if (newX < 0 || (newX > UISala.JPANELIMAGE_ANCHO + 1 - thisMesa.getIcon().getIconWidth()) || newY < 0 || (newY > UISala.JPANELIMAGE_ALTO + 1 - thisMesa.getIcon().getIconHeight()))
			// Devolver al panel sin a�adir una nueva
			thisMesa.setLocation(inicialX, inicialY);
		else {
			if (!encimaDeMesa) {
				JLabel nuevaMesa = new JLabel("");
				//nuevaMesa.setIcon(Constantes.ICONO_MESA_RESERVADA);
				nuevaMesa.setIcon(icono);
				nuevaMesa.setBounds(thisMesa.getBounds());
				parent.addNuevaMesa(nuevaMesa);
			}
			thisMesa.setLocation(inicialX, inicialY);
		}
	}

}
