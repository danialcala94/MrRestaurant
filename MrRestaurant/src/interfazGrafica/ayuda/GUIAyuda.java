package interfazGrafica.ayuda;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.*;

import interfazGrafica.GUIVentanaLogin;

public class GUIAyuda extends JFrame {

	/** Item de men� para la ayuda */
	private JMenuItem itemAyuda;

	private File fichero;
	private URL hsURL;
	private HelpSet helpset;
	private HelpBroker hb;
	
	private static GUIAyuda instancia = null;
	
	/**
	 * Crea las ventanas, pone la ayuda y visualiza la ventana principal.
	 */
	public GUIAyuda() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		try {
			// Carga el fichero de ayuda
			fichero = new File("javahelp/help_set.hs");
			hsURL = fichero.toURI().toURL();
	
			// Crea el HelpSet y el HelpBroker
			helpset = new HelpSet(getClass().getClassLoader(), hsURL);
			hb = helpset.createHelpBroker();
		} catch (Exception ex) {
			System.err.println("GUIAayuda() - Excepci�n: " + ex.getMessage());
		}
		System.out.println("Se acaba de setear la ayuda");
		// Hay que utilizar el JHINDEXER para que genere la base de datos con respecto a los ficheros que hay ahora. No he conseguido
		// que funcione y he metido una base de datos de otra cosa, que hace que no crashee pero tampoco encuentra ning�n resultado.
		this.pack();
	}
	
	public static GUIAyuda getInstancia(JMenuItem item, String helpId) {
		if (instancia == null) {
			instancia =  new GUIAyuda();
		}
		instancia.asignarBotonAyuda(item, helpId);
		return instancia;
	}
	
	/**
	 * Asigna al bot�n recibido la acci�n de mostrar la ayuda en la ventana correspondiente (helpId).
	 * @param item
	 * @param helpId
	 */
	public void asignarBotonAyuda(JMenuItem item, String helpId) {
		hb.enableHelpOnButton(item, helpId, helpset);
	}

}
