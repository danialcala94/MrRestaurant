package interfazGrafica.sala;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

import restaurante.Restaurante;
import restaurante.almacen.IngredientePedido;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;
import restaurante.sala.Comanda;
import restaurante.sala.Mesa;


public class GUIVentComanda extends JDialog implements ActionListener {

	private JPanel contentPane;
	private JButton btnComanda = new JButton("Confirmar comanda");
	private JButton btnAddPlato = new JButton("A\u00F1adir plato");
	private boolean completada = false;
	private ArrayList<Plato> listaAuxPlatos = new ArrayList<Plato>();
	private ArrayList<Plato> listaTotalPlatos = Restaurante.getRestaurante().getPlatos();
	private int siguienteY = 11; 
	private ArrayList<JTextField> listaplatos = new ArrayList<JTextField>();
	private ArrayList<JTextField> listacomentarios = new ArrayList<JTextField>();
	private ArrayList<JTextField> listacantidad = new ArrayList<JTextField>();
	private JPanel panel = new JPanel();
	private ArrayList<ComandaPlato> listaComandaPlatos = new ArrayList<ComandaPlato>();
	private Comanda comandaGenerada;
	private int numMesa;
	private JComboBox comboBox;
	private JButton btnAyuda;

	/**
	 * Create the frame.
	 */
	public GUIVentComanda(int numMesa) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setTitle("Comanda");
		this.setResizable(false);
		setBounds(100, 100, 816, 613);
		this.numMesa = numMesa;
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 39, 566, 524);
		contentPane.add(scrollPane);
		

		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		String[] nombrePlatos = new String[listaTotalPlatos.size()];
		for(int i=0;i<listaTotalPlatos.size();i++) {
			nombrePlatos[i] = listaTotalPlatos.get(i).getNombreCorto();
		}
		
		comboBox = new JComboBox(nombrePlatos);
		comboBox.setBounds(586, 108, 204, 20);
		contentPane.add(comboBox);
		
		JLabel lblNewLabel = new JLabel("Comentarios");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(318, 14, 227, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblPlato = new JLabel("Plato");
		lblPlato.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlato.setBounds(10, 14, 227, 14);
		contentPane.add(lblPlato);
		
		JLabel lblNewLabel_1 = new JLabel("Cantidad");
		lblNewLabel_1.setBounds(10, 14, 535, 14);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblPlato_1 = new JLabel(" Plato a a\u00F1adir");
		lblPlato_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlato_1.setBounds(586, 76, 204, 14);
		contentPane.add(lblPlato_1);
		

		btnAddPlato.setBounds(594, 314, 196, 23);
		btnAddPlato.addActionListener(this);
		contentPane.add(btnAddPlato);
		

		btnComanda.setBounds(594, 375, 196, 23);
		btnComanda.setEnabled(false);
		btnComanda.addActionListener(this);
		contentPane.add(btnComanda);
		
		JLabel lblComandaDeLa = new JLabel("Comanda de la mesa: "+(numMesa+1));
		lblComandaDeLa.setBounds(586, 212, 204, 14);
		contentPane.add(lblComandaDeLa);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.setBounds(594, 527, 196, 23);
		btnAyuda.addActionListener(this);
		contentPane.add(btnAyuda);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton aux = (JButton) e.getSource();
		if(btnAyuda==aux) {
			GUIAyuda.muestraAyuda(5);
		}
		if(btnAddPlato == aux) {
			int indexElegido = comboBox.getSelectedIndex();
			Plato platoElegido = listaTotalPlatos.get(indexElegido);
			listaAuxPlatos.add(platoElegido);
			creaEntrada(platoElegido.getNombreCorto());
			btnComanda.setEnabled(true);
			System.out.println("Plato a�adido");
		}
		if(btnComanda == aux) {
			ArrayList<ComandaPlato> listaComandaPlato = new ArrayList<ComandaPlato>();
			for(int i=0;i<listaplatos.size();i++) {
				String textoCantidad = listacantidad.get(i).getText();
				if(cantidadValida(textoCantidad)) {
					int cantidad = Integer.parseInt(textoCantidad);
					ComandaPlato comandaPlato = new ComandaPlato(listaAuxPlatos.get(i), cantidad, listacomentarios.get(i).getText());
					listaComandaPlato.add(comandaPlato);
				}else {
					return;
				}
			}
			//Cuando acabamos de generar la lista de comandaplatos ya podemos generar la comanda y guardarla en una variable
			Mesa mesa = Restaurante.getRestaurante().getMesas()[numMesa];
			comandaGenerada = new Comanda(mesa, listaComandaPlato);
			mesa.setComanda(comandaGenerada);
			completada = true;
			dispose();
		}
	}
	
	private void creaEntrada(String nombrePlato) {
		
		JTextField plato = new JTextField(nombrePlato);
		plato.setBounds(10, siguienteY, 213, 20);
		plato.setEditable(false);
		panel.add(plato);
		plato.setColumns(10);
		
		JTextField cantidad = new JTextField("1");
		cantidad.setVisible(true);
		cantidad.setBounds(233, siguienteY, 80, 20);
		panel.add(cantidad);
		cantidad.setColumns(10);
		
		JTextField comentario = new JTextField("a");	//Se debe iniciarlizar con texto y luego setearlo a "" para que se muestre el texto
		comentario.setText("");
		comentario.setVisible(true);
		comentario.setBounds(323, siguienteY, 213, 20);
		panel.add(comentario);
		comentario.setColumns(10);
		
		siguienteY+=50;
		
		listaplatos.add(plato);
		listacantidad.add(cantidad);
		listacomentarios.add(comentario);
		
		panel.setPreferredSize(new Dimension(540, siguienteY-20));
		this.setBounds(100, 100, 817, 614);
		this.setBounds(100, 100, 816, 613);
		//Cambiamos el tama�o para que la ventanaParpadee y se actualice
		//No se porque si no hcemos esto no se activa el scroll
	}
	
	private boolean cantidadValida(String text) {
		boolean valida = true;
		try {
			int cantidad = Integer.parseInt(text);
			if(cantidad<1) {
				JOptionPane.showMessageDialog(null, "La cantidad de un plato debe ser un numero entero mayor que 0","Error",JOptionPane.ERROR_MESSAGE);
				valida = false;
			}
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "La cantidad de un plato debe ser un numero entero mayor que 0","Error",JOptionPane.ERROR_MESSAGE);
			valida=false;
		}
		return valida;
	}
	
	public Comanda getComanda() {
		return comandaGenerada;
	}
	
	public boolean isCompletada() {
		return completada;
	}
	
}
