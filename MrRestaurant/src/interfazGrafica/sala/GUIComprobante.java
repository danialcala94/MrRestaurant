package interfazGrafica.sala;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class GUIComprobante extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnAyuda;


	/**
	 * Create the dialog.
	 */
	public GUIComprobante(String comprobante) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.setTitle("Compobante");
		setBounds(100, 100, 593, 589);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			btnAyuda = new JButton("Ayuda");
			btnAyuda.setBounds(10, 494, 89, 23);
			btnAyuda.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUIAyuda.muestraAyuda(4);
				}
			});
			contentPanel.add(btnAyuda);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 11, 557, 495);
			contentPanel.add(scrollPane);
			{
				JLabel lblEsteTextoSe = new JLabel("Este texto se enviar\u00EDa a la impresora");
				lblEsteTextoSe.setFont(new Font("Tahoma", Font.BOLD, 15));
				lblEsteTextoSe.setForeground(Color.RED);
				lblEsteTextoSe.setHorizontalAlignment(SwingConstants.CENTER);
				scrollPane.setColumnHeaderView(lblEsteTextoSe);
			}
			
			JTextArea textArea = new JTextArea(comprobante);
			scrollPane.setViewportView(textArea);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
			}
		}
	}
}
