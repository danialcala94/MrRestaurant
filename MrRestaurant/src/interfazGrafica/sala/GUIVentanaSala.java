package interfazGrafica.sala;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import restaurante.Restaurante;
import restaurante.cliente.Cliente;
import restaurante.empleados.Empleado;
import restaurante.sala.Mesa;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentanaSala extends JFrame implements ActionListener {

	private JPanel contentPane;
	private ImageIcon icoMesaLimpia = new ImageIcon(GUIVentanaSala.class.getResource("/recursos/mesaLibre.png"));
	private ImageIcon icoMesaSucia = new ImageIcon(GUIVentanaSala.class.getResource("/recursos/mesaSucia.png"));
	private ImageIcon icoMesaOcupada = new ImageIcon(GUIVentanaSala.class.getResource("/recursos/mesaOcupada.png"));
	private Mesa[] listaMesas = Restaurante.getRestaurante().getMesas();
	private int numMesas = 10; 
	private JLabel[] icoMesas = new JLabel[numMesas];
	private JButton[] botonesOcupar = new JButton[numMesas];
	private JButton[] botonesReclamar = new JButton[numMesas];
	private JButton[] botonesCobrar = new JButton[numMesas];
	private JButton[] botonesLimpiar = new JButton[numMesas];
	private JButton[] botonesComanda = new JButton[numMesas];
	private JButton btnAyuda = new JButton("Ayuda");
	private JButton btnActualizarVentana = new JButton("Actualizar Ventana ");
	private boolean isJefe;
	private Empleado empleado;
	
	

	/**
	 * Create the frame.
	 */
	
	//Si es Jefe
		//Ocupar mesa
		//Cobrar
		//Gestionar reclamacion
	//Si es camarero
		//Gestionar comanda
		//Limpiar mesa
	
	
	public GUIVentanaSala(boolean isJefe, Empleado empleado) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.isJefe=isJefe;
		this.empleado=empleado;
		String cargo = "Camarero";
		if(isJefe) {
			cargo = "Jefe de sala";
		}
		setTitle("Ventana Sala:   "+cargo+"   "+empleado.getNombre()+" ("+empleado.getDni()+")");
		setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1231, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

	
		btnActualizarVentana.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnActualizarVentana.setBounds(316, 448, 208, 42);
		btnActualizarVentana.addActionListener(this);
		contentPane.add(btnActualizarVentana);	

		btnAyuda.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnAyuda.addActionListener(this);
		btnAyuda.setBounds(701, 448, 208, 42);
		contentPane.add(btnAyuda);
		
		creaBotones();
		creaIconosMesas();
	}
	
	private void actualizar() {
		Mesa[] mesasAct = Restaurante.getRestaurante().getMesas();
		for(int i=0;i<mesasAct.length;i++) {
			if(mesasAct[i].isLimpia()) {
				iniciaMesaLimpia(i);
			}
			if(mesasAct[i].isOcupada()) {
				iniciaMesaOcupada(i);
			}
			if(mesasAct[i].isSucia()) {
				iniciaMesaSucia(i);
			}	
		}
	}
	
	private void creaIconosMesas(){
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {

				int posX = 10 + (j*250);
				int posY = 10 + (i*220);
				int numSillas = listaMesas[j+i*5].getNumMaxComensales();
				JLabel sillas = new JLabel("Sillas: "+String.valueOf(numSillas));
				sillas.setFont(new Font("Tahoma", Font.BOLD, 15));
				sillas.setBounds(posX+40, posY+150, 70, 14);
				contentPane.add(sillas);
				JLabel mesa = new JLabel("Mesa: "+(j+i*5+1));
				mesa.setFont(new Font("Tahoma", Font.BOLD, 15));
				mesa.setBounds(posX+40, posY+42, 70, 14);
				contentPane.add(mesa);
				JLabel aux = new JLabel("");
				aux.setBounds(posX, posY, 210, 210);
				icoMesas[j+(i*5)] = aux;
				contentPane.add(aux);
				if(listaMesas[j+(i*5)].isLimpia()) {
					iniciaMesaLimpia(j+i*5);
				}
				if(listaMesas[j+(i*5)].isSucia()) {
					iniciaMesaSucia(j+i*5);
				}
				if(listaMesas[j+(i*5)].isOcupada()) {
					iniciaMesaOcupada(j+i*5);
				}
			}
		}
	}
	
	
	private void creaBotones() {
			creaBotonesCobrar();
			creaBotonesOcupar();
			creaBotonesReclamar();
			creaBotonesLimpiar();
			creaBotonesComanda();

	}
	
	private void creaBotonesComanda() {
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {
				JButton btn = new JButton("Comanda");
				btn.addActionListener(this);
				int posX = 70 + (j*250);
				int posY = 100 + (i*224);
				btn.setBounds(posX, posY, 89, 23);
				btn.setVisible(false);
				contentPane.add(btn);
				botonesComanda[j+(i*5)] = btn;
			}
		}
	}

	private void creaBotonesOcupar() {		
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {
				JButton btn = new JButton("Ocupar");
				btn.addActionListener(this);
				int posX = 70 + (j*250);
				int posY = 100 + (i*224);
				btn.setBounds(posX, posY, 89, 23);
				btn.setVisible(false);
				contentPane.add(btn);
				botonesOcupar[j+(i*5)] = btn;
			}
		}
	
	}
	
	private void creaBotonesReclamar() {
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {
				JButton btn = new JButton("Reclamar");
				btn.addActionListener(this);
				int posX = 70 + (j*250);
				int posY = 125+ (i*224);
				btn.setBounds(posX, posY, 89, 23);
				btn.setVisible(false);
				contentPane.add(btn);
				botonesReclamar[j+(i*5)] = btn;
			}
		}
	}
	
	private void creaBotonesLimpiar() {
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {
				JButton btn = new JButton("Limpiar");
				btn.addActionListener(this);
				int posX = 70 + (j*250);
				int posY = 100 + (i*224);
				btn.setBounds(posX, posY, 89, 23);
				btn.setVisible(false);
				contentPane.add(btn);
				botonesLimpiar[j+(i*5)] = btn;
			}
		}
	}
	
	private void creaBotonesCobrar() {
		for(int i=0;i<2;i++) {
			for(int j=0;j<5;j++) {
				JButton btn = new JButton("Cobrar");
				btn.addActionListener(this);
				int posX = 70 + (j*250);
				int posY = 75 + (i*224);
				btn.setBounds(posX, posY, 89, 23);
				btn.setVisible(false);
				contentPane.add(btn);
				botonesCobrar[j+(i*5)] = btn;
			}
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		JButton aux = (JButton) e.getSource();
		if(btnActualizarVentana == aux) {
			actualizar();
			return;
		}
		if(btnAyuda == aux) {
			if(isJefe) {
				GUIAyuda.muestraAyuda(2);
			}else {
				GUIAyuda.muestraAyuda(1);
			}
			
			return;
		}
		for(int i=0;i<numMesas;i++) {
			if(botonesOcupar[i]==aux) {
				GUIVentSentarCliente ventSentarCliente = new GUIVentSentarCliente();
				ventSentarCliente.setModal(true);
				ventSentarCliente.setVisible(true);
				int numComensales;
				String nombreCliente;
				if(ventSentarCliente.isCompletado()) {
					numComensales=ventSentarCliente.getNumComensales();
					nombreCliente=ventSentarCliente.getNombreCliente();
				}else {
					System.out.println("[DEBUG] El sentar en mesa no se complet�");
					return;
				}
				if(listaMesas[i].getNumMaxComensales()<numComensales) {
					JOptionPane.showMessageDialog(null, "No hay suficientes sillas en esta mesa para los clientes indicados\nPor favor elija otra mesa","Error",JOptionPane.ERROR_MESSAGE);
					return;
				}else {
					Cliente cliente = new Cliente(nombreCliente);
					setMesaOcupada(i,numComensales,cliente);
				}
			}
			if(botonesCobrar[i]==aux) {
				Mesa[] auxMesas = Restaurante.getRestaurante().getMesas();
				if(auxMesas[i].getComanda()==null) {
					System.out.println("Esta mesa no ha pedido ningun plato a�n");
					JOptionPane.showMessageDialog(null, "Esta mesa todav�a no ha pedido ningun plato","Aviso",JOptionPane.INFORMATION_MESSAGE);
				}else {
					GUIVentCobrar ventcobrar = new GUIVentCobrar(auxMesas[i].getComanda());
					ventcobrar.setModal(true);
					ventcobrar.setVisible(true);
					if(ventcobrar.isCompletado()) {
						System.out.println("Se pag� correctamente");
						setMesaSucia(i);
					}else {
						System.out.println("No se pag�");
					}
				}
			}
			if(botonesReclamar[i]==aux) {
				GUIVentReclamacion ventReclamacion = new GUIVentReclamacion();
				ventReclamacion.setModal(true);
				ventReclamacion.setVisible(true);
				if(ventReclamacion.isCompletada()) {
					desactivaReclamacion(i);
				}else {
					System.out.println("[DEBUG]La reclamacion no se complet�");
				}
			}
			if(botonesLimpiar[i]==aux) {
				setMesaLimpia(i);
			}
			if(botonesComanda[i]==aux) {
				GUIVentComanda ventComanda = new GUIVentComanda(i);
				ventComanda.setModal(true);
				ventComanda.setVisible(true);
				if(ventComanda.isCompletada()) {
					listaMesas[i].setComanda(ventComanda.getComanda());
					//Ya se ha seteado la comanda en la mesa correspondiente
				}else {
					//Si la comanda no se genera bien no hacemos nada. el camarero la deberia volver a generar
				}
			}
		}
	}
	
	private void setMesaLimpia(int numMesa) {
		icoMesas[numMesa].setIcon(icoMesaLimpia);
		listaMesas[numMesa].marcarLimpia();
		botonesCobrar[numMesa].setVisible(false);
		botonesReclamar[numMesa].setVisible(false);
		botonesLimpiar[numMesa].setVisible(false);
		botonesComanda[numMesa].setVisible(false);
		if(isJefe) {
			botonesOcupar[numMesa].setVisible(true);
		}else {
			botonesOcupar[numMesa].setVisible(false);
		}
	}
	
	private void setMesaSucia(int numMesa) {
		icoMesas[numMesa].setIcon(icoMesaSucia);
		botonesOcupar[numMesa].setVisible(false);
		botonesCobrar[numMesa].setVisible(false);
		botonesReclamar[numMesa].setVisible(false);
		botonesComanda[numMesa].setVisible(false);
		botonesReclamar[numMesa].setEnabled(true);
		listaMesas[numMesa].marcarSucia();
		if(isJefe){
			botonesLimpiar[numMesa].setVisible(false);
		}else {
			botonesLimpiar[numMesa].setVisible(true);
		}
	}
	
	private void setMesaOcupada(int numMesa, int numComensales, Cliente cliente) {
		icoMesas[numMesa].setIcon(icoMesaOcupada);
		botonesOcupar[numMesa].setVisible(false);
		botonesLimpiar[numMesa].setVisible(false);
		listaMesas[numMesa].marcarOcupada(numComensales, cliente);
		if(isJefe){
			botonesCobrar[numMesa].setVisible(true);
			botonesReclamar[numMesa].setVisible(true);
		}else {
			botonesComanda[numMesa].setVisible(true);
		}
	}
	
	private void iniciaMesaLimpia(int numMesa) {
		icoMesas[numMesa].setIcon(icoMesaLimpia);
		botonesCobrar[numMesa].setVisible(false);
		botonesReclamar[numMesa].setVisible(false);
		botonesLimpiar[numMesa].setVisible(false);
		botonesComanda[numMesa].setVisible(false);
		if(isJefe) {
			botonesOcupar[numMesa].setVisible(true);
		}else {
			botonesOcupar[numMesa].setVisible(false);
		}
	}
	
	private void iniciaMesaSucia(int numMesa) {
		icoMesas[numMesa].setIcon(icoMesaSucia);
		botonesOcupar[numMesa].setVisible(false);
		botonesCobrar[numMesa].setVisible(false);
		botonesReclamar[numMesa].setVisible(false);
		botonesComanda[numMesa].setVisible(false);
		botonesReclamar[numMesa].setEnabled(true);
		if(isJefe){
			botonesLimpiar[numMesa].setVisible(false);
		}else {
			botonesLimpiar[numMesa].setVisible(true);
		}
	}
	
	private void iniciaMesaOcupada(int numMesa) {
		icoMesas[numMesa].setIcon(icoMesaOcupada);
		botonesOcupar[numMesa].setVisible(false);
		if(isJefe){
			botonesCobrar[numMesa].setVisible(true);
			botonesReclamar[numMesa].setVisible(true);
			botonesComanda[numMesa].setVisible(false);
		}else {
			botonesComanda[numMesa].setVisible(true);
			botonesCobrar[numMesa].setVisible(false);
			botonesReclamar[numMesa].setVisible(false);
		}
	}
	
	private void desactivaReclamacion(int numMesa) {
		botonesReclamar[numMesa].setEnabled(false);
	}	
}
