package interfazGrafica.sala;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Date;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;

import restaurante.Restaurante;
import restaurante.almacen.Ingrediente;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;
import restaurante.sala.Comanda;

public class GUIVentCobrar extends JDialog implements ActionListener {

	private JPanel contentPane;
	private JTextField textFieldPrecioTot;
	private JTextField textFieldCantEntregada;
	private JTextField textFieldVuelta;
	private JButton btnCalcVuelta = new JButton("Calcular Vuelta");
	private JButton btnMarcarPagado = new JButton("Marcar pagado");
	private JButton btnImprimirComprobante = new JButton("Imprimir comprobante");
	private JPanel panel = new JPanel();
	private Comanda comanda;
	private float precioTot = 0;
	private Date fecha = new Date();
	private String comprobante = Restaurante.getRestaurante().getNombre()+"\n\nFecha:"+fecha+"\n\n";
	private boolean completado = false;
	private JButton btnAyuda;
	


	/**
	 * Create the frame.
	 */
	public GUIVentCobrar(Comanda comanda) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.setTitle("Pago");
		this.comanda=comanda;
		setBounds(100, 100, 580, 691);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 544, 457);
		contentPane.add(scrollPane);
		

		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		int numPlatos = comanda.getComandaPlatos().size();
		panel.setPreferredSize(new Dimension(530, calcularTamaņo(numPlatos)));
		
		JLabel lblPrecioTotal = new JLabel("Precio total:");
		lblPrecioTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecioTotal.setBounds(10, 514, 119, 14);
		contentPane.add(lblPrecioTotal);
		
		textFieldPrecioTot = new JTextField();
		textFieldPrecioTot.setEditable(false);
		textFieldPrecioTot.setBounds(10, 544, 119, 20);
		contentPane.add(textFieldPrecioTot);
		textFieldPrecioTot.setColumns(10);
		
		textFieldCantEntregada = new JTextField();
		textFieldCantEntregada.setBounds(228, 544, 119, 20);
		contentPane.add(textFieldCantEntregada);
		textFieldCantEntregada.setColumns(10);
		

		btnCalcVuelta.setBounds(211, 575, 152, 23);
		btnCalcVuelta.addActionListener(this);
		contentPane.add(btnCalcVuelta);

		btnMarcarPagado.setEnabled(false);
		btnMarcarPagado.addActionListener(this);
		btnMarcarPagado.setBounds(101, 625, 167, 23);
		contentPane.add(btnMarcarPagado);

		btnImprimirComprobante.setEnabled(false);
		btnImprimirComprobante.addActionListener(this);
		btnImprimirComprobante.setBounds(313, 625, 167, 23);
		contentPane.add(btnImprimirComprobante);
		
		JLabel lblCantidadEntregada = new JLabel("Cantidad entregada:");
		lblCantidadEntregada.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidadEntregada.setBounds(228, 514, 119, 14);
		contentPane.add(lblCantidadEntregada);
		
		JLabel lblCantidadVuelta = new JLabel("Cantidad vuelta");
		lblCantidadVuelta.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidadVuelta.setBounds(418, 514, 119, 14);
		contentPane.add(lblCantidadVuelta);
		
		textFieldVuelta = new JTextField();
		textFieldVuelta.setEditable(false);
		textFieldVuelta.setBounds(418, 544, 119, 20);
		contentPane.add(textFieldVuelta);
		textFieldVuelta.setColumns(10);
		
		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidad.setBounds(237, 11, 73, 14);
		contentPane.add(lblCantidad);
		
		JLabel lblPrecio = new JLabel("Precio/ud");
		lblPrecio.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecio.setBounds(313, 11, 86, 14);
		contentPane.add(lblPrecio);
		
		JLabel lblPlato = new JLabel("Plato");
		lblPlato.setBounds(20, 11, 187, 14);
		contentPane.add(lblPlato);
		lblPlato.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblPrecio_1 = new JLabel("Precio");
		lblPrecio_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecio_1.setBounds(404, 11, 86, 14);
		contentPane.add(lblPrecio_1);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.setBounds(465, 2, 89, 23);
		btnAyuda.addActionListener(this);
		contentPane.add(btnAyuda);
		
		muestraComanda();
	}
	
	private void muestraComanda() {
		ArrayList<ComandaPlato> comandaPlatos = comanda.getComandaPlatos();
		for(int i=0; i<comandaPlatos.size();i++) {
			ComandaPlato ComPlato = comandaPlatos.get(i);
			Plato plato = ComPlato.getPlato();
			
			JTextField nombre = new JTextField(plato.getNombreCorto());
			nombre.setEditable(false);
			nombre.setBounds(10,8+(i*50),213,20);
			panel.add(nombre);
			
			JTextField cant = new JTextField(String.valueOf(ComPlato.getCantidad()));
			cant.setEditable(false);
			cant.setBounds(233, 8+(i*50), 58, 20);
			panel.add(cant);
			
			JTextField precioUd = new JTextField(String.valueOf(plato.getPrecio()));
			precioUd.setEditable(false);
			precioUd.setBounds(301, 8+(i*50), 86, 20);
			panel.add(precioUd);
			
			float precioParcial = plato.getPrecio()*ComPlato.getCantidad();
			precioTot += precioParcial;
			JTextField precio = new JTextField(String.valueOf(precioParcial));
			precio.setEditable(false);
			precio.setBounds(397, 8+(i*50), 86, 20);
			panel.add(precio);
			comprobante+="x"+ComPlato.getCantidad()+"\t"+plato.getNombreCorto()+"\t"+plato.getPrecio()+"\t"+precioParcial+"\n";
		}
		comprobante+="\nMuchas gracias por comer en "+Restaurante.getRestaurante().getNombre();
		textFieldPrecioTot.setText(String.valueOf(precioTot));
	}

	
	private int calcularTamaņo(int numElementos) {
		int tam;
		int tamOriginal = 435;
		if(numElementos<10) {
			tam = tamOriginal;
		}else {
			int excedente = numElementos-9;
			tam = tamOriginal + excedente*50;
		}
		return tam;
}

	public void actionPerformed(ActionEvent arg0) {
		JButton aux = (JButton) arg0.getSource();
		float entregado = 0;
		if(btnCalcVuelta==aux) {
			try{
				entregado = Float.parseFloat(textFieldCantEntregada.getText());
				if(entregado<precioTot) {
					JOptionPane.showMessageDialog(null, "La casilla de cantidad entregada debe tener un numero mayor o igual que el precio total","Error",JOptionPane.ERROR_MESSAGE);
					return;
				}
			}catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "La casilla de cantidad entregada debe tener un numero mayor o igual que el precio total","Error",JOptionPane.ERROR_MESSAGE);
				return;
			}catch(NullPointerException e) {
				JOptionPane.showMessageDialog(null, "La casilla de cantidad entregada debe tener un numero mayor o igual que el precio total","Error",JOptionPane.ERROR_MESSAGE);
				return;
			}
			float vuelta = entregado - precioTot;
			textFieldVuelta.setText(String.valueOf(vuelta));
			
			btnImprimirComprobante.setEnabled(true);
			btnMarcarPagado.setEnabled(true);
			btnCalcVuelta.setEnabled(false);
		}
		if(btnMarcarPagado==aux) {
			completado=true;
			dispose();
		}
		if(btnImprimirComprobante==aux) {
			GUIComprobante ventcomprobante = new GUIComprobante(this.comprobante);
			ventcomprobante.setModal(true);
			ventcomprobante.setVisible(true);
		}
		if(btnAyuda==aux) {
			GUIAyuda.muestraAyuda(3);
		}
	}
	
	public boolean isCompletado() {
		return completado;
	}
}
