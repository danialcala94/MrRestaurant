package interfazGrafica.sala;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.Reclamacion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIVentReclamacion extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldDNICliente;
	private JTextField textFieldNumReclamacion;
	
	private boolean completada = false;
	private Reclamacion reclamacion;
	private JButton btnAyuda;
	private JButton cancelButton;


	/**
	 * Create the dialog.
	 */
	public GUIVentReclamacion() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("Reclamacion");
		setBounds(100, 100, 450, 167);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel labelrReclamacion = new JLabel("Numero de reclamaci\u00F3n");
		labelrReclamacion.setBounds(10, 14, 174, 14);
		contentPanel.add(labelrReclamacion);
		
		JLabel labelCliente = new JLabel("DNI del cliente");
		labelCliente.setBounds(10, 62, 174, 14);
		contentPanel.add(labelCliente);
		
		textFieldNumReclamacion = new JTextField();
		textFieldNumReclamacion.setColumns(10);
		textFieldNumReclamacion.setBounds(194, 11, 230, 20);
		contentPanel.add(textFieldNumReclamacion);

		textFieldDNICliente = new JTextField();
		textFieldDNICliente.setColumns(10);
		textFieldDNICliente.setBounds(194, 59, 230, 20);
		contentPanel.add(textFieldDNICliente);
		JButton okButton = new JButton("OK");
		okButton.setBounds(269, 94, 63, 23);
		contentPanel.add(okButton);
		okButton.setActionCommand("OK");
		okButton.addActionListener(this);
		getRootPane().setDefaultButton(okButton);
		
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(this);
				cancelButton.setBounds(342, 94, 82, 23);
				contentPanel.add(cancelButton);
				cancelButton.setActionCommand("Cancel");
				
				btnAyuda = new JButton("Ayuda");
				btnAyuda.addActionListener(this);
				btnAyuda.setBounds(10, 94, 89, 23);
				contentPanel.add(btnAyuda);
				cancelButton.addActionListener(this);
		}	
	
	public boolean isCompletada() {
		return completada;
	}
	public Reclamacion getReclamacion() {
		return reclamacion;
	}


	public void actionPerformed(ActionEvent arg0) {
		JButton aux = (JButton) arg0.getSource();
		if(cancelButton==aux) {
			dispose();
		}
		if(btnAyuda==aux) {
			GUIAyuda.muestraAyuda(6);
		}
		if(aux.getText()=="OK") {
			//Accion Boton Ok
			if(compruebaDatosValidos()) {
				int numReclamacion = Integer.parseInt(textFieldNumReclamacion.getText());
				String DNICLiente = textFieldDNICliente.getText();
				reclamacion = new Reclamacion(numReclamacion,DNICLiente);
				//TODO
				//La reclamacion se guardaria en la bbdd o se enviar�a al restaurante
				completada = true;
				dispose();
			}else{
				JOptionPane.showMessageDialog(null, "El campo del n�mero de la reclamaci�n debe ser un n�mero entero","Error",JOptionPane.ERROR_MESSAGE);
			}
		}
		if(aux.getText()=="Cancel") {
			//Accion Boton Cancel
			completada = false;
			dispose();
		}
	}
	
	private boolean compruebaDatosValidos() {
		boolean valido=true;
		try {
			int aux = Integer.parseInt(textFieldNumReclamacion.getText());
		}catch (NumberFormatException e) {
			valido = false;
		}
		return valido;
	}
	
}

