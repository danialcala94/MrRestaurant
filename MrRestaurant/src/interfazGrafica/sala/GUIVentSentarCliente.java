package interfazGrafica.sala;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.Reclamacion;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.print.CancelablePrintJob;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;

public class GUIVentSentarCliente extends JDialog implements ActionListener{

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldNumComensales;
	private JButton btnOK;
	private JButton btnCancel;
	private boolean completado;
	private int numComensales;
	private String nombreCliente;
	private JButton btnAyuda;
	

	/**
	 * Create the frame.
	 */
	public GUIVentSentarCliente() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.setTitle("Ocupar mesa");
		setBounds(100, 100, 450, 179);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(194, 11, 230, 20);
		contentPane.add(textFieldNombre);
		
		textFieldNumComensales = new JTextField();
		textFieldNumComensales.setColumns(10);
		textFieldNumComensales.setBounds(194, 59, 230, 20);
		contentPane.add(textFieldNumComensales);
		
		JLabel lblNombreCliente = new JLabel("Nombre cliente");
		lblNombreCliente.setBounds(10, 14, 174, 14);
		contentPane.add(lblNombreCliente);
		
		JLabel lblNumeroDeComensales = new JLabel("Numero de comensales");
		lblNumeroDeComensales.setBounds(10, 62, 174, 14);
		contentPane.add(lblNumeroDeComensales);
		
		btnOK = new JButton("OK");
		btnOK.setBounds(269, 106, 65, 23);
		contentPane.add(btnOK);
		btnOK.setActionCommand("OK");
		
		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(344, 106, 80, 23);
		contentPane.add(btnCancel);
		btnCancel.setActionCommand("Cancel");
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.setBounds(10, 106, 89, 23);
		contentPane.add(btnAyuda);
		btnCancel.addActionListener(this);
		btnOK.addActionListener(this);
	}
	
	public int getNumComensales() {
		return numComensales;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public boolean isCompletado() {
		return completado;
	}
	

	private boolean compruebaDatosValidos() {
		boolean valido=true;
		try {
			int aux = Integer.parseInt(textFieldNumComensales.getText());
			if(aux < 1) {
				valido = false;
			}
		}catch (NumberFormatException e) {
			valido = false;
		}
		return valido;
	}


	public void actionPerformed(ActionEvent arg0) {
		JButton aux = (JButton) arg0.getSource();
		if(btnAyuda==aux) {
			GUIAyuda.muestraAyuda(7);
		}
		if(btnOK==aux) {
			//Accion Boton Ok
			if(compruebaDatosValidos()) {
				numComensales = Integer.parseInt(textFieldNumComensales.getText());
				nombreCliente = textFieldNombre.getText();
				completado = true;
				dispose();
			}else{
				JOptionPane.showMessageDialog(null, "El campo del n�mero de clientes debe tener un n�mero entero mayor que 1","Error",JOptionPane.ERROR_MESSAGE);
			}
		}
		if(btnCancel==aux) {
			//Accion Boton Cancel
			completado = false;
			dispose();
		}
	}

}
