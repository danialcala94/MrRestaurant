package interfazGrafica.basededatos;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.almacen.Proveedor;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUIVentanaCrearProveedor extends JDialog {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldTelefono1;
	private JTextField textFieldTelefono2;
	private JTextField textFieldEmail;
	private JTextField textFieldDireccion;
	private JTextField textFieldNIF;
	private JTextField textFieldCiudad;
	private JTextField textFieldPais;
	private JTextField textFieldPagWeb;
	private JButton btnOk;
	private JButton btnAyuda;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public GUIVentanaCrearProveedor() {
		setTitle("Crear proveedor");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 439, 278);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre*");
		lblNombre.setBounds(12, 13, 56, 16);
		contentPane.add(lblNombre);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(80, 10, 116, 22);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono1*");
		lblTelfono.setBounds(12, 51, 79, 16);
		contentPane.add(lblTelfono);
		
		textFieldTelefono1 = new JTextField();
		textFieldTelefono1.setBounds(80, 48, 116, 22);
		contentPane.add(textFieldTelefono1);
		textFieldTelefono1.setColumns(10);
		
		JLabel lblTelfono_1 = new JLabel("Tel\u00E9fono2");
		lblTelfono_1.setBounds(12, 92, 67, 16);
		contentPane.add(lblTelfono_1);
		
		textFieldTelefono2 = new JTextField();
		textFieldTelefono2.setBounds(80, 89, 116, 22);
		contentPane.add(textFieldTelefono2);
		textFieldTelefono2.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email*");
		lblEmail.setBounds(12, 131, 56, 16);
		contentPane.add(lblEmail);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(80, 128, 116, 22);
		contentPane.add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n*");
		lblDireccin.setBounds(227, 13, 84, 16);
		contentPane.add(lblDireccin);
		
		textFieldDireccion = new JTextField();
		textFieldDireccion.setBounds(293, 10, 116, 22);
		contentPane.add(textFieldDireccion);
		textFieldDireccion.setColumns(10);
		
		JLabel lblNif = new JLabel("NIF*");
		lblNif.setBounds(227, 51, 56, 16);
		contentPane.add(lblNif);
		
		textFieldNIF = new JTextField();
		textFieldNIF.setBounds(293, 48, 116, 22);
		contentPane.add(textFieldNIF);
		textFieldNIF.setColumns(10);
		
		JLabel lblCiudad = new JLabel("Ciudad*");
		lblCiudad.setBounds(227, 92, 56, 16);
		contentPane.add(lblCiudad);
		
		textFieldCiudad = new JTextField();
		textFieldCiudad.setBounds(293, 89, 116, 22);
		contentPane.add(textFieldCiudad);
		textFieldCiudad.setColumns(10);
		
		JLabel lblPas = new JLabel("Pa\u00EDs*");
		lblPas.setBounds(227, 131, 56, 16);
		contentPane.add(lblPas);
		
		textFieldPais = new JTextField();
		textFieldPais.setBounds(293, 128, 116, 22);
		contentPane.add(textFieldPais);
		textFieldPais.setColumns(10);
		
		textFieldPagWeb = new JTextField();
		textFieldPagWeb.setBounds(101, 166, 116, 22);
		contentPane.add(textFieldPagWeb);
		textFieldPagWeb.setColumns(10);
		
		JLabel lblPginaWeb = new JLabel("P\u00E1gina web");
		lblPginaWeb.setBounds(12, 169, 85, 16);
		contentPane.add(lblPginaWeb);
		
		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionOK();
			}
		});
		btnOk.setBounds(293, 193, 97, 25);
		contentPane.add(btnOk);
		
		btnAyuda = new JButton("AYUDA");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(23);
			}
		});
		btnAyuda.setBounds(12, 205, 89, 23);
		contentPane.add(btnAyuda);
	}
	
	public void accionOK(){
		if(!textFieldNombre.getText().equals("") && Integer.parseInt(textFieldTelefono1.getText()) > 600000000 && !textFieldEmail.getText().equals("") && !textFieldDireccion.getText().equals("")
			&& !textFieldNIF.getText().equals("") && !textFieldCiudad.getText().equals("") && !textFieldPais.getText().equals("")){
			Proveedor proveedor = new Proveedor();
			proveedor.setNombre(textFieldNombre.getText());
			proveedor.setTelefono(Integer.parseInt(textFieldTelefono1.getText()));
			proveedor.setEmail(textFieldEmail.getText());
			proveedor.setDireccion(textFieldDireccion.getText());
			proveedor.setNIF(textFieldNIF.getText());
			proveedor.setCiudad(textFieldCiudad.getText());
			proveedor.setPais(textFieldPais.getText());
			
			if(!textFieldTelefono2.getText().isEmpty()){
				proveedor.setTelefono2(Integer.parseInt(textFieldTelefono2.getText()));
			}
			
			if(!textFieldPagWeb.getText().isEmpty()){
				proveedor.setPaginaWeb(textFieldPagWeb.getText());
			}
			

			JOptionPane confirmarCreacion = new JOptionPane();
			confirmarCreacion.showMessageDialog(null, "El nuevo proveedor ha sido creado correctamente.");
			
			FachadaPersistencia.crear_Proveedor(proveedor);
			dispose();
		}else{
			JOptionPane errorMensaje = new JOptionPane();
			errorMensaje.showMessageDialog(null, "Debe rellenar los campos requeridos(*)");
		}
	}
}
