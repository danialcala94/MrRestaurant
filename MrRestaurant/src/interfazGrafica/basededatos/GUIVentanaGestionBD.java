package interfazGrafica.basededatos;

import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;
import javax.swing.*;

import interfazGrafica.GUIVentanaLogin;
import interfazGrafica.ayuda.GUIAyuda;
import persistencia.FachadaPersistencia;

public class GUIVentanaGestionBD extends JFrame {
	
	private static final String HELP_ID = "GUIVentanaGestionBD";
	
	public static final int ALERGENOS = 1;
	public static final int EMPLEADOS = 2;
	public static final int PROVEEDORES = 3;
	public static final int CREDENCIALES = 4;
	public static final int INGREDIENTES = 5;
	public static final int CATEGORIASINGREDIENTES = 6;
	public static final int CATEGORIASPLATOS = 7;
	public static final int PAGOSEMPLEADOS = 8;
	
	public GUIVentanaGestionBD thisw = this;
	
	JMenuBar menuBar = new JMenuBar();
	
	public GUIVentanaGestionBD() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		
		this.setJMenuBar(menuBar);
		JMenuItem itemArchivo = new JMenuItem("Archivo");
		menuBar.add(itemArchivo);
		JMenuItem itemAyuda = new JMenuItem("Ayuda");
		menuBar.add(itemAyuda);
		GUIAyuda.getInstancia(itemAyuda, HELP_ID);
		
		getContentPane().setLayout(null);
		this.setTitle("Gestor de Base de datos");
	
		// ALÉRGENOS
		
		JButton btnCrearAlergeno = new JButton("Crear");
		btnCrearAlergeno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIVentanaCrearAlergeno crearAlergeno = new GUIVentanaCrearAlergeno();
				crearAlergeno.setModal(true);
				crearAlergeno.setVisible(true);
			}
		});
		
		JLabel lblAlrgenos = new JLabel("");
		lblAlrgenos.setBackground(new Color(255, 255, 204));
		lblAlrgenos.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlrgenos.setBounds(0, 0, 123, 126);
		getContentPane().add(lblAlrgenos);
		btnCrearAlergeno.setMargin(new Insets(0, 0, 0, 0));
		btnCrearAlergeno.setBounds(10, 37, 100, 19);
		getContentPane().add(btnCrearAlergeno);
		
		JButton btnModificarAlergeno = new JButton("Modificar");
		btnModificarAlergeno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIVentanaElegirObjetoAModificar elegir = new GUIVentanaElegirObjetoAModificar(ALERGENOS);
				elegir.setModal(true);
				elegir.setVisible(true);
			}
		});
		btnModificarAlergeno.setMargin(new Insets(0, 0, 0, 0));
		btnModificarAlergeno.setBounds(10, 67, 100, 19);
		getContentPane().add(btnModificarAlergeno);
		
		JButton btnEliminarAlergeno = new JButton("Eliminar");
		btnEliminarAlergeno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(ALERGENOS);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarAlergeno.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarAlergeno.setBounds(10, 97, 100, 19);
		getContentPane().add(btnEliminarAlergeno);
		
		JLabel lblNewLabel = new JLabel("Al\u00E9rgenos");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 12, 100, 14);
		getContentPane().add(lblNewLabel);
		
		// EMPLEADOS
		
		JButton btnCrearEmpleado = new JButton("Crear");
		btnCrearEmpleado.setEnabled(false);
		btnCrearEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Mostrar pantalla para crear empleado");
			}
		});
		
		btnCrearEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnCrearEmpleado.setBounds(143, 37, 100, 19);
		getContentPane().add(btnCrearEmpleado);
		
		JButton btnModificarEmpleado = new JButton("Modificar");
		btnModificarEmpleado.setEnabled(false);
		btnModificarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para modificar empleado");
			}
		});
		btnModificarEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnModificarEmpleado.setBounds(143, 67, 100, 19);
		getContentPane().add(btnModificarEmpleado);
		
		JButton btnEliminarEmpleado = new JButton("Eliminar");
		btnEliminarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(EMPLEADOS);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarEmpleado.setBounds(143, 97, 100, 19);
		getContentPane().add(btnEliminarEmpleado);
		
		JLabel lblEmpleados = new JLabel("Empleados");
		lblEmpleados.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmpleados.setBounds(143, 12, 100, 14);
		getContentPane().add(lblEmpleados);
		
		// PROVEEDORES
		
		JButton btnCrearProveedor = new JButton("Crear");
		btnCrearProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaCrearProveedor crear = new GUIVentanaCrearProveedor();
				crear.setModal(true);
				crear.setVisible(true);
			}
		});
		btnCrearProveedor.setMargin(new Insets(0, 0, 0, 0));
		btnCrearProveedor.setBounds(276, 37, 100, 19);
		getContentPane().add(btnCrearProveedor);
		
		JButton btnModificarProveedor = new JButton("Modificar");
		btnModificarProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaElegirObjetoAModificar elegir = new GUIVentanaElegirObjetoAModificar(PROVEEDORES);
				elegir.setModal(true);
				elegir.setVisible(true);
			}
		});
		btnModificarProveedor.setMargin(new Insets(0, 0, 0, 0));
		btnModificarProveedor.setBounds(276, 67, 100, 19);
		getContentPane().add(btnModificarProveedor);
		
		JButton btnEliminarProveedor = new JButton("Eliminar");
		btnEliminarProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(PROVEEDORES);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarProveedor.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarProveedor.setBounds(276, 97, 100, 19);
		getContentPane().add(btnEliminarProveedor);
		
		JLabel lblProveedores_1 = new JLabel("Proveedores");
		lblProveedores_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblProveedores_1.setBounds(276, 12, 100, 14);
		getContentPane().add(lblProveedores_1);
		
		// INGREDIENTES
		
		JButton btnCrearIngrediente = new JButton("Crear");
		btnCrearIngrediente.setEnabled(false);
		btnCrearIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para crear ingrediente");
			}
		});
		btnCrearIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnCrearIngrediente.setBounds(10, 174, 100, 19);
		getContentPane().add(btnCrearIngrediente);
		
		JButton btnModificarIngrediente = new JButton("Modificar");
		btnModificarIngrediente.setEnabled(false);
		btnModificarIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para modificar ingrediente");
			}
		});
		btnModificarIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnModificarIngrediente.setBounds(10, 204, 100, 19);
		getContentPane().add(btnModificarIngrediente);
		
		JButton btnEliminarIngrediente = new JButton("Eliminar");
		btnEliminarIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(INGREDIENTES);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarIngrediente.setBounds(10, 234, 100, 19);
		getContentPane().add(btnEliminarIngrediente);
		
		JLabel lblIngredientes = new JLabel("Ingredientes");
		lblIngredientes.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngredientes.setBounds(10, 149, 100, 14);
		getContentPane().add(lblIngredientes);
		
		// CATEGORIASINGREDIENTES
		
		JButton btnCrearCategoriaIngrediente = new JButton("Crear");
		btnCrearCategoriaIngrediente.setEnabled(false);
		btnCrearCategoriaIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para crear categoriaIngrediente");
			}
		});
		btnCrearCategoriaIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnCrearCategoriaIngrediente.setBounds(143, 174, 100, 19);
		getContentPane().add(btnCrearCategoriaIngrediente);
		
		JButton btnModificarCategoriaIngrediente = new JButton("Modificar");
		btnModificarCategoriaIngrediente.setEnabled(false);
		btnModificarCategoriaIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para modificar categoriaIngrediente");
			}
		});
		btnModificarCategoriaIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnModificarCategoriaIngrediente.setBounds(143, 204, 100, 19);
		getContentPane().add(btnModificarCategoriaIngrediente);
		
		JButton btnEliminarCategoriaIngrediente = new JButton("Eliminar");
		btnEliminarCategoriaIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(CATEGORIASINGREDIENTES);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarCategoriaIngrediente.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarCategoriaIngrediente.setBounds(143, 234, 100, 19);
		getContentPane().add(btnEliminarCategoriaIngrediente);
		
		JLabel lblCategoriasIngredientes = new JLabel("Categ. Ingredientes");
		lblCategoriasIngredientes.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategoriasIngredientes.setBounds(143, 149, 100, 14);
		getContentPane().add(lblCategoriasIngredientes);
		
		// CATEGORIASPLATOS
		
		JButton btnCrearCategoriaPlato = new JButton("Crear");
		btnCrearCategoriaPlato.setEnabled(false);
		btnCrearCategoriaPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para crear categoriaPlato");
			}
		});
		btnCrearCategoriaPlato.setMargin(new Insets(0, 0, 0, 0));
		btnCrearCategoriaPlato.setBounds(276, 174, 100, 19);
		getContentPane().add(btnCrearCategoriaPlato);
		
		JButton btnModificarCategoriaPlato = new JButton("Modificar");
		btnModificarCategoriaPlato.setEnabled(false);
		btnModificarCategoriaPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para modificar categoriaPlato");
			}
		});
		btnModificarCategoriaPlato.setMargin(new Insets(0, 0, 0, 0));
		btnModificarCategoriaPlato.setBounds(276, 204, 100, 19);
		getContentPane().add(btnModificarCategoriaPlato);
		
		JButton btnEliminarCategoriaPlato = new JButton("Eliminar");
		btnEliminarCategoriaPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(CATEGORIASPLATOS);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarCategoriaPlato.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarCategoriaPlato.setBounds(276, 234, 100, 19);
		getContentPane().add(btnEliminarCategoriaPlato);
		
		JLabel lblCategPlatos = new JLabel("Categ. Platos");
		lblCategPlatos.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategPlatos.setBounds(276, 149, 100, 14);
		getContentPane().add(lblCategPlatos);
		
		// CREDENCIALES
		
		JButton btnCrearCredencial = new JButton("Crear");
		btnCrearCredencial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaCrearCredencial crear = new GUIVentanaCrearCredencial();
				crear.setModal(true);
				crear.setVisible(true);
			}
		});
		btnCrearCredencial.setMargin(new Insets(0, 0, 0, 0));
		btnCrearCredencial.setBounds(409, 37, 100, 19);
		getContentPane().add(btnCrearCredencial);
		
		JButton btnModificarCredencial = new JButton("Modificar");
		btnModificarCredencial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaElegirObjetoAModificar elegir = new GUIVentanaElegirObjetoAModificar(CREDENCIALES);
				elegir.setModal(true);
				elegir.setVisible(true);
			}
		});
		btnModificarCredencial.setMargin(new Insets(0, 0, 0, 0));
		btnModificarCredencial.setBounds(409, 67, 100, 19);
		getContentPane().add(btnModificarCredencial);
		
		JButton btnEliminarCredencial = new JButton("Eliminar");
		btnEliminarCredencial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(CREDENCIALES);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarCredencial.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarCredencial.setBounds(409, 97, 100, 19);
		getContentPane().add(btnEliminarCredencial);
		
		JLabel lblCredenciales = new JLabel("Credenciales");
		lblCredenciales.setHorizontalAlignment(SwingConstants.CENTER);
		lblCredenciales.setBounds(409, 12, 100, 14);
		getContentPane().add(lblCredenciales);
		
		// PAGOSEMPLEADOS
		
		JButton btnCrearPagoEmpleado = new JButton("Crear");
		btnCrearPagoEmpleado.setEnabled(false);
		btnCrearPagoEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para crear pagoEmpleado");
			}
		});
		btnCrearPagoEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnCrearPagoEmpleado.setBounds(409, 174, 100, 19);
		getContentPane().add(btnCrearPagoEmpleado);
		
		JButton btnModificarPagoEmpleado = new JButton("Modificar");
		btnModificarPagoEmpleado.setEnabled(false);
		btnModificarPagoEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mostrar pantalla para modificar pagoEmpleado");
			}
		});
		btnModificarPagoEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnModificarPagoEmpleado.setBounds(409, 204, 100, 19);
		getContentPane().add(btnModificarPagoEmpleado);
		
		JButton btnEliminarPagoEmpleado = new JButton("Eliminar");
		btnEliminarPagoEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIVentanaEliminarObjeto eliminar = new GUIVentanaEliminarObjeto(PAGOSEMPLEADOS);
				eliminar.setModal(true);
				eliminar.setVisible(true);
			}
		});
		btnEliminarPagoEmpleado.setMargin(new Insets(0, 0, 0, 0));
		btnEliminarPagoEmpleado.setBounds(409, 234, 100, 19);
		getContentPane().add(btnEliminarPagoEmpleado);
		
		JLabel lblPagosAEmpleados = new JLabel("Pagos a Empleados");
		lblPagosAEmpleados.setHorizontalAlignment(SwingConstants.CENTER);
		lblPagosAEmpleados.setBounds(409, 149, 100, 14);
		getContentPane().add(lblPagosAEmpleados);
		
		JButton btnCrearCopiaSeguridad = new JButton("Crear copia de seguridad");
		btnCrearCopiaSeguridad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser file = new JFileChooser();
				if (file.showSaveDialog(null) == 0) {
					String guarda = file.getSelectedFile().getAbsolutePath().replace("\\", "/");
					FachadaPersistencia.crearCopiaSeguridad(guarda);
				}
			}
		});
		btnCrearCopiaSeguridad.setBounds(10, 266, 233, 25);
		getContentPane().add(btnCrearCopiaSeguridad);
		
		JButton btnAyuda = new JButton("AYUDA");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//GUIAyuda.muestraAyuda(22);
			}
		});
		btnAyuda.setBounds(287, 267, 89, 23);
		getContentPane().add(btnAyuda);
	}
}
