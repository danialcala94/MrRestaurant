package interfazGrafica.basededatos;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.almacen.Alergeno;

public class GUIVentanaModificarAlergeno extends JDialog {
	private JTextField txt_nombre;
	public GUIVentanaModificarAlergeno(Alergeno alergenoObtenido) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setTitle("Modificar AL\u00C9RGENO");
		getContentPane().setLayout(null);
		setSize(400, 400);
		
		JLabel lblNombre = new JLabel("Nombre*");
		lblNombre.setBounds(10, 11, 81, 14);
		getContentPane().add(lblNombre);
		
		txt_nombre = new JTextField();
		txt_nombre.setToolTipText("Nombre del al\u00E9rgeno");
		txt_nombre.setBounds(10, 27, 146, 20);
		getContentPane().add(txt_nombre);
		txt_nombre.setColumns(10);
		
		JLabel lblDescripcion = new JLabel("Descripci\u00F3n");
		lblDescripcion.setBounds(10, 58, 81, 14);
		getContentPane().add(lblDescripcion);
		
		JTextArea textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setToolTipText("Descripci\u00F3n del al\u00E9rgeno");
		textArea.setBounds(10, 74, 360, 176);
		textArea.setLineWrap(true);
		getContentPane().add(textArea);
		
		// Setear datos recibidos
		txt_nombre.setText(alergenoObtenido.getNombre());
		textArea.setText(alergenoObtenido.getDescripcion());
		
		JButton btnCrear = new JButton("Modificar");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (esValido()) {
					Alergeno alergeno = new Alergeno(alergenoObtenido.getIDAlergeno());
					alergeno.setNombre(txt_nombre.getText());
					if (!textArea.getText().isEmpty() && textArea.getText().length() < 400)
						alergeno.setDescripcion(textArea.getText());
					if (FachadaPersistencia.modificar_Alergeno(alergeno) != null)
						JOptionPane.showMessageDialog(null,
							    "Se ha modificado el al�rgeno " + alergeno.getNombre(),
							    "Creado satisfactoriamente",
							    JOptionPane.INFORMATION_MESSAGE);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null,
						    "Debe rellenar los campos requeridos (*)",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnCrear.setBounds(142, 279, 89, 23);
		getContentPane().add(btnCrear);
		
		JButton btnAyuda = new JButton("AYUDA");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(24);
			}
		});
		btnAyuda.setBounds(281, 26, 89, 23);
		getContentPane().add(btnAyuda);
	}
	
	private boolean esValido() {
		if (!txt_nombre.getText().isEmpty() && txt_nombre.getText().length() <= 50)
			return true;
		return false;
	}
}
