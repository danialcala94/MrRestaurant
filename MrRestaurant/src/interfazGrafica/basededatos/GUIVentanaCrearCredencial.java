package interfazGrafica.basededatos;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.Restaurante;
import restaurante.empleados.CategoriaEmpleado;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;
import restaurante.empleados.NivelCredencial;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUIVentanaCrearCredencial extends JDialog {

	private JPanel contentPane;
	private JTextField textFieldUsuario;
	private JTextField textFieldContrase�a;
	JComboBox comboBoxNivelCredencial = new JComboBox();
	JComboBox comboBoxEmpleado = new JComboBox();
	ArrayList<NivelCredencial> listaNivelesCredneciales = FachadaPersistencia.obtenerTodos_NivelesCredenciales();
	ArrayList<Empleado> listaCategoriasEmpleados = FachadaPersistencia.obtenerTodos_Empleados();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIVentanaCrearCredencial frame = new GUIVentanaCrearCredencial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIVentanaCrearCredencial() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		listaNivelesCredneciales = FachadaPersistencia.obtenerTodos_NivelesCredenciales();
		listaCategoriasEmpleados = FachadaPersistencia.obtenerTodos_Empleados();
		setTitle("Crear credencial");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 226);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsurio = new JLabel("Usuario*");
		lblUsurio.setBounds(12, 13, 56, 16);
		contentPane.add(lblUsurio);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setBounds(80, 10, 116, 22);
		contentPane.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a*");
		lblContrasea.setBounds(222, 13, 83, 16);
		contentPane.add(lblContrasea);
		
		textFieldContrase�a = new JTextField();
		textFieldContrase�a.setBounds(304, 10, 116, 22);
		contentPane.add(textFieldContrase�a);
		textFieldContrase�a.setColumns(10);
		
		JLabel lblNivelCredencial = new JLabel("Nivel credencial*");
		lblNivelCredencial.setBounds(69, 65, 108, 16);
		contentPane.add(lblNivelCredencial);
		
	
		comboBoxNivelCredencial.setBounds(52, 84, 125, 22);
		contentPane.add(comboBoxNivelCredencial);
		for(int i=0;i<listaNivelesCredneciales.size();i++){
			comboBoxNivelCredencial.addItem(listaNivelesCredneciales.get(i).getNombre());
			System.out.println("A�adiendo... " + listaNivelesCredneciales.get(i).getIDNivelCredencial());
		}
		
		JLabel lblEmpleado = new JLabel("Empleado");
		lblEmpleado.setBounds(292, 65, 56, 16);
		contentPane.add(lblEmpleado);
		
		
		comboBoxEmpleado.setBounds(257, 84, 125, 22);
		contentPane.add(comboBoxEmpleado);
		for(int j=0;j<listaCategoriasEmpleados.size();j++){
			comboBoxEmpleado.addItem(listaCategoriasEmpleados.get(j).getNombre());
		}
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionOK();
			}
		});
		btnOk.setBounds(301, 141, 97, 25);
		contentPane.add(btnOk);
		
		JButton btnAyuda = new JButton("AYUDA");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIAyuda.muestraAyuda(23);
			}
		});
		btnAyuda.setBounds(69, 142, 89, 23);
		contentPane.add(btnAyuda);
	}
	
	public void accionOK(){
		if(!textFieldUsuario.getText().isEmpty() && !textFieldContrase�a.getText().isEmpty()){
			int indiceNivel = -3;// = comboBoxNivelCredencial.getSelectedIndex();
			for (int i = 0; i < listaNivelesCredneciales.size(); i++) {
				System.out.println(listaNivelesCredneciales.get(i).getNombre() + " - " + comboBoxNivelCredencial.getSelectedItem().toString());
				if (listaNivelesCredneciales.get(i).getNombre().equals(comboBoxNivelCredencial.getSelectedItem().toString())) {
					indiceNivel = i;
					break;
				}
			}
			int indiceEmpleado = -3;// = comboBoxEmpleado.getSelectedIndex();
			for (int i = 0; i < listaCategoriasEmpleados.size(); i++) {
				if (listaCategoriasEmpleados.get(i).getNombre().equals(comboBoxEmpleado.getSelectedItem().toString())) {
					indiceEmpleado = i;
					break;
				}
			}
			Credencial credencial = new Credencial();
			credencial.setContrase�a(textFieldContrase�a.getText());
			credencial.setUsuario(textFieldUsuario.getText());
			credencial.setNivel(listaNivelesCredneciales.get(indiceNivel));
			credencial.setEmpleado(listaCategoriasEmpleados.get(indiceEmpleado));
			
			JOptionPane confirmarCreacion = new JOptionPane();
			confirmarCreacion.showMessageDialog(null, "La nueva credencial ha sido creada correctamente.");
			
			FachadaPersistencia.crear_Credencial(credencial);
			dispose();
		}else{
			JOptionPane errorMensaje = new JOptionPane();
			errorMensaje.showMessageDialog(null, "Debe rellenar los campos requeridos (*)");
		}
		
	}
}
