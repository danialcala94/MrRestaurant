package interfazGrafica.basededatos;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.almacen.Alergeno;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.Proveedor;
import restaurante.cocina.CategoriaPlato;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;
import restaurante.empleados.PagoEmpleado;

public class GUIVentanaEliminarObjeto extends JDialog {
	public static final int ALERGENOS = 1;
	public static final int EMPLEADOS = 2;
	public static final int PROVEEDORES = 3;
	public static final int CREDENCIALES = 4;
	public static final int INGREDIENTES = 5;
	public static final int CATEGORIASINGREDIENTES = 6;
	public static final int CATEGORIASPLATOS = 7;
	public static final int PAGOSEMPLEADOS = 8;
	
	ArrayList<Alergeno> alergenos = new ArrayList<Alergeno>();
	ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
	ArrayList<Credencial> credenciales = new ArrayList<Credencial>();
	ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
	ArrayList<CategoriaIngrediente> categoriasingredientes = new ArrayList<CategoriaIngrediente>();
	ArrayList<CategoriaPlato> categoriasplatos = new ArrayList<CategoriaPlato>();
	ArrayList<PagoEmpleado> pagosempleados = new ArrayList<PagoEmpleado>();
	
	JComboBox comboBox = new JComboBox();
	JLabel lblNombreObjeto = new JLabel();
	
	public GUIVentanaEliminarObjeto(int selObjeto) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setSize(300, 300);
		recargarDatos(selObjeto);
		
		ArrayList<?> objetos;
		
		
		setTitle("Elegir objeto a eliminar");
		getContentPane().setLayout(null);
		
		//lblNombreObjeto = new JLabel("New label");
		lblNombreObjeto.setBounds(12, 13, 168, 16);
		getContentPane().add(lblNombreObjeto);
		
		//comboBox = new JComboBox();
		comboBox.setBounds(12, 31, 168, 22);
		getContentPane().add(comboBox);
		
		JButton btnElegir = new JButton("Eliminar");
		btnElegir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Comprobar qu� ID hab�a elegida y qu� valor ha cogido.
				if (selObjeto == ALERGENOS) {
					for (int i = 0; i < alergenos.size(); i++) {
						if (alergenos.get(i).getNombre().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + alergenos.get(i).getNombre(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_Alergeno(alergenos.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == EMPLEADOS) {
					for (int i = 0; i < empleados.size(); i++) {
						if (empleados.get(i).getNombre().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + empleados.get(i).getNombre(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_Empleado(empleados.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == PROVEEDORES) {
					for (int i = 0; i < proveedores.size(); i++) {
						if (proveedores.get(i).getNombre().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + proveedores.get(i).getNombre(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_Proveedor(proveedores.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == CREDENCIALES) {
					for (int i = 0; i < credenciales.size(); i++) {
						if (credenciales.get(i).getUsuario().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + credenciales.get(i).getUsuario(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_Credencial(credenciales.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == INGREDIENTES) {
					for (int i = 0; i < ingredientes.size(); i++) {
						if (ingredientes.get(i).getNombreLargo().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + ingredientes.get(i).getNombreLargo(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_Ingrediente(ingredientes.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == CATEGORIASINGREDIENTES) {
					for (int i = 0; i < categoriasingredientes.size(); i++) {
						if (categoriasingredientes.get(i).getNombre().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + categoriasingredientes.get(i).getNombre(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_CategoriaIngrediente(categoriasingredientes.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == CATEGORIASPLATOS) {
					for (int i = 0; i < categoriasplatos.size(); i++) {
						if (categoriasplatos.get(i).getNombre().equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + categoriasplatos.get(i).getNombre(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_CategoriaPlato(categoriasplatos.get(i));
								dispose();
							}
						}
					}
				} else if (selObjeto == PAGOSEMPLEADOS) {
					for (int i = 0; i < pagosempleados.size(); i++) {
						if ((pagosempleados.get(i).getEmpleado().getDni() + " - " + pagosempleados.get(i).getFecha() + " - " + pagosempleados.get(i).getCantidad()).equals(comboBox.getSelectedItem().toString())) {
							if (JOptionPane.showConfirmDialog(null, "Eliminar " + (pagosempleados.get(i).getEmpleado().getDni() + " - " + pagosempleados.get(i).getFecha() + " - " + pagosempleados.get(i).getCantidad()), "Eliminar", JOptionPane.YES_OPTION) == 0) {
								FachadaPersistencia.eliminar_PagoEmpleado(pagosempleados.get(i));
								dispose();
							}
						}
					}
				}
				recargarDatos(selObjeto);
			}
		});
		btnElegir.setBounds(44, 66, 97, 25);
		getContentPane().add(btnElegir);
		
		JButton btnAyuda = new JButton("AYUDA");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(26);
			}
		});
		btnAyuda.setBounds(166, 67, 89, 23);
		getContentPane().add(btnAyuda);
		
		
		/*if (selObjeto == ALERGENOS) {
			setTitle("Eliminar al�rgeno");
			lblNombreObjeto.setText("Al�rgenos");
			alergenos = FachadaPersistencia.obtenerTodos_Alergenos();
			// A�adir al�rgenos al combobox
			for (int i = 0; i < alergenos.size(); i++) {
				comboBox.addItem(alergenos.get(i).getNombre());
			}
		} else if (selObjeto == EMPLEADOS) {
			setTitle("Eliminar empleado");
			lblNombreObjeto.setText("Empleados");
			empleados = FachadaPersistencia.obtenerTodos_Empleados();
			for (int i = 0; i < empleados.size(); i++) {
				comboBox.addItem(empleados.get(i).getNombre());
			}
		} else if (selObjeto == PROVEEDORES) {
			setTitle("Eliminar proveedor");
			lblNombreObjeto.setText("Proveedores");
			proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
			for (int i = 0; i < proveedores.size(); i++) {
				comboBox.addItem(proveedores.get(i).getNombre());
			}
		} else if (selObjeto == CREDENCIALES) {
			setTitle("Eliminar credencial");
			lblNombreObjeto.setText("Credenciales");
			 credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
			 for (int i = 0; i < credenciales.size(); i++) {
					comboBox.addItem(credenciales.get(i).getUsuario());
				}
		} else if (selObjeto == INGREDIENTES) {
			setTitle("Eliminar ingrediente");
			lblNombreObjeto.setText("Ingredientes");
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			for (int i = 0; i < ingredientes.size(); i++) {
				comboBox.addItem(ingredientes.get(i).getNombreLargo());
			}
		} else if (selObjeto == CATEGORIASINGREDIENTES) {
			setTitle("Eliminar categor�a de ingrediente");
			lblNombreObjeto.setText("Categ. Ingredientes");
			categoriasingredientes = FachadaPersistencia.obtenerTodos_CategoriasIngredientes();
			for (int i = 0; i < categoriasingredientes.size(); i++) {
				comboBox.addItem(categoriasingredientes.get(i).getNombre());
			}
		} else if (selObjeto == CATEGORIASPLATOS) {
			setTitle("Eliminar categor�a de platos");
			lblNombreObjeto.setText("Categ. Platos");
			categoriasplatos = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
			for (int i = 0; i < categoriasplatos.size(); i++) {
				comboBox.addItem(categoriasplatos.get(i).getNombre());
			}
		} else if (selObjeto == PAGOSEMPLEADOS) {
			setTitle("Eliminar pago a empleado");
			lblNombreObjeto.setText("Pagos a empleados");
			pagosempleados = FachadaPersistencia.obtenerTodos_PagosEmpleados();
			for (int i = 0; i < pagosempleados.size(); i++) {
				comboBox.addItem(pagosempleados.get(i).getEmpleado().getDni() + " - " + pagosempleados.get(i).getFecha() + " - " + pagosempleados.get(i).getCantidad());
			}
		} else {
			JOptionPane.showMessageDialog(null,
				    "Objeto no v�lido",
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			dispose();
		}*/
		
		
	}
	private void recargarDatos(int id) {
		comboBox = new JComboBox();
		if (id == ALERGENOS) {
			setTitle("Eliminar al�rgenos");
			lblNombreObjeto.setText("Al�rgenos");
			alergenos = FachadaPersistencia.obtenerTodos_Alergenos();
			// A�adir al�rgenos al combobox
			for (int i = 0; i < alergenos.size(); i++) {
				comboBox.addItem(alergenos.get(i).getNombre());
			}
		} else if (id == EMPLEADOS) {
			setTitle("Eliminar empleado");
			lblNombreObjeto.setText("Empleados");
			empleados = FachadaPersistencia.obtenerTodos_Empleados();
			for (int i = 0; i < empleados.size(); i++) {
				comboBox.addItem(empleados.get(i).getNombre());
			}
		} else if (id == PROVEEDORES) {
			setTitle("Eliminar proveedor");
			lblNombreObjeto.setText("Proveedores");
			proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
			for (int i = 0; i < proveedores.size(); i++) {
				comboBox.addItem(proveedores.get(i).getNombre());
			}
		} else if (id == CREDENCIALES) {
			setTitle("Eliminar credencial");
			lblNombreObjeto.setText("Credenciales");
			 credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
			 for (int i = 0; i < credenciales.size(); i++) {
					comboBox.addItem(credenciales.get(i).getUsuario());
				}
		} else if (id == INGREDIENTES) {
			setTitle("Eliminar ingrediente");
			lblNombreObjeto.setText("Ingredientes");
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			for (int i = 0; i < ingredientes.size(); i++) {
				comboBox.addItem(ingredientes.get(i).getNombreLargo());
			}
		} else if (id == CATEGORIASINGREDIENTES) {
			setTitle("Eliminar categor�a de ingrediente");
			lblNombreObjeto.setText("Categ. Ingredientes");
			categoriasingredientes = FachadaPersistencia.obtenerTodos_CategoriasIngredientes();
			for (int i = 0; i < categoriasingredientes.size(); i++) {
				comboBox.addItem(categoriasingredientes.get(i).getNombre());
			}
		} else if (id == CATEGORIASPLATOS) {
			setTitle("Eliminar categor�a de platos");
			lblNombreObjeto.setText("Categ. Platos");
			categoriasplatos = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
			for (int i = 0; i < categoriasplatos.size(); i++) {
				comboBox.addItem(categoriasplatos.get(i).getNombre());
			}
		} else if (id == PAGOSEMPLEADOS) {
			setTitle("Eliminar pago a empleado");
			lblNombreObjeto.setText("Pagos a empleados");
			pagosempleados = FachadaPersistencia.obtenerTodos_PagosEmpleados();
			for (int i = 0; i < pagosempleados.size(); i++) {
				comboBox.addItem(pagosempleados.get(i).getEmpleado().getDni() + " - " + pagosempleados.get(i).getFecha() + " - " + pagosempleados.get(i).getCantidad());
			}
		} else {
			JOptionPane.showMessageDialog(null,
				    "Objeto no v�lido",
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			dispose();
		}
	}
}
