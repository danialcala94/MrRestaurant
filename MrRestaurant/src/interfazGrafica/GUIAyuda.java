package interfazGrafica;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class GUIAyuda extends JDialog implements ActionListener {


	private static GUIAyuda instanciaVentanaAyuda;
	private final JPanel contentPanel = new JPanel();
	private ArrayList<String> opciones = new ArrayList<String>();
	private ArrayList<String> ayuda = new ArrayList<String>();
	private static JComboBox comboBox;
	private JTextArea textArea;
	private JButton btnMostar;
	private JButton okButton;


	/**
	 * Create the dialog.
	 */
	private GUIAyuda() {
		this.setModal(true);
		this.setResizable(false);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		a�adeAyuda();
		a�adeOpciones();
		
		setBounds(100, 100, 450, 446);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 414, 327);
		contentPanel.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		
		JLabel lblOpcion = new JLabel("Opci�n");
		lblOpcion.setBounds(10, 11, 46, 14);
		contentPanel.add(lblOpcion);
		
		btnMostar = new JButton("Mostar");
		btnMostar.setBounds(314, 7, 89, 23);
		btnMostar.addActionListener(this);
		contentPanel.add(btnMostar);
		
		String[] listaOpciones = opciones.toArray(new String[opciones.size()]);
		comboBox = new JComboBox(listaOpciones);
		comboBox.setBounds(66, 8, 238, 20);
		contentPanel.add(comboBox);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
	
	private void a�adeOpciones() {
		opciones.add("Ventana login");					//0
		opciones.add("Ventana camarero");				//1
		opciones.add("Ventana jefe sala");				//2
		opciones.add("Ventana cobrar");					//3
		opciones.add("Ventana comprobante");			//4
		opciones.add("Ventana comanda");				//5
		opciones.add("Ventana reclamacion");			//6
		opciones.add("Ventana sentar cliente");			//7
		opciones.add("Ventana gerente");				//8
		opciones.add("Ventana consultar nominas");		//9
		opciones.add("Ventana a�adir plato");			//10
		opciones.add("Ventana a�adir ingrediente");		//11
		opciones.add("Ventana aprobar pedido");			//12
		opciones.add("Ventana eliminar plato");			//13
		opciones.add("Ventana seleccion plato");		//14
		opciones.add("Ventana modificar plato");		//15
		opciones.add("Ventana modificar ingredientes");	//16
		opciones.add("Ventana cocinero");				//17
		opciones.add("Ventana jefe de cocina");			//18
		opciones.add("Ventana inventario");				//19
		opciones.add("Ventana receta");					//20
		opciones.add("Ventana pedidos");				//21
		opciones.add("Ventana Gesti�n BD");				//22
		opciones.add("Ventana A�adir Objeto");			//23
		opciones.add("Ventana Modificar Objeto");		//24
		opciones.add("Ventana elegir objeto a modificar");//25
		opciones.add("Ventana elegir objeto a eliminar");//26
	}
	
	private void a�adeAyuda() {
		ayuda.add("Esta es la ventana de Login, desde aqu� pueden acceder al sistema todos los usuarios registrados en la aplicaci�n.\nEn caso de que se introduzcan unos datos de acceso erroneos el sistema lo mostrar� con un mensaje en rojo.\nUna vez se introduzcan los datos de acceso correctos de un usuario, el sistema mostrar� la ventana de dicho usuario y se cerrar� la ventana de Login.\nEn la versi�n de DEBUG la ventana de Login no se cerrar� para permitir la ejecuci�n de varios usuarios en una misma m�quina");
		ayuda.add("Esta es la ventana del camarero.\nEn ella se muestran tantas mesas como tenga el restaurante con la informaci�n sobre que mesa es y el numero de sillas que posee.\nCada mesa podr� tomar un color dependiendo de su situaci�n actual.\nSeg�n si una mesa est� ocupada o sucia, el camarero podr� tomar una comanda de esa mesa o limpiarla para que pueda ser ocupada de nuevo.\nMientras la mesa est� limpia el camarero no podr� realizar ninguna acci�n.\nCada cierto tiempo el camarero deber� utilizar el boton de actualizar para comprobar si ha habido algun cambio en las mesas y por tanto la ventana debe actualizarse");
		ayuda.add("Esta es la ventana del jefe de sala\nEn ella se muestran tantas mesas como tenga el restauntate con la informacion sobre que mesa es y el numero de sillas que posee.\nCada mesa podr� tomar un color dependiendo de su situacion actual.\nSeg�n si una meas est� libre o ocupada el jefe de sala podr� ocupar esta con un cliente, gestionar una reclamaci�n de un cliente o cobrarle al cliente la consumici�n realizada.\nMientras la mesa est� sucia el jefe de sala no podr� realizar ninguna acci�n.\nCada cierto tiempo el jefe de sala deber� utilizar el boton de actualizar para comprobar si ha habido algun cambio en las mesas y por tanto la ventana debe actualizarse");
		ayuda.add("Esta es la ventana que se muestra cuando un jefe de sala va a cobrar la consumici�n realizada por un cliente. En ella el jefe de sala podr� ver todos los platos que ha pedido el cliente, con el precio unitario de estos y el precio total.\nA continuaci�n el jefe de sala podr� introducir la cantidad de dinero dada por el cliente y el sistema calcular� la vuelta que debe darle. Adem�s podr� marcar la venta como completada (y la mesa pasar� a ser una mesa sucia), o mostrar un comprobante del pago (Igual al ticket que se imprimir�a)");
		ayuda.add("Este es el comprobante generado por el pago del restaurante.\nSeria el fichero que se enviar�a a la impresora para darle una copia f�sica al cliente");
		ayuda.add("Esta ventana nos permite introducir en el sistema los platos pedidos por una mesa.\nLa ventana permite seleccionar los platos pedidos en un comboBox y a�adirlos a la lista.\nUna vez a�adidos podremos modificar la cantidad de platos de ese tipo que se desean pedir, asi como a�adir un peque�o comentario que se mostrar� al cocinero que vaya a preparar ese plato.");
		ayuda.add("Esta ventana se mostrar� en el caso de que un cliente desee poner una reclamaci�n en el restaurante.\nLas reclamaciones son documentos por escrito que debe firmar el cliente y el restaurante, sin embargo desde esta ventana se a�ade la opci�n de mantener un registro digital de estas indicando simplemente el n�mero �nico de reclamaci�n (Viene impreso en la reclamaci�n) y el dni del cliente que desea poner la reclamaci�n");
		ayuda.add("Esta ventana permite indicar el nombre del cliente principal de la mesa seleccionada (El que va a pagar por ejemplo), asi como el numero de comensales para esa mesa.\nSi el numero de comensales introducidos es superior al numero de sillas de esa mesa el sistema mostrar� un error indicando este hecho y cancelar� la asignaci�n de la mesa");
		ayuda.add("Esta es la ventana del gerente.\nDesde esta ventana podr� controlar todos los aspectos administrativos o de base de datos del restaurante como pueden ser la aprobaci�n de un pedido pendiente, la consulta de las n�minas de los empleados o las operaciones relacionadas con los platos (a�adir, modificar o eliminar)");
		ayuda.add("Desde esta ventana se pueden ver las nominas de todos los empleados del restaurante. En la ventana se muestran los nombres de los empleados y su suelo mensual");
		ayuda.add("Desde esta ventana se podr� a�adir un plato al restaurante indicando su precio, nombre y categoria.\nAdem�s, de forma opcional se podr� indicar la receta necesaria para su preparacion y un peque�o comentario sobre el plato. Para a�adir el plato es necesario que se a�ada al menos un ingrediente, tras a�adir estos, el sistema mostrar� el precio minimo recomendado para ese plato y permitir� a�adirlo.\nSi al intentar a�adir el plato hemos omitido alguno de los datos imprescindibles, el sistema mostrar� un error y nos permitir� arreglarlo");
		ayuda.add("Esta ventana se obtiene desde la ventana de a�adir un nuevo plato al restaurante.\nEn ella podemos indicar los ingredientes que deseamos que tenga ese plato y la cantidad de estos.\nTras a�adir todos los ingredientes que queramos podremos pulsar el boton OK para tramitir estos datos a la ventana anterior");
		ayuda.add("Esta ventana permite al gerente elegir un pedido mediante el comboBox y el bot�n mostrar.\nA continuaci�n podr� visualizar los datos de ese pedido y elegir si marcar el pedido como confirmado o denegado.");
		ayuda.add("Desde esta ventana el cliente tiene la opcion de eliminar un plato del men� del restaurante.\nPara ello simplemente deberia seleccionar el plato que desea eliminar en el ComboBox y pulsar en el boton ok.\nTras esto se indicar� que el plato se ha eliminado de la base de datos y se volver� a la ventana anterior");
		ayuda.add("Esta ventana se muestra al elegir la opci�n de modificar un plato.\nEn ella podemos seleccionar el plato que deseamos modificar y pulsar OK");
		ayuda.add("Esta ventana permite modificar un plato del restaurante. En ella podremos cambiar el precio, el nombre, la categoria, la receta y la descripcion del plato. As� como a�adir una nueva lista de ingredientes que componen ese plato.\nEn ese caso la lista de ingredientes se reemplazar� por la nueva lista que generemos");
		ayuda.add("Esta ventana permite indicar los ingredientes y la cantidad de estos que componen un plato que se est� modificando. Haciendo uso del bot�n + podremos a�adir nuevos ingredientes al plato con una determinada cantidad.\nY el bot�n OK nos permitir� devolver esta lista a la ventana anterior.");
		ayuda.add("Esta ventana permite a un cocinero mostrar los platos pedidos por todas las mesas del restaurante.\nEn la parte izquierda de la ventana el cocinero podr� seleccionar la mesa de la cual desea mostrar el pedido, a continuaci�n esta informaci�n se mostrar� en la zona derecha.\nDurante la ejecuci�n del programa tan solo estar�n habilitados los botones de las mesas que tengan un pedido activo, de modo que cada cierto tiempo el cocinero tendr� que hacer uso del boton actualizar para comprobar el estado de estas.\nAdem�s, el cocinero podr� utilizar el bot�n Recetas para ver las recetas de todos los platos del restaurante por si lo necesita.");
		ayuda.add("Esta ventana permite a un jefe de cocina mostrar los platos pedidos por todas las mesas del restaurante.\nEn la parte izquierda de la ventana el jefe de cocina podr� seleccionar la mesa de la cual desea mostrar el pedido, a continuaci�n esta informaci�n se mostrar� en la zona derecha.\nDurante la ejecuci�n del programa tan solo estar�n habilitados los botones de las mesas que tengan un pedido activo, de modo que cada cierto tiempo el jefe de cocina tendr� que hacer uso del boton actualizar para comprobar el estado de estas.\nAdem�s, el jefe de cocina podr� utilizar el bot�n Recetas para ver las recetas de todos los platos del restaurante por si lo necesita.\nPor �ltimo el jefe de cocina podr� utilizar el bot�n inventario para que el sistema genere una lista con todos los productos y la cantidad que se deber�a pedir de cada uno de ellos para obtener el stock m�nimo deseable en el restaurante");
		ayuda.add("Esta ventana genera automaticamente una lista con todos los productos existentes en el almacen del restaurante.\nAdem�s, al lado de cada producto se muestra un n�mero que indica la cantidad de ese ingrediente que se deber�a pedir al proveedor para lograr el stock m�nimo deseable en el almacen.\nEl jefe de cocina puede modificar cualquier de estos campos y pulsar OK.\nSi el jefe de sala pulsa el bot�n OK la lista de pedido se almacenar� para que el gerente pueda aprobarla mas tarde.\nSi el jefe de cocina pulsa el boton cancelar la ventana se cerrar� y habr� servido para proporcionar al jefe de cocina una situaci�n del almacen");
		ayuda.add("Esta ventana permite a un cocinero o jefe de cocina mostrar la receta de cualquier palto.\nPara ello tan solo tendr�n que elegir una opci�n en el ComboBox y mostrar el bot�n Mostrar.");
		ayuda.add("Esta ventana permite visualizar todos los pedidos del restaurante.\nMediante el comboBox y el boton mostrar se puede seleccionar el pedido del que se quiere consultar la informaci�n.\nA continuaci�n, el jefe de sala podr� ver los detalles del pedido as� como su estado.\nMediante el bot�n Entregado el jefe de sala podr� marcar ese pedido como entregado.");
		ayuda.add("Esta ventana permite realizar operaciones sobre la base de datos.\nUtilizando las opciones habilitadas podr� crear, modificar o eliminar los objetos que desee.");
		ayuda.add("Esta ventana permite crear nuevos objetos en la base de datos de la aplicaci�n.\nSe le solicitar�n los datos necesarios para poder crear el objeto.");
		ayuda.add("Esta ventana permite modificar objetos ya existentes en la base de datos de la aplicaci�n.\nSe le mostrar�n los datos actuales en la base de datos y podr� actualizarlos.");
		ayuda.add("Esta ventana permite elegir un objeto para posteriormente modificarlo.\nUna vez seleccionado y pulsando el bot�n Modificar, aparecer� la ventana de modificaci�n del objeto.");
		ayuda.add("Esta ventana permite elegir un objeto para su posterior eliminaci�n de la base de datos.\nUna vez seleccionado y haciendo clic en Eliminar, aparecer� una ventana de confirmaci�n que permitir� confirmar la eliminaci�n si as� se desea.");
	}
	
	public static synchronized void muestraAyuda(int index) {
		if(instanciaVentanaAyuda==null) {
			instanciaVentanaAyuda = new GUIAyuda();
		}
		comboBox.setSelectedIndex(index);
		instanciaVentanaAyuda.imprime(index);
		instanciaVentanaAyuda.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		JButton aux = (JButton) arg0.getSource();
		if(okButton == aux) {
			dispose();
		}
		if(btnMostar == aux) {
			imprime(comboBox.getSelectedIndex());
		}
	}
	
	private void imprime(int index) {
		String texto = ayuda.get(index);
		textArea.setText(texto);
	}
	
}
