package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.Restaurante;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;

import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JComboBox;

public class GUIVentAprobarPedido extends JDialog {

	private JPanel contentPane;
	JTextArea textAreaPedido = new JTextArea();
	private ArrayList<Pedido> listaPedidos = Restaurante.getRestaurante().getPedidos();
	private JComboBox comboBox;
	private Pedido pedidoActual;
	private JButton btnConfirmarPedido;
	private JButton btnNewButton;


	/**
	 * Create the frame.
	 */
	public GUIVentAprobarPedido() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("Aprobar pedido");
		setBounds(100, 100, 332, 551);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		textAreaPedido.setBounds(10, 41, 306, 392);
		textAreaPedido.setEditable(false);
		contentPane.add(textAreaPedido);
		
		btnConfirmarPedido = new JButton("Confirmar pedido");
		btnConfirmarPedido.setEnabled(false);
		btnConfirmarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionBotonConfirmar();
			}
		});
		btnConfirmarPedido.setBounds(10, 444, 139, 25);
		contentPane.add(btnConfirmarPedido);
		
		btnNewButton = new JButton("Denegar pedido");
		btnNewButton.setEnabled(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBotonDenegar();
			}
		});
		btnNewButton.setBounds(167, 444, 139, 25);
		contentPane.add(btnNewButton);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(12);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(110, 480, 91, 31);
		contentPane.add(button);
		
		comboBox = new JComboBox();
		comboBox.setBounds(10, 10, 139, 20);
		contentPane.add(comboBox);
		
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				muestraPedido(comboBox.getSelectedIndex());
			}
		});
		btnMostrar.setBounds(167, 7, 139, 23);
		contentPane.add(btnMostrar);
		
		rellenaComboBox(listaPedidos.size());
	}
	
	private void rellenaComboBox(int numEntradas) {
		for(int i=0;i<numEntradas;i++) {
			comboBox.addItem(String.valueOf(i+1));
		}
	}
	
	public void muestraPedido(int index){
		pedidoActual = listaPedidos.get(index);
		String listaPedido = "";
		ArrayList<IngredientePedido> listaIngredientes = pedidoActual.getIngredientesPedido();
		
		for(int i = 0;i<listaIngredientes.size();i++){
			listaPedido = listaPedido + listaIngredientes.get(i).getIngrediente().getNombreLargo() + "\t" + listaIngredientes.get(i).getCantidad()+"\n";
		}
		listaPedido += "\n\n" + pedidoActual.getEstado().toUpperCase();
		
		textAreaPedido.setText(listaPedido);
		btnConfirmarPedido.setEnabled(true);
		btnNewButton.setEnabled(true);
	}
	
	public void accionBotonDenegar(){
		JOptionPane mensajeDenegar = new JOptionPane();
		mensajeDenegar.showMessageDialog(null, "El pedido "+(comboBox.getSelectedIndex()+1)+" ha sido denegado");
		pedidoActual.setEstado("Denegado");
		FachadaPersistencia.modificar_Pedido(pedidoActual);
		this.dispose();
	}
	
	public void accionBotonConfirmar(){
		JOptionPane mensajeConfirmar = new JOptionPane();
		mensajeConfirmar.showMessageDialog(null, "El pedido "+(comboBox.getSelectedIndex()+1)+" ha sido confirmado");
		pedidoActual.setEstado("Confirmado");
		FachadaPersistencia.modificar_Pedido(pedidoActual);
		this.dispose();
	}
}
