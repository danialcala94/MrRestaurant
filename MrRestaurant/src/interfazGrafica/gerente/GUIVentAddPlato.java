package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.Restaurante;
import restaurante.almacen.Ingrediente;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;
import restaurante.empleados.Empleado;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentAddPlato extends JDialog {

	private JPanel contentPane;
	private JTextField textFieldNombreCortoPlato;
	private JTextField textFieldPrecioFinal;
	JTextArea textAreaIngredientes = new JTextArea();
	JLabel lblPrecioRecomendado = new JLabel("Precio recomendado: 0.0");
	private ArrayList <IngredientePlato> listaIngPlato;
	JComboBox comboBoxCategoria = new JComboBox();
	JTextArea textAreaReceta = new JTextArea();
	JTextArea textAreaDescripcion = new JTextArea();
	JButton btnAddPlato = new JButton("A�adir plato");
	ArrayList<CategoriaPlato> listaCategorias = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
	String nombreCorto;
	String nombreLargo;
	String receta = "";
	String descripcion = "";
	float PrecioFinal = 0;
	CategoriaPlato categoria;
	Plato plato;
	private JTextField txtNombreLargoPlato;

	/**
	 * Create the frame.
	 */
	public GUIVentAddPlato(ArrayList<Ingrediente> listaIngrediente) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("A�adir plato");
		setBounds(100, 100, 696, 432);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textFieldNombreCortoPlato = new JTextField();
		textFieldNombreCortoPlato.setBounds(256, 39, 133, 22);
		contentPane.add(textFieldNombreCortoPlato);
		textFieldNombreCortoPlato.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre corto");
		lblNombre.setBounds(256, 16, 95, 16);
		contentPane.add(lblNombre);
		
		lblPrecioRecomendado.setBounds(12, 359, 217, 16);
		contentPane.add(lblPrecioRecomendado);
		
		JLabel lblPrecioFinal = new JLabel("Precio final");
		lblPrecioFinal.setBounds(12, 16, 84, 16);
		contentPane.add(lblPrecioFinal);
		
		textFieldPrecioFinal = new JTextField();
		textFieldPrecioFinal.setBounds(96, 13, 133, 22);
		contentPane.add(textFieldPrecioFinal);
		textFieldPrecioFinal.setColumns(10);
		
		
		comboBoxCategoria.setBounds(425, 50, 179, 22);
		contentPane.add(comboBoxCategoria);
		
		JLabel lblCategora = new JLabel("Categor\u00EDa");
		lblCategora.setBounds(425, 25, 56, 16);
		contentPane.add(lblCategora);
		
		
		textAreaIngredientes.setBounds(11, 74, 206, 272);
		textAreaIngredientes.setEditable(false);
		contentPane.add(textAreaIngredientes);
		
		JLabel lblIngredientes = new JLabel("Ingredientes");
		lblIngredientes.setBounds(12, 45, 78, 16);
		contentPane.add(lblIngredientes);
		
		generarComboBox(listaCategorias, comboBoxCategoria);
		
		JButton btnAddIngredientes = new JButton("A\u00F1adir ingredientes");
		btnAddIngredientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionAddIngredientes(listaIngrediente);
			}
		});
		btnAddIngredientes.setBounds(511, 321, 161, 25);
		contentPane.add(btnAddIngredientes);
		
		
		btnAddPlato.setEnabled(false);
		btnAddPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionAddPlato();
			}
		});
		btnAddPlato.setBounds(511, 350, 161, 25);
		contentPane.add(btnAddPlato);
		
		JLabel lblNombreLargo = new JLabel("Nombre largo");
		lblNombreLargo.setBounds(254, 78, 84, 16);
		contentPane.add(lblNombreLargo);
		
		txtNombreLargoPlato = new JTextField();
		txtNombreLargoPlato.setBounds(256, 100, 133, 22);
		contentPane.add(txtNombreLargoPlato);
		txtNombreLargoPlato.setColumns(10);
		
		JLabel lblReceta = new JLabel("Receta");
		lblReceta.setBounds(256, 151, 56, 16);
		contentPane.add(lblReceta);
		
		textAreaReceta.setLineWrap(true);
		textAreaReceta.setBounds(256, 180, 133, 166);
		contentPane.add(textAreaReceta);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(425, 89, 70, 16);
		contentPane.add(lblDescripcion);
		
		textAreaDescripcion.setLineWrap(true);
		textAreaDescripcion.setBounds(425, 119, 247, 177);
		contentPane.add(textAreaDescripcion);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(10);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(581, 7, 91, 31);
		contentPane.add(button);
		

		
	}
	
	public void accionAddIngredientes(ArrayList<Ingrediente> arrayListIng){
		String listadoIng = "";
		GUIVentAddIngredientes ventAddIngredientes = new GUIVentAddIngredientes(arrayListIng);
		ventAddIngredientes.setModal(true);
		ventAddIngredientes.setVisible(true);
		
		if(ventAddIngredientes.isA�adidaLista() == true){
			listaIngPlato = ventAddIngredientes.getListadoIngPlatoDevolver();
		}else {
			return;
		}
		
		for(int i=0;i<listaIngPlato.size();i++){
			listadoIng = listadoIng + listaIngPlato.get(i).getIngrediente().getNombreLargo() + "\n";
		}
		
		textAreaIngredientes.setText(listadoIng);
		lblPrecioRecomendado.setText("Precio recomendado: "+ventAddIngredientes.getPrecioRecomendado());
		btnAddPlato.setEnabled(true);
		
	}
	
	
	public void generarComboBox(ArrayList<CategoriaPlato> listadoCategorias, JComboBox comboCategoria){
		for(int i=0;i<listadoCategorias.size();i++){
			comboCategoria.addItem(listadoCategorias.get(i).getNombre());
		}
	}
	
	public void accionAddPlato(){
		int indice = 0;
		nombreCorto = textFieldNombreCortoPlato.getText();
		nombreLargo = txtNombreLargoPlato.getText();
		String stringPrecio = textFieldPrecioFinal.getText();
		
		if(nombreCorto.equals("") || nombreLargo.equals("") || stringPrecio.equals("")) {
			JOptionPane.showMessageDialog(null, "No se pueden introducir platos sin indicar su nombre y precio","Error",JOptionPane.ERROR_MESSAGE);
		}else {
			try {
				PrecioFinal = Float.parseFloat(stringPrecio);
				if(PrecioFinal<=0) {
					throw new NumberFormatException();
				}
			}catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "El valor del precio debe ser un n�mero positivo");
				return;
			}
			receta = textAreaReceta.getText();
			descripcion = textAreaDescripcion.getText();
			indice  = comboBoxCategoria.getSelectedIndex();
			categoria = listaCategorias.get(indice);
			plato = new Plato(nombreLargo, nombreCorto,descripcion, PrecioFinal, listaIngPlato, categoria, receta);
			FachadaPersistencia.crear_Plato(plato);
			dispose();
		}
	}
}
