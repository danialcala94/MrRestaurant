package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.cocina.Plato;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentSeleccionPlato extends JDialog {

	private JPanel contentPane;
	JComboBox comboBoxPlatos = new JComboBox();
	ArrayList<Plato> listaPlatos = FachadaPersistencia.obtenerTodos_Platos();
	Plato plato;
	
	/**
	 * Create the frame.
	 */
	public GUIVentSeleccionPlato() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("Seleccion plato");
		setBounds(100, 100, 255, 193);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPlatos = new JLabel("Platos");
		lblPlatos.setBounds(10, 11, 56, 16);
		contentPane.add(lblPlatos);
		
		
		comboBoxPlatos.setBounds(10, 40, 222, 22);
		contentPane.add(comboBoxPlatos);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBotonOK(listaPlatos, comboBoxPlatos);
			}
		});
		btnOk.setBounds(71, 73, 97, 25);
		contentPane.add(btnOk);
		
		mostrarPlatos(listaPlatos, comboBoxPlatos);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(14);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(71, 109, 97, 31);
		contentPane.add(button);
	}
	
	public void mostrarPlatos(ArrayList<Plato> platos, JComboBox comboPlatos){
		for(int i=0;i<platos.size();i++){
			comboPlatos.addItem(platos.get(i).getNombreLargo());
		}
	}
	
	public void accionBotonOK(ArrayList<Plato> platos, JComboBox comboPlatos){
		int indice;
		indice = comboPlatos.getSelectedIndex();
		plato = platos.get(indice);
		
		GUIVentModificarPlato VModificarPlato = new GUIVentModificarPlato(plato);
		VModificarPlato.setVisible(true);
	}
}
