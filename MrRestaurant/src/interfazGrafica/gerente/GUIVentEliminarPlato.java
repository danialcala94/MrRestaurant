package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.Restaurante;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.Pedido;
import restaurante.cocina.Plato;
import restaurante.empleados.Empleado;
import restaurante.empleados.*;

import persistencia.FachadaPersistencia;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentEliminarPlato extends JDialog {

	private JPanel contentPane;
	JComboBox comboBoxPlatos = new JComboBox();
	ArrayList<Plato> listaPlato = FachadaPersistencia.obtenerTodos_Platos();
	Plato platoEliminado;
	
	/**
	 * Create the frame.
	 */
	public GUIVentEliminarPlato() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("Eliminar plato");
		setBounds(100, 100, 227, 196);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		comboBoxPlatos.setBounds(10, 40, 186, 22);
		contentPane.add(comboBoxPlatos);
		
		JLabel lblPlato = new JLabel("Plato");
		lblPlato.setBounds(10, 11, 56, 16);
		contentPane.add(lblPlato);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionBotonOK();
			}
		});
		btnOk.setBounds(59, 87, 97, 25);
		contentPane.add(btnOk);
		
		mostrarPlatos(listaPlato, comboBoxPlatos);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(13);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(59, 123, 97, 31);
		contentPane.add(button);
	}
	
	public void mostrarPlatos(ArrayList<Plato> platos, JComboBox comboPlatos){
		for(int i=0;i<platos.size();i++){
			comboPlatos.addItem(platos.get(i).getNombreLargo());
		}
	}
	
	public void accionBotonOK(){
		String nombrePlato;
		int indice = - 3;
		
		nombrePlato = comboBoxPlatos.getSelectedItem().toString();
		for (int i = 0; i < listaPlato.size(); i++) {
			if (listaPlato.get(i).getNombreLargo().equals(nombrePlato)) {
				indice = i;
				break;
			}
		}
		//indice = comboBoxPlatos.getSelectedIndex();
		platoEliminado = listaPlato.get(indice);

		if (JOptionPane.showConfirmDialog(null, "Eliminar " + platoEliminado.getNombreLargo(), "Eliminar", JOptionPane.YES_OPTION) == 0) {
			FachadaPersistencia.eliminar_Plato(platoEliminado);
			JOptionPane.showMessageDialog(null, "Se ha eliminado el plato " + platoEliminado.getNombreLargo(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
		}
		dispose();
		
	}
}
