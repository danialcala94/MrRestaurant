package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.almacen.Ingrediente;
import restaurante.cocina.IngredientePlato;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentAddIngredientes extends JDialog {

	private JPanel contentPane;
	private JTextField textFieldCantidad;
	public String listadoIngredientes = "";
	JComboBox comboBoxIngredientes = new JComboBox();
	JTextArea textAreaListaIngredientes = new JTextArea("");
	ArrayList<Ingrediente> listaIngredientes;
	ArrayList <IngredientePlato> listadoIngPlatoDevolver = new ArrayList <IngredientePlato>();
	int contLlamadas = 0;
	boolean aņadidaLista = false;
	float precioRecomendado;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public GUIVentAddIngredientes(ArrayList<Ingrediente> listaIngredientes) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.listaIngredientes = listaIngredientes;
		setTitle("Aņadir ingredientes");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListadoIngredientes = new JLabel("Listado ingredientes");
		lblListadoIngredientes.setBounds(12, 13, 121, 16);
		contentPane.add(lblListadoIngredientes);
		
		
		textAreaListaIngredientes.setBounds(12, 42, 186, 198);
		textAreaListaIngredientes.setEditable(false);
		contentPane.add(textAreaListaIngredientes);
		
		
		comboBoxIngredientes.setBounds(237, 83, 164, 22);
		contentPane.add(comboBoxIngredientes);
		
		
		JLabel lblIngrediente = new JLabel("Ingrediente:");
		lblIngrediente.setBounds(237, 56, 90, 16);
		contentPane.add(lblIngrediente);
		
		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setBounds(237, 121, 56, 16);
		contentPane.add(lblCantidad);
		
		textFieldCantidad = new JTextField("1");
		textFieldCantidad.setBounds(237, 144, 116, 22);
		contentPane.add(textFieldCantidad);
		textFieldCantidad.setColumns(10);
		
		aņadirElementosCombo(listaIngredientes, comboBoxIngredientes);
		
		JButton buttonAgregar = new JButton("+");
		buttonAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionBotonAgregar(listaIngredientes, listadoIngPlatoDevolver);
			}
		});
		buttonAgregar.setBounds(261, 198, 66, 25);
		contentPane.add(buttonAgregar);
		
		JButton btnOkConfirmar = new JButton("OK");
		btnOkConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBotonConfirmar();
			}
		});
		btnOkConfirmar.setBounds(339, 198, 66, 25);
		contentPane.add(btnOkConfirmar);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(11);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(333, 11, 91, 31);
		contentPane.add(button);
	}
	
	public void accionBotonAgregar(ArrayList <Ingrediente> listaIngPlat, ArrayList <IngredientePlato> listaADevolver){
		String ingrediente = comboBoxIngredientes.getSelectedItem().toString();
		float cantidad = 0;
		
		try{
			cantidad = Float.parseFloat(textFieldCantidad.getText());
		}catch(java.lang.NumberFormatException e){
			JOptionPane mensajeErrorCantidad = new JOptionPane();
			mensajeErrorCantidad.showMessageDialog(this, "Introduzca un valor numerico.");
			textFieldCantidad.setText("");
			return;
		}
		
		int valor = comboBoxIngredientes.getSelectedIndex();
		Ingrediente ing = listaIngPlat.get(valor);
		// TODO
		IngredientePlato aux = new IngredientePlato(ing, cantidad);
		listaADevolver.add(aux);
		
		listadoIngredientes = listadoIngredientes +  ingrediente +"\t"+ cantidad + "\n";
		textAreaListaIngredientes.setText(listadoIngredientes);
		textFieldCantidad.setText("");
		
		contLlamadas++;
		
	}
	
	public void accionBotonConfirmar(){
		if(contLlamadas == 0){
			JOptionPane mensajeErrorCantidad = new JOptionPane();
			mensajeErrorCantidad.showMessageDialog(this, "Aņada los ingredientes necesarios.");
		}else{
			aņadidaLista = true;
			precioRecomendado = calcularPrecio(listadoIngPlatoDevolver);
			dispose();
		}
		
		
	}
	
	public void aņadirElementosCombo(ArrayList<Ingrediente> listadoIngredientes, JComboBox comboBoxIngre){
		for(int i=0;i<listadoIngredientes.size();i++){
			comboBoxIngre.addItem(listadoIngredientes.get(i).getNombreCorto());
		}	
	}
	
	public float calcularPrecio(ArrayList<IngredientePlato> listaIngredientePlato){
		float precioTotal = 0;
		float precioAux = 0;
		float cantidadAux = 0;
		
		for(int i = 0;i<listaIngredientePlato.size();i++){
			cantidadAux = listaIngredientePlato.get(i).getCantidad();
			System.out.println("Cantidad--------->"+cantidadAux+"\n");
			precioAux = listaIngredientePlato.get(i).getIngrediente().getPrecioBase();
			System.out.println("Precio--------->"+precioAux+"\n");
			precioTotal = precioTotal + (cantidadAux * precioAux);
			System.out.println("PrecioTot--------->"+precioTotal+"\n");
		}
		
		return precioTotal;
	}

	public ArrayList<IngredientePlato> getListadoIngPlatoDevolver() {
		return listadoIngPlatoDevolver;
	}

	public void setListadoIngPlatoDevolver(ArrayList<IngredientePlato> listadoIngPlatoDevolver) {
		this.listadoIngPlatoDevolver = listadoIngPlatoDevolver;
	}

	public boolean isAņadidaLista() {
		return aņadidaLista;
	}

	public void setAņadidaLista(boolean aņadidaLista) {
		this.aņadidaLista = aņadidaLista;
	}

	public float getPrecioRecomendado() {
		return precioRecomendado;
	}

	public void setPrecioRecomendado(float precioRecomendado) {
		this.precioRecomendado = precioRecomendado;
	}
}
