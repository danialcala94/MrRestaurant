package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalExclusionType;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import interfazGrafica.basededatos.GUIVentanaGestionBD;
import persistencia.FachadaPersistencia;
import restaurante.Restaurante;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.Pedido;
import restaurante.empleados.Empleado;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JLabel;

public class GUIVentanaGerente extends JFrame {

	private JPanel contentPane;
	private Empleado empleado;
	ArrayList<Ingrediente> listadoIngrediente = FachadaPersistencia.obtenerTodos_Ingredientes();

	/**
	 * Create the frame.
	 */
	public GUIVentanaGerente(Empleado empleado) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.empleado = empleado;
		setTitle("Ventana gerente:   "+empleado.getNombre()+" ("+empleado.getDni()+")");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 213);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAprobarPedido = new JButton("Aprobar pedido");
		btnAprobarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pedido aux = Restaurante.getRestaurante().getUltimoPedido();
				if(aux == null) {
					JOptionPane.showMessageDialog(null, "No se ha generado ningun pedido a�n\nEl Jefe de sala deber�a generar un pedido antes de aprobarlo","Aviso",JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				accionAprobarPedido(Restaurante.getRestaurante().getUltimoPedido());
			}
		});
		btnAprobarPedido.setBounds(10, 11, 156, 25);
		contentPane.add(btnAprobarPedido);
		
		JButton btnAddPlato = new JButton("A\u00F1adir plato");
		btnAddPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionAddPlato(listadoIngrediente);
			}
		});
		btnAddPlato.setBounds(192, 11, 156, 25);
		contentPane.add(btnAddPlato);
		
		JButton btnConsultarNominas = new JButton("Consultar n\u00F3minas");
		btnConsultarNominas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionConsultarNominas(Restaurante.getRestaurante().getEmpleados());
			}
		});
		btnConsultarNominas.setBounds(371, 74, 156, 25);
		contentPane.add(btnConsultarNominas);
		
		JButton btnModificarPlato = new JButton("Modificar plato");
		btnModificarPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionModificarPlato();
			}
		});
		btnModificarPlato.setBounds(371, 11, 156, 25);
		contentPane.add(btnModificarPlato);
		
		JButton btnEliminarPlato = new JButton("Eliminar plato");
		btnEliminarPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionEliminarPlato();
			}
		});
		btnEliminarPlato.setBounds(10, 74, 156, 25);
		contentPane.add(btnEliminarPlato);
		
		JButton button = new JButton("Ayuda");
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIAyuda.muestraAyuda(8);
			}
		});
		button.setBounds(182, 63, 177, 42);
		contentPane.add(button);
		
		JButton btnGestionarBaseDe = new JButton("Gestionar Base de Datos");
		btnGestionarBaseDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUIVentanaGestionBD bd = new GUIVentanaGestionBD();
				bd.setSize(550, 350);
				bd.setResizable(false);
				//bd.setModal(true);
				bd.setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
				bd.setVisible(true);
			}
		});
		btnGestionarBaseDe.setBounds(182, 131, 177, 25);
		contentPane.add(btnGestionarBaseDe);
	}
	
	public void accionAprobarPedido(Pedido pedido){
		GUIVentAprobarPedido ventAprobarPedido = new GUIVentAprobarPedido();
		ventAprobarPedido.setModal(true);
		ventAprobarPedido.setVisible(true);
	}
	
	public void accionAddPlato(ArrayList<Ingrediente> listaIngrediente){
		GUIVentAddPlato ventAddPlato = new GUIVentAddPlato(listaIngrediente);
		ventAddPlato.setVisible(true);
	}
	
	public void accionConsultarNominas(ArrayList <Empleado> listaEmpleados){
		GUIConsultaNominas ventConsultaNominas = new GUIConsultaNominas();
		ventConsultaNominas.setVisible(true);
	}
	
	public void accionModificarPlato(){
		GUIVentSeleccionPlato ventSeleccionPlato = new GUIVentSeleccionPlato();
		ventSeleccionPlato.setVisible(true);
	}
	
	public void accionEliminarPlato(){
		GUIVentEliminarPlato ventEliminarPlato = new GUIVentEliminarPlato();
		ventEliminarPlato.setVisible(true);
	}
}
