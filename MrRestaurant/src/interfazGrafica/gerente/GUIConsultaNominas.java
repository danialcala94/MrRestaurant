package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.empleados.Empleado;

import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIConsultaNominas extends JDialog {

	private JPanel contentPane;
	JTextArea textArea = new JTextArea();
	ArrayList<Empleado> listaEmpleados = FachadaPersistencia.obtenerTodos_Empleados();
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public GUIConsultaNominas() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		setTitle("Consultar n�minas");
		setBounds(100, 100, 391, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionBotonOk();
			}
		});
		btnOk.setBounds(268, 365, 97, 25);
		contentPane.add(btnOk);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 13, 355, 327);
		contentPane.add(scrollPane);
		
		
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(9);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(10, 351, 177, 42);
		contentPane.add(button);
		
		consultarEmpleados(listaEmpleados);
	}
	
	public void accionBotonOk(){
		dispose();
	}
	
	public void consultarEmpleados(ArrayList <Empleado> listadoEmpleados){
		String nominasEmpleados = "";
		for(int i=0;i<listadoEmpleados.size();i++){
			nominasEmpleados = nominasEmpleados + listadoEmpleados.get(i).getSalario() + "\t" + listadoEmpleados.get(i).getNombre() + "\n";
		}
		textArea.setText(nominasEmpleados);
	}
}
