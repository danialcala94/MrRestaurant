package interfazGrafica.gerente;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.almacen.Ingrediente;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentModificarPlato extends JDialog {

	private JPanel contentPane;
	private JTextField textFieldPrecio;
	private JTextField textFieldNombreCorto;
	private JTextField textFieldNombreLargo;
	JTextArea textAreaIngredientes = new JTextArea();
	JTextArea textAreaReceta = new JTextArea();
	JTextArea textAreaDescripcion = new JTextArea();
	JComboBox comboBoxCategoria = new JComboBox();
	Plato plato;
	ArrayList<IngredientePlato> listaIngredientes;
	ArrayList<CategoriaPlato> listaCategorias = FachadaPersistencia.obtenerTodos_CategoriasPlatos();

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public GUIVentModificarPlato(Plato plato) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.plato = plato;
		this.listaIngredientes = plato.getIngredientes();
		setTitle("Modificar plato");
		setBounds(100, 100, 718, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrecioFinal = new JLabel("Precio final*");
		lblPrecioFinal.setBounds(12, 13, 83, 16);
		contentPane.add(lblPrecioFinal);
		
		textFieldPrecio = new JTextField();
		textFieldPrecio.setBounds(12, 31, 83, 22);
		contentPane.add(textFieldPrecio);
		textFieldPrecio.setColumns(10);
		//Mostramos en elJTextField el precio del plato
		float precio = plato.getPrecio();
		textFieldPrecio.setText(String.valueOf(precio));
		
		JLabel lblNombreCorto = new JLabel("Nombre corto*");
		lblNombreCorto.setBounds(105, 13, 90, 16);
		contentPane.add(lblNombreCorto);
		
		textFieldNombreCorto = new JTextField();
		textFieldNombreCorto.setBounds(197, 10, 157, 22);
		contentPane.add(textFieldNombreCorto);
		textFieldNombreCorto.setColumns(10);
		//Mostramos el nombre corto del plato en el JTextField
		textFieldNombreCorto.setText(plato.getNombreCorto());
		
		JLabel lblCategoria = new JLabel("Categoria*: "+plato.getCategoria().getNombre());
		lblCategoria.setBounds(366, 13, 159, 16);
		contentPane.add(lblCategoria);
		
		
		comboBoxCategoria.setBounds(535, 10, 153, 22);
		contentPane.add(comboBoxCategoria);
		//Añadimos las categorias al comboBox
		añadirCategorias();
		
		JLabel lblIngredientes = new JLabel("Ingredientes*");
		lblIngredientes.setBounds(12, 66, 83, 16);
		contentPane.add(lblIngredientes);
		

		textAreaIngredientes.setEditable(false);
		textAreaIngredientes.setBounds(12, 84, 183, 277);
		textAreaIngredientes.setLineWrap(true);
		contentPane.add(textAreaIngredientes);
		//Mostramos los ingredientes del plato en el textArea
		String ingredientesMostrados = "";
		listaIngredientes = plato.getIngredientes();
		for(int i=0;i<listaIngredientes.size();i++){
			ingredientesMostrados = ingredientesMostrados + listaIngredientes.get(i).getIngrediente().getNombreLargo() + "\n";
		}
		textAreaIngredientes.setText(ingredientesMostrados);
		
		JLabel lblNombreLargo = new JLabel("Nombre largo*");
		lblNombreLargo.setBounds(105, 55, 103, 16);
		contentPane.add(lblNombreLargo);
		
		textFieldNombreLargo = new JTextField();
		textFieldNombreLargo.setBounds(207, 52, 190, 22);
		contentPane.add(textFieldNombreLargo);
		textFieldNombreLargo.setColumns(10);
		//Mostramos el nombre largo del plato en el JtextField
		textFieldNombreLargo.setText(plato.getNombreLargo());
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(468, 48, 78, 16);
		contentPane.add(lblDescripcion);
		
	
		textAreaDescripcion.setText("");
		textAreaDescripcion.setLineWrap(true);
		textAreaDescripcion.setBounds(470, 66, 218, 176);
		contentPane.add(textAreaDescripcion);
		//Mostramos en el JTextArea la descripcion del plato
		textAreaDescripcion.setText(plato.getDescripcion());
		System.out.println(plato.getDescripcion());
		
		JLabel lblReceta = new JLabel("Receta*");
		lblReceta.setBounds(230, 87, 65, 16);
		contentPane.add(lblReceta);
		
		
		textAreaReceta.setLineWrap(true);
		textAreaReceta.setBounds(227, 103, 212, 262);
		contentPane.add(textAreaReceta);
		//Mostramos la receta en el JTextArea
		textAreaReceta.setText(plato.getReceta());
		
		JButton btnModificarIngredientes = new JButton("Modificar ingredientes");
		btnModificarIngredientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionModificarIngredientes();
			}
		});
		btnModificarIngredientes.setBounds(468, 260, 220, 25);
		contentPane.add(btnModificarIngredientes);
		
		JButton btnModificarPlato = new JButton("Modificar plato");
		btnModificarPlato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionModificarPlato();
			}
		});
		btnModificarPlato.setBounds(468, 298, 220, 25);
		contentPane.add(btnModificarPlato);
		
		JButton button = new JButton("Ayuda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(15);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		button.setBounds(535, 334, 91, 31);
		contentPane.add(button);
		
	}
	
	public void añadirCategorias(){
		for(int i=0;i<listaCategorias.size();i++){
			comboBoxCategoria.addItem(listaCategorias.get(i).getNombre());
		}
	}
	
	
	public void accionModificarIngredientes(){
		String listadoIng = "";
		GUIModificarIngredientesPlato ventModificarIngredientes = new GUIModificarIngredientesPlato();
		ventModificarIngredientes.setModal(true);
		ventModificarIngredientes.setVisible(true);
		
		if(ventModificarIngredientes.isAñadidaLista() == true){
			listaIngredientes = ventModificarIngredientes.getListadoIngPlatoDevolver();
		}
		
		for(int i=0;i<listaIngredientes.size();i++){
			listadoIng = listadoIng + listaIngredientes.get(i).getIngrediente().getNombreLargo() + "\n";
		}
		textAreaIngredientes.setText(listadoIng);
	}
	
	public void accionModificarPlato(){
		int indiceCategoria = comboBoxCategoria.getSelectedIndex();
		String mensaje;
		String ingredientes = "";
		
		plato.setPrecio(Float.parseFloat(textFieldPrecio.getText()));
		plato.setCategoria(listaCategorias.get(indiceCategoria));
		plato.setNombreCorto(textFieldNombreCorto.getText());
		plato.setNombreLargo(textFieldNombreLargo.getText());
		plato.setIngredientes(listaIngredientes);
		plato.setDescripcion(textAreaDescripcion.getText());
		plato.setReceta(textAreaReceta.getText());
		
		for(int i=0;i<plato.getIngredientes().size();i++){
			ingredientes = ingredientes + plato.getIngredientes().get(i).getIngrediente().getNombreLargo()+"\n";
		}
		mensaje = "Si confirma, el plato quedará de la siguiente forma: \n\nidPlato: " + plato.getIDPlato() + "\nPrecio: "+plato.getPrecio()+"\nNombre corto: "+plato.getNombreCorto()+
				"\nNombre largo: "+plato.getNombreLargo()+"\nCategoria: "+plato.getCategoria().getNombre()+"\nDescripcion: "+plato.getDescripcion()+"\nReceta: "
				+plato.getReceta()+"\nIngredientes: "+ingredientes;
		if (JOptionPane.showConfirmDialog(this, mensaje, "Confirmar modificación", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
				FachadaPersistencia.modificar_Plato(plato);
				dispose();
		}
	}
}