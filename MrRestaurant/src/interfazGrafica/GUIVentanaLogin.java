package interfazGrafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.tablas.Alergenos;
import hibernate.utils.HibernateUtils;
import interfazGrafica.ayuda.GUIAyuda;
import interfazGrafica.cocina.GUIVentanaCocina;
import interfazGrafica.gerente.GUIVentanaGerente;
import interfazGrafica.sala.GUIVentanaSala;
import persistencia.FachadaPersistencia;
import restaurante.Constantes;
import restaurante.empleados.*;
import ui.sala.UISala;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.Dialog.ModalExclusionType;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class GUIVentanaLogin extends JFrame {
	
	private static final String HELP_ID = "GUIVentanaLogin";

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lblError, lblExito;
	
	private ArrayList<Credencial> credenciales = new ArrayList<Credencial>();
	
	private GUIVentanaCocina ventanaCocina;
	private GUIVentanaSala ventanaSala;
	private GUIVentanaGerente ventanaGerente;

	JMenuBar menuBar = new JMenuBar();
	
	private void constructorGeneral() {
		this.setResizable(false);
		setTitle("MRRestaurant - LOGIN");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(Constantes.WINDOW_FULLHD_1920_ANCHO / 2 - Constantes.WINDOW_LOGIN_ANCHO / 2,
				Constantes.WINDOW_FULLHD_1080_ALTO / 2 - Constantes.WINDOW_LOGIN_ALTO / 2,
				Constantes.WINDOW_LOGIN_ANCHO, Constantes.WINDOW_LOGIN_ALTO);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.setJMenuBar(menuBar);
		JMenuItem itemArchivo = new JMenuItem("Archivo");
		menuBar.add(itemArchivo);
		JMenuItem itemAyuda = new JMenuItem("Ayuda");
		menuBar.add(itemAyuda);
		GUIAyuda.getInstancia(itemAyuda, HELP_ID);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(10, 86, 74, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContasea = new JLabel("Contase\u00F1a");
		lblContasea.setBounds(10, 114, 74, 14);
		contentPane.add(lblContasea);
		
		textField = new JTextField();
		textField.setBounds(94, 83, 223, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gestionaBoton();
			}
		});
		btnNewButton.setBounds(228, 138, 89, 23);
		contentPane.add(btnNewButton);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(94, 111, 223, 20);
		contentPane.add(passwordField);
		
		lblError = new JLabel("Usuario o contrase\u00F1a incorrectos");
		lblError.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExito = new JLabel("Usuario y contraseña correctos");
		lblExito.setFont(new Font("Tahoma", Font.BOLD, 11));
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
        lblError.setBorder(border);
        lblExito.setBorder(border);
        lblError.setVisible(false);
        lblExito.setVisible(false);
        lblError.setOpaque(true);
        lblExito.setOpaque(true);
		lblError.setIcon(Constantes.ICONO_INCORRECTO);
		lblExito.setIcon(Constantes.ICONO_INCORRECTO);
		lblError.setBackground(new Color(255, 153, 153));
		lblExito.setBackground(new Color(153, 255, 204));
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblExito.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setBounds(10, 12, 307, 63);
		lblExito.setBounds(10, 12, 307, 63);
		contentPane.add(lblError);
		contentPane.add(lblExito);
		
		credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
	}

	/**
	 * Create the frame.
	 */
	public GUIVentanaLogin(Credencial[] listaCredenciales) {
		constructorGeneral();
		credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
	}
	

	/**
	 * @wbp.parser.constructor
	 */
	public GUIVentanaLogin(ArrayList<Credencial> listaCredenciales2) {
		Credencial[] aux = listaCredenciales2.toArray(new Credencial[listaCredenciales2.size()]);
		constructorGeneral();
		credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
	}

	public void gestionaBoton(){
		Credencial cred = validaAcceso();
		if (cred != null) {
			lblError.setVisible(false);
			lblExito.setVisible(true);
			
			//Prueba Hibernate
			/*Session session = HibernateUtils.getSessionFactory().openSession();
			System.out.println("Iniciando transacción...");
			Transaction tx = session.beginTransaction();
			
			Alergenos alergeno = new Alergenos("Prueba", "Sin descrip.");
			//alergeno.setIdAlergeno(24l);
			System.out.println("Guardando alérgeno...");
			session.save(alergeno);
			
			System.out.println("Commiteando cambios...");
			tx.commit();
			System.out.println("Cerrando sesión...");
			session.close();*/
			
			// Debemos dar acceso a la aplicación
			switch (cred.getNivel().getIDNivelCredencial()) {
			case Constantes.NIVELESEMPLEADOS_COCINERO:
				new UISala(Constantes.NIVELESEMPLEADOS_COCINERO);
				break;
			case Constantes.NIVELESEMPLEADOS_JEFEDECOCINA:
				new UISala(Constantes.NIVELESEMPLEADOS_JEFEDECOCINA);
				break;
			case Constantes.NIVELESEMPLEADOS_CAMARERO:
				new UISala(Constantes.NIVELESEMPLEADOS_CAMARERO);
				break;
			case Constantes.NIVELESEMPLEADOS_JEFEDESALA:
				new UISala(Constantes.NIVELESEMPLEADOS_CAMARERO);
				break;
			case Constantes.NIVELESEMPLEADOS_GERENTE:
				new UISala(Constantes.NIVELESEMPLEADOS_GERENTE);
				break;
			default:
				System.out.println("Error");
			}
		} else {
			System.err.println("[DEBUG]: Usuario y contraseña incorrectos.");
			lblError.setVisible(true);
			lblExito.setVisible(false);
			passwordField.setText("");
		}
		
		/*switch (nivelAcceso) {
		case 1:
			System.out.println("El usuario es un cocinero. Se muestra la cocina con isJefe = false");
			GUIVentanaCocina ventanaCocina = new GUIVentanaCocina(false,empleado);
			ventanaCocina.setVisible(true);
			break;
			
		case 2:
			System.out.println("El usuario es un jefe de cocina. Se muestra la cocina con isJefe = true");
			ventanaCocina = new GUIVentanaCocina(true,empleado);
			ventanaCocina.setVisible(true);
			break;
			
		case 3:
			System.out.println("El usuario es un camarero. Se muestra la sala con isJefe = false");
			ventanaSala = new GUIVentanaSala(false,empleado);
			ventanaSala.setVisible(true);
			ventanaSala.setModalExclusionType(ModalExclusionType.NO_EXCLUDE);
			break;
			
		case 4:
			System.out.println("El usuario es un jefe de sala. Se muestra la sala con isJefe = true");
			ventanaSala = new GUIVentanaSala(true,empleado);
			ventanaSala.setVisible(true);
			break;
			
		case 5:
			System.out.println("El usuario es el gerente. Se muestra la ventana del gerente");
			ventanaGerente = new GUIVentanaGerente(empleado);
			ventanaGerente.setVisible(true);
			break;
			

		default:
			JOptionPane.showMessageDialog(null, "[DEBUUG]Ha habido algun error muy raro\nSe ha introducido un Empleado con un id no soportado","Error [DEBUG]",JOptionPane.ERROR_MESSAGE);
			break;
		}

		textField.setText("");
		passwordField.setText("");
		//Despues de abrir la ventana que corresponda se cierra esta
		//En una version de debug podriamos dejar la ventana de Login abierta para poder abrir la interfaz de varios Empleados al mismo tiempo en un ordenador
		return;*/
	}
	
	/**
	 * Devuelve el credencial si coincide con los datos introducidos, o devuelve null si no existe.
	 * @return <strong>Credencial</strong> cred
	 */
	private Credencial validaAcceso() {
		System.out.println("+-------------------------------------------------------------------+");
		System.out.println("|           Los credenciales actuales en base de datos son          |");
		System.out.println("+-------------------------------------------------------------------+");
		for (int i = 0; i < credenciales.size(); i++)
			System.out.println("| Usuario: " + credenciales.get(i).getUsuario() + "\t\t\tContraseña: " + credenciales.get(i).getContraseña());
		System.out.println("+-------------------------------------------------------------------+");
		String user = textField.getText();
		String pass = new String(passwordField.getPassword());
		Credencial cred = null;
		for (int i = 0; i < credenciales.size(); i++) {
			cred = credenciales.get(i);
			if(cred.getUsuario().equals(user) && cred.getContraseña().equals(pass))
				return cred;
		}
		return null;
	}
}
