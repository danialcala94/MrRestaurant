package interfazGrafica.cocina;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import restaurante.Restaurante;
import restaurante.almacen.*;
import java.util.*;

public class GUIVentanaInventario extends JDialog {

	private JPanel contentPane;
	private JPanel panel;
	private JLabel[] arrayLabels;
	private JTextField[] arrayTextFields;
	private int numElementos=0;
	private ArrayList<IngredientePedido> lista;
	
	/**
	 * La ventana de creacion de ingrediente pedidos recibe una lista de ingrediente-pedidos y la muestra en pantalla
	 * 
	 * Permite al usuario modificar la lista y al pulsar en aceptar la devuelve
	 * 
	 * @param listaIngredientePedido Se trata de un arrayList que contiene objetos del tipo IngredientePedido, este a su vez contiene el ingrediente a pedir y la cantidad de este
	 */
	public GUIVentanaInventario() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		ArrayList<IngredientePedido> listaIngredientePedido = obtenerLista();
		this.lista=listaIngredientePedido;
		numElementos=lista.size();
		this.setTitle("Ventana inventario");
		this.arrayLabels = new JLabel[numElementos];
		this.arrayTextFields = new JTextField[numElementos];
		int espacioVertical = calculaEspacioVertical(numElementos);

		setResizable(false);
		setBounds(100, 100, 486, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 360, 319);
		contentPane.add(scrollPane);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(300, espacioVertical));
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.setBounds(381, 274, 89, 23);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(generaLista()) {
					dispose();
				}
			}
		});
		
		JButton button = new JButton("Cancelar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button.setBounds(381, 307, 89, 23);
		contentPane.add(button);
		
		JButton btnAyuda = new JButton("Ayuda");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(19);
			}
		});
		btnAyuda.setBounds(381, 10, 89, 23);
		contentPane.add(btnAyuda);
		
		generaLabels(numElementos);
		generaTextFields(numElementos);
		rellenaLabelsTextFields();
		
	}
	

	private int calculaEspacioVertical(int numElementos){
		int tam;
		int tamOriginal = 315;
		if(numElementos<=9) {	//Se calcula a ojo que 9 elementos entran bien separados entre ellos 35 pixeles
			tam=tamOriginal;			//319 es el tama�o del scroll pane para que no se muestren los scrolls
		}else{
			int excedente = numElementos-9;
			tam = tamOriginal+excedente*35;	//Los textos est�n a 35 pixeles cada uno del siguiente
		}
		return tam;
	}
	
	private void generaLabels(int num) {
		for(int i=0;i<num;i++) {
			JLabel label = new JLabel(String.valueOf(i+1));
			label.setBounds(10, 5+(i*35), 200, 14);	//Calcula la posicion que deberia ocupar ese label en el JPanel
			panel.add(label);
			arrayLabels[i] = label;
		}
	}
	
	private void generaTextFields(int num) {
		for(int i=0;i<num;i++) {
			JTextField textField = new JTextField();
			textField.setBounds(240, 5+(i*35), 100, 20);
			panel.add(textField);
			textField.setColumns(10);
			arrayTextFields[i] = textField;
		}
	}
	
	private void rellenaLabelsTextFields(){
		for(int i=0;i<this.lista.size();i++) {
			IngredientePedido aux = this.lista.get(i);
			Ingrediente ingrediente = aux.getIngrediente();
			arrayLabels[i].setText(ingrediente.getNombreCorto());
			arrayTextFields[i].setText(String.valueOf(aux.getCantidad()));
		}
	}
	
	private boolean generaLista(){
		for(int i=0;i<numElementos;i++) {
			JTextField textAux = arrayTextFields[i];
			if(textAux.getText().equalsIgnoreCase("")) {
				textAux.setText("0");
			}
			float cantidadNueva;
			try {
				cantidadNueva = Float.parseFloat(textAux.getText());
				if(cantidadNueva < 0) {
					//Mostrar error
					JOptionPane.showMessageDialog(null, "Las casillas de cantidad deberian tener un numero entero\nRevise sus datos y vuelva a intentarlo","Error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Las casillas de cantidad deberian tener un numero entero\nRevise sus datos y vuelva a intentarlo","Error",JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		for(int i=0;i<numElementos;i++){
			JTextField textAux = arrayTextFields[i];
			if(textAux.getText().equalsIgnoreCase("")) {
				textAux.setText("0");
			}
			IngredientePedido aux = lista.get(i);
			aux.setCantidad(Float.parseFloat(textAux.getText()));
		}
		//Si acaba de recorrer todos los elementos sin problema guardamos esa lista para devolverla
		JOptionPane.showMessageDialog(null, "Se ha almacenado el pedido preparado. Esperando aprobaci�n del gerente","Aviso",JOptionPane.INFORMATION_MESSAGE);
		//La lista con las cantidades definitivas estar� almacenada en Lista
		Pedido pedido = new Pedido(lista,"Generado");
		Restaurante.getRestaurante().addPedido(pedido);
		return true;
	}
	
	private ArrayList<IngredientePedido> obtenerLista() {
		ArrayList<Ingrediente> listaIngredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
		ArrayList<IngredientePedido> listaIngredientesNecesarios = new ArrayList<IngredientePedido>();
		for(int i=0; i<listaIngredientes.size(); i++) {
			Ingrediente ingrediente = listaIngredientes.get(i);
			float cantidad  = ingrediente.getStockMin() - ingrediente.getStockAct();
			if(cantidad<0) {
				cantidad = 0;
			}
			IngredientePedido ingredientePedido = new IngredientePedido(ingrediente, cantidad);
			listaIngredientesNecesarios.add(ingredientePedido);
		}
		return listaIngredientesNecesarios;
	}
}
