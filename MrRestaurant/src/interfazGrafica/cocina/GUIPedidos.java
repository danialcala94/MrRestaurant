package interfazGrafica.cocina;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.Restaurante;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GUIPedidos extends JDialog implements ActionListener {

	private JPanel contentPane;
	private JTextField textFieldEstado;
	private JLabel[] arrayLabels;
	private JTextField[] arrayTextFields;
	private int numPedidos;
	private JPanel panel;
	private ArrayList<Pedido> listaPedidos;
	private JComboBox comboBox;
	private JButton btnMostrar;
	private JButton btnEntregado;
	private JButton btnAyuda;
	private JButton btnOk;
	private Pedido pedidoActual;
	private JLabel lblIngrediente;
	private JLabel lblCantidad;

	/**
	 * Create the frame.
	 */
	public GUIPedidos() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setResizable(false);
		this.setTitle("Pedidos");
		
		listaPedidos = Restaurante.getRestaurante().getPedidos(); 
		numPedidos=listaPedidos.size();
		
		setBounds(100, 100, 548, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox();
		for(int i=0; i<numPedidos;i++) {
			comboBox.addItem(String.valueOf(i+1));
		}
		comboBox.setBounds(118, 11, 83, 20);
		contentPane.add(comboBox);
		
		JLabel lblPedidoAMostrar = new JLabel("Pedido a mostrar:");
		lblPedidoAMostrar.setBounds(10, 14, 98, 14);
		contentPane.add(lblPedidoAMostrar);
		
		btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(this);
		btnMostrar.setBounds(223, 11, 89, 23);
		contentPane.add(btnMostrar);
		
		btnEntregado = new JButton("Entregado");
		btnEntregado.addActionListener(this);	
		btnEntregado.setBounds(322, 11, 101, 23);
		contentPane.add(btnEntregado);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.addActionListener(this);
		btnAyuda.setBounds(433, 10, 89, 23);
		contentPane.add(btnAyuda);
		
		JLabel lblEntregado = new JLabel("Estado:");
		lblEntregado.setBounds(10, 39, 98, 14);
		contentPane.add(lblEntregado);
		
		textFieldEstado = new JTextField();
		textFieldEstado.setEditable(false);
		textFieldEstado.setBounds(118, 36, 83, 20);
		contentPane.add(textFieldEstado);
		textFieldEstado.setColumns(10);
		
		btnOk = new JButton("Ok");
		btnOk.addActionListener(this);
		btnOk.setBounds(433, 35, 89, 23);
		contentPane.add(btnOk);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 89, 502, 351);
		contentPane.add(scrollPane);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		
		lblIngrediente = new JLabel("Ingrediente");
		lblIngrediente.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngrediente.setBounds(33, 64, 208, 14);
		contentPane.add(lblIngrediente);
		
		lblCantidad = new JLabel("Cantidad");
		lblCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidad.setBounds(421, 64, 89, 14);
		contentPane.add(lblCantidad);
		
	}
	
	private int calculaEspacioVertical(int numElementos) {
		return numElementos*35;
	}

	private void generaLabels(int num) {
		for(int i=0;i<num;i++) {
			JLabel label = new JLabel(String.valueOf(i+1));
			label.setBounds(10, 11+(i*35), 293, 14);
			panel.add(label);
			arrayLabels[i] = label;
		}
	}
	
	private void generaTextFields(int num) {
		for(int i=0;i<num;i++) {
			JTextField textField = new JTextField();
			textField.setEditable(false);
			textField.setBounds(404, 8+(i*35), 86, 20);
			panel.add(textField);
			textField.setColumns(10);
			arrayTextFields[i] = textField;
		}
	}
	
	private void rellenaLabelsTextFields(ArrayList<IngredientePedido> listaIngredientes){
		for(int i=0;i<listaIngredientes.size();i++) {
			IngredientePedido aux = listaIngredientes.get(i);
			Ingrediente ingrediente = aux.getIngrediente();
			arrayLabels[i].setText(ingrediente.getNombreCorto());
			arrayTextFields[i].setText(String.valueOf(aux.getCantidad()));
		}
	}
	
	
	private void muestraPedido() {
		ArrayList<IngredientePedido> listaIngredientes = pedidoActual.getIngredientesPedido();
		int numeroItems = listaIngredientes.size();
		limpiaPantalla();
		arrayLabels = new JLabel[numeroItems];
		arrayTextFields = new JTextField[numeroItems];
		generaLabels(numeroItems);
		generaTextFields(numeroItems);
		rellenaLabelsTextFields(listaIngredientes);
		textFieldEstado.setText(pedidoActual.getEstado());
		panel.setPreferredSize(new Dimension(480,calculaEspacioVertical(numeroItems)));
	}
	
	private void limpiaPantalla() {
		if(arrayLabels!=null) {
			for(int i=0;i<arrayLabels.length;i++) {
				if(arrayLabels[i]!=null) {
					arrayLabels[i].setVisible(false);
				}
			}
		}
		if(arrayTextFields!=null) {
			for(int i=0;i<arrayTextFields.length;i++) {
				if(arrayTextFields[i]!=null) {
					arrayTextFields[i].setVisible(false);
				}
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		JButton aux = (JButton) e.getSource();
		if(btnMostrar==aux) {
			int indice = comboBox.getSelectedIndex();
			pedidoActual = listaPedidos.get(indice);
			muestraPedido();
		}
		if(btnEntregado==aux) {
			if(pedidoActual!=null) {
				pedidoActual.setEstado("Entregado");
				textFieldEstado.setText("Entregado");
			}
		}
		if(btnOk==aux) {
			dispose();
		}
		if(btnAyuda==aux) {
			GUIAyuda.muestraAyuda(21);
		}
	}
}
