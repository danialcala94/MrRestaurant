package interfazGrafica.cocina;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.fabric.FabricCommunicationException;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.cocina.Plato;

import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JScrollPane;

public class GUIVentanaReceta extends JDialog {

	private JPanel contentPane;
	private JTextArea textArea;
	private JComboBox comboBox;
	private ArrayList<Plato> listaPlatos = FachadaPersistencia.obtenerTodos_Platos();
	private JButton btnAyuda;
	
	/**
	 * Create the frame.
	 */
	public GUIVentanaReceta() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.setTitle("Receta");
		this.setResizable(false);
		setBounds(100, 100, 513, 452);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnOkReceta = new JButton("OK");
		btnOkReceta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnOkReceta.setBounds(390, 387, 97, 25);
		contentPane.add(btnOkReceta);
		
		comboBox = new JComboBox(addNombresPlatos());
		comboBox.setBounds(93, 11, 131, 20);
		contentPane.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 42, 477, 324);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				muestraReceta();
			}
		});
		btnMostrar.setBounds(287, 10, 89, 23);
		contentPane.add(btnMostrar);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUIAyuda.muestraAyuda(20);
			}
		});
		btnAyuda.setBounds(10, 388, 89, 23);
		contentPane.add(btnAyuda);
	}
	
	private String[] addNombresPlatos() {
		String[] nombrePlatos = new String[listaPlatos.size()];
		for(int i=0; i<listaPlatos.size();i++) {
			nombrePlatos[i] = listaPlatos.get(i).getNombreCorto();
		}
		return nombrePlatos;
	}
	
	private void muestraReceta() {
		int indice = comboBox.getSelectedIndex();
		Plato plato = listaPlatos.get(indice);
		String receta = plato.getReceta();
		textArea.setText(receta);
	}
}
