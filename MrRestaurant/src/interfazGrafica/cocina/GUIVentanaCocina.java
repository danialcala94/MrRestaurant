package interfazGrafica.cocina;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfazGrafica.GUIAyuda;
import interfazGrafica.GUIVentanaLogin;
import restaurante.Restaurante;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;
import restaurante.empleados.Empleado;
import restaurante.sala.Comanda;
import restaurante.sala.Mesa;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Toolkit;

public class GUIVentanaCocina extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JPanel panel;
	private int numElementos;
	private JButton[] arrayBotones;
	private JTextArea textoComanda;
	private boolean isJefe;
	private Empleado empleado;
	private Mesa[] listaMesas;
	private	JButton btnReceta;
	private JButton btnHacerInventario = new JButton("Inventario");
	private JButton btnAyuda;
	private JButton btnActualizar;
	private JButton btnPedidos;
	


	/**
	 * Create the frame.
	 */
	public GUIVentanaCocina(boolean isJefe, Empleado empleado) {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		this.isJefe=isJefe;
		this.setResizable(false);
		this.empleado=empleado;
		String cargo = "Cocinero";
		if(isJefe) {
			cargo = "Jefe de cocina";
		}
		setTitle("Ventana cocina:   "+cargo+"   "+empleado.getNombre()+" ("+empleado.getDni()+")");
		//TODO
		//Aun no se ha implementado las funciones del jefe de cocina
		listaMesas = Restaurante.getRestaurante().getMesas();
		numElementos=listaMesas.length;
		arrayBotones = new JButton[numElementos];
		
		
		int espacioVertical = calculaEspacioVertical(numElementos);
		
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 132, 288);
		contentPane.add(scrollPane);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(100, espacioVertical));
		
		btnReceta = new JButton("Recetas");
		if(isJefe) {
			btnReceta.setBounds(54, 310, 90, 23);	
		}else {
			btnReceta.setBounds(180, 310, 90, 23);
		}
		contentPane.add(btnReceta);
		btnReceta.addActionListener(this);
		
		
		btnHacerInventario.setBounds(324, 310, 90, 23);
		if(isJefe) {
			btnHacerInventario.setVisible(true);
		}else {
			btnHacerInventario.setVisible(false);
		}
		contentPane.add(btnHacerInventario);
		btnHacerInventario.addActionListener(this);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(152, 11, 303, 288);
		contentPane.add(scrollPane_1);
		
		textoComanda = new JTextArea();
		scrollPane_1.setViewportView(textoComanda);
		
		btnActualizar = new JButton("Actualizar Ventana ");
		btnActualizar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnActualizar.addActionListener(this);
		btnActualizar.setBounds(10, 344, 208, 42);
		contentPane.add(btnActualizar);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.addActionListener(this);
		btnAyuda.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnAyuda.setBounds(247, 344, 208, 42);
		contentPane.add(btnAyuda);
		
		btnPedidos = new JButton("Pedidos");
		btnPedidos.setBounds(186, 310, 89, 23);
		btnPedidos.addActionListener(this);
		if(isJefe) {
			btnPedidos.setVisible(true);
		}else {
			btnPedidos.setVisible(false);
		}
		contentPane.add(btnPedidos);
		
		generaBotones(numElementos);
		actualizar();
	}
	
	private int calculaEspacioVertical(int numElementos){
		int tam;
		int tamOriginal = 280;
		if(numElementos<=8) {	//Se calcula a ojo que 10 elementos entran bien separados entre ellos 35 pixeles
			tam=tamOriginal;			//319 es el tama�o del scroll pane para que no se muestren los scrolls
		}else{
			int excedente = numElementos-8;
			tam = tamOriginal+excedente*35;	//Los textos est�n a 35 pixeles cada uno del siguiente
		}
		return tam;
	}
	
	private void generaBotones(int num) {
		for(int i=0;i<num;i++) {
			JButton btn = new JButton("Mesa "+(i+1));
			btn.addActionListener(this);
			btn.setBounds(10, 5+(i*35), 100, 23);	//Calcula la posicion que deberia ocupar ese label en el JPanel
			panel.add(btn);
			arrayBotones[i] = btn;
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		JButton aux = (JButton) e.getSource();
		if(btnPedidos==aux) {
			System.out.println("Mostrar la ventana de los pedidos");
			GUIPedidos ventPedidos = new GUIPedidos();
			ventPedidos.setModal(true);
			ventPedidos.setVisible(true);
		}
		if(btnActualizar == aux) {
			actualizar();
		}
		if(btnAyuda == aux) {
			if(isJefe) {
				GUIAyuda.muestraAyuda(18);
			}else {
				GUIAyuda.muestraAyuda(17);
			}
		}
		if(btnReceta == aux) {
			GUIVentanaReceta ventReceta = new GUIVentanaReceta();
			ventReceta.setModal(true);
			ventReceta.setVisible(true);
		}
		if(btnHacerInventario == aux) {
			GUIVentanaInventario ventInventario = new GUIVentanaInventario();
			ventInventario.setModal(true);
			ventInventario.setVisible(true);
		}
		for(int i=0; i<numElementos;i++) {
			if(arrayBotones[i] == aux) {
				int numMesa = Integer.parseInt(aux.getText().replaceFirst("Mesa ", ""))-1;
				System.out.println("El numero de la mesa es: "+numMesa+" (Mesa "+(numMesa+1)+")");
				muestraComanda(numMesa);
			}
		}
	}
	
	private void muestraComanda(int numMesa) {
		textoComanda.setText(formateaTextoComanda(buscaMesa(numMesa).getComanda()));
	}
	
	private Mesa buscaMesa(int numMesa) {
		return Restaurante.getRestaurante().getMesas()[numMesa];
	}
	
	private String formateaTextoComanda(Comanda comanda) {
		String texto="";
		if(comanda == null) {
			texto = "Esta mesa todavia no ha pedido ningun plato";
		}else {
			ArrayList<ComandaPlato> aux = comanda.getComandaPlatos();
			for(int i=0;i<aux.size();i++) {
				ComandaPlato auxComPla = aux.get(i);
				Plato auxPlato = auxComPla.getPlato();
				texto += auxComPla.getCantidad() + "\t" + auxPlato.getNombreCorto()+"\t"+auxComPla.getComentario()+"\n";
			}
		}
		return texto;
	}
	
	private void actualizar() {
		textoComanda.setText("");
		for(int i=0; i<numElementos; i++) {
			if(listaMesas[i].getComanda()==null) {
				arrayBotones[i].setEnabled(false);
			}else {
				arrayBotones[i].setEnabled(true);
			}
		}
	}
}
