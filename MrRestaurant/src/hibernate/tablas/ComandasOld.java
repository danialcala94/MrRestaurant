package hibernate.tablas;
// Generated 04-feb-2018 16:01:10 by Hibernate Tools 5.1.0.Alpha1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ComandasOld generated by hbm2java
 */
@Entity
@Table(name="comandas_old")
public class ComandasOld implements java.io.Serializable {

	@Id
	@Column(name="idComanda")
	private Long idComanda;
	@Column(name="idMesa")
	private long idMesa;
	@Column(name="fecha")
	private Date fecha;
	@Column(name="numeroClientes")
	private int numeroClientes;

	public ComandasOld() {
	}

	public ComandasOld(long idComanda, long idMesa, Date fecha, int numeroClientes) {
		this.idComanda = idComanda;
		this.idMesa = idMesa;
		this.fecha = fecha;
		this.numeroClientes = numeroClientes;
	}

	public long getIdComanda() {
		return this.idComanda;
	}

	public void setIdComanda(long idComanda) {
		this.idComanda = idComanda;
	}

	public long getIdMesa() {
		return this.idMesa;
	}

	public void setIdMesa(long idMesa) {
		this.idMesa = idMesa;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getNumeroClientes() {
		return this.numeroClientes;
	}

	public void setNumeroClientes(int numeroClientes) {
		this.numeroClientes = numeroClientes;
	}

}
