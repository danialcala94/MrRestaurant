package hibernate.tablas;
// Generated 04-feb-2018 16:01:10 by Hibernate Tools 5.1.0.Alpha1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ComandasplatosOld generated by hbm2java
 */
@Entity
@Table(name="comandasplatos_old")
public class ComandasplatosOld implements java.io.Serializable {

	@Id
	@Column(name="idComandaPlato")
	private Long idComandaPlato;
	@Column(name="idComanda")
	private long idComanda;
	@Column(name="cantidad")
	private int cantidad;
	@Column(name="idPlato")
	private long idPlato;
	@Column(name="precio")
	private double precio;
	@Column(name="comentario")
	private String comentario;

	public ComandasplatosOld() {
	}

	public ComandasplatosOld(long idComandaPlato, long idComanda, int cantidad, long idPlato, double precio) {
		this.idComandaPlato = idComandaPlato;
		this.idComanda = idComanda;
		this.cantidad = cantidad;
		this.idPlato = idPlato;
		this.precio = precio;
	}

	public ComandasplatosOld(long idComandaPlato, long idComanda, int cantidad, long idPlato, double precio,
			String comentario) {
		this.idComandaPlato = idComandaPlato;
		this.idComanda = idComanda;
		this.cantidad = cantidad;
		this.idPlato = idPlato;
		this.precio = precio;
		this.comentario = comentario;
	}

	public long getIdComandaPlato() {
		return this.idComandaPlato;
	}

	public void setIdComandaPlato(long idComandaPlato) {
		this.idComandaPlato = idComandaPlato;
	}

	public long getIdComanda() {
		return this.idComanda;
	}

	public void setIdComanda(long idComanda) {
		this.idComanda = idComanda;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public long getIdPlato() {
		return this.idPlato;
	}

	public void setIdPlato(long idPlato) {
		this.idPlato = idPlato;
	}

	public double getPrecio() {
		return this.precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
