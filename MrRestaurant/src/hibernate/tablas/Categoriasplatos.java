package hibernate.tablas;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// Generated 04-feb-2018 16:01:10 by Hibernate Tools 5.1.0.Alpha1

/**
 * Categoriasplatos generated by hbm2java
 */
@Entity
@Table(name="categoriasplatos")
public class Categoriasplatos implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="idCategoriaPlato")
	private Long idCategoriaPlato;
	@Column(name="nombre")
	private String nombre;
	@Column(name="descripcion")
	private String descripcion;

	public Categoriasplatos() {
	}

	public Categoriasplatos(String nombre, String descripcion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Long getIdCategoriaPlato() {
		return this.idCategoriaPlato;
	}

	public void setIdCategoriaPlato(Long idCategoriaPlato) {
		this.idCategoriaPlato = idCategoriaPlato;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
