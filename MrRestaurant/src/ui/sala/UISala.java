package ui.sala;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import interfazGrafica.GUIVentanaLogin;
import interfazGrafica.JPanelImage;
import persistencia.FachadaPersistencia;
import restaurante.Constantes;
import restaurante.empleados.NivelCredencial;
import restaurante.sala.Mesa;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

import eventos.sala.AddNuevaMesaMouseListener;
import eventos.sala.MesaSalaMouseListener;
import java.awt.ScrollPane;

public class UISala extends JFrame {
	
	int idCredencial = -3;
	NivelCredencial nivelCredencial = null;
	
	JButton btnGestionarSala = new JButton();
	JButton btnPGS_Confirmar = new JButton();
	JButton btnPGS_Cancelar = new JButton();
	
	public static final int JPANELIMAGE_ANCHO = 801;
	public static final int JPANELIMAGE_ALTO = 600;
	public static final int MESA_ANCHOyALTO = 64;
	
	// Objetos de interfaz
	JPanelImage salonComedor = new JPanelImage(new ImageIcon(UISala.class.getResource("/recursos/sala/fondo_salon.png")).getImage());
	private JPanelImage salonComedor_1;
	ArrayList<MesaSalaMouseListener> mesasListeners = new ArrayList<MesaSalaMouseListener>();
	ArrayList<JLabel> mesas = new ArrayList<JLabel>();
	ArrayList<JLabel> itemsPGS = new ArrayList<JLabel>();
	ArrayList<JLabel> itemsSalon = new ArrayList<JLabel>();
	
	// Modificaci�n de sala
	boolean modificandoSala = false;
	
	public UISala(int idCredencial) {
		this.idCredencial = idCredencial;
		this.nivelCredencial = FachadaPersistencia.obtener_NivelCredencial_ByidNivelCredencial(idCredencial);
		setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setTitle("MRRestaurant - SAL�N COMEDOR");
		
		comprobarNivelAcceso();
		
		
		getContentPane().setLayout(null);
		setVisible(true);
		setBounds(0, 0, Constantes.WINDOW_FULLHD_1920_ANCHO, Constantes.WINDOW_FULLHD_1080_ALTO);
		
		initSala();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(211, 211, 211));
		panel.setBounds(801, 0, 1105, 600);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnGestionarSala = new JButton("Modificar sala");
		btnGestionarSala.setIcon(Constantes.ICONO_AJUSTES);
		btnGestionarSala.setMargin(Constantes.INSETS);
		btnGestionarSala.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (modificandoSala)
					desactivarGestionSala();
				else
					activarGestionSala();
			}
		});
		btnGestionarSala.setBounds(69, 577, 160, 23);
		panel.add(btnGestionarSala);
		
		
		initPanelGestionarSala();
		
		
		for (int i = 0; i < mesas.size(); i++) {
			salonComedor_1.add(mesas.get(i));
			MesaSalaMouseListener mesaSalaMouseListener = new MesaSalaMouseListener(mesas.get(i), this.mesas);
			mesas.get(i).addMouseListener(mesaSalaMouseListener);
			mesas.get(i).addMouseMotionListener(mesaSalaMouseListener);
			mesasListeners.add(mesaSalaMouseListener);
		}
		
	}
	
	private void comprobarNivelAcceso() {
		switch (this.idCredencial) {
		case Constantes.NIVELESEMPLEADOS_COCINERO:
			System.out.println("Construir para Cocinero");
			break;
		case Constantes.NIVELESEMPLEADOS_JEFEDECOCINA:
			System.out.println("Construir para Jefe de Cocina");
			break;
		case Constantes.NIVELESEMPLEADOS_CAMARERO:
			System.out.println("Construir para Camarero");
			break;
		case Constantes.NIVELESEMPLEADOS_JEFEDESALA:
			System.out.println("Construir para Jefe de Sala");
			break;
		case Constantes.NIVELESEMPLEADOS_GERENTE:
			System.out.println("Construir para Gerente");
			break;
		default:
			System.out.println("Error");
		}
	}
	
	public void addNuevaMesa(JLabel nuevaMesa) {
		mesas.add(nuevaMesa);
		MesaSalaMouseListener mesaSalaMouseListener = new MesaSalaMouseListener(nuevaMesa, this.mesas);
		mesaSalaMouseListener.activarModificacion();
		mesas.get(mesas.size() - 1).addMouseListener(mesaSalaMouseListener);
		mesas.get(mesas.size() - 1).addMouseMotionListener(mesaSalaMouseListener);
		mesasListeners.add(mesaSalaMouseListener);
		salonComedor_1.add(mesas.get(mesas.size() - 1));
		//repaint();
	}
	
	private void initSala() {
		// Se colocar� la sala en funci�n de los datos almacenados en bd con objetos, coordenadas y tipo de objetos.
		colocarMesas();
		
		// Mesas de prueba
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(Constantes.ICONO_MESA_OCUPADA);
		lblNewLabel.setBounds(35, 26, MESA_ANCHOyALTO, MESA_ANCHOyALTO);
		mesas.add(lblNewLabel);
		JLabel lblNewLabel1 = new JLabel("");
		lblNewLabel1.setIcon(Constantes.ICONO_MESA_LIBRE);
		lblNewLabel1.setBounds(135, 26, MESA_ANCHOyALTO, MESA_ANCHOyALTO);
		mesas.add(lblNewLabel1);
		JLabel lblNewLabel2 = new JLabel("");
		lblNewLabel2.setIcon(Constantes.ICONO_MESA_RESERVADA);
		lblNewLabel2.setBounds(235, 26, MESA_ANCHOyALTO, MESA_ANCHOyALTO);
		mesas.add(lblNewLabel2);
		JLabel lblNewLabel3 = new JLabel("");
		lblNewLabel3.setIcon(Constantes.ICONO_MESA_APREPARAR);
		lblNewLabel3.setBounds(335, 26, MESA_ANCHOyALTO, MESA_ANCHOyALTO);
		mesas.add(lblNewLabel3);
		
		itemsSalon.add(lblNewLabel); itemsSalon.add(lblNewLabel1); itemsSalon.add(lblNewLabel2); itemsSalon.add(lblNewLabel3);
		
		salonComedor_1 = new JPanelImage(new ImageIcon(UISala.class.getResource("/recursos/sala/fondo_salon.png")).getImage());
		salonComedor_1.setToolTipText("Sal\u00F3n comedor");
		//salonComedor.setBackground(new Color(244, 164, 96));
		salonComedor_1.setBounds(0, 0, 870, 600);
		getContentPane().add(salonComedor_1);
		salonComedor_1.setLayout(null);
	}
	
	/**
	 * Inicializa los componentes del panel de Gesti�n de sala que se activar� para modificar el sal�n (solo el gerente)
	 */
	private void initPanelGestionarSala() {
		
		// Panel lateral Gesti�n sala (PGS)
		JLabel lblPGS_Barra = new JLabel("");
		salonComedor_1.add(lblPGS_Barra);
		lblPGS_Barra.setHorizontalAlignment(SwingConstants.CENTER);
		lblPGS_Barra.setIcon(Constantes.ICONO_BARRA_CENTRO);
		lblPGS_Barra.setBounds(salonComedor_1.getWidth() - (64 - lblPGS_Barra.getIcon().getIconWidth() - (lblPGS_Barra.getIcon().getIconWidth() / 2)),
				0*64 + (64 - lblPGS_Barra.getIcon().getIconHeight() * 2 + (lblPGS_Barra.getIcon().getIconHeight() / 2)), 
				lblPGS_Barra.getIcon().getIconWidth(), lblPGS_Barra.getIcon().getIconHeight());
		itemsPGS.add(lblPGS_Barra);
		AddNuevaMesaMouseListener addNuevaMesaMouseListener = new AddNuevaMesaMouseListener(this, lblPGS_Barra, mesas);
		lblPGS_Barra.addMouseMotionListener(addNuevaMesaMouseListener);
		lblPGS_Barra.addMouseListener(addNuevaMesaMouseListener);
		JLabel lblPGS_Mesa = new JLabel("");
		salonComedor_1.add(lblPGS_Mesa);
		lblPGS_Mesa.setBounds(salonComedor_1.getWidth() - 64, 1*64, 64, 64);
		itemsPGS.add(lblPGS_Mesa);
		lblPGS_Mesa.setIcon(Constantes.ICONO_MESA_RESERVADA);
		addNuevaMesaMouseListener = new AddNuevaMesaMouseListener(this, lblPGS_Mesa, mesas);
		lblPGS_Mesa.addMouseMotionListener(addNuevaMesaMouseListener);
		lblPGS_Mesa.addMouseListener(addNuevaMesaMouseListener);
		JLabel lblPGS_Taburete = new JLabel("");
		salonComedor_1.add(lblPGS_Taburete);
		lblPGS_Taburete.setHorizontalAlignment(SwingConstants.CENTER);
		lblPGS_Taburete.setIcon(Constantes.ICONO_TABURETE_RESERVADO);
		lblPGS_Taburete.setBounds(salonComedor_1.getWidth() - (64 - lblPGS_Taburete.getIcon().getIconWidth() - (lblPGS_Taburete.getIcon().getIconWidth() / 2)),
				2*64 + (lblPGS_Taburete.getIcon().getIconHeight() * 2 + (lblPGS_Taburete.getIcon().getIconHeight() / 2)), 
				lblPGS_Taburete.getIcon().getIconWidth(), lblPGS_Taburete.getIcon().getIconHeight());
		itemsPGS.add(lblPGS_Taburete);
		addNuevaMesaMouseListener = new AddNuevaMesaMouseListener(this, lblPGS_Taburete, mesas);
		lblPGS_Taburete.addMouseMotionListener(addNuevaMesaMouseListener);
		lblPGS_Taburete.addMouseListener(addNuevaMesaMouseListener);
		JLabel lblPGS_Texto = new JLabel("Texto");
		salonComedor_1.add(lblPGS_Texto);
		lblPGS_Texto.setHorizontalAlignment(SwingConstants.CENTER);
		lblPGS_Texto.setBounds(salonComedor_1.getWidth() - 64, 3*64, 64, 64);
		itemsPGS.add(lblPGS_Texto);
		JLabel lblPGS_Columna = new JLabel("Columna");
		salonComedor_1.add(lblPGS_Columna);
		lblPGS_Columna.setHorizontalAlignment(SwingConstants.CENTER);
		lblPGS_Columna.setBounds(salonComedor_1.getWidth() - 64, 4*64, 64, 64);
		itemsPGS.add(lblPGS_Columna);
		JLabel lblPGS_Recuadro = new JLabel("Recuadro");
		salonComedor_1.add(lblPGS_Recuadro);
		lblPGS_Recuadro.setHorizontalAlignment(SwingConstants.CENTER);
		lblPGS_Recuadro.setBounds(salonComedor_1.getWidth() - 64, 5*64, 64, 64);
		itemsPGS.add(lblPGS_Recuadro);
		JLabel lblPGS_Fondo = new JLabel("");
		lblPGS_Fondo.setBackground(new Color(240, 230, 140));
		lblPGS_Fondo.setBounds(801, 0, 69, 600);
		lblPGS_Fondo.setOpaque(true);
		salonComedor_1.add(lblPGS_Fondo);
		itemsPGS.add(lblPGS_Fondo);
		
		/*int y = 0;
		for (int i = 0; i < itemsPGS.size(); i++) {
			if (itemsPGS.get(i).getIcon().getIconWidth() == 16) {
				itemsPGS.get(i).setBounds(salonComedor_1.getWidth() - 64 + 24,
						y + 24, itemsPGS.get(i).getIcon().getIconWidth(), itemsPGS.get(i).getIcon().getIconHeight());
			} else if (itemsPGS.get(i).getIcon().getIconWidth() == 64) {
				itemsPGS.get(i).setBounds(salonComedor_1.getWidth() - 64, y,
						itemsPGS.get(i).getIcon().getIconWidth(), itemsPGS.get(i).getIcon().getIconHeight());
			}
			y += 64;
		}*/
		
		
		for (int i = 0; i < itemsPGS.size(); i++)
			itemsPGS.get(i).setVisible(false);
				
		/*ArrayList<JButton> botones = new ArrayList<JButton>();
		
		btnPGS_Confirmar = new JButton("Confirmar");
		btnPGS_Confirmar.setIcon(Constantes.ICONO_CORRECTO);
		btnPGS_Confirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.err.println("Se deben producir los cambios que hayan acontecido en la sala y volver al estado general.");
				panel_GestionarSala.setVisible(false);
			}
		});
		btnPGS_Cancelar = new JButton("Cancelar");
		btnPGS_Cancelar.setIcon(Constantes.ICONO_INCORRECTO);
		btnPGS_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.err.println("Se debe volver al estado general no produciendo ning�n cambio.");
				panel_GestionarSala.setVisible(false);
			}
		});
		JButton papas = new JButton("asas");
		papas.setIcon(Constantes.ICONO_INCORRECTO);
		papas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.err.println("Se debe volver al estado general no produciendo ning�n cambio.");
				panel_GestionarSala.setVisible(false);
			}
		});
		botones.add(btnPGS_Confirmar);
		botones.add(btnPGS_Cancelar);
		botones.add(papas);
		
		// Tratar todo lo dem�s antes de a�adirlo al panel
		
		int x = 0; int y = 0;
		for (int i = 0; i < botones.size(); i++) {
			panel_GestionarSala.add(botones.get(i));
			botones.get(i).setBounds(x, y, Constantes.BOTONES_ANCHO, Constantes.BOTONES_ALTO);
			x += Constantes.BOTONES_ANCHO;
			if (i+1 % 3 == 0 && i != 0)
				y += Constantes.BOTONES_ALTO;
		}*/
	}
	
	private void activarGestionSala() {
		btnGestionarSala.setText("Dejar de modificar sala");
		for (int i = 0; i < itemsPGS.size(); i++) {
			itemsPGS.get(i).setVisible(true);
		}
		for (int i = 0; i < mesasListeners.size(); i++)
			mesasListeners.get(i).activarModificacion();
		modificandoSala = true;
	}
	
	private void desactivarGestionSala() {
		btnGestionarSala.setText("Modificar sala");
		for (int i = 0; i < itemsPGS.size(); i++)
			itemsPGS.get(i).setVisible(false);
		for (int i = 0; i < mesasListeners.size(); i++)
			mesasListeners.get(i).desactivarModificacion();
		modificandoSala = false;
	}
	
	/**
	 * Obtiene el n�mero de mesas existentes en el restaurante y las coloca en el sal�n en las coordenadas definidas tambi�n en la base de datos.
	 */
	private void colocarMesas() {
		ArrayList<Mesa> mesas = FachadaPersistencia.obtenerTodos_Mesas();
		for (int i = 0; i < mesas.size(); i++) {
			JLabel lbl_Mesa = new JLabel();
			lbl_Mesa.setIcon(Constantes.ICONO_MESA_LIBRE);
			lbl_Mesa.setBounds(mesas.get(i).getCoordX(), mesas.get(i).getCoordY(), MESA_ANCHOyALTO, MESA_ANCHOyALTO);
		}
	}
}
