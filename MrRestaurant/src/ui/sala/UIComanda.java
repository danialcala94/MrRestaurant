package ui.sala;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.utils.HibernateUtils;
import interfazGrafica.GUIVentanaLogin;
import persistencia.FachadaPersistencia;
import restaurante.Constantes;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JList;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JTextArea;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class UIComanda extends JFrame {
	
	int lastGridY = 0;
	final Insets insets = new Insets(0, 0, 0, 0);
	
	// Arrays para mantener la relaci�n de platos en comanda y poder actualizarla din�micamente
	ArrayList<JLabel> numeros = new ArrayList<JLabel>();
	ArrayList<GridBagConstraints> numeros_gbc = new ArrayList<GridBagConstraints>();
	ArrayList<JLabel> platos = new ArrayList<JLabel>();
	ArrayList<GridBagConstraints> platos_gbc = new ArrayList<GridBagConstraints>();
	ArrayList<JLabel> precios = new ArrayList<JLabel>();
	ArrayList<GridBagConstraints> precios_gbc = new ArrayList<GridBagConstraints>();
	ArrayList<JLabel> comentarios = new ArrayList<JLabel>();
	ArrayList<GridBagConstraints> comentarios_gbc = new ArrayList<GridBagConstraints>();
	ArrayList<Integer> idsPlatos = new ArrayList<Integer>();
	
	JPanel panel = new JPanel();
	
	public UIComanda() {
		getContentPane().setLayout(null);
		setVisible(true);
		setSize(312, 532);
		setIconImage(Toolkit.getDefaultToolkit().getImage(GUIVentanaLogin.class.getResource("/recursos/mrrestaurant_logo.png")));
		setTitle("MRRestaurant - COMANDA");
		
		setBounds(Constantes.WINDOW_FULLHD_1920_ANCHO / 2 - Constantes.WINDOW_COMANDA_ANCHO / 2,
				Constantes.WINDOW_FULLHD_1080_ALTO / 2 - Constantes.WINDOW_COMANDA_ALTO / 2,
				Constantes.WINDOW_COMANDA_ANCHO, Constantes.WINDOW_COMANDA_ALTO);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 296, 2000);
		panel.setBackground(new Color(255, 204, 153));
		panel.setOpaque(true);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{20, 100, 40};
		gbl_panel.rowHeights = new int[]{18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18};
		gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblCabecera_Numero = new JLabel("N\u00BA");
		GridBagConstraints gbc_lblCabecera_Numero = new GridBagConstraints();
		gbc_lblCabecera_Numero.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Numero.insets = insets;
		gbc_lblCabecera_Numero.gridx = 0;
		gbc_lblCabecera_Numero.gridy = 0;
		
		panel.add(lblCabecera_Numero, gbc_lblCabecera_Numero);
		lblCabecera_Numero.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Numero.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Numero.setBackground(new Color(255, 153, 0));
		lblCabecera_Numero.setOpaque(true);
		
		JLabel lblCabecera_Plato = new JLabel("Nombre del plato");
		lblCabecera_Plato.setOpaque(true);
		lblCabecera_Plato.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Plato.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Plato.setBackground(new Color(255, 204, 51));
		GridBagConstraints gbc_lblCabecera_Plato = new GridBagConstraints();
		gbc_lblCabecera_Plato.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Plato.insets = insets;
		gbc_lblCabecera_Plato.gridx = 1;
		gbc_lblCabecera_Plato.gridy = 0;
		panel.add(lblCabecera_Plato, gbc_lblCabecera_Plato);
		
		JLabel lblCabecera_Precio = new JLabel("Precio");
		lblCabecera_Precio.setOpaque(true);
		lblCabecera_Precio.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Precio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Precio.setBackground(new Color(255, 153, 0));
		GridBagConstraints gbc_lblCabecera_Precio = new GridBagConstraints();
		gbc_lblCabecera_Precio.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Precio.insets = insets;
		gbc_lblCabecera_Precio.gridx = 2;
		gbc_lblCabecera_Precio.gridy = 0;
		panel.add(lblCabecera_Precio, gbc_lblCabecera_Precio);
		
		
		// PLATOS
		
		// Creamos platos a partir de ComandaPlato
		/*Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		hibernate.tablas.Comandasplatos cp = session.get(hibernate.tablas.Comandasplatos.class, 3L);
		tx.commit();
		session.close();
		Hibernate me provee un objeto del tipo hibernate.tablas.Comandasplatos. Tengo que reprogramar el conversor Hibernate - mis objetos
		para que funcionen correctamente acorde al c�digo utilizado. */
		
	}
	
	/**
	 * A�ade a la interfaz de la comanda la ComandaPlato recibida por par�metro (esta ComandaPlato incluye cantidad, plato y precio).
	 * @param plato
	 */
	private void addPlato(ComandaPlato plato) {
		if (plato != null) {
			int indicePlato = obtenerPosicionPlatoEnComanda(plato.getPlato().getIDPlato());
			// Si el plato no est� todav�a en la comanda
			if (indicePlato == -3) {
				lastGridY++;
				JLabel lblNumero_prov = new JLabel(Integer.toString(plato.getCantidad()));
				JLabel lblPlato_prov = new JLabel(plato.getPlato().getNombreCorto());
				JLabel lblPrecio_prov = new JLabel(Float.toString(plato.getPrecio()) + " �");
				lblNumero_prov.setHorizontalAlignment(SwingConstants.CENTER);
				lblPlato_prov.setHorizontalAlignment(SwingConstants.CENTER);
				lblPrecio_prov.setHorizontalAlignment(SwingConstants.CENTER);
				
				// A�adimos los GBCs a los arrays
				numeros_gbc.add(obtenerConstantes(0, lastGridY));
				platos_gbc.add(obtenerConstantes(1, lastGridY));
				precios_gbc.add(obtenerConstantes(2, lastGridY));
				
				panel.add(lblNumero_prov, numeros_gbc.get(numeros_gbc.size() - 1));
				panel.add(lblPlato_prov, platos_gbc.get(platos_gbc.size() - 1));
				panel.add(lblPrecio_prov, precios_gbc.get(precios_gbc.size() - 1));
				
				numeros.add(lblNumero_prov);
				platos.add(lblPlato_prov);
				precios.add(lblPrecio_prov);
				idsPlatos.add(plato.getPlato().getIDPlato());
				
				// Si tiene comentario, a�adiremos una l�nea m�s, debajo del nombre del plato, dejando el n�mero y el precio vac�os a ambos lados.
				if (!plato.getComentario().isEmpty()) {
					System.out.println("Tenemos que a�adir comentario.");
					lastGridY++;
					JLabel lblComentario_prov = new JLabel(plato.getComentario());
					lblComentario_prov.setHorizontalAlignment(SwingConstants.CENTER);
					lblComentario_prov.setFont(new Font("Tahoma", Font.ITALIC, 11));
					comentarios_gbc.add(obtenerConstantes(1, lastGridY));
					panel.add(lblComentario_prov, comentarios_gbc.get(comentarios_gbc.size() - 1));
					comentarios.add(lblComentario_prov);
				} else {
					comentarios_gbc.add(null);
					comentarios.add(null);
				}
			} else {
				// Si el plato ya est� en la comanda, aumentaremos el n�mero y, por tanto, el precio, en la l�nea correspondiente
				int num = Integer.parseInt(numeros.get(indicePlato).getText());
				num += plato.getCantidad();
				numeros.get(indicePlato).setText(Integer.toString(num));
				
				float pre = Float.parseFloat(precios.get(indicePlato).getText().replace("�", ""));
				pre += plato.getPrecio();
				precios.get(indicePlato).setText(Float.toString(pre));
			}
		} else
			System.err.println("El plato introducido no es v�lido.");
	}
	
	/**
	 * Repinta por completo el panel para desplazar hacia arriba los platos si se ha eliminado uno.
	 */
	private void actualizarPanel() {
		panel.removeAll();
		JLabel lblCabecera_Numero = new JLabel("N\u00BA");
		GridBagConstraints gbc_lblCabecera_Numero = new GridBagConstraints();
		gbc_lblCabecera_Numero.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Numero.insets = insets;
		gbc_lblCabecera_Numero.gridx = 0;
		gbc_lblCabecera_Numero.gridy = 0;
		
		panel.add(lblCabecera_Numero, gbc_lblCabecera_Numero);
		lblCabecera_Numero.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Numero.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Numero.setBackground(new Color(255, 153, 0));
		lblCabecera_Numero.setOpaque(true);
		
		JLabel lblCabecera_Plato = new JLabel("Nombre del plato");
		lblCabecera_Plato.setOpaque(true);
		lblCabecera_Plato.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Plato.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Plato.setBackground(new Color(255, 204, 51));
		GridBagConstraints gbc_lblCabecera_Plato = new GridBagConstraints();
		gbc_lblCabecera_Plato.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Plato.insets = insets;
		gbc_lblCabecera_Plato.gridx = 1;
		gbc_lblCabecera_Plato.gridy = 0;
		panel.add(lblCabecera_Plato, gbc_lblCabecera_Plato);
		
		JLabel lblCabecera_Precio = new JLabel("Precio");
		lblCabecera_Precio.setOpaque(true);
		lblCabecera_Precio.setHorizontalAlignment(SwingConstants.CENTER);
		lblCabecera_Precio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCabecera_Precio.setBackground(new Color(255, 153, 0));
		GridBagConstraints gbc_lblCabecera_Precio = new GridBagConstraints();
		gbc_lblCabecera_Precio.fill = GridBagConstraints.BOTH;
		gbc_lblCabecera_Precio.insets = insets;
		gbc_lblCabecera_Precio.gridx = 2;
		gbc_lblCabecera_Precio.gridy = 0;
		panel.add(lblCabecera_Precio, gbc_lblCabecera_Precio);
		
		// Repintar el panel con los datos actualizados
		for (int i = 0; i < numeros.size(); i++) {
			panel.add(numeros.get(i), numeros_gbc.get(i));
			panel.add(platos.get(i), platos_gbc.get(i));
			panel.add(platos.get(i), platos_gbc.get(i));
			if (comentarios.get(i) != null)
				panel.add(comentarios.get(i), comentarios_gbc.get(i));
		}
	}
	
	/**
	 * Si solo hay un plato, lo elimina y los platos de m�s abajo suben en la comanda. Si hay m�s de uno, reduce la cantidad (y el precio).
	 * @param idPlato
	 */
	private void eliminarPlato(int idPlato) {
		int indicePlato = obtenerPosicionPlatoEnComanda(idPlato);
		int difGridY = -3;
		if (indicePlato != -3) {
			if (Integer.parseInt(numeros.get(indicePlato).getText()) == 1) {
				panel.remove(numeros.get(indicePlato));
				numeros.remove(indicePlato);
				numeros_gbc.remove(indicePlato);
				panel.remove(platos.get(indicePlato));
				platos.remove(indicePlato);
				platos_gbc.remove(indicePlato);
				panel.remove(precios.get(indicePlato));
				precios.remove(indicePlato);
				precios_gbc.remove(indicePlato);
				if (comentarios.get(indicePlato) == null)
					difGridY = 1;
				else {
					difGridY = 2;
					panel.remove(comentarios.get(indicePlato));
				}
				comentarios.remove(indicePlato);
				comentarios_gbc.remove(indicePlato);
				idsPlatos.remove(indicePlato);
				for (int i = indicePlato; i < idsPlatos.size(); i++) {
					// Actualizamos los valores de los GBC
					System.out.println("Bajando de " + numeros_gbc.get(i).gridy + " a " + (numeros_gbc.get(i).gridy - difGridY));
					numeros_gbc.get(i).gridy -= difGridY;
					platos_gbc.get(i).gridy -= difGridY;
					precios_gbc.get(i).gridy -= difGridY;
					if (comentarios.get(i) != null)
						comentarios_gbc.get(i).gridy -= difGridY;
					// �Al actualizar esto habr� que cambiarlos? O al estar referenciados en el array cambia todo...
				}
				lastGridY -= difGridY;
				actualizarPanel();
			} else
				numeros.get(indicePlato).setText(Integer.toString((Integer.parseInt(numeros.get(indicePlato).getText()) - 1))); 
		}
	}
	
	/**
	 * Devuelve la posici�n correspondiente al plato, o -3 si no se encuentra a�n en la comanda.
	 * @param idPlato
	 * @return <strong>-3</strong> Si no est� a�n en la comanda | <strong>indicePlato</strong> Si s� lo est�
	 */
	private int obtenerPosicionPlatoEnComanda(int idPlato) {
		for (int i = 0; i < idsPlatos.size(); i++) {
			if (idPlato == idsPlatos.get(i))
				return i;
		}
		return -3;
	}
	
	/**
	 * Asigna los par�metros necesarios para situarlo en el GridBagLayout. Debemos indicarle los �ndices de fila y de columna a utilizar.
	 * @param gridx
	 * @param gridy
	 * @return
	 */
	private GridBagConstraints obtenerConstantes(int gridx, int gridy) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = insets;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = gridx;
		gbc.gridy = gridy;
		return gbc;
	}
}
