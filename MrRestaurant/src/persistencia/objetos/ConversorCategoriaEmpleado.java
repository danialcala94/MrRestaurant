package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Alergeno;
import restaurante.almacen.Proveedor;
import restaurante.empleados.CategoriaEmpleado;

public class ConversorCategoriaEmpleado extends ConversorAbstracto {
	
	private static ConversorCategoriaEmpleado instanciaConversorCategoriaEmpleado;
	
	// Cach�
	private static ArrayList<CategoriaEmpleado> cache = new ArrayList<CategoriaEmpleado>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorCategoriaEmpleado getConversor() {
		if(instanciaConversorCategoriaEmpleado == null) {
			instanciaConversorCategoriaEmpleado = new ConversorCategoriaEmpleado();
		}
		return instanciaConversorCategoriaEmpleado;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaEmpleado categoriaEmpleado = (CategoriaEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaEmpleado.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASEMPLEADOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, categoriaEmpleado.getNombre());
				ps.setString(2, categoriaEmpleado.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + categoriaEmpleado.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + categoriaEmpleado.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaEmpleado.getNombre().isEmpty()");
			return null;
		}
		return categoriaEmpleado;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		CategoriaEmpleado categoriaEmpleado = (CategoriaEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaEmpleado.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			CategoriaEmpleado categoriaEmpleadoAEliminar = FachadaPersistencia.obtener_CategoriaEmpleado_ByidCategoriaEmpleado(categoriaEmpleado.getIDCategoriaEmpleado());
			try {
				// Comprobar que est� en la base de datos
				if (categoriaEmpleadoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASEMPLEADOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, categoriaEmpleadoAEliminar.getIDCategoriaEmpleado());
					ps.setString(2, categoriaEmpleadoAEliminar.getNombre());
					ps.setString(3, categoriaEmpleadoAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_CATEGORIASEMPLEADOS_ByidCategoriaEmpleado);
					ps.setInt(1, categoriaEmpleadoAEliminar.getIDCategoriaEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + categoriaEmpleadoAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					categoriaEmpleado = categoriaEmpleadoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + categoriaEmpleadoAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaEmpleado.getNombre().isEmpty()");
			return null;
		}
		return categoriaEmpleado;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaEmpleado categoriaEmpleado = (CategoriaEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaEmpleado.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				// Tengo que modificarlo
				CategoriaEmpleado categoriaEmpleadoAModificar = FachadaPersistencia.obtener_CategoriaEmpleado_ByidCategoriaEmpleado(categoriaEmpleado.getIDCategoriaEmpleado());
				if (categoriaEmpleadoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_CATEGORIASEMPLEADOS_ByidCategoriaEmpleado);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, categoriaEmpleado.getNombre());
					ps.setString(2, categoriaEmpleado.getDescripcion());
					ps.setInt(3, categoriaEmpleado.getIDCategoriaEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + categoriaEmpleado.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + categoriaEmpleado.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaEmpleado.getNombre().isEmpty()");
			return null;
		}
		return categoriaEmpleado;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		CategoriaEmpleado catEmp = buscarEnCache(id);
		
		if (catEmp == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idCategoriaEmpleado;
			String db_nombre;
			String db_descripcion;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASEMPLEADOS_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaEmpleado = rs.getInt("idCategoriaEmpleado");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hay que sustituir stockAct y stockMin
					catEmp = new CategoriaEmpleado(db_idCategoriaEmpleado, db_nombre, db_descripcion);
					
					addToCache(catEmp);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return catEmp;
	}
	
	@Override
	public synchronized ArrayList<CategoriaEmpleado> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<CategoriaEmpleado> categoriasEmpleados = new ArrayList<CategoriaEmpleado>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idCategoriaEmpleado;
		String db_nombre;
		String db_descripcion;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASEMPLEADOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idCategoriaEmpleado") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<CategoriaEmpleado>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaEmpleado = rs.getInt("idCategoriaEmpleado");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					categoriasEmpleados.add(new CategoriaEmpleado(db_idCategoriaEmpleado, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(categoriasEmpleados.get(categoriasEmpleados.size() - 1));
					if (categoriasEmpleados.get(categoriasEmpleados.size() - 1).getIDCategoriaEmpleado() > ultimaIdUtilizada)
						ultimaIdUtilizada = categoriasEmpleados.get(categoriasEmpleados.size() - 1).getIDCategoriaEmpleado();
				}
				cacheActualizada = true;
			} else
				categoriasEmpleados = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return categoriasEmpleados;
	}
	
	public synchronized ArrayList<CategoriaEmpleado> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(CategoriaEmpleado obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDCategoriaEmpleado() == obj.getIDCategoriaEmpleado()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaEmpleado buscarEnCache(CategoriaEmpleado obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoriaEmpleado() == obj.getIDCategoriaEmpleado()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaEmpleado buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoriaEmpleado() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}