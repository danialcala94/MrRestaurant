package persistencia.objetos;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;
import restaurante.almacen.Proveedor;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;

public class ConversorPlato extends ConversorAbstracto {
	
private static ConversorPlato instanciaConversorPlato;

	/* Nuevo sistema de cach� propuesto:
	 * Cada vez que traigamos un objeto de BD (obtener_X u obtenerTodos_X), lo almacenamos en el array cach� (comprobando previamente si est� o no).
	 * Si a�adimos, modificamos o eliminamos un objeto de BD (a�adir_X, modificar_X o eliminar_X), actualizamos el valor en el array de cach�.
	 * Si llamamos a obtenerTodos_X, para cada objeto tra�do comprobaremos si est� en el array cach�. Si no lo est�, lo a�adimos. Al terminar el m�todo,
	 * 		cacheActualizada = true; pues habremos tra�do todos los elementos de base de datos.
	 * Si seguimos estos pasos, podremos hacer una b�squeda en cach� antes de hacer consultas en BD y ahorrar tiempo de operaciones en disco (m�s lentas).
	 */
	
	// Cach�
	private static ArrayList<Plato> cache = new ArrayList<Plato>();
	
	private static boolean cacheActualizada = false;
	
	// Lista de CATEGORIASPLATOS e INGREDIENTESPLATOS
	private static ArrayList<CategoriaPlato> categoriasPlatos;
	private static ArrayList<IngredientePlato> ingredientesPlatos;
	
	
	public static synchronized ConversorPlato getConversor() {
		if(instanciaConversorPlato == null) {
			instanciaConversorPlato = new ConversorPlato();
		}
		return instanciaConversorPlato;
	}
	
	private ConversorPlato() {}

	/**
	 * El m�todo a�adir a BD no puede buscar en cach� nada, pues tiene que a�adir s� o s� a BD (y a�adir, s� o s�, a cach�).
	 */
	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Plato plato = (Plato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				ResultSet rs = null;
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PLATOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, plato.getNombreLargo());
				ps.setString(2, plato.getNombreCorto());
				ps.setString(3, plato.getDescripcion());
				ps.setDouble(4, plato.getPrecio());
				ps.setInt(5, plato.getCategoria().getIDCategoriaPlato());
				ps.setString(6, plato.getReceta());
				ps.executeUpdate();
				// Tengo que obtener el id con que se guarda para almacenarlo
				ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PLATOS);
				rs = ps.executeQuery();
				rs.last();
				int idPlato = rs.getInt("idPlato");
				
				ArrayList<IngredientePlato> ingredientes = plato.getIngredientes();
				for (int i = 0; i < ingredientes.size(); i++) {
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPLATOS);
					ps.setInt(1, idPlato);
					ps.setInt(2, ingredientes.get(i).getIngrediente().getIDIngrediente());
					ps.setFloat(3, ingredientes.get(i).getCantidad());
					ps.executeUpdate();
				}
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado el plato " + plato.getNombreLargo());
				// Es el m�todo a�adir, con lo cual no pod�a estar en cach�. Lo a�adir� s� o s�.
				almacenarEnCache(plato);
				return plato;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir el plato " + plato.getNombreLargo());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()");
			return null;
		}
	}
	
	/**
	 * El m�todo eliminar de BD, no puede buscar en cach� pues tiene que eliminar s� o s� de BD (y eliminar, por tanto, de cach�).
	 */
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Plato plato = (Plato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (plato.getIDPlato() <= 0 || plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Plato platoAEliminar = FachadaPersistencia.obtener_Plato_ByidPlato(plato.getIDPlato());
			ArrayList<IngredientePlato> ingredientesplatosAEliminar = FachadaPersistencia.obtenerTodos_IngredientesPlatos_ByidPlato(plato.getIDPlato());
			
			try {
				// Tengo que eliminarlo
				if (platoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PLATOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, platoAEliminar.getIDPlato());
					ps.setString(2, platoAEliminar.getNombreLargo());
					ps.setString(3, platoAEliminar.getNombreCorto());
					ps.setString(4, platoAEliminar.getDescripcion());
					ps.setDouble(5, platoAEliminar.getPrecio());
					ps.setInt(6, platoAEliminar.getCategoria().getIDCategoriaPlato());
					ps.setString(7, platoAEliminar.getReceta());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_PLATOS_ByidPlato);
					ps.setInt(1, platoAEliminar.getIDPlato());
					ps.executeUpdate();
					// Guardo los ingredientes en la tabla_old
					for (int i = 0; i < ingredientesplatosAEliminar.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPLATOS_OLD);
						ps.setInt(1, ingredientesplatosAEliminar.get(i).getIDIngredientePlato());
						ps.setInt(2, platoAEliminar.getIDPlato());
						ps.setInt(3, ingredientesplatosAEliminar.get(i).getIngrediente().getIDIngrediente());
						ps.setFloat(4, ingredientesplatosAEliminar.get(i).getCantidad());
						ps.executeUpdate();
					}
					// Los borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_INGREDIENTESPLATOS_ByidPlato);
					ps.setInt(1, platoAEliminar.getIDPlato());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado el plato " + plato.getNombreLargo() + " y sus IngredientePlato relacionados.");
					eliminarEnCache(platoAEliminar);
					return plato;
				} else {
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
					return null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar el plato " + plato.getNombreLargo());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: plato.getIDPlato() <= 0 || plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()");
			return null;
		}
	}

	/**
	 * Al intentar obtener un objeto, se buscar� por id. Se buscar� el identificador en cach�, y si no estuviera se buscar�a en BD y a�adir�a a cach�.
	 */
	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Plato pla = buscarEnCache(id);
		
		if (pla == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idPlato;
			String db_nombreLargo;
			String db_nombre;
			String db_descripcion;
			float db_precioVenta;
			CategoriaPlato db_categoriaPlato;	int db_idCategoriaPlato;
			String db_receta;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				categoriasPlatos = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
				ingredientesPlatos = FachadaPersistencia.obtenerTodos_IngredientesPlatos();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PLATOS_ByidPlato);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idPlato = rs.getInt("idPlato");
					db_nombreLargo = rs.getString("nombreLargo");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					db_precioVenta = (float)rs.getDouble("precioVenta");
					db_idCategoriaPlato = rs.getInt("idCategoriaPlato");
					db_receta = rs.getString("receta");
					
					// Hay que sustituir stockAct y stockMin
					pla = new Plato(db_idPlato, db_nombreLargo, db_nombre, db_descripcion, db_precioVenta, null, buscarEnCacheCategoriasPlatos(db_idCategoriaPlato), db_receta);
				}
				
				pla.setIngredientes(FachadaPersistencia.obtenerTodos_IngredientesPlatos_ByidPlato(pla.getIDPlato()));
				
				almacenarEnCache(pla);
				return pla;
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
				// FachadaPersistencia.desconectar();
				return null;
			}
		}
		return pla;
	}
	
	/**
	 * Si modificamos un objeto, lo vamos a modificar s� o s� en base de datos. A�adiremos el cambio a cach�, o lo a�adiremos cambiado si no estaba a�n.
	 */
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Plato plato = (Plato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (plato.getIDPlato() <= 0 || plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			try {
				Plato platoAModificar = FachadaPersistencia.obtener_Plato_ByidPlato(plato.getIDPlato());
				// Tengo que modificarlo
				if (platoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_PLATOS_ByidPlato);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, plato.getNombreLargo());
					ps.setString(2, plato.getNombreCorto());
					ps.setString(3, plato.getDescripcion());
					ps.setFloat(4, plato.getPrecio());
					ps.setInt(5, plato.getCategoria().getIDCategoriaPlato());
					ps.setString(6, plato.getReceta());
					ps.setInt(7, platoAModificar.getIDPlato());
					ps.executeUpdate();
					// Modificamos los IngredientePlato (eliminamos y creamos los nuevos)
					/* Podr�amos comprobar cu�les se han mantenido y a�adir solo los nuevos */
					// (eliminamos)
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_INGREDIENTESPLATOS_ByidPlato);
					ps.setInt(1, platoAModificar.getIDPlato());
					ps.executeUpdate();
					// (creamos los nuevos)
					ArrayList<IngredientePlato> ingredientes = plato.getIngredientes();
					for (int i = 0; i < ingredientes.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPLATOS);
						ps.setInt(1, platoAModificar.getIDPlato());
						ps.setInt(2, ingredientes.get(i).getIngrediente().getIDIngrediente());
						ps.setFloat(3, ingredientes.get(i).getCantidad());
						ps.executeUpdate();
					}
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado el plato " + plato.getNombreLargo() + " y sus IngredientePlato relacionados.");
					modificarEnCache(plato);
					return plato;
				} else {
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
					return null;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar el plato " + plato.getNombreLargo());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: plato.getIDPlato() <= 0 || plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()");
			return null;
		}
	}
	
	/**
	 * Obtener todos comprobar� si cacheActualizada = true. Si ocurre esto, no buscar� en BD y devolver� la cach�. Si no ocurre,
	 * buscar� todos los resultados en BD y actualizar� la cach� (a�adir� el nuevo valor que no hubiese).
	 */
	@Override
	public synchronized ArrayList<Plato> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Plato> platos = new ArrayList<Plato>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idPlato;
		String db_nombreLargo;
		String db_nombre;
		String db_descripcion;
		float db_precioVenta;
		CategoriaPlato db_categoriaPlato;	int db_idCategoriaPlato;
		String db_receta;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			categoriasPlatos = FachadaPersistencia.obtenerTodos_CategoriasPlatos();
			//ingredientesPlatos = FachadaPersistencia.obtenerTodos_IngredientesPlatos();
			
			PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PLATOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idPlato") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				//cache = new ArrayList<Plato>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idPlato = rs.getInt("idPlato");
					db_nombreLargo = rs.getString("nombreLargo");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					db_precioVenta = (float)rs.getDouble("precioVenta");
					db_idCategoriaPlato = rs.getInt("idCategoriaPlato");
					db_receta = rs.getString("receta");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					platos.add(new Plato(db_idPlato, db_nombreLargo, db_nombre, db_descripcion, db_precioVenta, null, buscarEnCacheCategoriasPlatos(db_idCategoriaPlato), db_receta));
				}
				
				/* A�adimos fuera del bucle los asociados para evitar superposici�n de ResultSets */
				for (int i = 0; i < platos.size(); i++) {
					platos.get(i).setIngredientes(FachadaPersistencia.obtenerTodos_IngredientesPlatos_ByidPlato(platos.get(i).getIDPlato()));
					// Actualizamos el �ltimo id de la cach�
					almacenarEnCache(platos.get(i));
					//if (platos.get(i).getIDPlato() > ultimaIdUtilizada)
						//ultimaIdUtilizada = platos.get(i).getIDPlato();
				}
				cacheActualizada = true;
			} else
				platos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: ConversorPlato -> obtenerTodos()" + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return platos;
	}
	
	public synchronized ArrayList<Plato> getCache() {
		return cache;
	}
	
	/**
	 * Busca en la tabla platos_old si hubiese alg�n plato cuya ID coincida con alguno de los platos en la tabla principal. Si lo hubiera, lo borra para evitar conflictos.
	 */
	public synchronized void limpiarKeysOld() {
		ArrayList<Integer> keys = new ArrayList<Integer>();
		ArrayList<Plato> platos = FachadaPersistencia.obtenerTodos_Platos();
		for (int i = 0; i < platos.size(); i++)
			keys.add(platos.get(i).getIDPlato());
		ArrayList<Plato> platos_old = new ArrayList<Plato>();
		try {
			ResultSet rs = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PLATOS_OLD).executeQuery();
			while (rs.next()) {
				platos_old.add(new Plato(rs.getInt("idPlato"), "", "", "", (float) 0.0, null, null, ""));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Borrar aquelos platos de platos_old cuya id coincida con los de la tabla platos
		for (int i = 0; i < platos_old.size(); i++) {
			for (int j = 0; j < platos.size(); j++) {
				if (platos_old.get(i).getIDPlato() == platos.get(j).getIDPlato()) {
					try {
						PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_PLATOS_OLD_ByidPlato);
						System.out.println("Limpiando el plato_old con id (" + platos_old.get(i).getIDPlato() + ")");
						ps.setInt(1, platos_old.get(i).getIDPlato());
						ps.executeUpdate();
						break;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void almacenarEnCache(Plato obj) {
		if (buscarIndiceEnCache(obj) == -3) {
			cache.add(obj);
			System.out.println("Almacenando en cach�.");
		}
	}
	
	/**
	 * Buscamos el objeto en cach�, y si existe y necesita ser modificar se modifica. Si existe y no lo necesita, no se hace nada (no deber�a ocurrir esto).
	 * Si no existe, lo a�adimos.
	 * @param obj
	 */
	public synchronized void modificarEnCache(Plato obj) {
		int indiceEncontrado = buscarIndiceEnCache(obj);
		if (indiceEncontrado != -3) {
			System.out.println(cache.get(indiceEncontrado).getNombreCorto() + " - " + obj.getNombreCorto() + " | " + cache.get(indiceEncontrado).getNombreLargo() + " - " + obj.getNombreLargo());
			if (cache.get(indiceEncontrado).getCategoria() != obj.getCategoria() || !cache.get(indiceEncontrado).getDescripcion().equals(obj.getDescripcion()) ||
					cache.get(indiceEncontrado).getIngredientes() != obj.getIngredientes() || !cache.get(indiceEncontrado).getNombreCorto().equals(obj.getNombreCorto()) ||
					!cache.get(indiceEncontrado).getNombreLargo().equals(obj.getNombreLargo()) || cache.get(indiceEncontrado).getPrecio() != obj.getPrecio() ||
					!cache.get(indiceEncontrado).getReceta().equals(obj.getReceta())) {
				System.out.println("Modificando en cach�.");
				cache.set(indiceEncontrado, obj);
			}
		} else
			almacenarEnCache(obj);
	}
	
	/**
	 * Busca el �ndice del objeto en cach�. Si existe => indice != -3, y eliminamos ese registro en cach�.
	 */
	private synchronized void eliminarEnCache(Plato obj) {
		int indice = buscarIndiceEnCache(obj);
		if (indice != -3) {
			cache.remove(indice);
			System.out.println("Eliminando de cach�.");
		}
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Plato buscarEnCache(Plato obj) {
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDPlato() == obj.getIDPlato()) {
				return cache.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Busca el objeto en cach� y devuelve el �ndice de la posici�n en la propia cach�.
	 * @param obj
	 * @return
	 */
	private synchronized int buscarIndiceEnCache(Plato obj) {
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDPlato() == obj.getIDPlato()) {
				System.out.println(this.getClass().getSimpleName() + " encontrado en cach�");
				return i;
			}
		}
		System.out.println(this.getClass().getSimpleName() + " no encontrado en cach�");
		return -3;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Plato buscarEnCache(int id) {
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDPlato() == id) {
				System.out.println(this.getClass().getSimpleName() + " encontrado en cach�");
				return cache.get(i);
			}
		}
		System.out.println(this.getClass().getSimpleName() + " no encontrado en cach�");
		return null;
	}
	
	private synchronized CategoriaPlato buscarEnCacheCategoriasPlatos(int id) {
		for (int i = 0; i < categoriasPlatos.size(); i++) {
			if (categoriasPlatos.get(i).getIDCategoriaPlato() == id) {
				return categoriasPlatos.get(i);
			}
		}
		return null;
	}
	
	private synchronized IngredientePlato buscarEnCacheIngredientesPlatos(int id) {
		for (int i = 0; i < ingredientesPlatos.size(); i++) {
			if (ingredientesPlatos.get(i).getIDIngredientePlato() == id) {
				return ingredientesPlatos.get(i);
			}
		}
		return null;
	}

	
}