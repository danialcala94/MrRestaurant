package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.Restaurante;
import restaurante.almacen.*;
import restaurante.empleados.Empleado;

public class ConversorIngrediente extends ConversorAbstracto {
	
	private static ConversorIngrediente instanciaConversorIngrediente;
	
	// Cach�
	private static ArrayList<Ingrediente> cache = new ArrayList<Ingrediente>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de MAGNITUD, CATEGORIAINGREDIENTE, PROVEEDOR y ALERGENO
	private static ArrayList<Magnitud> magnitudes;
	private static ArrayList<CategoriaIngrediente> categoriasIngredientes;
	private static ArrayList<Proveedor> proveedores;
	private static ArrayList<Alergeno> alergenos;
	
	
	public static synchronized ConversorIngrediente getConversor() {
		if(instanciaConversorIngrediente == null) {
			instanciaConversorIngrediente = new ConversorIngrediente();
		}
		return instanciaConversorIngrediente;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Ingrediente ingrediente = (Ingrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// nombreLargo, nombre, idMagnitud, idCategoriaIngrediente, idProveedor, codigoProveedor, idAlergeno, stockAct, stockMin
				ps.setString(1, ingrediente.getNombreLargo());
				ps.setString(2, ingrediente.getNombreCorto());
				ps.setInt(3, ingrediente.getMagnitud().getIDMagnitud());
				ps.setInt(4, ingrediente.getCategoria().getIDCategoria());
				ps.setInt(5, ingrediente.getProveedor().getIDProveedor());
				ps.setString(6, ingrediente.getCodigoProveedor());
				ps.setInt(7, ingrediente.getAlergeno().getIDAlergeno());
				ps.setFloat(9, ingrediente.getStockAct());
				ps.setFloat(10, ingrediente.getStockMin());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||\r\n" + 
					"				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||\r\n" + 
					"				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0");
			return null;
		}
		return ingrediente;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Ingrediente ingrediente = (Ingrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Ingrediente ingredienteAEliminar = FachadaPersistencia.obtener_Ingrediente_ByidIngrediente(ingrediente.getIDIngrediente());
			try {
				// Tengo que eliminarlo
				if (ingredienteAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idIngrediente, nombreLargo, nombre, idMagnitud, idCategoriaIngrediente, idProveedor, codigoProveedor, idAlergeno, stockAct, stockMin
					ps.setInt(1, ingredienteAEliminar.getIDIngrediente());
					ps.setString(2, ingredienteAEliminar.getNombreLargo());
					ps.setString(3, ingredienteAEliminar.getNombreCorto());
					ps.setInt(4, ingredienteAEliminar.getMagnitud().getIDMagnitud());
					ps.setInt(5, ingredienteAEliminar.getCategoria().getIDCategoria());
					ps.setInt(6, ingredienteAEliminar.getProveedor().getIDProveedor());
					ps.setString(7, ingredienteAEliminar.getCodigoProveedor());
					ps.setInt(8, ingredienteAEliminar.getAlergeno().getIDAlergeno());
					ps.setFloat(9, ingredienteAEliminar.getStockAct());
					ps.setFloat(10, ingredienteAEliminar.getStockMin());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_INGREDIENTES_ByidIngrediente);
					ps.setInt(1, ingredienteAEliminar.getIDIngrediente());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
					ingrediente = ingredienteAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||\r\n" + 
					"				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||\r\n" + 
					"				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0");
			return null;
		}
		return ingrediente;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Ingrediente ingrediente = (Ingrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			Ingrediente ingredienteAModificar = FachadaPersistencia.obtener_Ingrediente_ByidIngrediente(ingrediente.getIDIngrediente());
			try {
				// Tengo que modificarlo
				if (ingredienteAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_EMPLEADOS_ByidEmpleado);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// nombreLargo = ?, nombre = ?, idMagnitud = ?, idCategoriaIngrediente = ?, idProveedor = ?, codigoProveedor = ?,
					// idAlergeno = ?, stockAct = ?, stockMin = ? WHERE ingredientes.idIngrediente = ?
					ps.setString(1, ingrediente.getNombreLargo());
					ps.setString(2, ingrediente.getNombreCorto());
					ps.setInt(3, ingrediente.getMagnitud().getIDMagnitud());
					ps.setInt(4, ingrediente.getCategoria().getIDCategoria());
					ps.setInt(5, ingrediente.getProveedor().getIDProveedor());
					ps.setString(6, ingrediente.getCodigoProveedor());
					ps.setInt(7, ingrediente.getAlergeno().getIDAlergeno());
					ps.setFloat(8, ingrediente.getStockAct());
					ps.setFloat(9, ingrediente.getStockMin());
					ps.setInt(10, ingrediente.getIDIngrediente());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") " + ingrediente.getNombreCorto());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||\r\n" + 
					"				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||\r\n" + 
					"				ingrediente.getAlergeno().getIDAlergeno() < 0 || ingrediente.getStockAct() < 0 || ingrediente.getStockMin() < 0");
			return null;
		}
		return ingrediente;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Ingrediente ing = buscarEnCache(id);
		
		if (ing == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idIngrediente;
			String db_nombreLargo;
			String db_nombreCorto;
			Magnitud magnitud; 					int db_magnitud;
			CategoriaIngrediente categoria;		int db_categoriaIngrediente;
			Proveedor proveedor;				int db_proveedor;
			float db_precioBase;
			String db_codigoProveedor;
			Alergeno alergeno;					int db_alergeno;
			float db_stockMin;
			float db_stockAct;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				magnitudes = FachadaPersistencia.obtenerTodos_Magnitudes();
				categoriasIngredientes = FachadaPersistencia.obtenerTodos_CategoriasIngredientes();
				proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
				alergenos = FachadaPersistencia.obtenerTodos_Alergenos();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTES_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngrediente = rs.getInt("idIngrediente");
					db_nombreLargo = rs.getString("nombreLargo");
					db_nombreCorto = rs.getString("nombre");
					db_magnitud = rs.getInt("idMagnitud");
					db_categoriaIngrediente = rs.getInt("idCategoriaIngrediente");
					db_proveedor = rs.getInt("idProveedor");
					db_precioBase = (float)rs.getDouble("precioBase");
					db_codigoProveedor = rs.getString("codigoProveedor");
					db_alergeno = rs.getInt("idAlergeno");
					
					// Hay que sustituir stockAct y stockMin
					ing = new Ingrediente(db_idIngrediente, db_nombreLargo, db_nombreCorto, 
							buscarEnCacheMagnitudes(db_magnitud), buscarEnCacheCategoriasIngredientes(db_categoriaIngrediente),
							buscarEnCacheProveedores(db_proveedor), db_codigoProveedor,
							db_precioBase, buscarEnCacheAlergenos(db_alergeno), 0, 0);
					addToCache(ing);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return ing;
	}
	
	@Override
	public synchronized ArrayList<Ingrediente> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idIngrediente;
		String db_nombreLargo;
		String db_nombreCorto;
		Magnitud magnitud; 					int db_magnitud;
		CategoriaIngrediente categoria;		int db_categoriaIngrediente;
		Proveedor proveedor;				int db_proveedor;
		float db_precioBase;
		String db_codigoProveedor;
		Alergeno alergeno;					int db_alergeno;
		float db_stockMin;
		float db_stockAct;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			magnitudes = FachadaPersistencia.obtenerTodos_Magnitudes();
			categoriasIngredientes = FachadaPersistencia.obtenerTodos_CategoriasIngredientes();
			proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
			alergenos = FachadaPersistencia.obtenerTodos_Alergenos();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idIngrediente") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Ingrediente>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngrediente = rs.getInt("idIngrediente");
					db_nombreLargo = rs.getString("nombreLargo");
					db_nombreCorto = rs.getString("nombre");
					db_magnitud = rs.getInt("idMagnitud");
					db_categoriaIngrediente = rs.getInt("idCategoriaIngrediente");
					db_proveedor = rs.getInt("idProveedor");
					db_precioBase = (float)rs.getDouble("precioBase");
					db_codigoProveedor = rs.getString("codigoProveedor");
					db_alergeno = rs.getInt("idAlergeno");
					db_stockMin = rs.getFloat("stockMin");
					db_stockAct = rs.getFloat("stockAct");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					ingredientes.add(new Ingrediente(db_idIngrediente, db_nombreLargo, db_nombreCorto, 
							buscarEnCacheMagnitudes(db_magnitud), buscarEnCacheCategoriasIngredientes(db_categoriaIngrediente),
							buscarEnCacheProveedores(db_proveedor), db_codigoProveedor,
							db_precioBase, buscarEnCacheAlergenos(db_alergeno), db_stockMin, db_stockAct));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(ingredientes.get(ingredientes.size() - 1));
					if (ingredientes.get(ingredientes.size() - 1).getIDIngrediente() > ultimaIdUtilizada)
						ultimaIdUtilizada = ingredientes.get(ingredientes.size() - 1).getIDIngrediente();
				}
				cacheActualizada = true;
			} else
				ingredientes = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return ingredientes;
	}
	
	public synchronized ArrayList<Ingrediente> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Ingrediente obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDIngrediente() == obj.getIDIngrediente()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Ingrediente buscarEnCache(Ingrediente obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngrediente() == obj.getIDIngrediente()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Ingrediente buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngrediente() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized Magnitud buscarEnCacheMagnitudes(int id) {
		for (int i = 0; i < magnitudes.size(); i++) {
			if (magnitudes.get(i).getIDMagnitud() == id) {
				return magnitudes.get(i);
			}
		}
		return null;
	}
	
	private synchronized CategoriaIngrediente buscarEnCacheCategoriasIngredientes(int id) {
		for (int i = 0; i < categoriasIngredientes.size(); i++) {
			if (categoriasIngredientes.get(i).getIDCategoria() == id) {
				return categoriasIngredientes.get(i);
			}
		}
		return null;
	}
	
	private synchronized Proveedor buscarEnCacheProveedores(int id) {
		for (int i = 0; i < proveedores.size(); i++) {
			if (proveedores.get(i).getIDProveedor() == id) {
				return proveedores.get(i);
			}
		}
		return null;
	}
	
	private synchronized Alergeno buscarEnCacheAlergenos(int id) {
		for (int i = 0; i < alergenos.size(); i++) {
			if (alergenos.get(i).getIDAlergeno() == id) {
				return alergenos.get(i);
			}
		}
		return null;
	}
}