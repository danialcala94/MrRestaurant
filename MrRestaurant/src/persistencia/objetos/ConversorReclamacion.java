package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.Reclamacion;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.empleados.NivelCredencial;

public class ConversorReclamacion extends ConversorAbstracto {
	
	private static ConversorReclamacion instanciaConversorReclamacion;
	
	// Cach�
	private static ArrayList<Reclamacion> cache = new ArrayList<Reclamacion>();
	private static int ultimaIdUtilizada = -3;
	
	public static boolean cacheActualizada = false;
	
	public static synchronized ConversorReclamacion getConversor() {
		if(instanciaConversorReclamacion == null) {
			instanciaConversorReclamacion = new ConversorReclamacion();
		}
		return instanciaConversorReclamacion;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Reclamacion reclamacion = (Reclamacion) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_RECLAMACIONES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setInt(1, reclamacion.getNumReclamacion());
				ps.setString(2, reclamacion.getDNICliente());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + reclamacion.getNumReclamacion());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + reclamacion.getNumReclamacion());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()");
			return null;
		}
		return reclamacion;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Reclamacion reclamacion = (Reclamacion) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			Reclamacion reclamacionAEliminar = FachadaPersistencia.obtener_Reclamacion_ByidReclamacion(reclamacion.getIDReclamacion());
			try {
				// Comprobar que est� en la base de datos
				if (reclamacionAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_RECLAMACIONES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, reclamacionAEliminar.getIDReclamacion());
					ps.setInt(2, reclamacionAEliminar.getNumReclamacion());
					ps.setString(3, reclamacionAEliminar.getDNICliente());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_RECLAMACIONES_ByidReclamacion);
					ps.setInt(1, reclamacionAEliminar.getIDReclamacion());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + reclamacionAEliminar.getNumReclamacion());
					// Para que devuelva el objeto correcto
					reclamacion = reclamacionAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + reclamacionAEliminar.getNumReclamacion());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()");
			return null;
		}
		return reclamacion;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Reclamacion reclamacion = (Reclamacion) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				Reclamacion reclamacionAModificar = FachadaPersistencia.obtener_Reclamacion_ByidReclamacion(reclamacion.getIDReclamacion());
				if (reclamacionAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_RECLAMACIONES_ByidReclamacion);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, reclamacion.getNumReclamacion());
					ps.setString(2, reclamacion.getDNICliente());
					ps.setInt(3, reclamacion.getIDReclamacion());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + reclamacion.getNumReclamacion());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + reclamacion.getNumReclamacion());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()");
			return null;
		}
		return reclamacion;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Reclamacion rec = buscarEnCache(id);
		
		if (rec == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idReclamacion;
			int db_numReclamacion;
			String db_dni;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_RECLAMACIONES_ByidReclamacion);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idReclamacion = rs.getInt("idReclamacion");
					db_numReclamacion = rs.getInt("numReclamacion");
					db_dni = rs.getString("dni");
					
					// Hay que sustituir stockAct y stockMin
					rec = new Reclamacion(db_idReclamacion, db_numReclamacion, db_dni);
					
					addToCache(rec);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return rec;
	}
	
	@Override
	public synchronized ArrayList<Reclamacion> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Reclamacion> reclamaciones = new ArrayList<Reclamacion>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idReclamacion;
		int db_numReclamacion;
		String db_dni;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_RECLAMACIONES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idReclamacion") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Reclamacion>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idReclamacion = rs.getInt("idReclamacion");
					db_numReclamacion = rs.getInt("numReclamacion");
					db_dni = rs.getString("dni");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					reclamaciones.add(new Reclamacion(db_idReclamacion, db_numReclamacion, db_dni));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(reclamaciones.get(reclamaciones.size() - 1));
					if (reclamaciones.get(reclamaciones.size() - 1).getIDReclamacion() > ultimaIdUtilizada)
						ultimaIdUtilizada = reclamaciones.get(reclamaciones.size() - 1).getIDReclamacion();
				}
				cacheActualizada = true;
			} else
				reclamaciones = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return reclamaciones;
	}
	
	public synchronized ArrayList<Reclamacion> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Reclamacion obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDReclamacion() == obj.getIDReclamacion()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Reclamacion buscarEnCache(Reclamacion obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDReclamacion() == obj.getIDReclamacion()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Reclamacion buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDReclamacion() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}