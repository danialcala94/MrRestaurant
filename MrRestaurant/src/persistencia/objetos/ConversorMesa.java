package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.sala.Mesa;

public class ConversorMesa extends ConversorAbstracto {
	
	private static ConversorMesa instanciaConversorMesa;
	
	// Cach�
	private static ArrayList<Mesa> cache = new ArrayList<Mesa>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorMesa getConversor() {
		if(instanciaConversorMesa == null) {
			instanciaConversorMesa = new ConversorMesa();
		}
		return instanciaConversorMesa;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Mesa mesa = (Mesa) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_MESAS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setInt(1, mesa.getCapacidad());
				ps.setInt(2, mesa.getCoordX());
				ps.setInt(3, mesa.getCoordY());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4");
			return null;
		}
		return mesa;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Mesa mesa = (Mesa) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (mesa.getIDMesa() > 0 && mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			Mesa mesaAEliminar = FachadaPersistencia.obtener_Mesa_ByidMesa(mesa.getIDMesa());
			try {
				// Comprobar que est� en la base de datos
				if (mesaAEliminar != null) {
					// Lo guardo en la tabla_old
					/*PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_MESAS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, mesaAEliminar.getIDMesa());
					ps.setInt(2, mesaAEliminar.getCapacidad());
					ps.setInt(3, mesaAEliminar.getCoordX());
					ps.setInt(4, mesaAEliminar.getCoordY());
					ps.executeUpdate();*/
					// Lo borro de la tabla original
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_MESAS_ByidMesa);
					ps.setInt(1, mesaAEliminar.getIDMesa());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
					// Para que devuelva el objeto correcto
					mesa = mesaAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4");
			return null;
		}
		return mesa;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Mesa mesa = (Mesa) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (mesa.getIDMesa() > 0 && mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				// Tengo que modificarlo
				Mesa mesaAModificar = FachadaPersistencia.obtener_Mesa_ByidMesa(mesa.getIDMesa());
				if (mesaAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_MESAS_ByidMesa);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, mesa.getCapacidad());
					ps.setInt(2, mesa.getCoordX());
					ps.setInt(3, mesa.getCoordY());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + mesa.getCoordX() + " - " + mesa.getCoordY());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: mesa.getCoordX() > 0 && mesa.getCoordY() > 0 && mesa.getEstado() >= 0 && mesa.getEstado() <= 4");
			return null;
		}
		return mesa;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Mesa mesa = buscarEnCache(id);
		
		if (mesa == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idMesa, db_capacidad, db_coordX, db_coordY;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_MESAS_ByidMesa);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idMesa = rs.getInt("idMesa");
					db_capacidad = rs.getInt("capacidad");
					db_coordX = rs.getInt("coordX");
					db_coordY = rs.getInt("coordY");
					
					// Hay que sustituir stockAct y stockMin
					mesa = new Mesa(db_idMesa, db_capacidad, db_coordX, db_coordY);
					
					addToCache(mesa);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return mesa;
	}
	
	@Override
	public synchronized ArrayList<Mesa> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Mesa> mesas = new ArrayList<Mesa>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idMesa, db_capacidad, db_coordX, db_coordY;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_MESAS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idCategoriaIngrediente") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Mesa>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idMesa = rs.getInt("idMesa");
					db_capacidad = rs.getInt("capacidad");
					db_coordX = rs.getInt("coordX");
					db_coordY = rs.getInt("coordY");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					mesas.add(new Mesa(db_idMesa, db_capacidad, db_coordX, db_coordY));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(mesas.get(mesas.size() - 1));
					if (mesas.get(mesas.size() - 1).getIDMesa() > ultimaIdUtilizada)
						ultimaIdUtilizada = mesas.get(mesas.size() - 1).getIDMesa();
				}
				cacheActualizada = true;
			} else
				mesas = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return mesas;
	}
	
	public synchronized ArrayList<Mesa> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Mesa obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDMesa() == obj.getIDMesa()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Mesa buscarEnCache(Mesa obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDMesa() == obj.getIDMesa()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Mesa buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDMesa() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}
