package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;
import restaurante.empleados.CategoriaEmpleado;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;

public class ConversorIngredientePedido extends ConversorAbstracto {
	
	private static ConversorIngredientePedido instanciaConversorIngredientePedido;
	
	// Cach�
	private static ArrayList<IngredientePedido> cache = new ArrayList<IngredientePedido>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de MAGNITUD, CATEGORIAINGREDIENTE, PROVEEDOR y ALERGENO
	private static ArrayList<Ingrediente> ingredientes;
	private static ArrayList<Pedido> pedidos;
	
	
	public static synchronized ConversorIngredientePedido getConversor() {
		if(instanciaConversorIngredientePedido == null) {
			instanciaConversorIngredientePedido = new ConversorIngredientePedido();
		}
		return instanciaConversorIngredientePedido;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		IngredientePedido ingPed = buscarEnCache(id);
		
		if (ingPed == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idIngredientePedido;
			Ingrediente db_ingrediente;			int db_idIngrediente;
			Pedido db_pedido;					int db_idPedido;
			float db_precio;
			float db_cantidad;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPEDIDOS_ByidIngredientePedido);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngredientePedido = rs.getInt("idIngredientePedido");
					//db_idPedido = rs.getInt("idPedido");
					db_idIngrediente = rs.getInt("idIngrediente");
					db_precio = (float)rs.getDouble("precio");
					db_cantidad = (float)rs.getDouble("cantidad");
					
					// Hay que sustituir stockAct y stockMin
					ingPed = new IngredientePedido(db_idIngredientePedido, /*buscarEnCachePedidos(db_idPedido), */ buscarEnCacheIngredientes(db_idIngrediente), db_precio, db_cantidad);
					
					addToCache(ingPed);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return ingPed;
	}
	
	@Override
	public synchronized ArrayList<IngredientePedido> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<IngredientePedido> ingredientesPedidos = new ArrayList<IngredientePedido>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idIngredientePedido;
		Ingrediente db_ingrediente;			int db_idIngrediente;
		Pedido db_pedido;					int db_idPedido;
		float db_precio;
		float db_cantidad;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPEDIDOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idIngredientePedido") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<IngredientePedido>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngredientePedido = rs.getInt("idIngredientePedido");
					//db_idPedido = rs.getInt("idPedido");
					db_idIngrediente = rs.getInt("idIngrediente");
					db_precio = (float)rs.getDouble("precio");
					db_cantidad = (float)rs.getDouble("cantidad");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					ingredientesPedidos.add(new IngredientePedido(db_idIngredientePedido, buscarEnCacheIngredientes(db_idIngrediente), db_precio, db_cantidad));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(ingredientesPedidos.get(ingredientesPedidos.size() - 1));
					if (ingredientesPedidos.get(ingredientesPedidos.size() - 1).getIDIngredientePedido() > ultimaIdUtilizada)
						ultimaIdUtilizada = ingredientesPedidos.get(ingredientesPedidos.size() - 1).getIDIngredientePedido();
				}
				cacheActualizada = true;
			} else
				ingredientesPedidos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return ingredientesPedidos;
	}
	
	public synchronized ArrayList<IngredientePedido> obtenerTodos_ByidPedido(int idPedido) {
		// TODO Auto-generated method stub
		ArrayList<IngredientePedido> ingredientesPedidos = new ArrayList<IngredientePedido>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idIngredientePedido;
		Ingrediente db_ingrediente;			int db_idIngrediente;
		Pedido db_pedido;					int db_idPedido;
		float db_precio;
		float db_cantidad;
		
		System.out.println(this.getClass().getName() + " -> Buscando:");
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPEDIDOS_ByidPedido);
			// Setea el 'id' en el '?'
			ps.setInt(1, idPedido);
			rs = ps.executeQuery();
			
			/* Limpiamos la cach� para actualizarla por completo */
			//cache = new ArrayList<IngredientePedido>();
			/* Rellenar campos */
			while(rs.next()) {
				db_idIngredientePedido = rs.getInt("idIngredientePedido");
				//db_idPedido = rs.getInt("idPedido");
				db_idIngrediente = rs.getInt("idIngrediente");
				db_precio = (float)rs.getDouble("precio");
				db_cantidad = (float)rs.getDouble("cantidad");
				
				// Hacer que los objetos sean referenciados seg�n las ids obtenidas
				ingredientesPedidos.add(new IngredientePedido(db_idIngredientePedido, buscarEnCacheIngredientes(db_idIngrediente), db_precio, db_cantidad));
				
				// Actualizamos el �ltimo id de la cach�
				addToCache(ingredientesPedidos.get(ingredientesPedidos.size() - 1));
				if (ingredientesPedidos.get(ingredientesPedidos.size() - 1).getIDIngredientePedido() > ultimaIdUtilizada)
					ultimaIdUtilizada = ingredientesPedidos.get(ingredientesPedidos.size() - 1).getIDIngredientePedido();
			}
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando... BYID");
		
		return ingredientesPedidos;
	}
	
	public synchronized ArrayList<IngredientePedido> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(IngredientePedido obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDIngredientePedido() == obj.getIDIngredientePedido()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized IngredientePedido buscarEnCache(IngredientePedido obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngredientePedido() == obj.getIDIngredientePedido()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized IngredientePedido buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngredientePedido() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized Ingrediente buscarEnCacheIngredientes(int id) {
		for (int i = 0; i < ingredientes.size(); i++) {
			if (ingredientes.get(i).getIDIngrediente() == id) {
				return ingredientes.get(i);
			}
		}
		return null;
	}
	
	private synchronized Pedido buscarEnCachePedidos(int id) {
		for (int i = 0; i < pedidos.size(); i++) {
			if (pedidos.get(i).getIDPedido() == id) {
				return pedidos.get(i);
			}
		}
		return null;
	}

	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eliminar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}
}