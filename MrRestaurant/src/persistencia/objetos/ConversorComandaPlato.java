package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;
import restaurante.sala.Comanda;

public class ConversorComandaPlato extends ConversorAbstracto {
	
	private static ConversorComandaPlato instanciaConversorComandaPlato;
	
	// Cach�
	private static ArrayList<ComandaPlato> cache = new ArrayList<ComandaPlato>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorComandaPlato getConversor() {
		if(instanciaConversorComandaPlato == null) {
			instanciaConversorComandaPlato = new ConversorComandaPlato();
		}
		return instanciaConversorComandaPlato;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		ComandaPlato comandaPlato = (ComandaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDASPLATOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// idComanda, cantidad, idPlato, precio, comentario
				ps.setInt(1, comandaPlato.getIdComanda());
				// La fecha usada en las mesas deber�a ser de java.sql.Date
				ps.setInt(2, comandaPlato.getCantidad());
				ps.setInt(3, comandaPlato.getPlato().getIDPlato());
				ps.setFloat(4, comandaPlato.getPrecio());
				ps.setString(5, comandaPlato.getComentario());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0");
			return null;
		}
		return comandaPlato;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		ComandaPlato comandaPlato = (ComandaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		boolean existe = false;
		if (comandaPlato.getCantidad() < 0 || comandaPlato.getIDComandaPlato() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			ComandaPlato comandaPlatoAEliminar = FachadaPersistencia.obtener_ComandaPlato_ByidComandaPlato(comandaPlato.getIDComandaPlato());
			try {
				// Tengo que eliminarlo
				if (comandaPlatoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDASPLATOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idComandaPlato, idComanda, cantidad, idPlato, precio, comentario
					ps.setInt(1, comandaPlatoAEliminar.getIDComandaPlato());
					ps.setInt(2, comandaPlatoAEliminar.getIdComanda());
					ps.setInt(3, comandaPlatoAEliminar.getCantidad());
					ps.setInt(4, comandaPlatoAEliminar.getPlato().getIDPlato());
					ps.setFloat(5, comandaPlatoAEliminar.getPrecio());
					ps.setString(6, comandaPlatoAEliminar.getComentario());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_COMANDASPLATOS_ByidComandaPlato);
					ps.setInt(1, comandaPlatoAEliminar.getIDComandaPlato());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
					comandaPlato = comandaPlatoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0");
			return null;
		}
		return comandaPlato;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		ComandaPlato comandaPlato = (ComandaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (comandaPlato.getCantidad() < 0 || comandaPlato.getIDComandaPlato() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			ComandaPlato comandaPlatoAModificar = FachadaPersistencia.obtener_ComandaPlato_ByidComandaPlato(comandaPlato.getIDComandaPlato());
			try {
				// Tengo que modificarlo
				if (comandaPlatoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_COMANDASPLATOS_ByidComandaPlato);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idComanda = ?, cantidad = ?, idPlato = ?, precio = ?, comentario = ? WHERE comandasplatos.idComandaPlato = ?"
					ps.setInt(1, comandaPlato.getIdComanda());
					ps.setInt(2, comandaPlato.getCantidad());
					ps.setInt(3, comandaPlato.getPlato().getIDPlato());
					ps.setFloat(4, comandaPlato.getPrecio());
					ps.setString(5, comandaPlato.getComentario());
					ps.setInt(6, comandaPlato.getIDComandaPlato());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") Comanda " + comandaPlato.getIdComanda() + " - " + comandaPlato.getCantidad());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0");
			return null;
		}
		return comandaPlato;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		ComandaPlato comPla = buscarEnCache(id);
		
		if (comPla == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idComandaPlato;
			int db_idComanda;
			int db_idPlato = -3;
			int db_cantidad;
			float db_precio;
			String db_comentario;
			boolean db_servido;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDASPLATOS_ByidComandaPlato);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idComandaPlato = rs.getInt("idComandaPlato");
					db_idComanda = rs.getInt("idComanda");
					db_idPlato = rs.getInt("idPlato");
					db_cantidad = rs.getInt("cantidad");
					db_precio = (float)rs.getDouble("precio");
					db_comentario = rs.getString("comentario");
					// En persistencia no interesa si se sirvi� o no el plato
					//db_servido = rs.getBoolean("servido");
					
					// Si estamos leyendo de base de datos, deber�a haberse servido ya el plato
					comPla = new ComandaPlato(db_idComandaPlato, null, db_cantidad, db_precio, db_comentario, true);
				}
				
				// Obtener el Plato correspondiente al idPlato
				Plato db_plato = FachadaPersistencia.obtener_Plato_ByidPlato(db_idPlato);
				
				comPla.setPlato(db_plato);
				
				addToCache(comPla);
				
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return comPla;
	}
	
	@Override
	public synchronized ArrayList<ComandaPlato> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<ComandaPlato> comandasPlatos = new ArrayList<ComandaPlato>();
		ArrayList<Integer> idsPlatos = new ArrayList<Integer>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idComandaPlato;
		int db_idComanda;
		int db_idPlato;
		int db_cantidad;
		float db_precio;
		String db_comentario;
		boolean db_servido;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDASPLATOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idComandaPlato") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<ComandaPlato>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idComandaPlato = rs.getInt("idComandaPlato");
					db_idComanda = rs.getInt("idComanda");
					db_idPlato = rs.getInt("idPlato");
					idsPlatos.add(db_idPlato);
					db_cantidad = rs.getInt("cantidad");
					db_precio = (float)rs.getDouble("precio");
					db_comentario = rs.getString("comentario");
					// En persistencia no interesa si se sirvi� o no el plato
					//db_servido = rs.getBoolean("servido");
					
					comandasPlatos.add(new ComandaPlato(db_idComandaPlato, null, db_cantidad, db_precio, db_comentario, true));
				}
				
				// Asignamos el plato a cada ComandaPlato
				for (int i = 0; i < comandasPlatos.size(); i++) {
					comandasPlatos.get(i).setPlato(FachadaPersistencia.obtener_Plato_ByidPlato(idsPlatos.get(i)));
					// Actualizamos el �ltimo id de la cach�
					addToCache(comandasPlatos.get(i));
					if (comandasPlatos.get(i).getIDComandaPlato() > ultimaIdUtilizada)
						ultimaIdUtilizada = comandasPlatos.get(i).getIDComandaPlato();
				}
				cacheActualizada = true;
			} else
				comandasPlatos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return comandasPlatos;
	}
	
	public synchronized ArrayList<ComandaPlato> obtenerTodos_ByidComanda(int idComanda) {
		// TODO Auto-generated method stub
		ArrayList<ComandaPlato> comandasPlatos = new ArrayList<ComandaPlato>();
		ArrayList<Integer> idsPlatos = new ArrayList<Integer>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idComandaPlato;
		int db_idComanda;
		int db_idPlato;
		int db_cantidad;
		float db_precio;
		String db_comentario;
		boolean db_servido;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDASPLATOS_ByidComanda);
			rs = ps.executeQuery();
			
			System.out.println(this.getClass().getName() + ": -> Buscando: ");
			
			/* Limpiamos la cach� para actualizarla por completo */
			//cache = new ArrayList<ComandaPlato>();
			/* Rellenar campos */
			while(rs.next()) {
				db_idComandaPlato = rs.getInt("idComandaPlato");
				// No se usa actualmente
				// db_idComanda = rs.getInt("idComanda");
				db_idPlato = rs.getInt("idPlato");
				idsPlatos.add(db_idPlato);
				db_cantidad = rs.getInt("cantidad");
				db_precio = (float)rs.getDouble("precio");
				db_comentario = rs.getString("comentario");
				// En persistencia no interesa si se sirvi� o no el plato
				//db_servido = rs.getBoolean("servido");
				
				// No se almacena servido actualmente
				comandasPlatos.add(new ComandaPlato(db_idComandaPlato, null, db_cantidad, db_precio, db_comentario, true));
				
			}
			// Asignamos el plato a cada ComandaPlato
			for (int i = 0; i < comandasPlatos.size(); i++) {
				comandasPlatos.get(i).setPlato(FachadaPersistencia.obtener_Plato_ByidPlato(idsPlatos.get(i)));
				// Actualizamos el �ltimo id de la cach�
				//addToCache(comandasPlatos.get(i));
				if (comandasPlatos.get(i).getIDComandaPlato() > ultimaIdUtilizada)
					ultimaIdUtilizada = comandasPlatos.get(i).getIDComandaPlato();
			}
			
			
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return comandasPlatos;
	}
	
	public synchronized ArrayList<ComandaPlato> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(ComandaPlato obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDComandaPlato() == obj.getIDComandaPlato()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized ComandaPlato buscarEnCache(ComandaPlato obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDComandaPlato() == obj.getIDComandaPlato()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized ComandaPlato buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDComandaPlato() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}