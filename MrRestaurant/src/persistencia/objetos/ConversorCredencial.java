package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.Plato;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;
import restaurante.empleados.NivelCredencial;

public class ConversorCredencial extends ConversorAbstracto {
	
	private static ConversorCredencial instanciaConversorCredencial;
	
	// Cach�
	private static ArrayList<Credencial> cache = new ArrayList<Credencial>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorCredencial getConversor() {
		if(instanciaConversorCredencial == null) {
			instanciaConversorCredencial = new ConversorCredencial();
		}
		return instanciaConversorCredencial;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Credencial credencial = (Credencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CREDENCIALES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// nombre, contrasena, idNivelCredencial
				ps.setString(1, credencial.getUsuario());
				ps.setString(2, credencial.getContrase�a());
				ps.setInt(3, credencial.getNivel().getIDNivelCredencial());
				ps.executeUpdate();
				// Asignamos el idCredencial al empleado que se haya elegido
				int idCredencial = -3;
				Empleado empleadoAsociado = null;
				if (credencial.getEmpleado() != null) {
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CREDENCIALES);
					ResultSet rs = ps.executeQuery();
					rs.last();
					idCredencial = rs.getInt("idCredencial");
					rs.close();
					empleadoAsociado = FachadaPersistencia.obtener_Empleado_ByidEmpleado(credencial.getEmpleado().getIDEmpleado());
					empleadoAsociado.setCredenciales(new Credencial(idCredencial, null, null, null, null));
					System.out.println("Asign�ndole el IDNivelCredencial al empleado elegido)");
					
				}
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
				if (idCredencial != -3)
					FachadaPersistencia.modificar_Empleado(empleadoAsociado);
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null");
			return null;
		}
		return credencial;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Credencial credencial = (Credencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (credencial.getIDCredencial() < 0 || credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Credencial credencialAEliminar = FachadaPersistencia.obtener_Credencial_ByidCredencial(credencial.getIDCredencial());
			try {
				// Tengo que eliminarlo
				if (credencialAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CREDENCIALES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idCredencial, nombre, contrasena, idNivelCredencial
					ps.setInt(1, credencialAEliminar.getIDCredencial());
					ps.setString(2, credencialAEliminar.getUsuario());
					ps.setString(3, credencialAEliminar.getContrase�a());
					ps.setInt(4, credencialAEliminar.getNivel().getIDNivelCredencial());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_CREDENCIALES_ByidCredencial);
					ps.setInt(1, credencialAEliminar.getIDCredencial());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
					credencial = credencialAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null");
			return null;
		}
		return credencial;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Credencial credencial = (Credencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (credencial.getIDCredencial() < 0 || credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			Credencial credencialAModificar = FachadaPersistencia.obtener_Credencial_ByidCredencial(credencial.getIDCredencial());
			try {
				// Tengo que modificarlo
				if (credencialAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_CREDENCIALES_ByidCredencial);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// nombre, contrasena, idNivelCredencial
					ps.setString(1, credencial.getUsuario());
					ps.setString(2, credencial.getContrase�a());
					ps.setInt(3, credencial.getNivel().getIDNivelCredencial());
					ps.setInt(4, credencial.getIDCredencial());
					ps.executeUpdate();
					int idCredencial = -3;
					Empleado empleadoAsociado = null;
					if (credencial.getEmpleado() != null) {
						idCredencial = credencial.IDCredencial;
						empleadoAsociado = FachadaPersistencia.obtener_Empleado_ByidEmpleado(credencial.getEmpleado().getIDEmpleado());
						empleadoAsociado.setCredenciales(new Credencial(idCredencial, null, null, null, null));
						System.out.println("Asign�ndole el IDNivelCredencial al empleado elegido)");
						
					}
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
					if (idCredencial != -3)
						FachadaPersistencia.modificar_Empleado(empleadoAsociado);
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") " + credencial.getUsuario() + " - " + credencial.getContrase�a());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null");
			return null;
		}
		return credencial;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Credencial cre = buscarEnCache(id);
		
		if (cre == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idCredencial;
			String db_nombre;
			String db_contrasena;
			int db_idNivel = -3;
			int db_idEmpleado = -3;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CREDENCIALES_ByidCredencial);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idCredencial = rs.getInt("idCredencial");
					db_nombre = rs.getString("nombre");
					db_contrasena = rs.getString("contrasena");
					db_idNivel = rs.getInt("idNivelCredencial");

					// Si estamos leyendo de base de datos, deber�a haberse servido ya el plato
					cre = new Credencial(db_idCredencial, db_nombre, db_contrasena, null, null);
				}
				
				cre.setNivel(FachadaPersistencia.obtener_NivelCredencial_ByidNivelCredencial(db_idNivel));
				// Habr�a que comprobar si existe un empleado asociado o no
				ArrayList<Empleado> empleadosProv = FachadaPersistencia.obtenerTodos_Empleados_ByidCredencial(db_idEmpleado);
				if (empleadosProv.size() != 0)
					cre.setEmpleado(FachadaPersistencia.obtenerTodos_Empleados_ByidCredencial(db_idEmpleado).get(0));
				else
					cre.setEmpleado(null);
				
				addToCache(cre);
				
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return cre;
	}
	
	@Override
	public synchronized ArrayList<Credencial> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Credencial> credenciales = new ArrayList<Credencial>();
		ArrayList<Integer> idsNivelesCredenciales = new ArrayList<Integer>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idCredencial;
		String db_nombre;
		String db_contrasena;
		int db_idNivel;
		int db_idEmpleado;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CREDENCIALES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idCredencial") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Credencial>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idCredencial = rs.getInt("idCredencial");
					db_nombre = rs.getString("nombre");
					db_contrasena = rs.getString("contrasena");
					db_idNivel = rs.getInt("idNivelCredencial");
					idsNivelesCredenciales.add(db_idNivel);
					credenciales.add(new Credencial(db_idCredencial, db_nombre, db_contrasena, null, null));
				}
				
				for (int i = 0; i < credenciales.size(); i++) {
					credenciales.get(i).setNivel(FachadaPersistencia.obtener_NivelCredencial_ByidNivelCredencial(idsNivelesCredenciales.get(i)));
					// Habr�a que comprobar si tiene un empleado asociado o no
					//credenciales.get(i).setEmpleado(FachadaPersistencia.obtenerTodos_Empleados_ByidCredencial(credenciales.get(i).getIDCredencial()).get(0));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(credenciales.get(i));
					if (credenciales.get(i).getIDCredencial() > ultimaIdUtilizada)
						ultimaIdUtilizada = credenciales.get(i).getIDCredencial();
				}
				cacheActualizada = true;
				
			} else
				credenciales = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return credenciales;
	}
	
	public synchronized ArrayList<Credencial> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Credencial obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDCredencial() == obj.getIDCredencial()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Credencial buscarEnCache(Credencial obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCredencial() == obj.getIDCredencial()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Credencial buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCredencial() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}