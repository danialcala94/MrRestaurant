package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;

public class ConversorIngredientePlato extends ConversorAbstracto {
	
private static ConversorIngredientePlato instanciaConversorIngredientePlato;
	
	// Cach�
	private static ArrayList<IngredientePlato> cache = new ArrayList<IngredientePlato>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de MAGNITUD, CATEGORIAINGREDIENTE, PROVEEDOR y ALERGENO
	private static ArrayList<Ingrediente> ingredientes;
	private static ArrayList<Plato> platos;
	
	
	public static synchronized ConversorIngredientePlato getConversor() {
		if(instanciaConversorIngredientePlato == null) {
			instanciaConversorIngredientePlato = new ConversorIngredientePlato();
		}
		return instanciaConversorIngredientePlato;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		IngredientePlato ingPla = buscarEnCache(id);
		
		if (ingPla == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idIngredientePlato;
			Ingrediente db_ingrediente;			int db_idIngrediente;
			Plato db_plato;						int db_idPlato;
			float db_cantidad;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
				//platos = FachadaPersistencia.obtenerTodos_Platos();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPLATOS_ByidIngredientePlato);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngredientePlato = rs.getInt("idIngredientePlato");
					db_idIngrediente = rs.getInt("idIngrediente");
					//db_idPlato = rs.getInt("idPlato");
					db_cantidad = (float)rs.getDouble("cantidad");
					
					// Hay que sustituir stockAct y stockMin
					ingPla = new IngredientePlato(db_idIngredientePlato, buscarEnCacheIngredientes(db_idIngrediente), /*buscarEnCachePedidos(db_idPedido), */ db_cantidad);
					
					addToCache(ingPla);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return ingPla;
	}
	
	@Override
	public synchronized ArrayList<IngredientePlato> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<IngredientePlato> ingredientesPlatos = new ArrayList<IngredientePlato>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idIngredientePlato;
		Ingrediente db_ingrediente;			int db_idIngrediente;
		Plato db_plato;						int db_idPlato;
		float db_cantidad;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			//platos = FachadaPersistencia.obtenerTodos_Platos();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPLATOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idIngredientePlato") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<IngredientePlato>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idIngredientePlato = rs.getInt("idIngredientePlato");
					db_idIngrediente = rs.getInt("idIngrediente");
					//db_idPlato = rs.getInt("idPlato");
					db_cantidad = (float)rs.getDouble("cantidad");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					ingredientesPlatos.add(new IngredientePlato(db_idIngredientePlato, buscarEnCacheIngredientes(db_idIngrediente), /*buscarEnCachePedidos(db_idPedido), */ db_cantidad));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(ingredientesPlatos.get(ingredientesPlatos.size() - 1));
					if (ingredientesPlatos.get(ingredientesPlatos.size() - 1).getIDIngredientePlato() > ultimaIdUtilizada)
						ultimaIdUtilizada = ingredientesPlatos.get(ingredientesPlatos.size() - 1).getIDIngredientePlato();
				}
				cacheActualizada = true;
			} else
				ingredientesPlatos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return ingredientesPlatos;
	}
	
	public synchronized ArrayList<IngredientePlato> obtenerTodos_ByidPlato(int idPlato) {
		// TODO Auto-generated method stub
		ArrayList<IngredientePlato> ingredientesPlatos = new ArrayList<IngredientePlato>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idIngredientePlato;
		Ingrediente db_ingrediente;			int db_idIngrediente;
		Plato db_plato;						int db_idPlato;
		float db_cantidad;
		
		System.out.println(this.getClass().getName() + " -> Buscando:");
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			ingredientes = FachadaPersistencia.obtenerTodos_Ingredientes();
			//platos = FachadaPersistencia.obtenerTodos_Platos();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_INGREDIENTESPLATOS_ByidPlato);
			ps.setInt(1, idPlato);
			rs = ps.executeQuery();
			
			/* Limpiamos la cach� para actualizarla por completo */
			//cache = new ArrayList<IngredientePlato>();
			/* Rellenar campos */
			while(rs.next()) {
				db_idIngredientePlato = rs.getInt("idIngredientePlato");
				db_idIngrediente = rs.getInt("idIngrediente");
				//db_idPlato = rs.getInt("idPlato");
				db_cantidad = (float)rs.getDouble("cantidad");
				
				// Hacer que los objetos sean referenciados seg�n las ids obtenidas
				ingredientesPlatos.add(new IngredientePlato(db_idIngredientePlato, buscarEnCacheIngredientes(db_idIngrediente), /*buscarEnCachePedidos(db_idPedido), */ db_cantidad));
				
				// Actualizamos el �ltimo id de la cach�
				addToCache(ingredientesPlatos.get(ingredientesPlatos.size() - 1));
				if (ingredientesPlatos.get(ingredientesPlatos.size() - 1).getIDIngredientePlato() > ultimaIdUtilizada)
					ultimaIdUtilizada = ingredientesPlatos.get(ingredientesPlatos.size() - 1).getIDIngredientePlato();
			}
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: ConversorIngredientePlato -> obtenerTodos_ByidPlato()" + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando... BYID");
		
		return ingredientesPlatos;
	}
	
	public synchronized ArrayList<IngredientePlato> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(IngredientePlato obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDIngredientePlato() == obj.getIDIngredientePlato()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized IngredientePlato buscarEnCache(IngredientePlato obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngredientePlato() == obj.getIDIngredientePlato()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized IngredientePlato buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDIngredientePlato() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized Ingrediente buscarEnCacheIngredientes(int id) {
		for (int i = 0; i < ingredientes.size(); i++) {
			if (ingredientes.get(i).getIDIngrediente() == id) {
				return ingredientes.get(i);
			}
		}
		return null;
	}
	
	private synchronized Plato buscarEnCachePlatos(int id) {
		for (int i = 0; i < platos.size(); i++) {
			if (platos.get(i).getIDPlato() == id) {
				return platos.get(i);
			}
		}
		return null;
	}

	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eliminar(Object objeto) {
		// TODO Auto-generated method stub
		return null;
	}
}