package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Alergeno;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.empleados.CategoriaEmpleado;

public class ConversorCategoriaIngrediente extends ConversorAbstracto {
	
	private static ConversorCategoriaIngrediente instanciaConversorCategoriaIngrediente;
	
	// Cach�
	private static ArrayList<CategoriaIngrediente> cache = new ArrayList<CategoriaIngrediente>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorCategoriaIngrediente getConversor() {
		if(instanciaConversorCategoriaIngrediente == null) {
			instanciaConversorCategoriaIngrediente = new ConversorCategoriaIngrediente();
		}
		return instanciaConversorCategoriaIngrediente;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaIngrediente categoriaIngrediente = (CategoriaIngrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaIngrediente.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASINGREDIENTES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, categoriaIngrediente.getNombre());
				ps.setString(2, categoriaIngrediente.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + categoriaIngrediente.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + categoriaIngrediente.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaIngrediente.getNombre().isEmpty()");
			return null;
		}
		return categoriaIngrediente;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		CategoriaIngrediente categoriaIngrediente = (CategoriaIngrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaIngrediente.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			CategoriaIngrediente categoriaIngredienteAEliminar = FachadaPersistencia.obtener_CategoriaIngrediente_ByidCategoriaIngrediente(categoriaIngrediente.getIDCategoria());
			try {
				// Comprobar que est� en la base de datos
				if (categoriaIngredienteAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASINGREDIENTES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, categoriaIngredienteAEliminar.getIDCategoria());
					ps.setString(2, categoriaIngredienteAEliminar.getNombre());
					ps.setString(3, categoriaIngredienteAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_CATEGORIASINGREDIENTES_ByidCategoriaIngrediente);
					ps.setInt(1, categoriaIngredienteAEliminar.getIDCategoria());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + categoriaIngredienteAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					categoriaIngrediente = categoriaIngredienteAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + categoriaIngredienteAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaIngrediente.getNombre().isEmpty()");
			return null;
		}
		return categoriaIngrediente;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaIngrediente categoriaIngrediente = (CategoriaIngrediente) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaIngrediente.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				// Tengo que modificarlo
				CategoriaIngrediente categoriaIngredienteAModificar = FachadaPersistencia.obtener_CategoriaIngrediente_ByidCategoriaIngrediente(categoriaIngrediente.getIDCategoria());
				if (categoriaIngredienteAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_CATEGORIASINGREDIENTES_ByidCategoriaIngrediente);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, categoriaIngrediente.getNombre());
					ps.setString(2, categoriaIngrediente.getDescripcion());
					ps.setInt(3, categoriaIngrediente.getIDCategoria());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + categoriaIngrediente.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + categoriaIngrediente.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaIngrediente.getNombre().isEmpty()");
			return null;
		}
		return categoriaIngrediente;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		CategoriaIngrediente catIng = buscarEnCache(id);
		
		if (catIng == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idCategoriaIngrediente;
			String db_nombre;
			String db_descripcion;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASINGREDIENTES_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaIngrediente = rs.getInt("idCategoriaIngrediente");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hay que sustituir stockAct y stockMin
					catIng = new CategoriaIngrediente(db_idCategoriaIngrediente, db_nombre, db_descripcion);
					
					addToCache(catIng);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return catIng;
	}
	
	@Override
	public synchronized ArrayList<CategoriaIngrediente> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<CategoriaIngrediente> categoriasIngredientes = new ArrayList<CategoriaIngrediente>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idCategoriaIngrediente;
		String db_nombre;
		String db_descripcion;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASINGREDIENTES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idCategoriaIngrediente") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<CategoriaIngrediente>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaIngrediente = rs.getInt("idCategoriaIngrediente");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					categoriasIngredientes.add(new CategoriaIngrediente(db_idCategoriaIngrediente, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(categoriasIngredientes.get(categoriasIngredientes.size() - 1));
					if (categoriasIngredientes.get(categoriasIngredientes.size() - 1).getIDCategoria() > ultimaIdUtilizada)
						ultimaIdUtilizada = categoriasIngredientes.get(categoriasIngredientes.size() - 1).getIDCategoria();
				}
				cacheActualizada = true;
			} else
				categoriasIngredientes = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return categoriasIngredientes;
	}
	
	public synchronized ArrayList<CategoriaIngrediente> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(CategoriaIngrediente obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDCategoria() == obj.getIDCategoria()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaIngrediente buscarEnCache(CategoriaIngrediente obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoria() == obj.getIDCategoria()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaIngrediente buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoria() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}