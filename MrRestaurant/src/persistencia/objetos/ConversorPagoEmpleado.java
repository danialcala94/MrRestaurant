package persistencia.objetos;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.cocina.CategoriaPlato;
import restaurante.empleados.Empleado;
import restaurante.empleados.PagoEmpleado;

public class ConversorPagoEmpleado extends ConversorAbstracto {
	
	private static ConversorPagoEmpleado instanciaConversorPagoEmpleado;
	
	// Cach�
	private static ArrayList<PagoEmpleado> cache = new ArrayList<PagoEmpleado>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de EMPLEADO
		private static ArrayList<Empleado> empleados = FachadaPersistencia.obtenerTodos_Empleados();
	
	public static synchronized ConversorPagoEmpleado getConversor() {
		if(instanciaConversorPagoEmpleado == null) {
			instanciaConversorPagoEmpleado = new ConversorPagoEmpleado();
		}
		return instanciaConversorPagoEmpleado;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		PagoEmpleado pagoEmpleado = (PagoEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PAGOSEMPLEADOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// cantidad, idEmpleado, fecha
				ps.setFloat(1, pagoEmpleado.getCantidad());
				ps.setInt(2, pagoEmpleado.getEmpleado().getIDEmpleado());
				ps.setDate(3, pagoEmpleado.getFecha());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
				+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
						+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0");
			return null;
		}
		return pagoEmpleado;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		PagoEmpleado pagoEmpleado = (PagoEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			PagoEmpleado pagoEmpleadoAEliminar = FachadaPersistencia.obtener_PagoEmpleado_ByidPagoEmpleado(pagoEmpleado.getIDPagoEmpleado());
			try {
				// Comprobar que est� en la base de datos
				if (pagoEmpleadoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PAGOSEMPLEADOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idPagoEmpleado, cantidad, idEmpleado, fecha
					ps.setInt(1, pagoEmpleadoAEliminar.getIDPagoEmpleado());
					ps.setFloat(2, pagoEmpleadoAEliminar.getCantidad());
					ps.setInt(3, pagoEmpleadoAEliminar.getEmpleado().getIDEmpleado());
					ps.setDate(4, pagoEmpleadoAEliminar.getFecha());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_PAGOSEMPLEADOS_ByidPagoEmpleado);
					ps.setInt(1, pagoEmpleadoAEliminar.getIDPagoEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
							+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
					// Para que devuelva el objeto correcto
					pagoEmpleado = pagoEmpleadoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
						+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0");
			return null;
		}
		return pagoEmpleado;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		PagoEmpleado pagoEmpleado = (PagoEmpleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				PagoEmpleado pagoEmpleadoAModificar = FachadaPersistencia.obtener_PagoEmpleado_ByidPagoEmpleado(pagoEmpleado.getIDPagoEmpleado());
				if (pagoEmpleadoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_PAGOSEMPLEADOS_ByidPagoEmpleado);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// cantidad = ?, idEmpleado = ?, fecha = ? WHERE pagosempleados.idPagoEmpleado = ?
					ps.setFloat(1, pagoEmpleado.getCantidad());
					ps.setInt(2, pagoEmpleado.getEmpleado().getIDEmpleado());
					ps.setDate(3, pagoEmpleado.getFecha());
					ps.setInt(4, pagoEmpleadoAModificar.getIDPagoEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
							+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + pagoEmpleado.getEmpleado().getDni() + " - " 
						+ pagoEmpleado.getCantidad() + " - " + pagoEmpleado.getFecha());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() <= 0");
			return null;
		}
		return pagoEmpleado;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		PagoEmpleado pagEmp = buscarEnCache(id);
		
		if (pagEmp == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idPagoEmpleado;
			float db_cantidad;
			Empleado db_empleado;			int db_idEmpleado;
			Date db_fecha;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PAGOSEMPLEADOS_ByidPagoEmpleado);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idPagoEmpleado = rs.getInt("idPagoEmpleado");
					db_cantidad = (float)rs.getDouble("cantidad");
					db_idEmpleado = rs.getInt("idEmpleado");
					db_fecha = rs.getDate("fecha");
					
					// Hay que sustituir stockAct y stockMin
					pagEmp = new PagoEmpleado(db_idPagoEmpleado, db_cantidad, buscarEnCacheEmpleados(db_idEmpleado), db_fecha);
					
					addToCache(pagEmp);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return pagEmp;
	}
	
	@Override
	public synchronized ArrayList<PagoEmpleado> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<PagoEmpleado> pagosEmpleados = new ArrayList<PagoEmpleado>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idPagoEmpleado;
		float db_cantidad;
		Empleado db_empleado;			int db_idEmpleado;
		Date db_fecha;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PAGOSEMPLEADOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idPagoEmpleado") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<PagoEmpleado>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idPagoEmpleado = rs.getInt("idPagoEmpleado");
					db_cantidad = (float)rs.getDouble("cantidad");
					db_idEmpleado = rs.getInt("idEmpleado");
					db_fecha = rs.getDate("fecha");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					pagosEmpleados.add(new PagoEmpleado(db_idPagoEmpleado, db_cantidad, buscarEnCacheEmpleados(db_idEmpleado), db_fecha));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(pagosEmpleados.get(pagosEmpleados.size() - 1));
					if (pagosEmpleados.get(pagosEmpleados.size() - 1).getIDPagoEmpleado() > ultimaIdUtilizada)
						ultimaIdUtilizada = pagosEmpleados.get(pagosEmpleados.size() - 1).getIDPagoEmpleado();
				}
				cacheActualizada = true;
			} else
				pagosEmpleados = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return pagosEmpleados;
	}
	
	public synchronized ArrayList<PagoEmpleado> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(PagoEmpleado obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDPagoEmpleado() == obj.getIDPagoEmpleado()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized PagoEmpleado buscarEnCache(PagoEmpleado obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDPagoEmpleado() == obj.getIDPagoEmpleado()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized PagoEmpleado buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDPagoEmpleado() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized Empleado buscarEnCacheEmpleados(int id) {
		for (int i = 0; i < empleados.size(); i++) {
			if (empleados.get(i).getIDEmpleado() == id) {
				return empleados.get(i);
			}
		}
		return null;
	}
}