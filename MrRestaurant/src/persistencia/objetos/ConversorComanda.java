package persistencia.objetos;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;
import restaurante.sala.Comanda;

public class ConversorComanda extends ConversorAbstracto {
	
	private static ConversorComanda instanciaConversorComanda;
	
	// Cach�
	private static ArrayList<Comanda> cache = new ArrayList<Comanda>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorComanda getConversor() {
		if(instanciaConversorComanda == null) {
			instanciaConversorComanda = new ConversorComanda();
		}
		return instanciaConversorComanda;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Comanda comanda = (Comanda) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (comanda.getFecha() == null || comanda.getMesa() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDAS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// idMesa, fecha, numeroClientes
				ps.setInt(1, comanda.getMesa().getIDMesa());
				// La fecha usada en las mesas deber�a ser de java.sql.Date
				ps.setDate(2, (java.sql.Date)comanda.getFecha());
				ps.setInt(3, comanda.getMesa().getNumActComensales());
				ps.executeUpdate();
				// Tengo que obtener el id con que se guarda para almacenarlo
				ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDAS);
				ResultSet rs = ps.executeQuery();
				rs.last();
				int idComanda = rs.getInt("idComanda");
				
				ArrayList<ComandaPlato> comandasPlatos = comanda.getComandaPlatos();
				for (int i = 0; i < comandasPlatos.size(); i++) {
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDASPLATOS);
					// idComanda, cantidad, idPlato, precio, comentario
					ps.setInt(1, idComanda);
					ps.setInt(2, comandasPlatos.get(i).getCantidad());
					ps.setInt(3, comandasPlatos.get(i).getPlato().getIDPlato());
					// El precio se autocalcula a partir del individual del plato y la cantidad
					ps.setFloat(4, comandasPlatos.get(i).getPrecio());
					ps.setString(5, comandasPlatos.get(i).getComentario());
					ps.executeUpdate();
				}
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa() + " - " + comanda.getFecha().toString());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa()+ " - " + comanda.getFecha().toString());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comanda.getFecha() == null || comanda.getMesa() == null");
			return null;
		}
		return comanda;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Comanda comanda = (Comanda) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (comanda.getIDComanda() < 0 || comanda.getFecha() == null || comanda.getMesa() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Comanda comandaAEliminar = FachadaPersistencia.obtener_Comanda_ByidComanda(comanda.getIDComanda());
			ArrayList<ComandaPlato> comandasPlatosAEliminar = FachadaPersistencia.obtenerTodos_ComandasPlatos_ByidComanda(comandaAEliminar.getIDComanda());
			try {
				// Tengo que eliminarlo
				if (comandaAEliminar != null && comandasPlatosAEliminar.size() > 0) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDAS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idComanda, idMesa, fecha, numeroClientes
					ps.setInt(1, comandaAEliminar.getIDComanda());
					ps.setInt(2, comandaAEliminar.getMesa().getIDMesa());
					ps.setDate(3, (java.sql.Date)comandaAEliminar.getFecha());
					ps.setInt(4, comandaAEliminar.getMesa().getNumActComensales());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_COMANDAS_ByidComanda);
					ps.setInt(1, comandaAEliminar.getIDComanda());
					ps.executeUpdate();
					// Guardo los comandasplatos en la tabla_old
					for (int i = 0; i < comandasPlatosAEliminar.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDASPLATOS_OLD);
						// idComandaPlato, idComanda, cantidad, idPlato, precio, comentario
						ps.setInt(1, comandasPlatosAEliminar.get(i).getIDComandaPlato());
						ps.setInt(2, comandaAEliminar.getIDComanda());
						ps.setInt(3, comandasPlatosAEliminar.get(i).getCantidad());
						ps.setInt(4, comandasPlatosAEliminar.get(i).getPlato().getIDPlato());
						ps.setFloat(5, comandasPlatosAEliminar.get(i).getPrecio());
						ps.setString(6, comandasPlatosAEliminar.get(i).getComentario());
						ps.executeUpdate();
					}
					// Los borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_COMANDASPLATOS_ByidComanda);
					ps.setInt(1, comandaAEliminar.getIDComanda());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa()+ " - " + comanda.getFecha().toString());
					comanda = comandaAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa()+ " - " + comanda.getFecha().toString());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comanda.getFecha() == null || comanda.getMesa() == null");
			return null;
		}
		return comanda;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Comanda comanda = (Comanda) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (comanda.getIDComanda() < 0 || comanda.getFecha() == null || comanda.getMesa() == null)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			Comanda comandaAModificar = FachadaPersistencia.obtener_Comanda_ByidComanda(comanda.getIDComanda());
			ArrayList<ComandaPlato> comandasPlatosAModificar = FachadaPersistencia.obtenerTodos_ComandasPlatos_ByidComanda(comandaAModificar.getIDComanda());
			try {
				// Tengo que modificarlo
				if (comandaAModificar != null && comandasPlatosAModificar.size() > 0) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_COMANDAS_ByidComanda);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idMesa = ?, fecha = ?, numeroClientes = ? WHERE comandas.idComanda = ?"
					ps.setInt(1, comanda.getMesa().getIDMesa());
					ps.setDate(2, (java.sql.Date)comanda.getFecha());
					ps.setInt(3, comanda.getMesa().getNumActComensales());
					ps.setInt(4, comanda.getIDComanda());
					ps.executeUpdate();
					// Modificamos los ComandaPlato (eliminamos y creamos los nuevos)
					/* Podr�amos comprobar cu�les se han mantenido y a�adir solo los nuevos */
					// (eliminamos)
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_COMANDASPLATOS_ByidComanda);
					ps.setInt(1, comandaAModificar.getIDComanda());
					ps.executeUpdate();
					// (creamos los nuevos)
					ArrayList<ComandaPlato> comandasPlatos = comandaAModificar.getComandaPlatos();
					for (int i = 0; i < comandasPlatos.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_COMANDASPLATOS);
						// idComanda, cantidad, idPlato, precio, comentario
						ps.setInt(1, comandaAModificar.getIDComanda());
						ps.setInt(2, comandasPlatos.get(i).getCantidad());
						ps.setInt(3, comandasPlatos.get(i).getPlato().getIDPlato());
						ps.setFloat(4, comandasPlatos.get(i).getPrecio());
						ps.setString(5, comandasPlatos.get(i).getComentario());
						ps.executeUpdate();
					}
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa()+ " - " + comanda.getFecha().toString());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") Mesa " + comanda.getMesa().getIDMesa()+ " - " + comanda.getFecha().toString());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: comanda.getFecha() == null || comanda.getMesa() == null");
			return null;
		}
		return comanda;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Comanda com = buscarEnCache(id);
		
		if (com == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idComanda;
			int db_idMesa;
			Date db_fecha;
			int db_numeroClientes;
			float db_precio;
			ArrayList<ComandaPlato> db_comandas;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDAS_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idComanda = rs.getInt("idComanda");
					db_idMesa = rs.getInt("idMesa");
					db_fecha = rs.getDate("fecha");
					// Faltas incorporarlo en base de datos
					//db_precio = (float)rs.getDouble("precio");
					// La mesa tiene el n�mero de comensales sentados, esto interesa en la persistencia, no en ejecuci�n porque lo tiene la mesa
					db_numeroClientes = rs.getInt("numeroClientes");
					
					// Hay que sustituir stockAct y stockMin
					com = new Comanda(db_idComanda, db_idMesa, db_fecha, null);
				}
				
				// Obtener las comandasplatos que tengan como idComanda la que estamos utilizando
				com.setComandaPlatos(FachadaPersistencia.obtenerTodos_ComandasPlatos_ByidComanda(com.getIDComanda()));
				
				addToCache(com);
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return com;
	}
	
	@Override
	public synchronized ArrayList<Comanda> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Comanda> comandas = new ArrayList<Comanda>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idComanda;
		int db_idMesa;
		Date db_fecha;
		int db_numeroClientes;
		float db_precio;
		ArrayList<ComandaPlato> db_comandas;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_COMANDAS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idComanda") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Comanda>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idComanda = rs.getInt("idComanda");
					db_idMesa = rs.getInt("idMesa");
					db_fecha = rs.getDate("fecha");
					// Faltas incorporarlo en base de datos
					//db_precio = (float)rs.getDouble("precio");
					// La mesa tiene el n�mero de comensales sentados, esto interesa en la persistencia, no en ejecuci�n porque lo tiene la mesa
					db_numeroClientes = rs.getInt("numeroClientes");
					// Obtener las comandasplatos que tengan como idComanda la que estamos utilizando
					//db_comandas = FachadaPersistencia.obtenerComandasplatosByidComanda(id);
					
					// No le pasamos el precio, lo autocalcula luego
					comandas.add(new Comanda(db_idComanda, db_idMesa, db_fecha, null));
				}
				
				for (int i = 0; i < comandas.size(); i++) {
					comandas.get(i).setComandaPlatos(FachadaPersistencia.obtenerTodos_ComandasPlatos_ByidComanda(comandas.get(i).getIDComanda()));
					// Actualizamos el �ltimo id de la cach�
					addToCache(comandas.get(i));
					if (comandas.get(i).getIDComanda() > ultimaIdUtilizada)
						ultimaIdUtilizada = comandas.get(i).getIDComanda();
				}
				cacheActualizada = true;
			} else
				comandas = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return comandas;
	}
	
	public synchronized ArrayList<Comanda> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Comanda obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDComanda() == obj.getIDComanda()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Comanda buscarEnCache(Comanda obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDComanda() == obj.getIDComanda()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Comanda buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDComanda() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}