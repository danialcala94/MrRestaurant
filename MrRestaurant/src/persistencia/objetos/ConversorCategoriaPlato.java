package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.CategoriaIngrediente;
import restaurante.cocina.CategoriaPlato;

public class ConversorCategoriaPlato extends ConversorAbstracto {
	
	private static ConversorCategoriaPlato instanciaConversorCategoriaPlato;
	
	// Cach�
	private static ArrayList<CategoriaPlato> cache = new ArrayList<CategoriaPlato>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorCategoriaPlato getConversor() {
		if(instanciaConversorCategoriaPlato == null) {
			instanciaConversorCategoriaPlato = new ConversorCategoriaPlato();
		}
		return instanciaConversorCategoriaPlato;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaPlato categoriaPlato = (CategoriaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaPlato.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASPLATOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, categoriaPlato.getNombre());
				ps.setString(2, categoriaPlato.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + categoriaPlato.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + categoriaPlato.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaPlato.getNombre().isEmpty()");
			return null;
		}
		return categoriaPlato;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		CategoriaPlato categoriaPlato = (CategoriaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaPlato.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			CategoriaPlato categoriaPlatoAEliminar = FachadaPersistencia.obtener_CategoriaPlato_ByidCategoriaPlato(categoriaPlato.getIDCategoriaPlato());
			try {
				// Comprobar que est� en la base de datos
				if (categoriaPlatoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_CATEGORIASINGREDIENTES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, categoriaPlatoAEliminar.getIDCategoriaPlato());
					ps.setString(2, categoriaPlatoAEliminar.getNombre());
					ps.setString(3, categoriaPlatoAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_CATEGORIASPLATOS_ByidCategoriaPlato);
					ps.setInt(1, categoriaPlatoAEliminar.getIDCategoriaPlato());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + categoriaPlatoAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					categoriaPlato = categoriaPlatoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + categoriaPlatoAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaPlato.getNombre().isEmpty()");
			return null;
		}
		return categoriaPlato;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		CategoriaPlato categoriaPlato = (CategoriaPlato) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (categoriaPlato.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// Tengo que modificarlo
				CategoriaPlato categoriaPlatoAModificar = FachadaPersistencia.obtener_CategoriaPlato_ByidCategoriaPlato(categoriaPlato.getIDCategoriaPlato());
				if (categoriaPlatoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_CATEGORIASINGREDIENTES_ByidCategoriaIngrediente);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, categoriaPlato.getNombre());
					ps.setString(2, categoriaPlato.getDescripcion());
					ps.setInt(3, categoriaPlato.getIDCategoriaPlato());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + categoriaPlato.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + categoriaPlato.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: categoriaPlato.getNombre().isEmpty()");
			return null;
		}
		return categoriaPlato;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		CategoriaPlato catPla = buscarEnCache(id);
		
		if (catPla == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idCategoriaPlato;
			String db_nombre;
			String db_descripcion;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASPLATOS_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaPlato = rs.getInt("idCategoriaPlato");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hay que sustituir stockAct y stockMin
					catPla = new CategoriaPlato(db_idCategoriaPlato, db_nombre, db_descripcion);
					
					addToCache(catPla);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return catPla;
	}
	
	@Override
	public synchronized ArrayList<CategoriaPlato> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<CategoriaPlato> categoriasPlatos = new ArrayList<CategoriaPlato>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idCategoriaPlato;
		String db_nombre;
		String db_descripcion;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_CATEGORIASPLATOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idCategoriaPlato") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<CategoriaPlato>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idCategoriaPlato = rs.getInt("idCategoriaPlato");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					categoriasPlatos.add(new CategoriaPlato(db_idCategoriaPlato, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(categoriasPlatos.get(categoriasPlatos.size() - 1));
					if (categoriasPlatos.get(categoriasPlatos.size() - 1).getIDCategoriaPlato() > ultimaIdUtilizada)
						ultimaIdUtilizada = categoriasPlatos.get(categoriasPlatos.size() - 1).getIDCategoriaPlato();
				}
				cacheActualizada = true;
			} else
				categoriasPlatos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: ConversorCategoriaPlato -> obtenerTodos()" + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return categoriasPlatos;
	}
	
	public synchronized ArrayList<CategoriaPlato> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(CategoriaPlato obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDCategoriaPlato() == obj.getIDCategoriaPlato()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaPlato buscarEnCache(CategoriaPlato obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoriaPlato() == obj.getIDCategoriaPlato()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized CategoriaPlato buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDCategoriaPlato() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}