package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.*;
import restaurante.cocina.CategoriaPlato;

public class ConversorMagnitud extends ConversorAbstracto {
	
	private static ConversorMagnitud instanciaConversorMagnitud;
	
	// Cach�
	private static ArrayList<Magnitud> cache = new ArrayList<Magnitud>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorMagnitud getConversor() {
		if(instanciaConversorMagnitud == null) {
			instanciaConversorMagnitud = new ConversorMagnitud();
		}
		return instanciaConversorMagnitud;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Magnitud magnitud = (Magnitud) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (magnitud.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_MAGNITUDES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, magnitud.getNombre());
				ps.setString(2, magnitud.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + magnitud.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + magnitud.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: magnitud.getNombre().isEmpty()");
			return null;
		}
		return magnitud;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Magnitud magnitud = (Magnitud) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (magnitud.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			Magnitud magnitudAEliminar = FachadaPersistencia.obtener_Magnitud_ByidMagnitud(magnitud.getIDMagnitud());
			try {
				// Comprobar que est� en la base de datos
				if (magnitudAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_MAGNITUDES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, magnitudAEliminar.getIDMagnitud());
					ps.setString(2, magnitudAEliminar.getNombre());
					ps.setString(3, magnitudAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_MAGNITUDES_ByidMagnitud);
					ps.setInt(1, magnitudAEliminar.getIDMagnitud());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + magnitudAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					magnitud = magnitudAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + magnitudAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: magnitud.getNombre().isEmpty()");
			return null;
		}
		return magnitud;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Magnitud magnitud = (Magnitud) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (magnitud.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				Magnitud magnitudAModificar = FachadaPersistencia.obtener_Magnitud_ByidMagnitud(magnitud.getIDMagnitud());
				if (magnitudAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_MAGNITUDES_ByidMagnitud);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, magnitud.getNombre());
					ps.setString(2, magnitud.getDescripcion());
					ps.setInt(3, magnitud.getIDMagnitud());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + magnitud.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + magnitud.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: magnitud.getNombre().isEmpty()");
			return null;
		}
		return magnitud;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Magnitud mag = buscarEnCache(id);
		
		if (mag == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idMagnitud;
			String db_nombre;
			String db_descripcion;
			
			String sqlQuery = "SELECT * FROM magnitudes WHERE magnitudes.idMagnitud = ?";
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(sqlQuery);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idMagnitud = rs.getInt("idMagnitud");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hay que sustituir stockAct y stockMin
					mag = new Magnitud(db_idMagnitud, db_nombre, db_descripcion);
					
					addToCache(mag);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return mag;
	}
	
	@Override
	public synchronized ArrayList<Magnitud> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Magnitud> magnitudes = new ArrayList<Magnitud>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idMagnitud;
		String db_nombre;
		String db_descripcion;
		
		String sqlQuery = SentenciasSQL.SELECT_FROM_MAGNITUDES;
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(sqlQuery);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idMagnitud") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Magnitud>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idMagnitud = rs.getInt("idMagnitud");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					magnitudes.add(new Magnitud(db_idMagnitud, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(magnitudes.get(magnitudes.size() - 1));
					if (magnitudes.get(magnitudes.size() - 1).getIDMagnitud() > ultimaIdUtilizada)
						ultimaIdUtilizada = magnitudes.get(magnitudes.size() - 1).getIDMagnitud();
				}
				cacheActualizada = true;
			} else
				magnitudes = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return magnitudes;
	}
	
	public synchronized ArrayList<Magnitud> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Magnitud obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDMagnitud() == obj.getIDMagnitud()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Magnitud buscarEnCache(Magnitud obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDMagnitud() == obj.getIDMagnitud()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Magnitud buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDMagnitud() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}