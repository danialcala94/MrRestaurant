package persistencia.objetos;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Ingrediente;
import restaurante.almacen.IngredientePedido;
import restaurante.almacen.Pedido;
import restaurante.almacen.Proveedor;
import restaurante.cocina.IngredientePlato;

public class ConversorPedido extends ConversorAbstracto {
	
	private static ConversorPedido instanciaConversorPedido;
	
	// Cach�
	private static ArrayList<Pedido> cache = new ArrayList<Pedido>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de MAGNITUD, CATEGORIAINGREDIENTE, PROVEEDOR y ALERGENO
	private static ArrayList<Proveedor> proveedores;
	private static ArrayList<IngredientePedido> ingredientesPedidos;
	
	
	public static synchronized ConversorPedido getConversor() {
		if(instanciaConversorPedido == null) {
			instanciaConversorPedido = new ConversorPedido();
		}
		return instanciaConversorPedido;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Pedido pedido = (Pedido) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PEDIDOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// idProveedor, fecha, estado, precio
				ps.setInt(1, pedido.getProveedor().getIDProveedor());
				ps.setDate(2, (java.sql.Date)pedido.getFecha());
				ps.setString(3, pedido.getEstado());
				ps.setFloat(4, pedido.getPrecio());
				ps.executeUpdate();
				ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PEDIDOS);
				ResultSet rs = ps.executeQuery();
				rs.last();
				int idPedido = rs.getInt("idPedido");
				ArrayList<IngredientePedido> ingredientes = pedido.getIngredientesPedido();
				for (int i = 0; i < ingredientes.size(); i++) {
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPEDIDOS);
					// idPedido, idIngrediente, precio, cantidad
					ps.setInt(1, idPedido);
					ps.setInt(2, ingredientes.get(i).getIngrediente().getIDIngrediente());
					ps.setFloat(3, ingredientes.get(i).getPrecio());
					ps.setFloat(4, ingredientes.get(i).getCantidad());
					ps.executeUpdate();
				}
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0");
			return null;
		}
		return pedido;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Pedido pedido = (Pedido) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Pedido pedidoAEliminar = FachadaPersistencia.obtener_Pedido_ByidPedido(pedido.getIDPedido());
			ArrayList<IngredientePedido> ingredientespedidosAEliminar = FachadaPersistencia.obtenerTodos_IngredientesPedidos_ByidPedido(pedido.getIDPedido());
			try {
				// Tengo que eliminarlo
				if (pedidoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PEDIDOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idPedido, idProveedor, fecha, estado, precio
					ps.setInt(1, pedidoAEliminar.getIDPedido());
					ps.setInt(2, pedidoAEliminar.getProveedor().getIDProveedor());
					ps.setDate(3, (java.sql.Date)pedidoAEliminar.getFecha());
					ps.setString(4, pedidoAEliminar.getEstado());
					ps.setFloat(5, pedidoAEliminar.getPrecio());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_PEDIDOS_ByidPedido);
					ps.setInt(1, pedidoAEliminar.getIDPedido());
					ps.executeUpdate();
					// Guardo los ingredientes en la tabla_old
					for (int i = 0; i < ingredientespedidosAEliminar.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPEDIDOS_OLD);
						ps.setInt(1, ingredientespedidosAEliminar.get(i).getIDIngredientePedido());
						ps.setInt(2, pedidoAEliminar.getIDPedido());
						ps.setInt(3, ingredientespedidosAEliminar.get(i).getIngrediente().getIDIngrediente());
						ps.setFloat(4, ingredientespedidosAEliminar.get(i).getPrecio());
						ps.setFloat(5, ingredientespedidosAEliminar.get(i).getCantidad());
						ps.executeUpdate();
					}
					// Los borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_INGREDIENTESPEDIDOS_ByidPedido);
					ps.setInt(1, pedidoAEliminar.getIDPedido());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
					pedido = pedidoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0");
			return null;
		}
		return pedido;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Pedido pedido = (Pedido) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			Pedido pedidoAModificar = FachadaPersistencia.obtener_Pedido_ByidPedido(pedido.getIDPedido());
			try {
				// Tengo que modificarlo
				if (pedidoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_PEDIDOS_ByidPedido);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idProveedor = ?, fecha = ?, estado = ?, precio = ? WHERE pedidos.idPedido = ?
					ps.setInt(1, pedido.getProveedor().getIDProveedor());
					ps.setDate(2, (java.sql.Date)pedido.getFecha());
					ps.setString(3, pedido.getEstado());
					ps.setFloat(4, pedido.getPrecio());
					ps.setInt(5, pedido.getIDPedido());
					ps.executeUpdate();
					// Modificamos los IngredientePedido (eliminamos y creamos los nuevos)
					/* Podr�amos comprobar cu�les se han mantenido y a�adir solo los nuevos */
					// (eliminamos)
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_INGREDIENTESPEDIDOS_ByidPedido);
					ps.setInt(1, pedidoAModificar.getIDPedido());
					ps.executeUpdate();
					// (creamos los nuevos)
					ArrayList<IngredientePedido> ingredientes = pedidoAModificar.getIngredientesPedido();
					for (int i = 0; i < ingredientes.size(); i++) {
						ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_INGREDIENTESPEDIDOS);
						ps.setInt(1, pedidoAModificar.getIDPedido());
						ps.setInt(2, ingredientes.get(i).getIngrediente().getIDIngrediente());
						ps.setFloat(3, ingredientes.get(i).getPrecio());
						ps.setFloat(4, ingredientes.get(i).getCantidad());
						ps.executeUpdate();
					}
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") " + pedido.getFecha() + " - " + pedido.getEstado());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0");
			return null;
		}
		return pedido;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Pedido ped = buscarEnCache(id);
		
		if (ped == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idPedido;
			Proveedor db_proveedor;				int db_idProveedor;
			Date db_fecha;
			String db_estado;
			float db_precio;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
				ingredientesPedidos = FachadaPersistencia.obtenerTodos_IngredientesPedidos();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PEDIDOS_ByidPedido);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idPedido = rs.getInt("idPedido");
					db_idProveedor = rs.getInt("idProveedor");
					db_fecha = rs.getDate("fecha");
					db_precio = (float)rs.getDouble("precio");
					db_estado = rs.getString("estado");
					
					// Hay que sustituir stockAct y stockMin
					ped = new Pedido(db_idPedido, buscarEnCacheProveedores(db_idProveedor), FachadaPersistencia.obtenerTodos_IngredientesPedidos_ByidPedido(db_idPedido), 
							db_fecha, db_precio, db_estado);
					
					addToCache(ped);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return ped;
	}
	
	@Override
	public synchronized ArrayList<Pedido> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idPedido;
		Proveedor db_proveedor;				int db_idProveedor;
		Date db_fecha;
		String db_estado;
		float db_precio;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			proveedores = FachadaPersistencia.obtenerTodos_Proveedores();
			ingredientesPedidos = FachadaPersistencia.obtenerTodos_IngredientesPedidos();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PEDIDOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idPedido") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Pedido>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idPedido = rs.getInt("idPedido");
					db_idProveedor = rs.getInt("idProveedor");
					db_fecha = rs.getDate("fecha");
					db_precio = (float)rs.getDouble("precio");
					db_estado = rs.getString("estado");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					pedidos.add(new Pedido(db_idPedido, buscarEnCacheProveedores(db_idProveedor), null, 
							db_fecha, db_precio, db_estado));
					
					
				}
				
				for (int i = 0; i < pedidos.size(); i++) {
					pedidos.get(i).setIngredientesPedido(FachadaPersistencia.obtenerTodos_IngredientesPedidos_ByidPedido(pedidos.get(i).getIDPedido()));
					// Actualizamos el �ltimo id de la cach�
					addToCache(pedidos.get(i));
					if (pedidos.get(i).getIDPedido() > ultimaIdUtilizada)
						ultimaIdUtilizada = pedidos.get(i).getIDPedido();
				}
				cacheActualizada = true;
			} else
				pedidos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return pedidos;
	}
	
	public synchronized ArrayList<Pedido> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Pedido obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDPedido() == obj.getIDPedido()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Pedido buscarEnCache(Pedido obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDPedido() == obj.getIDPedido()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Pedido buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDPedido() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized Proveedor buscarEnCacheProveedores(int id) {
		for (int i = 0; i < proveedores.size(); i++) {
			if (proveedores.get(i).getIDProveedor() == id) {
				return proveedores.get(i);
			}
		}
		return null;
	}
	
	private synchronized IngredientePedido buscarEnCacheIngredientesPedidos(int id) {
		for (int i = 0; i < ingredientesPedidos.size(); i++) {
			if (ingredientesPedidos.get(i).getIDIngredientePedido() == id) {
				return ingredientesPedidos.get(i);
			}
		}
		return null;
	}
}