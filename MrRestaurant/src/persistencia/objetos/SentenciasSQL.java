package persistencia.objetos;

public final class SentenciasSQL {
	public static final String SELECT_FROM_INGREDIENTES = "SELECT * FROM ingredientes";
	public static final String SELECT_FROM_INGREDIENTES_ByID = "SELECCT * FROM ingredientes WHERE ingredientes.idIngrediente = ?";
	public static final String INSERT_INTO_INGREDIENTES = "INSERT INTO ingredientes (nombreLargo, nombre, idMagnitud, idCategoriaIngrediente, idProveedor, codigoProveedor, idAlergeno, stockAct, stockMin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String INSERT_INTO_INGREDIENTES_OLD = "INSERT INTO ingredientes (idIngrediente, nombreLargo, nombre, idMagnitud, idCategoriaIngrediente, idProveedor, codigoProveedor, idAlergeno, stockAct, stockMin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_INGREDIENTES_ByidIngrediente = "DELETE FROM ingredientes WHERE ingredientes.idIngrediente = ?";
	public static final String UPDATE_INGREDIENTES_ByidIngrediente = "UPDATE ingredientes SET nombreLargo = ?, nombre = ?, idMagnitud = ?, idCategoriaIngrediente = ?, idProveedor = ?, codigoProveedor = ?, idAlergeno = ?, stockAct = ?, stockMin = ? WHERE ingredientes.idIngrediente = ?;";
	
	public static final String SELECT_FROM_MAGNITUDES = "SELECT * FROM magnitudes";
	public static final String SELECT_FROM_MAGNITUDES_ByID = "SELECT * FROM magnitudes WHERE magnitudes.idMagnitud = ?";
	public static final String INSERT_INTO_MAGNITUDES = "INSERT INTO magnitudes (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_MAGNITUDES_OLD = "INSERT INTO magnitudes_old (idMagnitud, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_MAGNITUDES_ByidMagnitud = "DELETE FROM magnitudes WHERE magnitudes.idMagnitud = ?";
	public static final String UPDATE_MAGNITUDES_ByidMagnitud = "UPDATE magnitudes SET nombre = ?, descripcion = ? WHERE magnitudes.idMagnitud = ?";
	
	public static final String SELECT_FROM_ALERGENOS_ByID = "SELECT * FROM alergenos WHERE alergenos.idAlergeno = ?";
	public static final String SELECT_FROM_ALERGENOS = "SELECT * FROM alergenos";
	public static final String INSERT_INTO_ALERGENOS = "INSERT INTO alergenos (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_ALERGENOS_OLD = "INSERT INTO alergenos_old (idAlergeno, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_ALERGENOS_ByidAlergeno = "DELETE FROM alergenos WHERE alergenos.idAlergeno = ?";
	public static final String UPDATE_ALERGENOS_ByidAlergeno = "UPDATE alergenos SET nombre = ?, descripcion = ? WHERE alergenos.idAlergeno = ?";
	
	public static final String SELECT_FROM_CATEGORIASINGREDIENTES_ByID = "SELECT * FROM categoriasIngredientes WHERE categoriasIngredientes.idCategoriaIngrediente = ?";
	public static final String SELECT_FROM_CATEGORIASINGREDIENTES = "SELECT * FROM categoriasIngredientes";
	public static final String INSERT_INTO_CATEGORIASINGREDIENTES = "INSERT INTO categoriasingredientes (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_CATEGORIASINGREDIENTES_OLD = "INSERT INTO categoriasingredientes_old (idCategoriaIngrediente, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_CATEGORIASINGREDIENTES_ByidCategoriaIngrediente = "DELETE FROM categoriasingredientes WHERE categoriasingredientes.idCategoriaIngrediente = ?";
	public static final String UPDATE_CATEGORIASINGREDIENTES_ByidCategoriaIngrediente = "UPDATE categoriasingredientes SET nombre = ?, descripcion = ? WHERE categoriasingredientes.idCategoriaIngrediente = ?";
	
	public static final String SELECT_FROM_PROVEEDORES = "SELECT * FROM proveedores";
	public static final String SELECT_FROM_PROVEEDORES_ByID = "SELECT * FROM proveedores WHERE proveedores.idProveedor = ?";
	public static final String INSERT_INTO_PROVEEDORES = "INSERT INTO proveedores (nombre, telefonoUno, telefonoDos, correoElectronico, direccion, paginaWeb, nif, ciudad, pais) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String INSERT_INTO_PROVEEDORES_OLD = "INSERT INTO proveedores_old (idProveedor, nombre, telefonoUno, telefonoDos, correoElectronico, direccion, paginaWeb, nif, ciudad, pais) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_PROVEEDORES_ByidProveedor = "DELETE FROM proveedores WHERE proveedores.idProveedor = ?";
	public static final String UPDATE_PROVEEDORES_ByidProveedor = "UPDATE proveedores SET nombre = ?, telefonoUno = ?, telefonoDos = ?, correoElectronico = ?, direccion = ?, paginaWeb = ?, nif = ?, ciudad = ?, pais = ? WHERE proveedores.idProveedor = ?";
	
	public static final String SELECT_FROM_CATEGORIASEMPLEADOS = "SELECT * FROM categoriasempleados";
	public static final String SELECT_FROM_CATEGORIASEMPLEADOS_ByID = "SELECT * FROM categoriasempleados WHERE categoriasempleados.idCategoriaEmpleado = ?";
	public static final String INSERT_INTO_CATEGORIASEMPLEADOS = "INSERT INTO categoriasempleados (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_CATEGORIASEMPLEADOS_OLD = "INSERT INTO categoriasempleados_old (idCategoriaEmpleado, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_CATEGORIASEMPLEADOS_ByidCategoriaEmpleado = "DELETE FROM categoriasempleados WHERE categoriasempleados.idCategoriaEmpleado = ?";
	public static final String UPDATE_CATEGORIASEMPLEADOS_ByidCategoriaEmpleado = "UPDATE categoriasempleados SET nombre = ?, descripcion = ? WHERE categoriasempleados.idCategoriaEmpleado = ?";
	
	public static final String SELECT_FROM_CATEGORIASPLATOS = "SELECT * FROM categoriasplatos";
	public static final String SELECT_FROM_CATEGORIASPLATOS_ByID = "SELECT * FROM categoriasplatos WHERE categoriasplatos.idCategoriaPlato = ?";
	public static final String INSERT_INTO_CATEGORIASPLATOS = "INSERT INTO categoriasplatos (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_CATEGORIASPLATOS_OLD = "INSERT INTO categoriasplatos_old (idCategoriaPlato, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_CATEGORIASPLATOS_ByidCategoriaPlato = "DELETE FROM categoriasplatos WHERE categoriasplatos.idCategoriaPlato = ?";
	public static final String UPDATE_CATEGORIASPLATOS_ByidCategoriaPlato = "UPDATE categoriasplatos SET nombre = ?, descripcion = ? WHERE categoriasplatos.idCategoriaPlato = ?";
	
	public static final String SELECT_FROM_COMANDASPLATOS = "SELECT * FROM comandasplatos";
	public static final String SELECT_FROM_COMANDASPLATOS_ByidComandaPlato = "SELECT * FROM comandasplatos WHERE comandasplatos.idComandaPlato = ?";
	public static final String SELECT_FROM_COMANDASPLATOS_ByidComanda = "SELECT * FROM comandasplatos WHERE comandasplatos.idComanda = ?";
	public static final String INSERT_INTO_COMANDASPLATOS = "INSERT INTO comandasplatos (idComanda, cantidad, idPlato, precio, comentario) VALUES (?, ?, ?, ?, ?);";
	public static final String INSERT_INTO_COMANDASPLATOS_OLD = "INSERT INTO comandasplatos_old (idComandaPlato, idComanda, cantidad, idPlato, precio, comentario) VALUES (?, ?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_COMANDASPLATOS_ByidComandaPlato = "DELETE FROM comandasplatos WHERE comandasplatos.idComandaPlato = ?";
	public static final String DELETE_FROM_COMANDASPLATOS_ByidComanda = "DELETE FROM comandasplatos WHERE comandasplatos.idComanda = ?";
	public static final String UPDATE_COMANDASPLATOS_ByidComandaPlato = "UPDATE comandasplatos SET idComanda = ?, cantidad = ?, idPlato = ?, precio = ?, comentario = ? WHERE comandasplatos.idComandaPlato = ?";
	
	public static final String SELECT_FROM_COMANDAS = "SELECT * FROM comandas";
	public static final String SELECT_FROM_COMANDAS_ByID = "SELECT * FROM comandas WHERE comandas.idComanda = ?";
	public static final String INSERT_INTO_COMANDAS = "INSERT INTO comandas (idMesa, fecha, numeroClientes) VALUES (?, ?, ?);";
	public static final String INSERT_INTO_COMANDAS_OLD = "INSERT INTO comandas_old (idComanda, idMesa, fecha, numeroClientes) VALUES (?, ?, ?, ?);";
	public static final String DELETE_FROM_COMANDAS_ByidComanda = "DELETE FROM comandas WHERE comandas.idComanda = ?";
	public static final String UPDATE_COMANDAS_ByidComanda = "UPDATE comandas SET idMesa = ?, fecha = ?, numeroClientes = ? WHERE comandas.idComanda = ?";
	
	public static final String SELECT_FROM_CREDENCIALES = "SELECT * FROM credenciales";
	public static final String SELECT_FROM_CREDENCIALES_ByidCredencial = "SELECT * FROM credenciales WHERE credenciales.idCredencial = ?";
	public static final String INSERT_INTO_CREDENCIALES = "INSERT INTO credenciales (nombre, contrasena, idNivelCredencial) VALUES (?, ?, ?);";
	public static final String INSERT_INTO_CREDENCIALES_OLD = "INSERT INTO credenciales_old (idCredencial, nombre, contrasena, idNivelCredencial) VALUES (?, ?, ?, ?);";
	public static final String DELETE_FROM_CREDENCIALES_ByidCredencial = "DELETE FROM credenciales WHERE credenciales.idCredencial = ?";
	public static final String UPDATE_CREDENCIALES_ByidCredencial = "UPDATE credenciales SET nombre = ?, contrasena = ?, idNivelCredencial = ? WHERE credenciales.idCredencial = ?";
	
	public static final String SELECT_FROM_EMPLEADOS = "SELECT * FROM empleados";
	public static final String SELECT_FROM_EMPLEADOS_ByidEmpleado = "SELECT * FROM empleados WHERE empelados.idEmpleado = ?";
	public static final String INSERT_INTO_EMPLEADOS = "INSERT INTO empleados (dni, nombreCompleto, telefono, idCategoriaEmpleado, idCredencial, salarioBase) VALUES (?, ?, ?, ?, ?, ?);";
	public static final String INSERT_INTO_EMPLEADOS_OLD = "INSERT INTO empleados (idEmpleado, dni, nombreCompleto, telefono, idCategoriaEmpleado, idCredencial, salarioBase) VALUES (?, ?, ?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_EMPLEADOS_ByidEmpleado = "DELETE FROM empleados WHERE empleados.idEmpleado = ?";
	public static final String UPDATE_EMPLEADOS_ByidEmpleado = "UPDATE empleados SET dni = ?, nombreCompleto = ?, telefono = ?, idCategoriaEmpleado = ?, idCredencial = ?, salarioBase = ? WHERE empleados.idEmpleado = ?";
	
	public static final String SELECT_FROM_INGREDIENTESPEDIDOS = "SELECT * FROM ingredientespedidos";
	public static final String SELECT_FROM_INGREDIENTESPEDIDOS_ByidIngredientePedido = "SELECT * FROM ingredientespedidos WHERE ingredientespedidos.idIngredientePedido = ?";
	public static final String SELECT_FROM_INGREDIENTESPEDIDOS_ByidPedido = "SELECT * FROM ingredientespedidos WHERE ingredientespedidos.idPedido = ?";
	public static final String INSERT_INTO_INGREDIENTESPEDIDOS = "INSERT INTO ingredientespedidos (idPedido, idIngrediente, precio, cantidad) VALUES (?, ?, ?, ?);";
	public static final String INSERT_INTO_INGREDIENTESPEDIDOS_OLD = "INSERT INTO ingredientespedidos_old (idIngredientePedido, idPedido, idIngrediente, precio, cantidad) VALUES (?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_INGREDIENTESPEDIDOS_ByidIngredientePedido = "DELETE FROM ingredientespedidos WHERE ingredientespedidos.idIngredientePedido = ?";
	public static final String DELETE_FROM_INGREDIENTESPEDIDOS_ByidPedido = "DELETE FROM ingredientespedidos WHERE ingredientespedidos.idPedido = ?";
	
	public static final String SELECT_FROM_INGREDIENTESPLATOS = "SELECT * FROM ingredientesplatos";
	public static final String SELECT_FROM_INGREDIENTESPLATOS_ByidIngredientePlato = "SELECT * FROM ingredientesplatos WHERE ingredientesplatos.idIngredientePlato = ?";
	public static final String SELECT_FROM_INGREDIENTESPLATOS_ByidPlato = "SELECT * FROM ingredientesplatos WHERE ingredientesplatos.idPlato = ?";
	public static final String INSERT_INTO_INGREDIENTESPLATOS = "INSERT INTO ingredientesplatos (idPlato, idIngrediente, cantidad) VALUES (?, ?, ?);";
	public static final String INSERT_INTO_INGREDIENTESPLATOS_OLD = "INSERT INTO ingredientesplatos_old (idIngredientePlato, idPlato, idIngrediente, cantidad) VALUES (?, ?, ?, ?);";
	public static final String DELETE_FROM_INGREDIENTESPLATOS_ByidPlato = "DELETE FROM ingredientesplatos WHERE ingredientesplatos.idPlato = ?";
	
	public static final String SELECT_FROM_NIVELESCREDENCIALES = "SELECT * FROM nivelescredenciales";
	public static final String SELECT_FROM_NIVELESCREDENCIALES_ByidNivelCredencial = "SELECT * FROM nivelescredenciales WHERE nivelescredenciales.idNivelCredencial = ?";
	public static final String INSERT_INTO_NIVELESCREDENCIALES = "INSERT INTO nivelescredenciales (nombre, descripcion) VALUES (?, ?);";
	public static final String INSERT_INTO_NIVELESCREDENCIALES_OLD = "INSERT INTO nivelescredenciales_old (idNivelCredencial, nombre, descripcion) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_NIVELESCREDENCIALES_ByidNivelCredencial = "DELETE FROM nivelescredenciales WHERE nivelescredenciales.idNivelCredencial = ?";
	public static final String UPDATE_NIVELESCREDENCIALES_ByidNivelCredencial = "UPDATE nivelescredenciales SET nombre = ?, descripcion = ? WHERE nivelescredenciales.idNivelCredencial = ?";
	
	public static final String SELECT_FROM_PAGOSEMPLEADOS = "SELECT * FROM pagosempleados";
	public static final String SELECT_FROM_PAGOSEMPLEADOS_ByidPagoEmpleado = "SELECT * FROM pagosempleados WHERE pagosempleados.idPagoEmpleado = ?";
	public static final String INSERT_INTO_PAGOSEMPLEADOS = "INSERT INTO pagosempleados (cantidad, idEmpleado, fecha) VALUES (?, ?, ?);";
	public static final String INSERT_INTO_PAGOSEMPLEADOS_OLD = "INSERT INTO pagosempleados_old (idPagoEmpleado, cantidad, idEmpleado, fecha) VALUES (?, ?, ?, ?);";
	public static final String DELETE_FROM_PAGOSEMPLEADOS_ByidPagoEmpleado = "DELETE FROM pagosempleados WHERE pagosempleados.idPagoEmpleado = ?";
	public static final String UPDATE_PAGOSEMPLEADOS_ByidPagoEmpleado = "UPDATE pagosempleados SET cantidad = ?, idEmpleado = ?, fecha = ? WHERE pagosempleados.idPagoEmpleado = ?";
	
	public static final String SELECT_FROM_PEDIDOS = "SELECT * FROM pedidos";
	public static final String SELECT_FROM_PEDIDOS_ByidPedido = "SELECT * FROM pedidos WHERE pedidos.idPedido = ?";
	public static final String INSERT_INTO_PEDIDOS = "INSERT INTO pedidos (idProveedor, fecha, estado, precio) VALUES (?, ?, ?, ?);";
	public static final String INSERT_INTO_PEDIDOS_OLD = "INSERT INTO pedidos_old (idPedido, idProveedor, fecha, estado, precio) VALUES (?, ?, ?, ?, ?);";
	public static final String DELETE_FROM_PEDIDOS_ByidPedido = "DELETE FROM pedidos WHERE pedidos.idPedido = ?";
	public static final String UPDATE_PEDIDOS_ByidPedido = "UPDATE pedidos SET idProveedor = ?, fecha = ?, estado = ?, precio = ? WHERE pedidos.idPedido = ?;";
	
	public static final String SELECT_FROM_PLATOS = "SELECT * FROM platos";
	public static final String SELECT_FROM_PLATOS_ByidPlato = "SELECT * FROM platos WHERE platos.idPlato = ?";
	public static final String INSERT_INTO_PLATOS = "INSERT INTO platos (nombreLargo, nombre, descripcion, precioVenta, idCategoriaPlato, receta) VALUES (?, ?, ?, ?, ?, ?);";
	public static final String INSERT_INTO_PLATOS_OLD = "INSERT INTO platos_old (idPlato, nombreLargo, nombre, descripcion, precioVenta, idCategoriaPlato, receta) VALUES (?, ?, ?, ?, ?, ?, ?)";
	public static final String DELETE_FROM_PLATOS_ByidPlato = "DELETE FROM platos WHERE platos.idPlato = ?";
	public static final String UPDATE_PLATOS_ByidPlato = "UPDATE platos SET nombreLargo = ?, nombre = ?, descripcion = ?, precioVenta = ?, idCategoriaPlato = ?, receta = ? WHERE platos.idPlato = ?";
	public static final String SELECT_FROM_PLATOS_OLD = "SELECT * FROM platos_old";
	public static final String DELETE_FROM_PLATOS_OLD_ByidPlato = "DELETE FROM platos_old WHERE platos_old.idPlato = ?;";
	
	public static final String SELECT_FROM_RECLAMACIONES = "SELECT * FROM reclamaciones";
	public static final String SELECT_FROM_RECLAMACIONES_ByidReclamacion = "SELECT * FROM reclamaciones WHERE reclamaciones.idReclamacion = ?";
	public static final String INSERT_INTO_RECLAMACIONES = "INSERT INTO reclamaciones (numReclamacion, dni) VALUES (?, ?);";
	public static final String INSERT_INTO_RECLAMACIONES_OLD = "INSERT INTO reclamaciones_old (idReclamacion, numReclamacion, dni) VALUES (?, ?, ?);";
	public static final String DELETE_FROM_RECLAMACIONES_ByidReclamacion = "DELETE FROM reclamaciones WHERE reclamaciones.idReclamacion = ?";
	public static final String UPDATE_RECLAMACIONES_ByidReclamacion = "UPDATE reclamaciones SET numReclamacion = ?, dni = ? WHERE reclamaciones.idReclamacion = ?";
	
	public static final String INSERT_EXAMPLES_INGREDIENTESPEDIDOS = "INSERT INTO 'ingredientespedidos' ('idIngredientePedido', 'idPedido', 'idIngrediente', 'precio', 'cantidad') VALUES (NULL, '1', '6', '2', '5'), (NULL, '1', '9', '9', '3'), (NULL, '2', '5', '2', '2'), (NULL, '2', '16', '5.8', '2');";
	
	public static final String INSERT_INTO_MESAS = "INSERT INTO mesas (capacidad, coordX, coordY) VALUES (?, ?, ?)";
	public static final String DELETE_FROM_MESAS_ByidMesa = "DELETE FROM mesas WHERE mesas.idMesa = ?";
	public static final String UPDATE_MESAS_ByidMesa = "UPDATE mesas SET capacidad = ?, coordX = ?, coordY = ? WHERE mesas.idMesa = ?";
	public static final String SELECT_FROM_MESAS_ByidMesa = "SELECT * FROM mesas WHERE mesas.idMesa = ?";
	public static final String SELECT_FROM_MESAS = "SELECT * FROM mesas";
}
