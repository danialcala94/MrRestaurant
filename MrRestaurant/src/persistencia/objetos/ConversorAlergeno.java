package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.*;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;

public class ConversorAlergeno extends ConversorAbstracto {
	
	private static ConversorAlergeno instanciaConversorAlergeno;
	
	// Cach�
	private static ArrayList<Alergeno> cache = new ArrayList<Alergeno>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorAlergeno getConversor() {
		if(instanciaConversorAlergeno == null) {
			instanciaConversorAlergeno = new ConversorAlergeno();
		}
		return instanciaConversorAlergeno;
	}
	
	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Alergeno alergeno = (Alergeno) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (alergeno.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				FachadaPersistencia.getConnection().setAutoCommit(false);
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_ALERGENOS);
				ps.setString(1, alergeno.getNombre());
				ps.setString(2, alergeno.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado el al�rgeno " + alergeno.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir el al�rgeno " + alergeno.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: alergeno.getNombre().isEmpty()");
			return null;
		}
		return alergeno;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Alergeno alergeno = (Alergeno) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (alergeno.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			Alergeno alergenoAEliminar = FachadaPersistencia.obtener_Alergeno_ByidAlergeno(alergeno.getIDAlergeno());
			try {
				// Comprobar que est� en la base de datos
				if (alergenoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_ALERGENOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, alergenoAEliminar.getIDAlergeno());
					ps.setString(2, alergenoAEliminar.getNombre());
					ps.setString(3, alergenoAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_ALERGENOS_ByidAlergeno);
					ps.setInt(1, alergenoAEliminar.getIDAlergeno());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado el al�rgeno " + alergenoAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					alergeno = alergenoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar el al�rgeno " + alergenoAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: alergeno.getNombre().isEmpty()");
			return null;
		}
		return alergeno;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Alergeno alergeno = (Alergeno) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (alergeno.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				Alergeno alergenoAModificar = FachadaPersistencia.obtener_Alergeno_ByidAlergeno(alergeno.getIDAlergeno());
				if (alergenoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_ALERGENOS_ByidAlergeno);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, alergeno.getNombre());
					ps.setString(2, alergeno.getDescripcion());
					ps.setInt(3, alergeno.getIDAlergeno());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado el al�rgeno " + alergeno.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar el plato " + alergeno.getNombre());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: alergeno.getNombre().isEmpty()");
			return null;
		}
		return alergeno;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Alergeno ale = buscarEnCache(id);
		
		if (ale == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idAlergeno;
			String db_nombre;
			String db_descripcion;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_ALERGENOS_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idAlergeno = rs.getInt("idAlergeno");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hay que sustituir stockAct y stockMin
					ale = new Alergeno(db_idAlergeno, db_nombre, db_descripcion);
					
					addToCache(ale);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return ale;
	}
	
	@Override
	public synchronized ArrayList<Alergeno> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Alergeno> alergenos = new ArrayList<Alergeno>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idAlergeno;
		String db_nombre;
		String db_descripcion;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_ALERGENOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idAlergeno") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Alergeno>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idAlergeno = rs.getInt("idAlergeno");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					alergenos.add(new Alergeno(db_idAlergeno, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(alergenos.get(alergenos.size() - 1));
					if (alergenos.get(alergenos.size() - 1).getIDAlergeno() > ultimaIdUtilizada)
						ultimaIdUtilizada = alergenos.get(alergenos.size() - 1).getIDAlergeno();
				}
				cacheActualizada = true;
			} else
				alergenos = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		return alergenos;
	}
	
	public synchronized ArrayList<Alergeno> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Alergeno obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDAlergeno() == obj.getIDAlergeno()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Alergeno buscarEnCache(Alergeno obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDAlergeno() == obj.getIDAlergeno()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Alergeno buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDAlergeno() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}