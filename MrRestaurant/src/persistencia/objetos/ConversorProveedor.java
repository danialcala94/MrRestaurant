package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.Proveedor;
import restaurante.empleados.NivelCredencial;

public class ConversorProveedor extends ConversorAbstracto {
	
	private static ConversorProveedor instanciaConversorProveedor;
	
	// Cach�
	private static ArrayList<Proveedor> cache = new ArrayList<Proveedor>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorProveedor getConversor() {
		if(instanciaConversorProveedor == null) {
			instanciaConversorProveedor = new ConversorProveedor();
		}
		return instanciaConversorProveedor;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Proveedor proveedor = (Proveedor) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PROVEEDORES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// nombre, telefonoUno, telefonoDos, correoElectronico, direccion, paginaWeb, nif, ciudad, pais
				ps.setString(1, proveedor.getNombre());
				ps.setInt(2, proveedor.getTelefono());
				// telefonoDos puede ser NULL
				if (proveedor.getTelefono2() == 0)
					ps.setString(3, null);
				else
					ps.setInt(3, proveedor.getTelefono2());
				ps.setString(4, proveedor.getEmail());
				ps.setString(5, proveedor.getDireccion());
				// paginaWeb puede ser NULL
				ps.setString(6, proveedor.getPaginaWeb());
				ps.setString(7, proveedor.getNIF());
				ps.setString(8, proveedor.getCiudad());
				ps.setString(9, proveedor.getPais());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + proveedor.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + proveedor.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||\r\n" + 
					"				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||\r\n" + 
					"				proveedor.getPais().isEmpty()");
			return null;
		}
		return proveedor;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Proveedor proveedor = (Proveedor) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			Proveedor proveedorAEliminar = FachadaPersistencia.obtener_Proveedor_ByidProveedor(proveedor.getIDProveedor());
			try {
				// Comprobar que est� en la base de datos
				if (proveedorAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_PROVEEDORES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// nombre, telefonoUno, telefonoDos, correoElectronico, direccion, paginaWeb, nif, ciudad, pais
					ps.setInt(1, proveedorAEliminar.getIDProveedor());
					ps.setString(2, proveedorAEliminar.getNombre());
					ps.setInt(3, proveedorAEliminar.getTelefono());
					// telefonoDos puede ser NULL
					ps.setInt(4, proveedorAEliminar.getTelefono2());
					ps.setString(5, proveedorAEliminar.getEmail());
					ps.setString(6, proveedorAEliminar.getDireccion());
					// paginaWeb puede ser NULL
					ps.setString(7, proveedorAEliminar.getPaginaWeb());
					ps.setString(8, proveedorAEliminar.getNIF());
					ps.setString(9, proveedorAEliminar.getCiudad());
					ps.setString(10, proveedorAEliminar.getPais());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_PROVEEDORES_ByidProveedor);
					ps.setInt(1, proveedorAEliminar.getIDProveedor());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + proveedorAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					proveedor = proveedorAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + proveedorAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||\r\n" + 
					"				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||\r\n" + 
					"				proveedor.getPais().isEmpty()");
			return null;
		}
		return proveedor;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Proveedor proveedor = (Proveedor) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				Proveedor proveedorAModificar = FachadaPersistencia.obtener_Proveedor_ByidProveedor(proveedor.getIDProveedor());
				if (proveedorAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_PROVEEDORES_ByidProveedor);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// nombre, telefonoUno, telefonoDos, correoElectronico, direccion, paginaWeb, nif, ciudad, pais
					ps.setString(1, proveedor.getNombre());
					ps.setInt(2, proveedor.getTelefono());
					// telefonoDos puede ser NULL
					ps.setInt(3, proveedor.getTelefono2());
					ps.setString(4, proveedor.getEmail());
					ps.setString(5, proveedor.getDireccion());
					// paginaWeb puede ser NULL
					ps.setString(6, proveedor.getPaginaWeb());
					ps.setString(7, proveedor.getNIF());
					ps.setString(8, proveedor.getCiudad());
					ps.setString(9, proveedor.getPais());
					ps.setInt(10, proveedor.getIDProveedor());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + proveedor.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + proveedor.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||\r\n" + 
					"				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||\r\n" + 
					"				proveedor.getPais().isEmpty()");
			return null;
		}
		return proveedor;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Proveedor pro = buscarEnCache(id);
		
		if (pro == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idProveedor;
			String db_nombre;
			int db_telefono;
			int db_telefono2;
			String db_email;
			String db_direccion;
			String db_paginaWeb;
			String db_NIF;
			String db_ciudad;
			String db_pais;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PROVEEDORES_ByID);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idProveedor = rs.getInt("idProveedor");
					db_nombre = rs.getString("nombre");
					db_telefono = rs.getInt("telefonoUno");
					db_telefono2 = rs.getInt("telefonoDos");
					db_email = rs.getString("correoElectronico");
					db_direccion = rs.getString("direccion");
					db_paginaWeb = rs.getString("paginaWeb");
					db_NIF = rs.getString("nif");
					db_ciudad = rs.getString("ciudad");
					db_pais = rs.getString("pais");
					
					// Hay que sustituir stockAct y stockMin
					pro = new Proveedor(db_idProveedor, db_nombre, db_telefono, db_telefono2, db_email, db_direccion, db_paginaWeb, db_NIF, db_ciudad, db_pais);
					
					addToCache(pro);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return pro;
	}
	
	@Override
	public synchronized ArrayList<Proveedor> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idProveedor;
		String db_nombre;
		int db_telefono;
		int db_telefono2;
		String db_email;
		String db_direccion;
		String db_paginaWeb;
		String db_NIF;
		String db_ciudad;
		String db_pais;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_PROVEEDORES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idProveedor") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Proveedor>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idProveedor = rs.getInt("idProveedor");
					db_nombre = rs.getString("nombre");
					db_telefono = rs.getInt("telefonoUno");
					db_telefono2 = rs.getInt("telefonoDos");
					db_email = rs.getString("correoElectronico");
					db_direccion = rs.getString("direccion");
					db_paginaWeb = rs.getString("paginaWeb");
					db_NIF = rs.getString("nif");
					db_ciudad = rs.getString("ciudad");
					db_pais = rs.getString("pais");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					proveedores.add(new Proveedor(db_idProveedor, db_nombre, db_telefono, db_telefono2, db_email, db_direccion, db_paginaWeb, db_NIF, db_ciudad, db_pais));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(proveedores.get(proveedores.size() - 1));
					if (proveedores.get(proveedores.size() - 1).getIDProveedor() > ultimaIdUtilizada)
						ultimaIdUtilizada = proveedores.get(proveedores.size() - 1).getIDProveedor();
				}
				cacheActualizada = true;
			} else
				proveedores = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return proveedores;
	}
	
	public synchronized ArrayList<Proveedor> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Proveedor obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDProveedor() == obj.getIDProveedor()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Proveedor buscarEnCache(Proveedor obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDProveedor() == obj.getIDProveedor()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Proveedor buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDProveedor() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}