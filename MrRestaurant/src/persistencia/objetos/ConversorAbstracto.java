package persistencia.objetos;

import java.util.ArrayList;

public abstract class ConversorAbstracto {
	public abstract Object obtener(int id);
	public abstract Object almacenar(Object objeto);
	public abstract Object modificar(Object objeto);
	public abstract Object eliminar(Object objeto);
	public abstract ArrayList<?> obtenerTodos();
}
