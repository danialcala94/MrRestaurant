package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.empleados.CategoriaEmpleado;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;

public class ConversorEmpleado extends ConversorAbstracto {
	
	private static ConversorEmpleado instanciaConversorEmpleado;
	
	// Cach�
	private static ArrayList<Empleado> cache = new ArrayList<Empleado>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	// Lista de MAGNITUD, CATEGORIAINGREDIENTE, PROVEEDOR y ALERGENO
	private static ArrayList<CategoriaEmpleado> categoriasEmpleados;
	private static ArrayList<Credencial> credenciales;
	
	
	public static synchronized ConversorEmpleado getConversor() {
		if(instanciaConversorEmpleado == null) {
			instanciaConversorEmpleado = new ConversorEmpleado();
		}
		return instanciaConversorEmpleado;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		Empleado empleado = (Empleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo objeto
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_EMPLEADOS);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				// dni, nombreCompleto, telefono, idCategoriaEmpleado, idCredencial, salarioBase
				ps.setString(1, empleado.getDni());
				ps.setString(2, empleado.getNombre());
				ps.setInt(3, empleado.getTelefono());
				ps.setInt(4, empleado.getCategoria().getIDCategoriaEmpleado());
				ps.setInt(5, empleado.getCredenciales().getIDCredencial());
				ps.setFloat(6, empleado.getSalario());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: empleado.getCategoria() == null || empleado.getCredenciales() == null \r\n" + 
					"				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0");
			return null;
		}
		return empleado;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		Empleado empleado = (Empleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (empleado.getIDEmpleado() < 0 || empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			Empleado empleadoAEliminar = FachadaPersistencia.obtener_Empleado_ByidEmpleado(empleado.getIDEmpleado());
			try {
				// Tengo que eliminarlo
				if (empleadoAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_EMPLEADOS_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// idEmpleado, dni, nombreCompleto, telefono, idCategoriaEmpleado, idCredencial, salarioBase
					ps.setInt(1,  empleadoAEliminar.getIDEmpleado());
					ps.setString(2, empleadoAEliminar.getDni());
					ps.setString(3, empleadoAEliminar.getNombre());
					ps.setInt(4, empleadoAEliminar.getTelefono());
					ps.setInt(5, empleadoAEliminar.getCategoria().getIDCategoriaEmpleado());
					ps.setInt(6, empleadoAEliminar.getCredenciales().getIDCredencial());
					ps.setFloat(7, empleadoAEliminar.getSalario());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_EMPLEADOS_ByidEmpleado);
					ps.setInt(1, empleadoAEliminar.getIDEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
					empleado = empleadoAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: empleado.getCategoria() == null || empleado.getCredenciales() == null \r\n" + 
					"				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0");
			return null;
		}
		return empleado;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		Empleado empleado = (Empleado) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (empleado.getIDEmpleado() < 0 || empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0)
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el plato
		if (esCorrecto) {
			Empleado empleadoAModificar = FachadaPersistencia.obtener_Empleado_ByidEmpleado(empleado.getIDEmpleado());
			try {
				// Tengo que modificarlo
				if (empleadoAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_EMPLEADOS_ByidEmpleado);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					// dni = ?, nombreCompleto = ?, telefono = ?, idCategoriaEmpleado = ?, idCredencial = ?, salarioBase = ?
					// WHERE credenciales.idCredencial = ?
					ps.setString(1, empleado.getDni());
					ps.setString(2, empleado.getNombre());
					ps.setInt(3, empleado.getTelefono());
					ps.setInt(4, empleado.getCategoria().getIDCategoriaEmpleado());
					ps.setInt(5, empleado.getCredenciales().getIDCredencial());
					ps.setFloat(6, empleado.getSalario());
					ps.setInt(7, empleado.getIDEmpleado());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ") " + empleado.getNombre() + " - " + empleado.getDni());
				e.printStackTrace();
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: empleado.getCategoria() == null || empleado.getCredenciales() == null \r\n" + 
					"				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0");
			return null;
		}
		return empleado;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		Empleado emp = buscarEnCache(id);
		
		if (emp == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idEmpleado;
			String db_dni;
			String db_nombreCompleto;
			int db_telefono;
			CategoriaEmpleado categoriaEmpleado;int db_idCategoriaEmpleado;
			Credencial credencial;				int db_idCredencial;
			float db_salarioBase;
			
			ResultSet rs = null;
			try {
				
				/* Obtiene los valores de cach� o base de datos del conversor adecuado */
				categoriasEmpleados = FachadaPersistencia.obtenerTodos_CategoriasEmpleados();
				credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_EMPLEADOS_ByidEmpleado);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idEmpleado = rs.getInt("idEmpleado");
					db_dni = rs.getString("dni");
					db_nombreCompleto = rs.getString("nombreCompleto");
					db_telefono = rs.getInt("telefono");
					db_idCategoriaEmpleado = rs.getInt("idCategoriaEmpleado");
					db_idCredencial = rs.getInt("idCredencial");
					db_salarioBase = (float)rs.getDouble("salarioBase");
					
					// Hay que sustituir stockAct y stockMin
					emp = new Empleado(db_idEmpleado, db_dni, db_nombreCompleto, db_telefono,
							buscarEnCacheCategoriasEmpleados(db_idCategoriaEmpleado), db_salarioBase,
							buscarEnCacheCredenciales(db_idCredencial));
					
					addToCache(emp);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return emp;
	}
	
	@Override
	public synchronized ArrayList<Empleado> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idEmpleado;
		String db_dni;
		String db_nombreCompleto;
		int db_telefono;
		CategoriaEmpleado categoriaEmpleado;int db_idCategoriaEmpleado;
		Credencial credencial;				int db_idCredencial;
		float db_salarioBase;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			categoriasEmpleados = FachadaPersistencia.obtenerTodos_CategoriasEmpleados();
			credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_EMPLEADOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idEmpleado") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<Empleado>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idEmpleado = rs.getInt("idEmpleado");
					db_dni = rs.getString("dni");
					db_nombreCompleto = rs.getString("nombreCompleto");
					db_telefono = rs.getInt("telefono");
					db_idCategoriaEmpleado = rs.getInt("idCategoriaEmpleado");
					db_idCredencial = rs.getInt("idCredencial");
					db_salarioBase = (float)rs.getDouble("salarioBase");
					
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					empleados.add(new Empleado(db_idEmpleado, db_dni, db_nombreCompleto, db_telefono,
							buscarEnCacheCategoriasEmpleados(db_idCategoriaEmpleado), db_salarioBase,
							buscarEnCacheCredenciales(db_idCredencial)));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(empleados.get(empleados.size() - 1));
					if (empleados.get(empleados.size() - 1).getIDEmpleado() > ultimaIdUtilizada)
						ultimaIdUtilizada = empleados.get(empleados.size() - 1).getIDEmpleado();
				}
				cacheActualizada = true;
			} else
				empleados = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return empleados;
	}
	
	public synchronized ArrayList<Empleado> obtenerTodos_Empleados_ByidCredencial(int idCredencial) {
		// TODO Auto-generated method stub
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idEmpleado;
		String db_dni;
		String db_nombreCompleto;
		int db_telefono;
		CategoriaEmpleado categoriaEmpleado;int db_idCategoriaEmpleado;
		Credencial credencial;				int db_idCredencial;
		float db_salarioBase;
		
		ResultSet rs = null;
		try {
			
			/* Obtiene los valores de cach� o base de datos del conversor adecuado */
			categoriasEmpleados = FachadaPersistencia.obtenerTodos_CategoriasEmpleados();
			credenciales = FachadaPersistencia.obtenerTodos_Credenciales();
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_EMPLEADOS);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idEmpleado") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				//cache = new ArrayList<Empleado>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idEmpleado = rs.getInt("idEmpleado");
					db_dni = rs.getString("dni");
					db_nombreCompleto = rs.getString("nombreCompleto");
					db_telefono = rs.getInt("telefono");
					db_idCategoriaEmpleado = rs.getInt("idCategoriaEmpleado");
					db_idCredencial = rs.getInt("idCredencial");
					db_salarioBase = (float)rs.getDouble("salarioBase");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					empleados.add(new Empleado(db_idEmpleado, db_dni, db_nombreCompleto, db_telefono,
							buscarEnCacheCategoriasEmpleados(db_idCategoriaEmpleado), db_salarioBase,
							buscarEnCacheCredenciales(db_idCredencial)));
					
					// Actualizamos el �ltimo id de la cach�
					//addToCache(empleados.get(empleados.size() - 1));
					if (empleados.get(empleados.size() - 1).getIDEmpleado() > ultimaIdUtilizada)
						ultimaIdUtilizada = empleados.get(empleados.size() - 1).getIDEmpleado();
				}
			} else
				empleados = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return empleados;
	}
	
	public synchronized ArrayList<Empleado> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(Empleado obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDEmpleado() == obj.getIDEmpleado()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Empleado buscarEnCache(Empleado obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDEmpleado() == obj.getIDEmpleado()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized Empleado buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDEmpleado() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	private synchronized CategoriaEmpleado buscarEnCacheCategoriasEmpleados(int id) {
		for (int i = 0; i < categoriasEmpleados.size(); i++) {
			if (categoriasEmpleados.get(i).getIDCategoriaEmpleado() == id) {
				return categoriasEmpleados.get(i);
			}
		}
		return null;
	}
	
	private synchronized Credencial buscarEnCacheCredenciales(int id) {
		for (int i = 0; i < credenciales.size(); i++) {
			if (credenciales.get(i).getIDCredencial() == id) {
				return credenciales.get(i);
			}
		}
		return null;
	}
}