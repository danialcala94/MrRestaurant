package persistencia.objetos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import persistencia.FachadaPersistencia;
import restaurante.almacen.*;
import restaurante.empleados.NivelCredencial;

public class ConversorNivelCredencial extends ConversorAbstracto {
	
	private static ConversorNivelCredencial instanciaConversorNivelCredencial;
	
	// Cach�
	private static ArrayList<NivelCredencial> cache = new ArrayList<NivelCredencial>();
	private static int ultimaIdUtilizada = -3;
	
	private static boolean cacheActualizada = false;
	
	public static synchronized ConversorNivelCredencial getConversor() {
		if(instanciaConversorNivelCredencial == null) {
			instanciaConversorNivelCredencial = new ConversorNivelCredencial();
		}
		return instanciaConversorNivelCredencial;
	}

	@Override
	public synchronized Object almacenar(Object objeto) {
		// TODO Auto-generated method stub
		NivelCredencial nivelCredencial = (NivelCredencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (nivelCredencial.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_NIVELESCREDENCIALES);
				FachadaPersistencia.getConnection().setAutoCommit(false);
				ps.setString(1, nivelCredencial.getNombre());
				ps.setString(2, nivelCredencial.getDescripcion());
				ps.executeUpdate();
				FachadaPersistencia.getConnection().commit();
				FachadaPersistencia.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado: (" + this.getClass().getName() + ")" + nivelCredencial.getNombre());
				cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido a�adir: (" + this.getClass().getName() + ")" + nivelCredencial.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: nivelCredencial.getNombre().isEmpty()");
			return null;
		}
		return nivelCredencial;
	}
	
	@Override
	public synchronized Object eliminar(Object objeto) {
		// Lo eliminar� de la tabla principal pero lo pasar� a una tabla nombretabla_old
		NivelCredencial nivelCredencial = (NivelCredencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (nivelCredencial.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Eliminar si est� bien
		if (esCorrecto) {
			NivelCredencial nivelCredencialAEliminar = FachadaPersistencia.obtener_NivelCredencial_ByidNivelCredencial(nivelCredencial.getIDNivelCredencial());
			try {
				// Comprobar que est� en la base de datos
				if (nivelCredencialAEliminar != null) {
					// Lo guardo en la tabla_old
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.INSERT_INTO_NIVELESCREDENCIALES_OLD);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setInt(1, nivelCredencialAEliminar.getIDNivelCredencial());
					ps.setString(2, nivelCredencialAEliminar.getNombre());
					ps.setString(3, nivelCredencialAEliminar.getDescripcion());
					ps.executeUpdate();
					// Lo borro de la tabla original
					ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.DELETE_FROM_NIVELESCREDENCIALES_ByidNivelCredencial);
					ps.setInt(1, nivelCredencialAEliminar.getIDNivelCredencial());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha eliminado: (" + this.getClass().getName() + ")" + nivelCredencialAEliminar.getNombre());
					// Para que devuelva el objeto correcto
					nivelCredencial = nivelCredencialAEliminar;
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido eliminar: (" + this.getClass().getName() + ")" + nivelCredencialAEliminar.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: nivelCredencial.getNombre().isEmpty()");
			return null;
		}
		return nivelCredencial;
	}
	
	@Override
	public Object modificar(Object objeto) {
		// TODO Auto-generated method stub
		NivelCredencial nivelCredencial = (NivelCredencial) objeto;
		
		// Comprobaci�n de seguridad
		boolean esCorrecto = true;
		if (nivelCredencial.getNombre().isEmpty())
			esCorrecto = false;
		// �Comprobaci�n de repetidos?
		
		// Modificar el objeto
		if (esCorrecto) {
			try {
				// Comprobar que est� en la base de datos
				
				// Tengo que modificarlo
				NivelCredencial nivelCredencialAModificar = FachadaPersistencia.obtener_NivelCredencial_ByidNivelCredencial(nivelCredencial.getIDNivelCredencial());
				if (nivelCredencialAModificar != null) {
					// Modificamos el plato
					PreparedStatement ps = FachadaPersistencia.getConnection().prepareStatement(SentenciasSQL.UPDATE_NIVELESCREDENCIALES_ByidNivelCredencial);
					FachadaPersistencia.getConnection().setAutoCommit(false);
					ps.setString(1, nivelCredencial.getNombre());
					ps.setString(2, nivelCredencial.getDescripcion());
					ps.setInt(3, nivelCredencial.getIDNivelCredencial());
					ps.executeUpdate();
					FachadaPersistencia.getConnection().commit();
					FachadaPersistencia.getConnection().setAutoCommit(true);
					System.out.println("Se ha modificado: (" + this.getClass().getName() + ")" + nivelCredencial.getNombre());
					cacheActualizada = false;
				} else
					System.err.println("No se ha encontrado el registro correspondiente a ese ID.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					FachadaPersistencia.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido modificar: (" + this.getClass().getName() + ")" + nivelCredencial.getNombre());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: nivelCredencial.getNombre().isEmpty()");
			return null;
		}
		return nivelCredencial;
	}

	@Override
	public synchronized Object obtener(int id) {
		// TODO Auto-generated method stub
		NivelCredencial nivCre = buscarEnCache(id);
		
		if (nivCre == null) {
			/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
			// Objetos							// Identificadores de objetos
			int db_idNivelCredencial;
			String db_nombre;
			String db_descripcion;
			
			ResultSet rs = null;
			try {
				
				PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_NIVELESCREDENCIALES_ByidNivelCredencial);
				// Setea el 'id' en el '?'
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				/* Rellenar campos */
				while(rs.next()) {
					db_idNivelCredencial = rs.getInt("idNivelCredencial");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// �Para qu� es el nivel (0) si ya tiene un id y un nombre propios?
					nivCre = new NivelCredencial(db_idNivelCredencial, 0, db_nombre, db_descripcion);
					
					addToCache(nivCre);
				}
			} catch (SQLException sqlExc) {
				System.err.println("SQLException: " + sqlExc.getMessage());
			}
			FachadaPersistencia.desconectar();
		}
		
		return nivCre;
	}
	
	@Override
	public synchronized ArrayList<NivelCredencial> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<NivelCredencial> nivelesCredenciales = new ArrayList<NivelCredencial>();
		
		/* El de la izquierda es con los objetos ya referenciados, y el de la derecha solo con las ids */
		// Objetos							// Identificadores de objetos
		int db_idNivelCredencial;
		String db_nombre;
		String db_descripcion;
		
		ResultSet rs = null;
		try {
			
			PreparedStatement ps = FachadaPersistencia.getFachadaPersistencia().getConnection().prepareStatement(SentenciasSQL.SELECT_FROM_NIVELESCREDENCIALES);
			rs = ps.executeQuery();
			
			// Contabilizamos los elementos en base de datos y el �ltimo id utilizado y comparamos con cach�
			/*int contadorBD = 0;
			while (rs.next()) {
				contadorBD++;
				if (rs.isLast()) {
					if (rs.getInt("idNivelCredencial") == ultimaIdUtilizada && cache.size() == contadorBD)
						cacheActualizada = true;
					else {
						rs.beforeFirst();
						break;
					}
				}
			}*/
			
			System.out.println(this.getClass().getName() + ": -> Buscando: " + !cacheActualizada);
			
			if (!cacheActualizada) {
				/* Limpiamos la cach� para actualizarla por completo */
				cache = new ArrayList<NivelCredencial>();
				/* Rellenar campos */
				while(rs.next()) {
					db_idNivelCredencial = rs.getInt("idNivelCredencial");
					db_nombre = rs.getString("nombre");
					db_descripcion = rs.getString("descripcion");
					
					// Hacer que los objetos sean referenciados seg�n las ids obtenidas
					nivelesCredenciales.add(new NivelCredencial(db_idNivelCredencial, 0, db_nombre, db_descripcion));
					
					// Actualizamos el �ltimo id de la cach�
					addToCache(nivelesCredenciales.get(nivelesCredenciales.size() - 1));
					if (nivelesCredenciales.get(nivelesCredenciales.size() - 1).getIDNivelCredencial() > ultimaIdUtilizada)
						ultimaIdUtilizada = nivelesCredenciales.get(nivelesCredenciales.size() - 1).getIDNivelCredencial();
				}
				cacheActualizada = true;
			} else
				nivelesCredenciales = cache;
		} catch (SQLException sqlExc) {
			System.err.println("SQLException: " + sqlExc.getMessage());
		}
		FachadaPersistencia.desconectar();
		
		System.out.println(this.getClass().getName() + ": -> Acabando...");
		
		return nivelesCredenciales;
	}
	
	public synchronized ArrayList<NivelCredencial> getCache() {
		return cache;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach� para a�adirlo (o no a�adirlo)
	 * @param ing
	 */
	public synchronized void addToCache(NivelCredencial obj) {
		boolean add = true;
		
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getIDNivelCredencial() == obj.getIDNivelCredencial()) {
				add = false;
				break;
			}
		}
		
		if (add)
			cache.add(obj);
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized NivelCredencial buscarEnCache(NivelCredencial obj) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDNivelCredencial() == obj.getIDNivelCredencial()) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Comprueba si el elemento est� o no en cach�, para devolverlo o devolver null si no estuviera
	 * @param ing
	 * @return
	 */
	private synchronized NivelCredencial buscarEnCache(int id) {
		if (cacheActualizada) {
			for (int i = 0; i < cache.size(); i++) {
				if (cache.get(i).getIDNivelCredencial() == id) {
					return cache.get(i);
				}
			}
		}
		return null;
	}
}