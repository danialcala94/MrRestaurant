package persistencia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.mysql.jdbc.Statement;

import persistencia.objetos.*;
import restaurante.almacen.*;
import restaurante.cocina.CategoriaPlato;
import restaurante.cocina.ComandaPlato;
import restaurante.cocina.IngredientePlato;
import restaurante.cocina.Plato;
import restaurante.empleados.CategoriaEmpleado;
import restaurante.empleados.Credencial;
import restaurante.empleados.Empleado;
import restaurante.empleados.NivelCredencial;
import restaurante.empleados.PagoEmpleado;
import restaurante.sala.Comanda;
import restaurante.sala.Mesa;
import restaurante.Reclamacion;
import restaurante.Restaurante;
/**
 *-- singleton --
 * @author Daniel
 *
 */
public class FachadaPersistencia {
	
	// Enlace base de datos: https://drive.google.com/open?id=1lpn7neesz6gW37LpMB8lZ6rWApOizQLr
	/*final static String DB_CONNECTION_SENTENCE_AZURE = "jdbc:mysql://mrrestaurant.database.windows.net/mrrestaurant";
	final static String DB_USERNAME_AZURE = "masterrestaurant";
	final static String DB_PASSWORD_AZURE = "MR2018Restaurant";
	
	// Base de datos AZURE 
    final static String DBAZURE_HOST = "masterrestaurant.mysql.database.azure.com";
    final static String DBAZURE_DATABASE = "masterrestaurant";
    final static String DBAZURE_USER = "masterrestaurant@masterrestaurant";
    final static String DBAZURE_PASSWORD = "MR2018Restaurant";*/
	
	
	final static String DB_CONNECTION_SENTENCE = "jdbc:mysql://localhost:3306/" + FachadaPersistencia.DB_NAME;
	final static String DB_USERNAME = "root";
	final static String DB_PASSWORD = "";
	final static String DB_NAME = "pruebas_mrrestaurant";
	static String RUTA_MYSQLDUMP = "C:/xampp/mysql/bin/mysqldump";
	static String RUTA_MYSQL = "C:/xampp/mysql/bin/mysql";

	private static FachadaPersistencia instanciaFachadaPersistencia;
	
	private static Connection cn = null;
	
	private static boolean xamppEncontrado = false;
	
	public static synchronized FachadaPersistencia getFachadaPersistencia() {
		if(instanciaFachadaPersistencia == null) {
			instanciaFachadaPersistencia = new FachadaPersistencia();
		}
		return instanciaFachadaPersistencia;
	}
	
	public static synchronized Connection getConnection() {
		if (cn == null) {
			conectar();
		}
		return cn;
	}
	
	public static synchronized void conectar() {
		if (cn == null) {
			try {
				//String url = "jdbc:mysql://" + DBAZURE_HOST + "/" + DBAZURE_DATABASE;
				/*String url ="jdbc:mysql://" + DBAZURE_HOST + ":3306/" + DBAZURE_DATABASE + "?useSSL=true&requireSSL=false";
				System.out.println("Vamos a intentar conectar con: " + url);
	            // Set connection properties.
	            Properties properties = new Properties();
	            properties.setProperty("user", DBAZURE_USER);
	            properties.setProperty("password", DBAZURE_PASSWORD);
	            properties.setProperty("useSSL", "true");
	            properties.setProperty("verifyServerCertificate", "true");
	            properties.setProperty("requireSSL", "false");

	            // get connection
	            cn = DriverManager.getConnection(url, DBAZURE_USER, DBAZURE_PASSWORD);
	            System.out.println("Conectando con: " + url);*/
				System.out.println("Conectando con: " + DB_CONNECTION_SENTENCE);
				cn = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD);
			} catch (SQLException sqlExc) {
				System.err.println("SQLException - Error al conectar: " + sqlExc.getMessage());
			}
		}
	}
	
	public static synchronized void desconectar() {
		if (cn != null) {
			try {
				System.out.println("Desconect�ndose de: " + DB_CONNECTION_SENTENCE);
				cn.close();
			} catch (SQLException sqlExc) {
				System.err.println("SQLException - Error al desconectar: " + sqlExc.getMessage());
			}
		}
		cn = null;
	}

	
	/* CONSULTAS POR TABLAS */
	/* -- alergenos -- */
	public static ArrayList<Alergeno> obtenerTodos_Alergenos() {
		return new ConversorAlergeno().obtenerTodos();
	}
	public static Alergeno obtener_Alergeno_ByidAlergeno(int idAlergeno) {
		return (Alergeno) new ConversorAlergeno().obtener(idAlergeno);
	}
	/**
	 * No hace absolutamente nada si: <strong>alergeno.getNombre().isEmpty()</strong>
	 * @param alergeno
	 * @return
	 */
	public static Alergeno crear_Alergeno(Alergeno alergeno) {
		return (Alergeno) new ConversorAlergeno().almacenar(alergeno);
	}
	/**
	 * Buscar� el <strong>al�rgeno</strong> con la misma ID en base de datos, y lo dejar� tal y como est� el par�metro pasado <u>alergeno</u>.
	 * No hace absolutamente nada si: <strong>alergeno.getNombre().isEmpty()</strong>
	 * @param alergeno
	 * @return
	 */
	public static Alergeno modificar_Alergeno(Alergeno alergeno) {
		return (Alergeno) new ConversorAlergeno().modificar(alergeno);
	}
	/**
	 * No hace absolutamente nada si: <strong>alergeno.getNombre().isEmpty()</strong>
	 * @param alergeno
	 * @return
	 */
	public static Alergeno eliminar_Alergeno(Alergeno alergeno) {
		return (Alergeno) new ConversorAlergeno().eliminar(alergeno);
	}
	
	/* -- categoriasEmpleados -- */
	public static ArrayList<CategoriaEmpleado> obtenerTodos_CategoriasEmpleados() {
		return new ConversorCategoriaEmpleado().obtenerTodos();
	}
	public static CategoriaEmpleado obtener_CategoriaEmpleado_ByidCategoriaEmpleado(int idCategoriaEmpleado) {
		return (CategoriaEmpleado) new ConversorCategoriaEmpleado().obtener(idCategoriaEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaEmpleado.getNombre().isEmpty()</strong>
	 * @param categoriaEmpleado
	 * @return
	 */
	public static CategoriaEmpleado crear_CategoriaEmpleado(CategoriaEmpleado categoriaEmpleado) {
		return (CategoriaEmpleado) new ConversorCategoriaEmpleado().almacenar(categoriaEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaEmpleado.getNombre().isEmpty()</strong>
	 * @param categoriaEmpleado
	 * @return
	 */
	public static CategoriaEmpleado modificar_CategoriaEmpleado(CategoriaEmpleado categoriaEmpleado) {
		return (CategoriaEmpleado) new ConversorCategoriaEmpleado().modificar(categoriaEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaEmpleado.getNombre().isEmpty()</strong>
	 * @param categoriaEmpleado
	 * @return
	 */
	public static CategoriaEmpleado eliminar_CategoriaEmpleado(CategoriaEmpleado categoriaEmpleado) {
		return (CategoriaEmpleado) new ConversorCategoriaEmpleado().eliminar(categoriaEmpleado);
	}
	
	/* -- categoriasIngredientes -- */
	public static ArrayList<CategoriaIngrediente> obtenerTodos_CategoriasIngredientes() {
		return new ConversorCategoriaIngrediente().obtenerTodos();
	}
	public static CategoriaIngrediente obtener_CategoriaIngrediente_ByidCategoriaIngrediente(int idCategoriaIngrediente) {
		return (CategoriaIngrediente) new ConversorCategoriaIngrediente().obtener(idCategoriaIngrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaIngrediente.getNombre().isEmpty()</strong>
	 * @param categoriaIngrediente
	 * @return
	 */
	public static CategoriaIngrediente crear_CategoriaIngrediente(CategoriaIngrediente categoriaIngrediente) {
		return (CategoriaIngrediente) new ConversorCategoriaIngrediente().almacenar(categoriaIngrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaIngrediente.getNombre().isEmpty()</strong>
	 * @param categoriaIngrediente
	 * @return
	 */
	public static CategoriaIngrediente modificar_CategoriaIngrediente(CategoriaIngrediente categoriaIngrediente) {
		return (CategoriaIngrediente) new ConversorCategoriaIngrediente().modificar(categoriaIngrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaIngrediente.getNombre().isEmpty()</strong>
	 * @param categoriaIngrediente
	 * @return
	 */
	public static CategoriaIngrediente eliminar_CategoriaIngrediente(CategoriaIngrediente categoriaIngrediente) {
		return (CategoriaIngrediente) new ConversorCategoriaIngrediente().eliminar(categoriaIngrediente);
	}
	
	/* -- categoriasPlatos -- */
	public static ArrayList<CategoriaPlato> obtenerTodos_CategoriasPlatos() {
		return new ConversorCategoriaPlato().obtenerTodos();
	}
	public static CategoriaPlato obtener_CategoriaPlato_ByidCategoriaPlato(int idCategoriaPlato) {
		return (CategoriaPlato) new ConversorCategoriaPlato().obtener(idCategoriaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaPlato.getNombre().isEmpty()</strong>
	 * @param categoriaPlato
	 * @return
	 */
	public static CategoriaPlato crear_CategoriaPlato(CategoriaPlato categoriaPlato) {
		return (CategoriaPlato) new ConversorCategoriaPlato().almacenar(categoriaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaPlato.getNombre().isEmpty()</strong>
	 * @param categoriaPlato
	 * @return
	 */
	public static CategoriaPlato modificar_CategoriaPlato(CategoriaPlato categoriaPlato) {
		return (CategoriaPlato) new ConversorCategoriaPlato().modificar(categoriaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>categoriaPlato.getNombre().isEmpty()</strong>
	 * @param categoriaPlato
	 * @return
	 */
	public static CategoriaPlato eliminar_CategoriaPlato(CategoriaPlato categoriaPlato) {
		return (CategoriaPlato) new ConversorCategoriaPlato().eliminar(categoriaPlato);
	}
	
	/* -- comandas -- */
	public static ArrayList<Comanda> obtenerTodos_Comandas() {
		return new ConversorComanda().obtenerTodos();
	}
	public static Comanda obtener_Comanda_ByidComanda(int idComanda) {
		return (Comanda) new ConversorComanda().obtener(idComanda);
	}
	/**
	 * No hace absolutamente nada si: <strong>comanda.getFecha() == null || comanda.getMesa() == null</strong>
	 * @param comanda
	 * @return
	 */
	public static Comanda crear_Comanda(Comanda comanda) {
		return (Comanda) new ConversorComanda().almacenar(comanda);
	}
	/**
	 * No hace absolutamente nada si: <strong>comanda.getFecha() == null || comanda.getMesa() == null</strong>
	 * @param comanda
	 * @return
	 */
	public static Comanda modificar_Comanda(Comanda comanda) {
		return (Comanda) new ConversorComanda().modificar(comanda);
	}
	/**
	 * No hace absolutamente nada si: <strong>comanda.getFecha() == null || comanda.getMesa() == null</strong>
	 * @param comanda
	 * @return
	 */
	public static Comanda eliminar_Comanda(Comanda comanda) {
		return (Comanda) new ConversorComanda().eliminar(comanda);
	}
	
	/* -- comandasPlatos -- */
	public static ArrayList<ComandaPlato> obtenerTodos_ComandasPlatos() {
		return new ConversorComandaPlato().obtenerTodos();
	}
	public static ArrayList<ComandaPlato> obtenerTodos_ComandasPlatos_ByidComanda(int idComanda) {
		return new ConversorComandaPlato().obtenerTodos_ByidComanda(idComanda);
	}
	public static ComandaPlato obtener_ComandaPlato_ByidComandaPlato(int idComandaPlato) {
		return (ComandaPlato) new ConversorComandaPlato().obtener(idComandaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0</strong>
	 * @param comandaPlato
	 * @return
	 */
	public static ComandaPlato crear_ComandaPlato(ComandaPlato comandaPlato) {
		return (ComandaPlato) new ConversorComandaPlato().almacenar(comandaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0</strong>
	 * @param comandaPlato
	 * @return
	 */
	public static ComandaPlato modificar_ComandaPlato(ComandaPlato comandaPlato) {
		return (ComandaPlato) new ConversorComandaPlato().modificar(comandaPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>comandaPlato.getCantidad() < 0 || comandaPlato.getPlato() == null || comandaPlato.getPrecio() < 0</strong>
	 * @param comandaPlato
	 * @return
	 */
	public static ComandaPlato eliminar_ComandaPlato(ComandaPlato comandaPlato) {
		return (ComandaPlato) new ConversorComandaPlato().eliminar(comandaPlato);
	}
	
	/* -- credenciales -- */
	public static ArrayList<Credencial> obtenerTodos_Credenciales() {
		return new ConversorCredencial().obtenerTodos();
	}
	public static Credencial obtener_Credencial_ByidCredencial(int idCredencial) {
		return (Credencial) new ConversorCredencial().obtener(idCredencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null</strong>
	 * @param credencial
	 * @return
	 */
	public static Credencial crear_Credencial(Credencial credencial) {
		return (Credencial) new ConversorCredencial().almacenar(credencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null</strong>
	 * @param credencial
	 * @return
	 */
	public static Credencial modificar_Credencial(Credencial credencial) {
		return (Credencial) new ConversorCredencial().modificar(credencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>credencial.getUsuario().isEmpty() || credencial.getContrase�a().isEmpty() || credencial.getNivel() == null</strong>
	 * @param credencial
	 * @return
	 */
	public static Credencial eliminar_Credencial(Credencial credencial) {
		return (Credencial) new ConversorCredencial().eliminar(credencial);
	}
	
	/* -- empleados -- */
	public static ArrayList<Empleado> obtenerTodos_Empleados() {
		return new ConversorEmpleado().obtenerTodos();
	}
	public static ArrayList<Empleado> obtenerTodos_Empleados_ByidCredencial(int idCredencial) {
		return new ConversorEmpleado().obtenerTodos_Empleados_ByidCredencial(idCredencial);
	}
	public static Empleado obtener_Empleado_ByidEmpleado(int idEmpleado) {
		return (Empleado) new ConversorEmpleado().obtener(idEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0</strong>
	 * @param empleado
	 * @return
	 */
	public static Empleado crear_Empleado(Empleado empleado) {
		return (Empleado) new ConversorEmpleado().almacenar(empleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0</strong>
	 * @param empleado
	 * @return
	 */
	public static Empleado modificar_Empleado(Empleado empleado) {
		return (Empleado) new ConversorEmpleado().modificar(empleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>empleado.getCategoria() == null || empleado.getCredenciales() == null 
				|| empleado.getDni().isEmpty() || empleado.getNombre().isEmpty() || empleado.getSalario() <= 0</strong>
	 * @param empleado
	 * @return
	 */
	public static Empleado eliminar_Empleado(Empleado empleado) {
		return (Empleado) new ConversorEmpleado().eliminar(empleado);
	}
	
	/* -- ingredientes -- */
	public static ArrayList<Ingrediente> obtenerTodos_Ingredientes() {
		return new ConversorIngrediente().obtenerTodos();
	}
	public static Ingrediente obtener_Ingrediente_ByidIngrediente(int idIngrediente) {
		return (Ingrediente) new ConversorIngrediente().obtener(idIngrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0)</strong>
	 * @param ingrediente
	 * @return
	 */
	public static Ingrediente crear_Ingrediente(Ingrediente ingrediente) {
		return (Ingrediente) new ConversorIngrediente().almacenar(ingrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0)</strong>
	 * @param ingrediente
	 * @return
	 */
	public static Ingrediente modificar_Ingrediente(Ingrediente ingrediente) {
		return (Ingrediente) new ConversorIngrediente().modificar(ingrediente);
	}
	/**
	 * No hace absolutamente nada si: <strong>ingrediente.getNombreLargo().isEmpty() || ingrediente.getNombreCorto().isEmpty() || ingrediente.getMagnitud().getIDMagnitud() < 0 ||
				ingrediente.getCategoria().getIDCategoria() < 0 || ingrediente.getProveedor().getIDProveedor() < 0 || ingrediente.getPrecioBase() < 0 ||
				ingrediente.getAlergeno().getIDAlergeno() < 0)</strong>
	 * @param ingrediente
	 * @return
	 */
	public static Ingrediente eliminar_Ingrediente(Ingrediente ingrediente) {
		return (Ingrediente) new ConversorIngrediente().eliminar(ingrediente);
	}
	
	/* -- ingredientesPedidos -- */
	public static ArrayList<IngredientePedido> obtenerTodos_IngredientesPedidos() {
		return new ConversorIngredientePedido().obtenerTodos();
	}
	public static ArrayList<IngredientePedido> obtenerTodos_IngredientesPedidos_ByidPedido(int idPedido) {
		return new ConversorIngredientePedido().obtenerTodos_ByidPedido(idPedido);
	}
	// Falta por implementar crear y modificar y eliminar
	
	/* -- ingredientesPlatos -- */
	public static ArrayList<IngredientePlato> obtenerTodos_IngredientesPlatos() {
		return new ConversorIngredientePlato().obtenerTodos();
	}
	public static ArrayList<IngredientePlato> obtenerTodos_IngredientesPlatos_ByidPlato(int idPlato) {
		return new ConversorIngredientePlato().obtenerTodos_ByidPlato(idPlato);
	}
	// Falta por implementar crear y modificar y eliminar
	
	/* -- magnitudes -- */
	public static ArrayList<Magnitud> obtenerTodos_Magnitudes() {
		return new ConversorMagnitud().obtenerTodos();
	}
	public static Magnitud obtener_Magnitud_ByidMagnitud(int idMagnitud) {
		return (Magnitud) new ConversorMagnitud().obtener(idMagnitud);
	}
	/**
	 * No hace absolutamente nada si: <strong>magnitud.getNombre().isEmpty()</strong>
	 * @param magnitud
	 * @return
	 */
	public static Magnitud crear_Magnitud(Magnitud magnitud) {
		return (Magnitud) new ConversorMagnitud().almacenar(magnitud);
	}
	/**
	 * No hace absolutamente nada si: <strong>magnitud.getNombre().isEmpty()</strong>
	 * @param magnitud
	 * @return
	 */
	public static Magnitud modificar_Magnitud(Magnitud magnitud) {
		return (Magnitud) new ConversorMagnitud().modificar(magnitud);
	}
	/**
	 * No hace absolutamente nada si: <strong>magnitud.getNombre().isEmpty()</strong>
	 * @param magnitud
	 * @return
	 */
	public static Magnitud eliminar_Magnitud(Magnitud magnitud) {
		return (Magnitud) new ConversorMagnitud().eliminar(magnitud);
	}
	
	/* -- nivelesCredenciales -- */
	public static ArrayList<NivelCredencial> obtenerTodos_NivelesCredenciales() {
		return new ConversorNivelCredencial().obtenerTodos();
	}
	public static NivelCredencial obtener_NivelCredencial_ByidNivelCredencial(int idNivelCredencial) {
		return (NivelCredencial) new ConversorNivelCredencial().obtener(idNivelCredencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>nivelCredencial.getNombre().isEmpty()</strong>
	 * @param nivelCredencial
	 * @return
	 */
	public static NivelCredencial crear_NivelCredencial(NivelCredencial nivelCredencial) {
		return (NivelCredencial) new ConversorNivelCredencial().almacenar(nivelCredencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>nivelCredencial.getNombre().isEmpty()</strong>
	 * @param nivelCredencial
	 * @return
	 */
	public static NivelCredencial modificar_NivelCredencial(NivelCredencial nivelCredencial) {
		return (NivelCredencial) new ConversorNivelCredencial().modificar(nivelCredencial);
	}
	/**
	 * No hace absolutamente nada si: <strong>nivelCredencial.getNombre().isEmpty()</strong>
	 * @param nivelCredencial
	 * @return
	 */
	public static NivelCredencial eliminar_NivelCredencial(NivelCredencial nivelCredencial) {
		return (NivelCredencial) new ConversorNivelCredencial().eliminar(nivelCredencial);
	}
	
	/* -- pagosEmpleados -- */
	public static ArrayList<PagoEmpleado> obtenerTodos_PagosEmpleados() {
		return new ConversorPagoEmpleado().obtenerTodos();
	}
	public static PagoEmpleado obtener_PagoEmpleado_ByidPagoEmpleado(int idPagoEmpleado) {
		return (PagoEmpleado) new ConversorPagoEmpleado().obtener(idPagoEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() < 0</strong>
	 * @param pagoEmpleado
	 * @return
	 */
	public static PagoEmpleado crear_PagoEmpleado(PagoEmpleado pagoEmpleado) {
		return (PagoEmpleado) new ConversorPagoEmpleado().almacenar(pagoEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() < 0</strong>
	 * @param pagoEmpleado
	 * @return
	 */
	public static PagoEmpleado modificar_PagoEmpleado(PagoEmpleado pagoEmpleado) {
		return (PagoEmpleado) new ConversorPagoEmpleado().modificar(pagoEmpleado);
	}
	/**
	 * No hace absolutamente nada si: <strong>pagoEmpleado.getCantidad() < 0 || pagoEmpleado.getFecha() == null || pagoEmpleado.getEmpleado().getIDEmpleado() < 0</strong>
	 * @param pagoEmpleado
	 * @return
	 */
	public static PagoEmpleado eliminar_PagoEmpleado(PagoEmpleado pagoEmpleado) {
		return (PagoEmpleado) new ConversorPagoEmpleado().eliminar(pagoEmpleado);
	}
	
	/* -- pedidos -- */
	public static ArrayList<Pedido> obtenerTodos_Pedidos() {
		return new ConversorPedido().obtenerTodos();
	}
	public static Pedido obtener_Pedido_ByidPedido(int idPedido) {
		return (Pedido) new ConversorPedido().obtener(idPedido);
	}
	/**
	 * No hace absolutamente nada si: <strong>pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0</strong>
	 * @param pedido
	 * @return
	 */
	public static Pedido crear_Pedido(Pedido pedido) {
		return (Pedido) new ConversorPedido().almacenar(pedido);
	}
	/**
	 * No hace absolutamente nada si: <strong>pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0</strong>
	 * @param pedido
	 * @return
	 */
	public static Pedido modificar_Pedido(Pedido pedido) {
		return (Pedido) new ConversorPedido().modificar(pedido);
	}
	/**
	 * No hace absolutamente nada si: <strong>pedido.getProveedor().getIDProveedor() < 0 || pedido.getFecha() == null || pedido.getEstado().isEmpty() || pedido.getPrecio() < 0</strong>
	 * @param pedido
	 * @return
	 */
	public static Pedido eliminar_Pedido(Pedido pedido) {
		return (Pedido) new ConversorPedido().eliminar(pedido);
	}
	
	/* -- platos -- */
	public static ArrayList<Plato> obtenerTodos_Platos() {
		return ConversorPlato.getConversor().obtenerTodos();
	}
	public static Plato obtener_Plato_ByidPlato(int idPlato) {
		return (Plato) ConversorPlato.getConversor().obtener(idPlato);
	}
	/**
	 * No hace absolutamente nada si: <strong>plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()</strong>
	 * @param plato
	 * @return
	 */
	public static Plato crear_Plato(Plato plato) {
		return (Plato) ConversorPlato.getConversor().almacenar(plato);
	}
	/**
	 * No hace absolutamente nada si: <strong>plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()</strong>
	 * @param plato
	 * @return
	 */
	public static Plato modificar_Plato(Plato plato) {
		return (Plato) ConversorPlato.getConversor().modificar(plato);
	}
	/**
	 * No hace absolutamente nada si: <strong>plato.getNombreLargo().isEmpty() || plato.getNombreCorto().isEmpty() || plato.getPrecio() < 0 || plato.getCategoria() == null || plato.getReceta().isEmpty()</strong>
	 * @param plato
	 * @return
	 */
	public static Plato eliminar_Plato(Plato plato) {
		return (Plato) ConversorPlato.getConversor().eliminar(plato);
	}
	
	
	
	/* -- proveedores -- */
	public static ArrayList<Proveedor> obtenerTodos_Proveedores() {
		return new ConversorProveedor().obtenerTodos();
	}
	public static Proveedor obtener_Proveedor_ByidProveedor(int idProveedor) {
		return (Proveedor) new ConversorProveedor().obtener(idProveedor);
	}
	/**
	 * No hace absolutamente nada si: <strong>proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty()</strong>
	 * @param proveedor
	 * @return
	 */
	public static Proveedor crear_Proveedor(Proveedor proveedor) {
		return (Proveedor) new ConversorProveedor().almacenar(proveedor);
	}
	/**
	 * No hace absolutamente nada si: <strong>proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty()</strong>
	 * @param proveedor
	 * @return
	 */
	public static Proveedor modificar_Proveedor(Proveedor proveedor) {
		return (Proveedor) new ConversorProveedor().modificar(proveedor);
	}
	/**
	 * No hace absolutamente nada si: <strong>proveedor.getNombre().isEmpty() || proveedor.getNIF().isEmpty() || proveedor.getTelefono() < 600000000 ||
				proveedor.getEmail().isEmpty() || proveedor.getDireccion().isEmpty() || proveedor.getCiudad().isEmpty() ||
				proveedor.getPais().isEmpty()</strong>
	 * @param proveedor
	 * @return
	 */
	public static Proveedor eliminar_Proveedor(Proveedor proveedor) {
		return (Proveedor) new ConversorProveedor().eliminar(proveedor);
	}
	
	/* -- reclamaciones -- */
	public static ArrayList<Reclamacion> obtenerTodos_Reclamaciones() {
		return new ConversorReclamacion().obtenerTodos();
	}
	public static Reclamacion obtener_Reclamacion_ByidReclamacion(int idReclamacion) {
		return (Reclamacion) new ConversorReclamacion().obtener(idReclamacion);
	}
	/**
	 * No hace absolutamente nada si: <strong>reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()</strong>
	 * @param idReclamacion
	 * @return
	 */
	public static Reclamacion crear_Reclamacion(Reclamacion reclamacion) {
		return (Reclamacion) new ConversorReclamacion().almacenar(reclamacion);
	}
	/**
	 * No hace absolutamente nada si: <strong>reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()</strong>
	 * @param idReclamacion
	 * @return
	 */
	public static Reclamacion modificar_Reclamacion(Reclamacion reclamacion) {
		return (Reclamacion) new ConversorReclamacion().modificar(reclamacion);
	}
	/**
	 * No hace absolutamente nada si: <strong>reclamacion.getNumReclamacion() < 0 || reclamacion.getDNICliente().isEmpty()</strong>
	 * @param idReclamacion
	 * @return
	 */
	public static Reclamacion eliminar_Reclamacion(Reclamacion reclamacion) {
		return (Reclamacion) new ConversorReclamacion().eliminar(reclamacion);
	}
	
	/* -- mesas -- */
	public static ArrayList<Mesa> obtenerTodos_Mesas() {
		return new ConversorMesa().obtenerTodos();
	}
	public static Mesa obtener_Mesa_ByidMesa(int idMesa) {
		return (Mesa) new ConversorMesa().obtener(idMesa);
	}
	public static Mesa crear_Mesa(Mesa mesa) {
		return (Mesa) new ConversorMesa().almacenar(mesa);
	}
	public static Mesa modificar_Mesa(Mesa mesa) {
		return (Mesa) new ConversorMesa().modificar(mesa);
	}
	public static Mesa eliminar_Mesa(Mesa mesa) {
		return (Mesa) new ConversorMesa().eliminar(mesa);
	}
	
	/* GENERALES */
	public static synchronized boolean crearCopiaSeguridad(String ruta) {
		// Buscar la ruta de XAMPP
		if (!xamppEncontrado) {
			String unidades[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
			                   "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "Y", "Z"};
	        for (int j = 0; j < unidades.length; j++) {
	        	String path = unidades[j] + ":/";
				String files;
		        File folder = new File(path);
		        File[] listOfFiles = folder.listFiles(); 
	        	if (folder.exists() && !xamppEncontrado) {
		        	if (!xamppEncontrado) {
			        	for (int i = 0; i < listOfFiles.length; i++) {
			                files = listOfFiles[i].getName();
			                if (files.equals("xampp")) {
			                	File[] inXampp = listOfFiles[i].listFiles();
			                	boolean isXampp = false;
			                	if (listOfFiles[i].isDirectory()) {
			                		for (int x = 0; x < inXampp.length; x++) {
			                			if (inXampp[x].getName().equals("mysql")) {
			                				isXampp = true;
			                				break;
			                			}
			                		}
			                		if (isXampp) {
				                		xamppEncontrado = true;
				                		System.out.println("Seteando ruta MYSQLDUMP a: " + listOfFiles[i].getAbsolutePath().replace('\\', '/') + "/mysql/bin/mysqldump");
				                		RUTA_MYSQLDUMP = listOfFiles[i].getAbsolutePath().replace('\\', '/') + "/mysql/bin/mysqldump";
				                		break;
			                		}
			                	}
			                }
				        }
		        	}
	        	}
	        }
		}
		
        //Calendar c = Calendar.getInstance();
        //String fecha = String.valueOf(c.getTimeInMillis());
        try {
            Runtime runtime = Runtime.getRuntime();
            File backupFile;
            if (ruta.contains(".sql"))
            	backupFile = new File(ruta);
            else
            	backupFile = new File(ruta + ".sql");
            FileWriter fw = new FileWriter(backupFile);
            Process child = runtime.exec(FachadaPersistencia.RUTA_MYSQLDUMP + " --opt --password=  --user=root "
                    + "--databases " + FachadaPersistencia.DB_NAME + " -R"); 
            InputStreamReader irs = new InputStreamReader(child.getInputStream());
            BufferedReader br = new BufferedReader(irs);
            System.out.println("Creando copia de seguridad de: " + FachadaPersistencia.DB_NAME);
            String line;
            while ((line = br.readLine()) != null ) {
                fw.write(line + "\n");
            }
            
            fw.close();
            irs.close();
            br.close();
        } catch(Exception e) {
        	System.err.println("No se ha podido realizar la copia de seguridad: " + e.getMessage());
        }
        return true;
    }
	
	/*public static boolean restaurarCopiaSeguridad3() {
		boolean ok = false;
		FileWriter arch = null;
	    try{       
	        //sentencia para crear el BackUp
	         Process run = Runtime.getRuntime().exec("cmd /c mysqldump --host=127.0.0.1 --port=3306" +
	        " --user=root --password=" +
	        " --complete-insert --extended-insert " + FachadaPersistencia.DB_NAME);
	        //se guarda en memoria el backup
	        InputStream in = run.getInputStream();
	        //inicializamos para poder las lineas del backup
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        //para guardar los datos como string
	        StringBuffer temp = new StringBuffer();
	        int count;
	        //hacemos una arreglo con una longitud apoximada de caracteres
	        char[] cbuf = new char[10485760];
	        //Empezamos a leer las l�neas obtenidas
	        while ((count = br.read(cbuf, 0, 3000)) != -1){
	            //anexamos a nuestro buffer de string la l�nea leida
	            //se agrega el arreglo obtenido y la longitud de este
	            temp.append(cbuf, 0, count);
	        }
	        //cerramos los buffers
	        br.close();
	        in.close();        
	        // se crea y escribe el archivo SQL anexandole la ruta espec�fica
	        arch = new FileWriter(new File(FachadaPersistencia.DB_NAME).getAbsolutePath() + ".sql");
	        //para poder escribir sobre un fichero usaremos las clase PrintWriter
	        PrintWriter pw = new PrintWriter(arch);
	        //escribimos el fichero a�adiendo salto de l�nea                                                    
	        pw.println(temp.toString());  
	        ok=true;
	   }
	    catch (Exception ex){
	            ex.printStackTrace();
	    } finally {
	       try {           
	         if (null != arch)
	             arch.close();
	       } catch (Exception e2) {
	           e2.printStackTrace();
	       }
	    }   
	    return ok; 
	}
	
	public static boolean restaurarCopiaSeguridad2() {
		try {
            String path = new File(FachadaPersistencia.DB_NAME).getAbsolutePath();
            try {
                //Statement stmt = (Statement) FachadaPersistencia.getConnection().createStatement();
            	Statement stmt = (Statement) DriverManager.getConnection("jdbc:mysql://" + "" + "localhost" + "/" + "mysql", "root", "").createStatement();
                stmt.execute("drop database if exists " + FachadaPersistencia.DB_NAME + ";");
                stmt.execute("create database " + FachadaPersistencia.DB_NAME + ";");
                stmt.close();
                System.out.println("cmd /c mysql --user=root --password= " + FachadaPersistencia.DB_NAME + " < " + path);
                //Runtime.getRuntime().exec("cmd /c mysql --user=root --password=     " + FachadaPersistencia.DB_NAME + " < " + path);
                Runtime.getRuntime().exec(FachadaPersistencia.RUTA_MYSQL + " --opt --password=  --user=root "
                    + "--databases " + FachadaPersistencia.DB_NAME);
                //FachadaPersistencia.desconectar();
                System.out.println("Base de datos " + FachadaPersistencia.DB_NAME + " restaurada con �xito.");
            } catch (SQLException ex) {
                System.err.println("No se pudo restaurar la copia de seguridad: " + ex.getMessage());
            }
        } catch (IOException ex) {
            System.err.println("Error con el fichero: " + ex.getMessage());
        }
		return true;
	}*/
}


